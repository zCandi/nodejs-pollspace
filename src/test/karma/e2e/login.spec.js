'use strict';

describe('E2E:', function () {

	beforeEach(function() {
	    
	});

	it('should expose login facebook button', function() {     
        browser().navigateTo('/#!/login');

        element('.admin-padding > a').query(function($el, done) {
            $el.click();
            done();
        });

    //console.log(browser().location().url());

        expect(element('.admin-padding > a').html()).toBe('log in here.');
    });
})