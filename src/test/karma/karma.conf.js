'use strict';

// Karma configuration
// Generated on Sat Oct 05 2013 22:00:14 GMT+0700 (ICT)

module.exports = function(config) {
    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: '../../',


        // frameworks to use
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            "public/extra_js/jquery.2.0.3.min.js",
            "public/lib/jquery-ui/jquery-ui.js",
            "public/lib/angular/angular.js",
            "public/lib/angular-animate/angular-animate.min.js",
            "public/lib/angular-cookies/angular-cookies.js",
            "public/lib/angular-resource/angular-resource.js",
            "public/lib/angular-route/angular-route.js",
            "public/lib/angular-mocks/angular-mocks.js",
            "public/lib/angular-ui-router/release/angular-ui-router.js",
            "public/lib/angular-sanitize/angular-sanitize.js",
            "public/lib/angular-strap/dist/angular-strap.js",
            "public/lib/angular-strap/dist/angular-strap.tpl.js",
            "public/lib/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js",
            "public/lib/angular-filesystem/src/filesystem.js",
            "public/lib/angular-spinner/angular-spinner.min.js",
            "public/lib/jquery-touchswipe/jquery.touchSwipe.min.js",
            "public/lib/ngstorage/ngStorage.min.js",
            "public/lib/highcharts/highcharts-all.js",
            "public/lib/highcharts/modules/no-data-to-display.js",
            "public/lib/highcharts-ng/src/highcharts-ng.js",
            "public/lib/iCheck/icheck.min.js",
            "public/extra_js/jquery.mmenu.min.all.js",
            "public/extra_js/jquery-picture-min.js",
            "public/extra_js/sortable.js",
            "public/extra_js/randexp.js",
            "public/extra_js/jquery.nanoscroller.min.js",
            "public/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.js",
            "public/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput-angular.js",
            "public/lib/bootstrap-validator/dist/validator.min.js",
            "public/lib/moment/min/moment.min.js",
            "public/lib/underscore/underscore.js",
            "public/lib/cryptojs/lib/Crypto.js",
            "public/lib/cryptojs/lib/SHA1.js",
            "public/lib/async/lib/async.js",
            "public/lib/phpjs/functions/array/array_keys.js",
            "public/lib/phpjs/functions/array/array_values.js",
            "public/lib/phpjs/functions/url/urlencode.js",
            "public/lib/phpjs/functions/url/in_array.js",
            "public/lib/angular-wysiwyg/src/angular-wysiwyg.js",
            'public/js/app.js',
            'public/js/config.js',
            'public/js/directives.js',
            'public/js/filters.js',
            'public/js/**/*.js',
            'public/js/init.js',           
            'test/karma/unit/**/*.js'
        ],


        // list of files to exclude
        exclude: [

        ],


        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        //reporters: ['progress'],
        reporters: ['progress', 'coverage'],

        // coverage
        preprocessors: {
            // source files, that you wanna generate coverage for
            // do not include tests or libraries
            // (these files will be instrumented by Istanbul)
            'public/js/**/*.js': ['coverage'],
            'public/views/*.html': ['coverage'],
            'public/views/**/*.html': ['coverage'],
            'public/views/**/**/*.html': ['coverage'],
            'public/views/**/**/**/*.html': ['coverage']
        },

        coverageReporter: {
            type: 'html',
            dir: 'test/coverage/'
        },

        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['Chrome'],


        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,


        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: true
    });
};
