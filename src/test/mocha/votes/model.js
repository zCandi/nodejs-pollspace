'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    User = mongoose.model('User'),
    Prop = mongoose.model('Prop'),
    Vote = mongoose.model('Votes');

//Globals
var vote, prop, user;

//The tests
describe('<Unit Test>', function() {
    describe('Model Vote:', function() {
        this.timeout(10000);

        before(function(done) {
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestvote@test.com',
                username: 'zCandi',
                password: 'password',// new RandExp(/([A-Za-z0-9]){6}/i).gen(),
                provider: 'local'
            });

            prop = new Prop({
                guid: 'uttestprop',
                name: 'unit test prop',
                desc: 'unit test prop description',
                details: 'unit test prop details',
                type: 'image'
            });

            vote = new Vote();

            done();
        });

        after(function(done) {
            vote.remove();
            user.remove();
            prop.remove();            
            done();
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                user.save(function(err, usr) {
                    should.not.exist(err);
                    vote.user = usr;

                    prop.save(function(err, pro){
                        should.not.exist(err);
                        vote.prop = pro;

                        vote.save(done);
                    });
                });
            });            
        });      
    });
});
