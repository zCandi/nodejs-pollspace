'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Channel = mongoose.model('Channel'),
    User = mongoose.model('User'),
    Prop = mongoose.model('Prop');


//Globals
var prop, prop2, channel, user;

//The tests
describe('<Unit Test>', function() {
    describe('Model Prop:', function() {
        this.timeout(10000);

        before(function(done) {
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestprop1@test.com',
                username: 'zCandi',
                password: 'password',
                provider: 'local'
            });

            channel = new Channel();
            channel.guid = 'utchannelprop1';
            channel.type = 'single';
            channel.mode = 'image';
            channel.name = 'Unit test channel';

            prop = new Prop({
                name: 'unit test prop',
                image: 'a.png',
                desc: 'unit test prop description',
                details: 'unit test prop details',
                type: 'image'
            });

            prop2 = new Prop(prop);
            done();
        });

        after(function(done) {
            prop.remove();
            channel.remove();
            user.remove();
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test prop', function(done) {
                Prop.find({ guid: 'usertestprop' }, function(err, props) {
                    props.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                user.save(function(err, usr) {
                    should.not.exist(err);
                    prop.suggested_by = usr;

                    channel.save(function(err, ch){
                        should.not.exist(err);
                        prop.channel = ch;

                        prop.save(done);
                    });
                });
            });

            it('should fail to save an existing prop again', function(done) {
                // prop.save();
                return prop2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });
        
        describe('Method Modify', function() {
            it('should modify a prop without problems', function(done) {
                Prop.findOne({ guid: prop.guid }, function(err, pro) {
                    should.not.exist(err);
                    pro.name.should.equal('unit test prop');
                    prop = pro;
                    prop.name = 'unit test prop modify';
                    return prop.save(function(err, pro){
                        should.not.exist(err);
                        pro.name.should.equal('unit test prop modify');
                        done();
                    });
                });
            });
        });        
    });
});
