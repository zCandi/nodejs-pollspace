'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    User = mongoose.model('User');

//Globals
var user, user2, user_facebook, user_twitter, user_google;

//The tests
describe('<Unit Test>', function() {
    describe('Model User:', function() {
        this.timeout(10000);

        before(function(done) {
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertest1@test.com',
                username: 'zCandi',
                password: 'password',// new RandExp(/([A-Za-z0-9]){6}/i).gen(),
                provider: 'local'
            });
            user2 = new User(user);

            done();
        });

        after(function(done) {
            user.remove();
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test user', function(done) {
                User.find({ email: 'usertest1@test.com' }, function(err, users) {
                    users.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                user.save(done);
            });

            it('should fail to save an existing user again', function(done) {
                user.save();
                return user2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should fail to save without firstname', function(done) {
                user.firstname = '';
                return user.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should fail to save without lastname', function(done) {
                user.lastname = '';
                return user.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            describe ('Save facebook user', function(){
                before(function(done){
                    user_facebook = new User();
                    
                    var facebook_data = {};
                    facebook_data.email = 'fbuser@gmail.com';
                    facebook_data.id = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                    facebook_data.first_name = 'Tony';
                    facebook_data.last_name = 'Ho';
                    facebook_data.age = 33;
                    facebook_data.gender = 'Male';

                    user_facebook.facebook_data = facebook_data;

                    done();
                });

                after(function(done){
                    user_facebook.remove();
                    done();
                });

                it ('should fail to save without local info', function(done){
                    return user_facebook.save(function(err) {
                        should.exist(err);
                        done();
                    });
                });

                it('should be able to save without problems', function(done) {
                    user_facebook.username = 'zCandi';
                    user_facebook.firstname = 'Candi';
                    user_facebook.lastname = 'Ho';
                    user_facebook.email = 'fbuser@gmail.com';
                    user_facebook.password = 'password';
                    user_facebook.provider = 'facebook';

                    user_facebook.save(done);
                });
            });

            describe ('Save twitter user', function(){
                before(function(done){
                    user_twitter = new User();
                    
                    var twitter_data = {};
                    twitter_data.email = 'twuser@gmail.com';
                    twitter_data.id = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                    twitter_data.name = 'Tony Ho';
                    twitter_data.screen_name = 'zCandi';

                    user_twitter.twitter_data = twitter_data;

                    done();
                });

                after(function(done){
                    user_twitter.remove();
                    done();
                });

                it ('should fail to save without local info', function(done){
                    return user_twitter.save(function(err) {
                        should.exist(err);
                        done();
                    });
                });

                it('should be able to save without problems', function(done) {
                    user_twitter.username = 'zCandi';
                    user_twitter.firstname = 'Candi';
                    user_twitter.lastname = 'Ho';
                    user_twitter.email = 'twuser@gmail.com';
                    user_twitter.password = 'password';
                    user_twitter.provider = 'twitter';
                    
                    user_twitter.save(done);
                });
            });   

            describe ('Save google user', function(){
                before(function(done){
                    user_google = new User();
                    
                    var google_data = {};
                    google_data.email = 'gguser@gmail.com';
                    google_data.id = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                    google_data.first_name = 'Tony';
                    google_data.last_name = 'Ho';
                    google_data.name = 'Tony Ho';
                    google_data.gender = 'Male';

                    user_google.google_data = google_data;

                    done();
                });

                after(function(done){
                    user_google.remove();
                    done();
                });

                it ('should fail to save without local info', function(done){
                    return user_google.save(function(err) {
                        should.exist(err);
                        done();
                    });
                });

                it('should be able to save without problems', function(done) {
                    user_google.username = 'zCandi';
                    user_google.firstname = 'Candi';
                    user_google.lastname = 'Ho';
                    user_google.email = 'gguser@gmail.com';
                    user_google.password = 'password';
                    user_google.provider = 'twitter';
                    
                    user_google.save(done);
                });
            });             
        });
        
        describe('Method Modify', function() {
            it('should modify an user without problems', function(done) {
                User.findOne({ email: 'usertest1@test.com' }, function(err, usr) {
                    usr.username.should.equal('zCandi');
                    user = usr;
                    user.username = 'Nicky';
                    return user.save(function(err, usr){
                        should.not.exist(err);
                        usr.username.should.equal('Nicky');
                        done();
                    });
                });
            });
        });        
    });
});
