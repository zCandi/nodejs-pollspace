'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Img = mongoose.model('Image');

//Globals
var img;

//The tests
describe('<Unit Test>', function() {
    describe('Model Image:', function() {
        this.timeout(10000);

        before(function(done) {
            img = new Img({
                X: 0,
                Y: 0,
                W: 100,
                H: 100
            });

            done();
        });

        after(function(done) {
            img.remove();            
            done();
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                img.save(done);
            });            
        });      
    });
});
