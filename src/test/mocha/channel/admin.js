'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    ChannelAdmin = mongoose.model('ChannelAdmin');

//Globals
var chAdmin, chAdmin2;

//The tests
describe('<Unit Test>', function() {
    describe('Model ChannelAdmin:', function() {
        this.timeout(10000);

        before(function(done) {
            chAdmin = new ChannelAdmin();
            chAdmin.email = 'chadmin@gmail.com';
            chAdmin.firstname = 'Tony';
            chAdmin.lastname = 'Ho';
            chAdmin.phone = '01698332191';
            chAdmin.company = 'FRT';

            chAdmin2 = new ChannelAdmin(chAdmin);

            done();
        });

        after(function(done) {
            chAdmin.remove();
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test channel', function(done) {
                ChannelAdmin.find({ email: 'chadmin@gmail.com' }, function(err, chAdmins) {
                    chAdmins.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                chAdmin.save(done);
            });
        });

        describe('Method Modify', function() {
            it('should modify a channel without problems', function(done) {
                ChannelAdmin.findOne({ email: 'chadmin@gmail.com' }, function(err, ch) {
                    ch.firstname.should.equal('Tony');
                    chAdmin = ch;
                    chAdmin.email = 'chadmin1@gmail.com';
                    return chAdmin.save(function(err, chn){
                        should.not.exist(err);
                        chn.email.should.equal('chadmin1@gmail.com');
                        done();
                    });
                });
            });
        });
        
    });
});
