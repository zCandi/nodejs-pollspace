'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Channel = mongoose.model('Channel'),
    User = mongoose.model('User'),
    ChannelLeaderboard = mongoose.model('ChannelLeaderboard');

//Globals
var chLeaderboard, channel, user;

//The tests
describe('<Unit Test>', function() {
    describe('Model ChannelLeaderboard:', function() {
        this.timeout(10000);

        before(function(done) {
            //set channelLeaderboar
            chLeaderboard = new ChannelLeaderboard();

            //set channel
            channel = new Channel();
            channel.guid = 'utchannelleaderboar';
            channel.type = 'single';
            channel.mode = 'image';
            channel.name = 'Unit test channel';

            //set user
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestchleaderboar@test.com',
                username: 'zCandi',
                password: 'password',
                provider: 'local'
            });

            done();
        });

        after(function(done) {
            chLeaderboard.remove();
            channel.remove();
            user.remove();            
            done();
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                user.save(function(err, usr) {
                    should.not.exist(err);
                    chLeaderboard.user = usr;

                    channel.save(function(err, ch){
                        should.not.exist(err);
                        chLeaderboard.channel = ch;

                        chLeaderboard.save(done);
                    });
                });
            });
        });        
    });
});
