'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Channel = mongoose.model('Channel');

//Globals
var channel, channel2;

//The tests
describe('<Unit Test>', function() {
    describe('Model Channel:', function() {
        this.timeout(10000);

        before(function(done) {
            channel = new Channel();
            channel.guid = 'utchannel';
            channel.type = 'single';
            channel.mode = 'image';
            channel.name = 'Unit test channel';

            channel2 = new Channel(channel);

            done();
        });

        after(function(done) {
            channel.remove();
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test channel', function(done) {
                Channel.find({ guid: 'utchannel' }, function(err, channels) {
                    channels.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                channel.save(done);
            });

            it('should fail to save an existing channel again', function(done) {
                channel.save();
                return channel2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });

        describe('Method Modify', function() {
            it('should modify a channel without problems', function(done) {
                Channel.findOne({ guid: 'utchannel' }, function(err, ch) {
                    ch.guid.should.equal('utchannel');
                    channel = ch;
                    channel.mode = 'standard';
                    return channel.save(function(err, chn){
                        should.not.exist(err);
                        chn.guid.should.equal('utchannel');
                        done();
                    });
                });
            });
        });
        
    });
});
