'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    User = mongoose.model('User'),
    Prop = mongoose.model('Prop'),
    Share = mongoose.model('Share');

//Globals
var share, share2, user, prop;

//The tests
describe('<Unit Test>', function() {
    describe('Model Share:', function() {
        this.timeout(10000);

        before(function(done) {
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestshare@test.com',
                username: 'zCandi',
                password: 'password',// new RandExp(/([A-Za-z0-9]){6}/i).gen(),
                provider: 'local'
            });

            prop = new Prop({
                guid: 'uttestprop',
                name: 'unit test prop',
                desc: 'unit test prop description',
                details: 'unit test prop details',
                type: 'image'
            });


            share = new Share({guid: 'uttestshare'});
            share2 = new Share(share);

            done();
        });

        after(function(done) {            
            share.remove();
            user.remove();
            prop.remove();
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test share', function(done) {
                Share.find({ guid: 'uttestshare' }, function(err, shares) {
                    shares.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                user.save(function(err, usr) {
                    should.not.exist(err);
                    share.user = usr;

                    prop.save(function(err, pro){
                        should.not.exist(err);
                        share.prop = pro;

                        share.save(done);
                    });
                });
            });

            it('should fail to save an existing user again', function(done) {
                share.save();
                return share2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
                        
        });    
    });
});
