'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    User = mongoose.model('User'),
    Channel = mongoose.model('Channel'),
    Photo = mongoose.model('Photo');

//Globals
var photo, photo2, user, channel;

//The tests
describe('<Unit Test>', function() {
    describe('Model Photo:', function() {
        this.timeout(10000);

        before(function(done) {
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestphoto@test.com',
                username: 'zCandi',
                password: 'password',// new RandExp(/([A-Za-z0-9]){6}/i).gen(),
                provider: 'local'
            });

            //set channel
            channel = new Channel();
            channel.guid = 'utchannelphoto1';
            channel.type = 'single';
            channel.mode = 'image';
            channel.name = 'Unit test channel';

            photo = new Photo({
                image: 'a.png'
            });
            photo2 = new Photo(photo);

            done();
        });

        after(function(done) {
            photo.remove();
            user.remove();
            channel.remove();
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test photo', function(done) {
                Photo.find({ guid: 'usertestphoto' }, function(err, photos) {
                    photos.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                user.save(function(err, usr) {
                    should.not.exist(err);
                    photo.user = usr;

                    channel.save(function(err, ch){
                        should.not.exist(err);
                        photo.channel = ch;

                        photo.save(done);
                    });
                });
            });

            it('should fail to save an existing photo again', function(done) {
                photo.save();
                return photo2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });           
        });
        
        describe('Method Modify', function() {
            it('should modify a photo without problems', function(done) {
                Photo.findOne({ guid: photo.guid }, function(err, p) {
                    should.not.exist(err);
                    p.image.should.equal('a.png');
                    photo = p;
                    photo.image = 'b.png';
                    return photo.save(function(err, pho){
                        should.not.exist(err);
                        pho.image.should.equal('b.png');
                        done();
                    });
                });
            });
        });        
    });
});
