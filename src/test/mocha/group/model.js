'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    User = mongoose.model('User'),
    Group = mongoose.model('Group');

//Globals
var group, user, admin;

//The tests
describe('<Unit Test>', function() {
    describe('Model Group:', function() {
        this.timeout(10000);

        before(function(done) {
            user = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestgroup@test.com',
                username: 'zCandi',
                password: 'password',// new RandExp(/([A-Za-z0-9]){6}/i).gen(),
                provider: 'local',
                is_admin: false
            });

            admin = new User({
                firstname: 'Tony',
                lastname: 'Ho',
                email: 'usertestgroupadmin@test.com',
                username: 'zCandi',
                password: 'password',// new RandExp(/([A-Za-z0-9]){6}/i).gen(),
                provider: 'local',
                is_admin: true
            });

            group = new Group({
                name: 'utgroup1'
            });

            done();
        });

        after(function(done) {
            group.remove();
            user.remove();
            admin.remove();            
            done();
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                user.save(function(err, usr) {
                    should.not.exist(err);
                    var users = [];
                    users.push(usr);

                    group.users = users;

                    admin.save(function(err, ad){
                        should.not.exist(err);
                        group.admin = ad;

                        group.save(done);
                    });
                });
            });            
        });      
    });
});
