'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Channel = mongoose.model('Channel'),
    Prop = mongoose.model('Prop'),
    PropSet = mongoose.model('PropSet');

//Globals
var proset, proset2, prop, channel;

//The tests
describe('<Unit Test>', function() {
    describe('Model PropSet:', function() {
        this.timeout(10000);

        before(function(done) {
            channel = new Channel();
            channel.guid = 'utchannel';
            channel.type = 'single';
            channel.mode = 'image';
            channel.name = 'Unit test channel';

            prop = new Prop({
                guid: 'uttestprop',
                name: 'unit test prop',
                desc: 'unit test prop description',
                details: 'unit test prop details',
                type: 'image'
            });

            proset = new PropSet({
                title: 'ut prop set',
                image: 'a.png'
            });
            proset2 = new PropSet(proset);

            done();
        });

        after(function(done) {
            proset.remove();
            channel.remove();
            prop.remove();            
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test propset', function(done) {
                PropSet.find({ guid: 'uttestpropset' }, function(err, prosets) {
                    prosets.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                prop.save(function(err, pro) {
                    should.not.exist(err);
                    var props = [];
                    props.push(pro);                    
                    proset.props = props;

                    channel.save(function(err, ch){
                        should.not.exist(err);
                        proset.channel = ch;

                        proset.save(done);
                    });
                });
            });

            it('should fail to save an existing propset again', function(done) {
                proset.save();
                return proset2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });
        
        describe('Method Modify', function() {
            it('should modify a propset without problems', function(done) {
                PropSet.findOne({ guid: proset.guid }, function(err, ps) {
                    ps.title.should.equal('ut prop set');
                    ps.image.should.equal('a.png');
                    proset = ps;
                    proset.title = 'ut prop set modify';
                    proset.image = 'b.png';
                    return proset.save(function(err, ps){
                        should.not.exist(err);
                        ps.title.should.equal('ut prop set modify');
                        ps.image.should.equal('b.png');
                        done();
                    });
                });
            });
        });     
    });
});
