'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Signup = mongoose.model('Signup');

//Globals
var signup, signup2;

//The tests
describe('<Unit Test>', function() {
    describe('Model Vote:', function() {
        this.timeout(10000);

        before(function(done) {
            signup = new Signup({
                firstname: 'Tony',
                lastname: 'Ho',
                company: 'FRT',
                email: 'uttestsignup@test.com'
            });
            signup2 = new Signup(signup);

            done();
        });

        after(function(done) {
            signup.remove();            
            done();
        });

        describe('Method Save', function() {
            it('should begin without the test propset', function(done) {
                Signup.find({ email: 'uttestsignup@test.com' }, function(err, signups) {
                    signups.should.have.length(0);
                    done();
                });
            });

            it('should be able to save without problems', function(done) {
                signup.save(done);
            });

            it('should fail to save an existing propset again', function(done) {
                signup.save();
                return signup2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });
        
        describe('Method Modify', function() {
            it('should modify a signup without problems', function(done) {
                Signup.findOne({ email: 'uttestsignup@test.com' }, function(err, s) {
                    s.firstname.should.equal('Tony');
                    s.lastname.should.equal('Ho');
                    signup = s;
                    signup.firstname = 'Nicky';

                    return signup.save(function(err){
                        should.not.exist(err);
                        s.firstname.should.equal('Nicky');
                        done();
                    });
                });
            });
        });       
    });
});
