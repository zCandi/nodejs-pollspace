module.exports = {
	protocol: 'http',
	domain: 'pollspacedev.com',
	channel: 'bahdtest',
	//admin user
	admin: {
		email: 'marketing@vogueintl.com',
		pw: 'vogueintl'
	},
	//super admin user
	super_admin: {
		email: 'admin@flashprops.com',
		pw: 'flashprops2014'
	},
	//local user
	local: {
		email: '',
		pw: ''
	},
	//fb account
	fb: {
		email: '',
		pw: ''
	},

	//tw account
	tw: {
		email: '',
		pw: ''
	},

	channels: {
		guid: 'bahdtest',
		name: 'e2e test create channel name',
		desc: 'e2e test create channel description',
		logo: '../images/w3logo.jpeg',
		picture: '../images/02.jpg',
		leaderboard_desc: 'Leaderboar description rich text editor!!!',
		photo_desc: 'Photo page description rich text editor!!!',
		photo_page_image: '../images/01.jpg',
		color_t: '#3c505e',
		color_one: '#ffffff',
		color_two: '#eb726a',
		color_third: '#d1eb6a',
		points_voting: '5',
		points_correct: '5',
		points_incorrect: '5',
		points_streakbonus: '5',
		points_share_classic: '5',
		points_suggest_classic: '5',
		points_completed: '10',
		points_share_opinion: '5',
		points_suggest_opinion: '5'
	},

	prop: {
		name: 'e2e single image poll',
		desc: 'e2e single image poll with rich text editor!',
		image: '../images/tony-umezawa.jpg',
		contestants: [
			{
				email: 'test1@gmail.com',
				image: '../images/dana-admin.png'
			},
			{
				email: 'test2@gmail.com',
				image: '../images/google.png'
			},
			{
				email: 'test3@gmail.com',
				image: '../images/twitter.png'
			},
			{
				email: 'test4@gmail.com',
				image: '../images/w3logo.jpeg'
			},
		]
	},
	prop_single: {
		name: 'e2e single poll',
		details: 'e2e single poll details!',
		image: '../images/03.jpg',
		tags: 'single poll',
		option1: 'A',
		option2: 'B',
		option3: 'C',
		option4: 'D'
	},
	prop_multiple: {
		title: 'e2e multiple poll',
		image: '../images/04.jpg',
		thank_you_text: 'e2e multiple poll thank you text!!!',
		questions: [
			{
				name: 'What is my name?',
				res1: 'Tony',
				res2: 'Nicky',
				res3: 'Suna',
				res4: 'Lucky'
			},
			{
				name: 'What is her name?',
				res1: 'Sabina',
				res2: 'Nicky',
				res3: 'Suna',
				res4: 'Lucky'
			},
			{
				name: 'What is his name?',
				res1: 'Join',
				res2: 'Lucas',
				res3: 'Frank',
				res4: 'Michal'
			}
		]
	},
	product: {
		name: 'e2e test product poll',
		desc: 'e2e test product poll description!',
		items: [
			{
				name: 'pro1',
				tags: 'pro1',
				image: '../images/01.jpg'
			},
			{
				name: 'pro2',
				tags: 'pro2',
				image: '../images/02.jpg'
			},
			{
				name: 'pro3',
				tags: 'pro3',
				image: '../images/03.jpg'
			},
			{
				name: 'pro4',
				tags: 'pro4',
				image: '../images/04.jpg'
			}
		]
	},

	submit_photo: '../images/tony-umezawa.jpg'
}