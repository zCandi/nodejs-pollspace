// var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var HtmlReporter = require('protractor-html-screenshot-reporter');

exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: [
            '../login.spec.js', 
            '../admin/create_product_poll.spec.js', 
            '../logout.spec.js'
  ],
  // onPrepare: function() {
  //     jasmine.getEnv().addReporter(
  //       new Jasmine2HtmlReporter({
  //         savePath: './test/e2e/reports/'
  //       })
  //     );
  //  }
  onPrepare: function() {
      // Add a screenshot reporter and store screenshots to `/tmp/screnshots`: 
      jasmine.getEnv().addReporter(new HtmlReporter({
         baseDirectory: './test/e2e/reports/admin/'
      }));
  },
  multiCapabilities: [{
    browserName: 'chrome'
  }]
}