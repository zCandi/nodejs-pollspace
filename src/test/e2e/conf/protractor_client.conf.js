// var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var HtmlReporter = require('protractor-html-screenshot-reporter');

exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: [
            '../login_regular_user.spec.js', 
            '../client/product_votes.spec.js',
  //          '../client/voting_sharing.spec.js', 
  //          '../client/submit_photo.spec.js', 
            '../logout.spec.js'],
  // onPrepare: function() {
  //     jasmine.getEnv().addReporter(
  //       new Jasmine2HtmlReporter({
  //         savePath: './test/e2e/reports/'
  //       })
  //     );
  //  }
  onPrepare: function() {
      // Add a screenshot reporter and store screenshots to `/tmp/screnshots`: 
      jasmine.getEnv().addReporter(new HtmlReporter({
         baseDirectory: '../reports/client/'
      }));
  },
  multiCapabilities: [{
    browserName: 'chrome'
  }]
}