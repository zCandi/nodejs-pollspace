// var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var HtmlReporter = require('protractor-html-screenshot-reporter');

exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: [
            '../login_super_admin.spec.js', 
            '../admin/create_standard_channel.spec.js',
//            '../admin/edit_image_channel.spec.js',
//            '../logout.spec.js'
  ],
  // onPrepare: function() {
  //     jasmine.getEnv().addReporter(
  //       new Jasmine2HtmlReporter({
  //         savePath: './test/e2e/reports/'
  //       })
  //     );
  //  }
  onPrepare: function() {
      // Add a screenshot reporter and store screenshots to `/tmp/screnshots`: 
      jasmine.getEnv().addReporter(new HtmlReporter({
         baseDirectory: './test/e2e/reports/super_admin/'
      }));
  },
  multiCapabilities: [{
    browserName: 'chrome'
  }]
}