var conf = require('./conf/config.js');
// spec.js
describe("Protractor e2e test scripts - login function for admin user: ", function() {
  var url = conf.protocol + '://' + conf.channel + '.' + conf.domain;

  beforeEach(function() {
    // browser.wait(2000);  	
    // browser.get('http://bahdtest.pollspacedev.com');
  });

  afterEach(function() {   
    // browser.get('http://bahdtest.pollspacedev.com/logout');
  });

   it('should have a local login form', function() {
    browser.get(url);
    browser.waitForAngular();

    var hereLink = element(by.linkText('here'));
    expect(hereLink.getTagName()).toBe('a');
    hereLink.click();

    var emailInput = element.all(by.id('email')).first();
    var passInput = element.all(by.id('password')).first();
    var btnLogin = element.all(by.css('.channel-standard-button')).first();

    expect(emailInput.getTagName()).toEqual('input');
    expect(passInput.getTagName()).toEqual('input');
    expect(btnLogin.getTagName()).toEqual('button');

    emailInput.sendKeys(conf.super_admin.email);
    passInput.sendKeys(conf.super_admin.pw);
   });

   it('should have a successful login Local user', function() {    
    var btnLogin = element.all(by.css('.channel-standard-button')).first();
    
    btnLogin.click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('/prop/manage');
   });
});