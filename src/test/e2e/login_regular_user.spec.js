var conf = require('./conf/config.js');
// spec.js
describe('Protractor e2e test scripts - login function for regular user: ', function() {
  var url = conf.protocol + '://' + conf.channel + '.' + conf.domain;

  beforeEach(function() {
    // browser.wait(2000);  	
    // browser.get('http://bahdtest.pollspacedev.com');
  });

  afterEach(function() {   
    // browser.get('http://bahdtest.pollspacedev.com/logout');
  });

  it('should have a fb login button', function() {
    browser.get(url);
    browser.waitForAngular();

    var btnFB = browser.findElement(by.css('.fb-button'));
    expect(btnFB.getText()).toEqual('Sign in with Facebook');
  });

  it('should go to Facebook login page', function() {
    var btnFB = browser.findElement(by.css('.fb-button'));
    browser.ignoreSynchronization=true;
    btnFB.click();

    var emailInput = element(by.id('email'));
    var passInput = element(by.id('pass'));
    var btnLogin = element(by.id('loginbutton'));

    expect(emailInput).toBeTruthy();
    expect(passInput).toBeTruthy();
    expect(btnLogin).toBeTruthy();

    emailInput.sendKeys(conf.fb.email);
    passInput.sendKeys(conf.fb.pw);

   });

  it('should have a successful login Facebook user', function() {   
    var btnLogin = element(by.id('loginbutton'));
    btnLogin.click();
    browser.sleep(10000);
    browser.waitForAngular();
    // expect(browser.getCurrentUrl()).toContain('/single/image/');    
   });

});