var conf = require('../conf/config.js');

describe('Pollspace e2e - voting and sharing: ', function(){
	var mainMenu = element.all(by.css('.nav-tabs > li'));
	var voteBtn, shareFbBtn, shareTwBtn, pastWinnersBtn, contestantsOptions, propContainer;
	var choosen = false;

	beforeEach(function() {	
	    
  	});

	afterEach(function() {   
	    
	});

	it ('should have 4 tabs in main menu', function(){
		expect(mainMenu.count()).toEqual(4);
		//check 4 tabs in main menu
		expect(mainMenu.get(0).element(by.tagName('a')).getAttribute('href')).toContain('/#!/final/four');
		expect(mainMenu.get(1).element(by.tagName('a')).getAttribute('href')).toContain('/#!/previous/winners');
		expect(mainMenu.get(2).element(by.tagName('a')).getAttribute('href')).toContain('/#!/upload/photo');
		expect(mainMenu.get(3).element(by.tagName('a')).getAttribute('href')).toContain('/#!/leaders');
	});

	it ('should expose final four page and there is show the prop that just created', function(){
		expect(mainMenu.get(0).getAttribute('class')).toContain('active');

		mainMenu.get(1).click();
		browser.waitForAngular();
		
		mainMenu.get(0).click();
		browser.waitForAngular();
		browser.sleep(3000);
	});

	it ("should expose the prop's name", function(){
		//check voting button
		propContainer = element.all(by.css('.contestent-triangles-container')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(propContainer.element(by.css('h3')).getText()).not.toEqual('');
	});

	it ("should expose the week of", function(){
		//check week of
		var weekof = element.all(by.css('span.contestent-weekof')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(weekof.isPresent()).toBeTruthy();
		expect(weekof.getText()).not.toEqual('');
	});

	it ("should expose the prop's image", function(){
		//check prop's image
		var propImg = element.all(by.css('.triangle > .triangles-container > img')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(propImg.isPresent()).toBeTruthy();
	});

	it ("should expose a vote button", function(){
		voteBtn = propContainer.element(by.css('button.vote-button'));
		expect(voteBtn.isPresent()).toBeTruthy();
	});

	it ("should expose the contestants section", function(){
		//check contestants options
		contestantsOptions = element.all(by.repeater('answer in prop.answers')).filter(function(el){
			return el.isDisplayed();
		});
		expect(contestantsOptions.count()).toEqual(4);
	});

	describe ('vote button: ', function(){
		beforeEach(function() {
			var contestantsChecked = element.all(by.css('.contestent-profile-container.active')).filter(function(el){
				return el.isDisplayed();
			});

			if (contestantsChecked.count() > 0)
				choosen = true;
	  	});

		if (choosen == false) {
			it ("should disabled the vote button if there is not selected  contestant", function(){
				expect(voteBtn.getAttribute('disabled')).toBeTruthy();
			});
		} else {
			it ("should enabled the vote button if there is selected contestant", function(){
				expect(voteBtn.getAttribute('disabled')).toBeFalsy();
			});
		}
	});

	describe ('selected contestant: ', function(){
		beforeEach(function() {
			contestantsOptions.get(0).click();
	  	});

	  	it ('should selected contestant when click on itself if there is not yet selected', function(){
	  		expect(contestantsOptions.get(0).element(by.css('.contestent-profile-container.active')).isPresent()).toBeTruthy();
	  	});

	  	it ('should unselected contestant when click on itself if there is selected', function(){
	  		expect(contestantsOptions.get(0).element(by.css('.contestent-profile-container.active')).isPresent()).toBeFalsy();
	  	});

	  	it ('should have just only one selected', function(){
	  		//check contestant A
	  		expect(contestantsOptions.get(0).element(by.css('.contestent-profile-container.active')).isPresent()).toBeTruthy();
	  		expect(element.all(by.css('.contestent-profile-container.active')).filter(function(el){
	  			return el.isDisplayed();
	  		}).count()).toEqual(1);

	  		//check contestant B 
	  		contestantsOptions.get(1).click();
	  		browser.sleep(1000);
	  		expect(contestantsOptions.get(1).element(by.css('.contestent-profile-container.active')).isPresent()).toBeTruthy();
	  		expect(element.all(by.css('.contestent-profile-container.active')).filter(function(el){
	  			return el.isDisplayed();
	  		}).count()).toEqual(1);

	  		//check contestant C
	  		contestantsOptions.get(2).click();
	  		browser.sleep(1000);
	  		expect(contestantsOptions.get(2).element(by.css('.contestent-profile-container.active')).isPresent()).toBeTruthy();
	  		expect(element.all(by.css('.contestent-profile-container.active')).filter(function(el){
	  			return el.isDisplayed();
	  		}).count()).toEqual(1);

	  		//check contestant D
	  		contestantsOptions.get(3).click();
	  		browser.sleep(1000);
	  		expect(contestantsOptions.get(3).element(by.css('.contestent-profile-container.active')).isPresent()).toBeTruthy();
	  		expect(element.all(by.css('.contestent-profile-container.active')).filter(function(el){
	  			return el.isDisplayed();
	  		}).count()).toEqual(1);
	  	});
	});

	describe ("should expose thanks form when click on vote's button", function(){
		it ('should moving to thanks page', function(){
			voteBtn.click();
			browser.waitForAngular();
			browser.sleep(5000);
			expect(browser.getCurrentUrl()).toContain('/vote/thanks');
		});

		it ("should expose the past winners's button", function(){
			pastWinnersBtn = propContainer.element(by.xpath('//a[@href="/#!/previous/winners" and @type="button"]'));
			expect(pastWinnersBtn.isPresent()).toBeTruthy();
			expect(pastWinnersBtn.getText()).toEqual('PAST WINNERS');
		});

		it ("should expose the share on facebook's button", function(){
			shareFbBtn = propContainer.element(by.id('facebookShareLink'));
			expect(shareFbBtn.isPresent()).toBeTruthy();
		});

		it ("should expose the share on twitter's button", function(){
			shareTwBtn = propContainer.element(by.id('twitterShareLink'));
			expect(shareTwBtn.isPresent()).toBeTruthy();
		});

		it ("should expose the contestants section and should have one selected", function(){
			//check contestants options
			contestantsOptions = element.all(by.repeater('answer in prop.answers')).filter(function(el){
				return el.isDisplayed();
			});
			expect(contestantsOptions.count()).toEqual(4);
			//should have one selected
			expect(element.all(by.css('.contestent-profile-container.active')).filter(function(el){
	  			return el.isDisplayed();
	  		}).count()).toEqual(1);
		});
	});

	describe ('should be share to facebook successful', function(){
		it ("should navigate to share facebook's page", function(){
			shareFbBtn.click();
			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[1]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('facebook.com/sharer/sharer.php');
			  }); 
			});			
		});

		it ("should have a share on facebook's button and share successful", function(){
			var btnShare = element(by.xpath('//button[@name="__CONFIRM__"]'));
			expect(btnShare.isPresent()).toBeTruthy();
			btnShare.click();
			browser.sleep(3000);
			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[0]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('/single/image/');
			  }); 
			});	
		});	
	});

	describe ('should be share to twitter successful', function(){
		it ("should navigate to share twitter's page", function(){
			shareTwBtn.click();
			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[1]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('twitter.com/intent/tweet');
			  }); 
			});			
		});

		it ("should expose a twitter login form", function(){
			var emailInput = element(by.id('username_or_email'));
			var pwInput = element(by.id('password'));

			expect(emailInput.isPresent()).toBeTruthy();
			expect(pwInput.isPresent()).toBeTruthy();

			emailInput.sendKeys(conf.tw.email);
			pwInput.sendKeys(conf.tw.pw);
		});	

		it ("should have the Tweet's button and share successful", function(){
			var submitBtn = element(by.xpath('//input[@type="submit"]'));
			expect(submitBtn.isPresent()).toBeTruthy();

			submitBtn.click();
			browser.sleep(3000);

			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[0]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('/single/image/');
			  }); 
			});	
		});	
	});

	it ("should moving to previous winners's page when click on past winners's button", function(){
		pastWinnersBtn.click();
		browser.waitForAngular();
		expect(browser.getCurrentUrl()).toContain('previous/winners');
		expect(mainMenu.get(1).getAttribute('class')).toContain('active');
	});
});