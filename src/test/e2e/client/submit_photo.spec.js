var conf = require('../conf/config.js');
var path = require('path');

describe ('Pollspace e2e - submit photo: ', function(){
	var mainMenu = element.all(by.css('.nav-tabs > li'));
	var uploadPhotoBtn, shareFbBtn, shareTwBtn;

	beforeEach(function() {	
	    
  	});

	afterEach(function() {   
	    
	});

	it ('should have 4 tabs in main menu', function(){
		expect(mainMenu.count()).toEqual(4);
		//check 4 tabs in main menu
		expect(mainMenu.get(0).element(by.tagName('a')).getAttribute('href')).toContain('/#!/final/four');
		expect(mainMenu.get(1).element(by.tagName('a')).getAttribute('href')).toContain('/#!/previous/winners');
		expect(mainMenu.get(2).element(by.tagName('a')).getAttribute('href')).toContain('/#!/upload/photo');
		expect(mainMenu.get(3).element(by.tagName('a')).getAttribute('href')).toContain('/#!/leaders');
	});

	it ("should navigate to submit photo's page when click on Submit Your Photo's tab", function(){
		mainMenu.get(2).click();
		browser.waitForAngular();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toContain('/#!/upload/photo');
	});

	it ("should have a page's description", function(){
		expect(element(by.css('h3')).isPresent()).toBeTruthy();
	});

	it ("should have a page's image", function(){
		expect(element(by.css('.shampoo-pad > img')).isPresent()).toBeTruthy();
	});

	it ('should have an input field of instagram username', function(){
		expect(element(by.model('photo.instagram')).isPresent()).toBeTruthy();
	});

	it ('should have an upload photo button and it should disabled', function(){
		uploadPhotoBtn = element(by.id('create'));
		expect(uploadPhotoBtn.isPresent()).toBeTruthy();
		expect(uploadPhotoBtn.getAttribute('class')).toContain('disabled');
	});

	describe('check upload file section', function() {
		var photoImg = element(by.model('photo.image'));

		it('should have a form of upload image using uploadcare service', function() {
		  expect(photoImg.isPresent()).toBeTruthy();
		  expect(photoImg.getAttribute('value')).not.toBeFalsy();
		  //check image wrapper
		  var imgWrap = element(by.css('.prop-image-wrap'));
		  expect(imgWrap.isPresent()).toBeTruthy();
		  //check upload button
		  var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).first();
		  expect(btnUpload.isPresent()).toBeTruthy();
		  btnUpload.click();
		  browser.sleep(1000);
		});

		it('should expose uploadcare widget and upload an image successful', function() {
		  var absolutePath = path.resolve(__dirname, conf.submit_photo);
		  //check uploadcare input file type
		  var inputFile = element(by.xpath('//input[@type="file"]'));
		  expect(inputFile.isPresent()).toBeTruthy();
		  //upload an image
		  inputFile.sendKeys(absolutePath);
		  browser.sleep(15000);
		});

		it('should complete image processing of uploadcare widget and show on form', function() {    
		  //check upload sucessfull
		  var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
		  expect(sucessBtn.isPresent()).toBeTruthy();
		  sucessBtn.click();
		  browser.waitForAngular();
		  expect(photoImg.getAttribute('value')).not.toBeFalsy();
		});
	});

	it ('should enabled the upload photo button and upload successful', function(){
		expect(uploadPhotoBtn.getAttribute('class')).not.toContain('disabled');
		uploadPhotoBtn.click();
		browser.waitForAngular();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toContain('/#!/upload/photo/thanks');
	});

	it ("should expose the photo that just uploaded", function(){
		expect(element(by.css('.triangles-container > img')).isPresent()).toBeTruthy();
	});

	it ("should expose the share on facebook's button", function(){
		shareFbBtn = element(by.id('facebookShareLink'));
		expect(shareFbBtn.isPresent()).toBeTruthy();
	});

	it ("should expose the share on twitter's button", function(){
		shareTwBtn = element(by.id('twitterShareLink'));
		expect(shareTwBtn.isPresent()).toBeTruthy();
	});

	describe ('should be share to facebook successful', function(){
		it ("should navigate to share facebook's page", function(){
			shareFbBtn.click();
			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[1]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('facebook.com/sharer/sharer.php');
			  }); 
			});			
		});

		it ("should have a share on facebook's button and share successful", function(){
			var btnShare = element(by.xpath('//button[@name="__CONFIRM__"]'));
			expect(btnShare.isPresent()).toBeTruthy();
			btnShare.click();
			browser.sleep(3000);
			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[0]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('/#!/upload/photo/thanks');
			  }); 
			});	
		});	
	});

	describe ('should be share to twitter successful', function(){
		it ("should navigate to share twitter's page", function(){
			shareTwBtn.click();
			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[1]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('twitter.com/intent/tweet');
			  }); 
			});	
			browser.sleep(3000);		
		});

		// it ("should expose a twitter login form", function(){
		// 	var emailInput = element(by.id('username_or_email'));
		// 	var pwInput = element(by.id('password'));

		// 	expect(emailInput.isPresent()).toBeTruthy();
		// 	expect(pwInput.isPresent()).toBeTruthy();

		// 	emailInput.sendKeys(conf.tw.email);
		// 	pwInput.sendKeys(conf.tw.pw);
		// });	

		it ("should have the Tweet's button and share successful", function(){
			var submitBtn = element(by.xpath('//input[@type="submit"]'));
			expect(submitBtn.isPresent()).toBeTruthy();

			submitBtn.click();
			browser.sleep(3000);

			browser.getAllWindowHandles().then(function(handles){
			  browser.switchTo().window(handles[0]).then(function(){
			  	expect(browser.getCurrentUrl()).toContain('/#!/upload/photo/thanks');
			  }); 
			});	
		});	
	});
});