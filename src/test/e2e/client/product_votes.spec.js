var conf = require('../conf/config.js');

describe('Pollspace e2e - Votes product: ', function(){
	var mainMenu = element.all(by.css('.nav-tabs > li'));
	var btnStartPlaying, listProducts;
	var choosen = false;

	beforeEach(function() {	
	    
  	});

	afterEach(function() {   
	    
	});

	it ('should have 4 tabs in main menu', function(){
		expect(mainMenu.count()).toEqual(4);
		//check 4 tabs in main menu
		expect(mainMenu.get(0).element(by.tagName('a')).getAttribute('href')).toContain('/#!/final/four');
		expect(mainMenu.get(1).element(by.tagName('a')).getAttribute('href')).toContain('/#!/previous/winners');
		expect(mainMenu.get(2).element(by.tagName('a')).getAttribute('href')).toContain('/#!/upload/photo');
		expect(mainMenu.get(3).element(by.tagName('a')).getAttribute('href')).toContain('/#!/leaders');
	});

	it ('should expose the product vote page and there is show the lastest product', function(){
		expect(mainMenu.get(0).getAttribute('class')).toContain('active');

		expect(browser.getCurrentUrl()).toContain('/product/vote');
	});

	it ("should expose list products", function(){
		//check list products
		listProducts = element.all(by.repeater('(key, answer) in prop.answers')).filter(function(el){
			return el.isDisplayed();
		});
		expect(listProducts.count()).toBeGreaterThan(0);
	});

	it ("should expose 2 buttons START PLAYING", function(){
		btnStartPlaying = element.all(by.xpath("//button[@type='button']")).filter(function(el){
			return el.isDisplayed();
		});
		expect(btnStartPlaying.count()).toEqual(2);
		expect(btnStartPlaying.get(0).getText()).toBe('START PLAYING');
		expect(btnStartPlaying.get(1).getText()).toBe('START PLAYING');
	});

	it ("should expose alert popup when click on START PLAYING button if not selected any product", function(){
		btnStartPlaying.get(0).click();
		browser.sleep(2000);

		// check alert popup box
		var popupAlert = browser.switchTo().alert();
		// expect(popupAlert.isPresent()).toBeTruthy();
		expect(popupAlert.getText()).toContain('Please select at least one product to vote. Thanks!');
		
		//Close alert
		popupAlert.accept();
		browser.sleep(1000);
	});

	it ("should voting finish and mote to submit photo's page", function(){
		var products = element.all(by.css('.product-vote-container'));

		expect(products.count()).toEqual(listProducts.count());

		listProducts.get(0).click();
		var proActive = element.all(by.css('.product-vote-container.active'));
		expect(proActive.count()).toBeGreaterThan(0);

		btnStartPlaying.get(0).click();
		browser.waitForAngular();
		browser.sleep(2000);

		expect(browser.getCurrentUrl()).toContain('/upload/photo');
	});
});