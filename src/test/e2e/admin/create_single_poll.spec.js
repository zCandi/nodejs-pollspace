var path = require('path');
var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin create single poll: ", function(){
	var mainMenu = element.all(by.xpath('//div[@ng-switch-when="true"]/ul/li'));
  	var createPollTab, allPollsTab, leaderboardTab, channelSetting;
  	var saveBtn, previewBtn; 

	beforeEach(function() {

	});

	afterEach(function() {   
	});

	it('should have an admin menu', function() {       
		expect(mainMenu.count()).toBe(4);

		createPollTab = mainMenu.get(0).element(by.tagName('a'));
		expect(createPollTab.isPresent()).toBeTruthy();
		expect(createPollTab.getAttribute('href')).toContain('/prop/create'); 

		allPollsTab = mainMenu.get(1).element(by.tagName('a'));
		expect(allPollsTab.isPresent()).toBeTruthy();
		expect(allPollsTab.getAttribute('href')).toContain('/prop/manage'); 

		leaderboardTab = mainMenu.get(2).element(by.tagName('a'));
		expect(leaderboardTab.isPresent()).toBeTruthy();
		expect(leaderboardTab.getAttribute('href')).toContain('/leaderboard'); 

		channelSetting = mainMenu.get(3).element(by.tagName('a'));
		expect(channelSetting.isPresent()).toBeTruthy();
		expect(channelSetting.getAttribute('href')).toContain('/edit'); 
	});

	it('should go to admin create poll page', function() {
		createPollTab.click();
		browser.waitForAngular();
		expect(mainMenu.get(0).getAttribute('class')).toBe('active');
	});

	it ("should expose two buttons are Create Poll and Create Quiz", function(){
		var createPollBtn = element(by.buttonText('Create Poll'));
		expect(createPollBtn.isPresent()).toBeTruthy();
		expect(element(by.buttonText('Create Quiz')).isPresent()).toBeTruthy();

		//create a poll
		createPollBtn.click();
		browser.sleep(1000);
	});

	it ("should expose three buttons are Create Single Poll, Create Image Poll and Create Multiple Poll", function(){
		var createSinglePollBtn = element(by.buttonText('Create Single Poll'));

		expect(createSinglePollBtn.isPresent()).toBeTruthy();
		expect(element(by.buttonText('Create Image Poll')).isPresent()).toBeTruthy();
		expect(element(by.buttonText('Create Multiple Poll')).isPresent()).toBeTruthy();

		//create a single poll
		createSinglePollBtn.click();
		browser.waitForAngular();
		browser.sleep(2000);
	});

	it ("should have Save and Preview button and Save button is disabled", function(){
		saveBtn = element(by.buttonText('Save'));
		previewBtn = element(by.buttonText('Preview'));

		expect(saveBtn.isPresent()).toBeTruthy();
		expect(previewBtn.isPresent()).toBeTruthy();
		expect(saveBtn.getAttribute('class')).toContain('disabled');
	});

	it ("should expose three errors when click on Save button", function(){
		saveBtn.click();
		browser.sleep(1000);

		var errors = element.all(by.css('.with-errors')).filter(function(el){
			return el.isDisplayed();
		});
		expect(errors.count()).toEqual(3);
	});

	it ("should have the type input hidden field", function(){
		expect(element(by.model('type')).isPresent()).toBeTruthy();
	});

	it ("should have the name's input field", function(){
		var name = element(by.model('prop.name'));

		expect(name.isPresent()).toBeTruthy();
		name.sendKeys(conf.prop_single.name);
	});

	it ("should have the details's textarea field", function(){
		var details = element(by.model('prop.details'));

		expect(details.isPresent()).toBeTruthy();
		details.sendKeys(conf.prop_single.details);
	});

	describe('Admin create single poll: check upload file section', function() {
		var propImg = element(by.model('prop.image'));

		it('should have a form of upload image using uploadcare service', function() {
			expect(propImg.isPresent()).toBeTruthy();
			expect(propImg.getAttribute('value')).toEqual('/img/fp/ps740x280.png');
			//check image wrapper
			var imgWrap = element(by.css('.prop-image-wrap'));
			expect(imgWrap.isPresent()).toBeTruthy();
			//check upload button
			var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).first();
			expect(btnUpload.isPresent()).toBeTruthy();
			btnUpload.click();
			browser.sleep(1000);
		});

		it('should expose uploadcare widget and upload an image successful', function() {
			var absolutePath = path.resolve(__dirname, conf.prop_single.image);
			//check uploadcare input file type
			var inputFile = element(by.xpath('//input[@type="file"]'));
			expect(inputFile.isPresent()).toBeTruthy();
			//upload an image
			inputFile.sendKeys(absolutePath);
			browser.sleep(15000);
		});

		it('should complete image processing of uploadcare widget and show on form', function() {    
			//check upload sucessfull
			var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
			expect(sucessBtn.isPresent()).toBeTruthy();
			sucessBtn.click();
			browser.waitForAngular();
			browser.sleep(2000);
			expect(propImg.getAttribute('value')).not.toBe('');
			expect(propImg.getAttribute('value')).not.toBe('/img/fp/ps740x280.png');
		});
	});

	it ("should have the tags's input field", function(){
		var tags = element(by.model('prop.tags'));

		expect(tags.isPresent()).toBeTruthy();
	});

	it ("should have four options", function(){
		var opt1 = element(by.model('prop.answers[0].name'));
		var opt2 = element(by.model('prop.answers[1].name'));
		var opt3 = element(by.model('prop.answers[2].name'));
		var opt4 = element(by.model('prop.answers[3].name'));

		expect(opt1.isPresent()).toBeTruthy();
		expect(opt2.isPresent()).toBeTruthy();
		expect(opt3.isPresent()).toBeTruthy();
		expect(opt4.isPresent()).toBeTruthy();

		opt1.sendKeys(conf.prop_single.option1);
		opt2.sendKeys(conf.prop_single.option2);
		opt3.sendKeys(conf.prop_single.option3);
		opt4.sendKeys(conf.prop_single.option4);
	});

	it ("should enabled the Save button and no errors appear", function(){
		expect(saveBtn.getAttribute('class')).not.toContain('disabled');

		var errors = element.all(by.css('.with-errors')).filter(function(el){
			return el.isDisplayed();
		});
		expect(errors.count()).toEqual(0);
	});

	describe ("check preview function", function(){
		it ("should expose a dialog Preview Prop", function(){
			previewBtn.click();
			browser.sleep(2000);

			expect(element(by.css('.modal-dialog')).isPresent()).toBeTruthy();
		});

		it ("should expose the channel's name", function(){
			expect(element(by.linkText(conf.channels.name)).isPresent()).toBeTruthy();
		});

		it ("should expose the prop's name", function(){
			expect(element(by.binding('prop.name')).isPresent()).toBeTruthy();
		});

		it ("should expose the prop's image", function(){
			expect(element(by.css('.prop-image')).isPresent()).toBeTruthy();
		});

		it ("should expose four options", function(){
			expect(element(by.buttonText(conf.prop_single.option1)).isPresent()).toBeTruthy();
			expect(element(by.buttonText(conf.prop_single.option2)).isPresent()).toBeTruthy();
			expect(element(by.buttonText(conf.prop_single.option3)).isPresent()).toBeTruthy();
			expect(element(by.buttonText(conf.prop_single.option4)).isPresent()).toBeTruthy();
		});

		it ("should have a close button and close dialog when click on it", function(){
			var closeBtn = element(by.buttonText('×'));

			expect(closeBtn.isPresent()).toBeTruthy();
			closeBtn.click();
			browser.sleep(1000);
		});
	});

	it ("should save sucessfull", function(){
		saveBtn.click();
		browser.waitForAngular();

		expect(mainMenu.get(1).getAttribute('class')).toBe('active');
		expect(browser.getCurrentUrl()).toContain('/edit/single');
	});

	describe ("checking and update/publish the poll just created", function(){		

		it ("should have the name's input field and contains the value just created", function(){
			var name = element(by.model('prop.name'));

			expect(name.isPresent()).toBeTruthy();
			expect(name.getAttribute('value')).toEqual(conf.prop_single.name);
		});

		it ("should have the details's textarea field and contains the value just created", function(){
			var details = element(by.model('prop.details'));

			expect(details.isPresent()).toBeTruthy();
			expect(details.getAttribute('value')).toEqual(conf.prop_single.details);
		});

		it ("should have the tags's input field", function(){
			var tags = element(by.model('prop.tags'));

			expect(tags.isPresent()).toBeTruthy();
		});

		it ("should expose the image's section", function(){
			var image = element(by.model('prop.image'));
			var imagebk = element(by.model('prop.imagebk'));

			expect(image.isPresent()).toBeTruthy();
			expect(imagebk.isPresent()).toBeTruthy();
		});

		it ("should have four options and contains the value just created", function(){
			var opt1 = element(by.model('prop.answers[0].name'));
			var opt2 = element(by.model('prop.answers[1].name'));
			var opt3 = element(by.model('prop.answers[2].name'));
			var opt4 = element(by.model('prop.answers[3].name'));

			expect(opt1.isPresent()).toBeTruthy();
			expect(opt2.isPresent()).toBeTruthy();
			expect(opt3.isPresent()).toBeTruthy();
			expect(opt4.isPresent()).toBeTruthy();

			expect(opt1.getAttribute('value')).toEqual(conf.prop_single.option1);
			expect(opt2.getAttribute('value')).toEqual(conf.prop_single.option2);
			expect(opt3.getAttribute('value')).toEqual(conf.prop_single.option3);
			expect(opt4.getAttribute('value')).toEqual(conf.prop_single.option4);
		});

		it ("should checked on the Created status", function(){
			var created = element.all(by.css('.prop-state-active')).first();

			expect(created.getText()).toBe('Created');
			expect(created.element(by.css('.fa-check-circle')).isPresent()).toBeTruthy();
		});

		describe ("check the publish function", function(){
			var fbBtn, twBtn;

			it ("should inactive on the Published status", function(){
				expect(element(by.css('.prop-state-inactive')).getText()).toBe('Published');
			});

			it ("should have the publish button and publishing successfull", function(){
				publishBtn = element(by.buttonText('Publish'));

				expect(publishBtn.isPresent()).toBeTruthy();

				//publish the prop
				publishBtn.click();
				browser.sleep(2000);
			});
			
			it ("should expose the share on Facebook button and share on Twitter button", function(){
				fbBtn = element(by.buttonText('Share on facebook'));
				twBtn = element(by.buttonText('Share via twitter'));

				expect(fbBtn.isPresent()).toBeTruthy();
				expect(twBtn.isPresent()).toBeTruthy();
			});

			it ("should checked on the Published status", function(){
				var published = element.all(by.css('.prop-state-active')).get(1);

				expect(published.getText()).toBe('Published');
				expect(published.element(by.css('.fa-check-circle')).isPresent()).toBeTruthy();
			});

			it ("should expose the View Embedded Code button", function(){
				expect(element(by.buttonText('View Embedded Code')).isPresent()).toBeTruthy();
			});
		});

		it ("should have a Save button", function(){
			expect(element(by.buttonText('Save')).isPresent()).toBeTruthy();
		});

		it ("should have a Delete button", function(){
			expect(element(by.linkText('Delete')).isPresent()).toBeTruthy();
		});

		it ("should have a Preview button", function(){
			expect(element(by.buttonText('Preview')).isPresent()).toBeTruthy();
		});
	});
});