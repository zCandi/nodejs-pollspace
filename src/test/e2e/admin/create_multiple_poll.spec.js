var path = require('path');
var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin create multiple poll: ", function(){
	var mainMenu = element.all(by.xpath('//div[@ng-switch-when="true"]/ul/li'));
  	var createPollTab, allPollsTab, leaderboardTab, channelSetting;
  	var saveBtn, addQuestionBtn; 

	beforeEach(function() {

	});

	afterEach(function() {   
	});

	it('should have an admin menu', function() {       
		expect(mainMenu.count()).toBe(4);

		createPollTab = mainMenu.get(0).element(by.tagName('a'));
		expect(createPollTab.isPresent()).toBeTruthy();
		expect(createPollTab.getAttribute('href')).toContain('/prop/create'); 

		allPollsTab = mainMenu.get(1).element(by.tagName('a'));
		expect(allPollsTab.isPresent()).toBeTruthy();
		expect(allPollsTab.getAttribute('href')).toContain('/prop/manage'); 

		leaderboardTab = mainMenu.get(2).element(by.tagName('a'));
		expect(leaderboardTab.isPresent()).toBeTruthy();
		expect(leaderboardTab.getAttribute('href')).toContain('/leaderboard'); 

		channelSetting = mainMenu.get(3).element(by.tagName('a'));
		expect(channelSetting.isPresent()).toBeTruthy();
		expect(channelSetting.getAttribute('href')).toContain('/edit'); 
	});

	it('should go to admin create poll page', function() {
		createPollTab.click();
		browser.waitForAngular();
		expect(mainMenu.get(0).getAttribute('class')).toBe('active');
	});

	it ("should expose two buttons are Create Poll and Create Quiz", function(){
		var createPollBtn = element(by.buttonText('Create Poll'));
		expect(createPollBtn.isPresent()).toBeTruthy();
		expect(element(by.buttonText('Create Quiz')).isPresent()).toBeTruthy();

		//create a poll
		createPollBtn.click();
		browser.sleep(1000);
	});

	it ("should expose three buttons are Create Single Poll, Create Image Poll and Create Multiple Poll", function(){
		var createMultiplePollBtn = element(by.buttonText('Create Multiple Poll'));

		expect(createMultiplePollBtn.isPresent()).toBeTruthy();
		expect(element(by.buttonText('Create Image Poll')).isPresent()).toBeTruthy();
		expect(element(by.buttonText('Create Single Poll')).isPresent()).toBeTruthy();

		//create multiple poll
		createMultiplePollBtn.click();
		browser.waitForAngular();
		browser.sleep(2000);

		expect(browser.getCurrentUrl()).toContain('/prop/create/multiple/opinion');
	});

	it ("should have the Add Question button", function(){
		addQuestionBtn = element(by.xpath('//span[@ng-click="addQuestion()"]'));

		expect(addQuestionBtn.isPresent()).toBeTruthy();
	});

	it ("should have the Save Poll button and it is disabled", function(){
		saveBtn = element(by.xpath('//button[@type="submit"]'));

		expect(saveBtn.isPresent()).toBeTruthy();
		expect(saveBtn.getText()).toBe('Save Poll');
		expect(saveBtn.getAttribute('class')).toContain('disabled');
	});

	it ("should have the title input field", function(){
		var title = element(by.model('propset.title'));

		expect(title.isPresent()).toBeTruthy();
		title.sendKeys(conf.prop_multiple.title);
	});

	describe('Admin create multiple poll: check upload file section', function() {
		var propImg = element(by.model('propset.image'));

		it('should have a form of upload image using uploadcare service', function() {
			expect(propImg.isPresent()).toBeTruthy();
			expect(propImg.getAttribute('value')).toEqual('/img/fp/ps740x280.png');
			//check image wrapper
			var imgWrap = element(by.css('.prop-image-wrap'));
			expect(imgWrap.isPresent()).toBeTruthy();
			//check upload button
			var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).first();
			expect(btnUpload.isPresent()).toBeTruthy();
			btnUpload.click();
			browser.sleep(1000);
		});

		it('should expose uploadcare widget and upload an image successful', function() {
			var absolutePath = path.resolve(__dirname, conf.prop_multiple.image);
			//check uploadcare input file type
			var inputFile = element(by.xpath('//input[@type="file"]'));
			expect(inputFile.isPresent()).toBeTruthy();
			//upload an image
			inputFile.sendKeys(absolutePath);
			browser.sleep(15000);
		});

		it('should complete image processing of uploadcare widget and show on form', function() {    
			//check upload sucessfull
			var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
			expect(sucessBtn.isPresent()).toBeTruthy();
			sucessBtn.click();
			browser.waitForAngular();
			browser.sleep(2000);
			expect(propImg.getAttribute('value')).not.toBe('');
			expect(propImg.getAttribute('value')).not.toBe('/img/fp/ps740x280.png');
		});
	});

	it ("should have the thank you page textarea field", function(){
		var thankyou = element(by.model('propset.thankyoupage_text'));

		expect(thankyou.isPresent()).toBeTruthy();
		expect(thankyou.getTagName()).toBe('textarea');

		thankyou.sendKeys(conf.prop_multiple.thank_you_text);
	});

	describe ("check the questions section", function(){
		var questions = element.all(by.repeater('question in propset.props'));
		var totalQuestions;

		it ("should have the questions section and there are at least 2 questions", function(){
			expect(questions.count()).toBeGreaterThan(1);

			totalQuestions = questions.count();
		});

		it ("should setup the first question sucessfull", function(){
			var question = questions.get(0);
			var saveQuestion = question.element(by.xpath('//span[@ng-click="saveQuestion(question)"]'));

			expect(question.element(by.model('question.name')).isPresent()).toBeTruthy();
			question.element(by.model('question.name')).sendKeys(conf.prop_multiple.questions[0].name);
			question.element(by.model('question.answers[0].name')).sendKeys(conf.prop_multiple.questions[0].res1);
			question.element(by.model('question.answers[1].name')).sendKeys(conf.prop_multiple.questions[0].res2);
			question.element(by.model('question.answers[2].name')).sendKeys(conf.prop_multiple.questions[0].res3);
			question.element(by.model('question.answers[3].name')).sendKeys(conf.prop_multiple.questions[0].res4);

			saveQuestion.click();
			browser.sleep(3000);

			expect(question.element(by.xpath('//span[@ng-click="deleteQuestion(question)"]')).isPresent()).toBeTruthy();
			expect(saveQuestion.isDisplayed()).toBeFalsy();
		});

		it ("should setup the second question sucessfull", function(){
			var question = questions.get(1);
			var saveQuestion = question.element(by.xpath('//span[@ng-click="saveQuestion(question)"]'));

			expect(question.element(by.model('question.name')).isPresent()).toBeTruthy();
			question.element(by.model('question.name')).sendKeys(conf.prop_multiple.questions[1].name);
			question.element(by.model('question.answers[0].name')).sendKeys(conf.prop_multiple.questions[1].res1);
			question.element(by.model('question.answers[1].name')).sendKeys(conf.prop_multiple.questions[1].res2);
			question.element(by.model('question.answers[2].name')).sendKeys(conf.prop_multiple.questions[1].res3);
			question.element(by.model('question.answers[3].name')).sendKeys(conf.prop_multiple.questions[1].res4);

			saveQuestion.click();
			browser.sleep(3000);

			expect(question.element(by.xpath('//span[@ng-click="deleteQuestion(question)"]')).isPresent()).toBeTruthy();
			expect(saveQuestion.isDisplayed()).toBeFalsy();
		});

		it ("should enabled the Save Poll button", function(){
			expect(saveBtn.getAttribute('class')).not.toContain('disabled');
		});

		it ("should the add question button works", function(){
			addQuestionBtn.click();
			browser.sleep(1000);

			expect(questions.count()).toBeGreaterThan(totalQuestions);
			totalQuestions = questions.count();
		});

		it ("should setup the thirt question sucessfull", function(){
			var question = questions.get(2);
			var saveQuestion = question.element(by.xpath('//span[@ng-click="saveQuestion(question)"]'));

			expect(question.element(by.model('question.name')).isPresent()).toBeTruthy();
			question.element(by.model('question.name')).sendKeys(conf.prop_multiple.questions[2].name);
			question.element(by.model('question.answers[0].name')).sendKeys(conf.prop_multiple.questions[2].res1);
			question.element(by.model('question.answers[1].name')).sendKeys(conf.prop_multiple.questions[2].res2);
			question.element(by.model('question.answers[2].name')).sendKeys(conf.prop_multiple.questions[2].res3);
			question.element(by.model('question.answers[3].name')).sendKeys(conf.prop_multiple.questions[2].res4);

			saveQuestion.click();
			browser.sleep(3000);

			expect(question.element(by.xpath('//span[@ng-click="deleteQuestion(question)"]')).isPresent()).toBeTruthy();
			expect(saveQuestion.isDisplayed()).toBeFalsy();
		});

		xit ("should delete question sucessfull", function(){
			var question = questions.last();
			var delBtn = question.element(by.xpath('//span[@ng-click="deleteQuestion(question)"]'));

			delBtn.click();
			browser.sleep(2000);

			expect(questions.count()).toBeLessThan(totalQuestions);
		});
	});

	it ("should save the poll sucessfull", function(){
		saveBtn.click();
		browser.waitForAngular();
		browser.sleep(2000);

		expect(browser.getCurrentUrl()).toContain('/edit/multiple');
	});

	describe ("checking and update/publish the poll just created", function(){

		it ("should have the title input field and contains the value just created", function(){
			var title = element(by.model('propset.title'));

			expect(title.isPresent()).toBeTruthy();
			expect(title.getAttribute('value')).toEqual(conf.prop_multiple.title);
		});

		it ("should have the image section", function(){
			var image = element(by.model('propset.image'));
			var imagebk = element(by.model('propset.imagebk'));

			expect(image.isPresent()).toBeTruthy();
			expect(imagebk.isPresent()).toBeTruthy();

			expect(image.getAttribute('value')).toEqual(imagebk.getAttribute('value'));
		});

		it ("should have the thank you page textarea field and contains the value just created", function(){
			var thankyou = element(by.model('propset.thankyoupage_text'));

			expect(thankyou.isPresent()).toBeTruthy();
			expect(thankyou.getAttribute('value')).toEqual(conf.prop_multiple.thank_you_text);
		});

		describe ("check questions section", function(){
			var questions = element.all(by.repeater('question in propset.props'));
			var totalQuestions;

			it ("should have at least two questions and contains the data just created", function(){
				expect(questions.count()).toBeGreaterThan(1);
				totalQuestions = questions.count();

				questions.each(function(el, index){
					expect(el.element(by.binding('question.name')).getText()).toBe(conf.prop_multiple.questions[index].name);

					expect(el.element(by.binding('question.answers[0].name')).getText()).toBe(conf.prop_multiple.questions[index].res1);
					expect(el.element(by.binding('question.answers[1].name')).getText()).toBe(conf.prop_multiple.questions[index].res2);
					expect(el.element(by.binding('question.answers[2].name')).getText()).toBe(conf.prop_multiple.questions[index].res3);
					expect(el.element(by.binding('question.answers[3].name')).getText()).toBe(conf.prop_multiple.questions[index].res4);
				});
			});

			it ("should have the Delete Question button and the Edit Question button", function(){
				questions.each(function(el, index){
					expect(el.element(by.xpath('//span[@ng-click="deleteQuestion(question)"]')).isPresent()).toBeTruthy();
					expect(el.element(by.xpath('//span[@ng-click="editQuestion(question)"]')).isPresent()).toBeTruthy();
				});
			});

			it ("should do delete a question sucessfull", function(){
				var question = questions.last();

				question.element(by.xpath('//span[@ng-click="deleteQuestion(question)"]')).click();
				browser.sleep(1000);

				expect(questions.count()).toBeLessThan(totalQuestions);
			});

			it ("should do edit a question sucessfull", function(){
				var question = questions.last();

				question.element(by.xpath('//span[@ng-click="editQuestion(question)"]')).click();
				browser.sleep(1000);


			});
		});
	});
});