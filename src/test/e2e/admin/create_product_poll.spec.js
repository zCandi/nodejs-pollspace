var path = require('path');
var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin create product poll: ", function(){
	var mainMenu = element.all(by.xpath('//div[@ng-switch-when="true"]/ul/li'));
  	var createPollTab, allPollsTab, leaderboardTab, submittedPhotoTab;
  	var saveBtn, addProductBtn; 

	beforeEach(function() {

	});

	afterEach(function() {   
	
	});

	it ('should have an admin menu', function() {       
		expect(mainMenu.count()).toBe(4);

		createPollTab = mainMenu.get(0).element(by.tagName('a'));
		expect(createPollTab.isPresent()).toBeTruthy();
		expect(createPollTab.getAttribute('href')).toContain('/prop/create'); 

		allPollsTab = mainMenu.get(1).element(by.tagName('a'));
		expect(allPollsTab.isPresent()).toBeTruthy();
		expect(allPollsTab.getAttribute('href')).toContain('/prop/manage'); 

		leaderboardTab = mainMenu.get(2).element(by.tagName('a'));
		expect(leaderboardTab.isPresent()).toBeTruthy();
		expect(leaderboardTab.getAttribute('href')).toContain('/leaderboard'); 

		submittedPhotoTab = mainMenu.get(3).element(by.tagName('a'));
		expect(submittedPhotoTab.isPresent()).toBeTruthy();
		expect(submittedPhotoTab.getAttribute('href')).toContain('/submitted/photos'); 
	});

	it ('should have a menu link', function() {
		var menu = element.all(by.xpath("//a[@ng-click='openMenu()']")).filter(function(el){
		  return el.isDisplayed();
		});

		menu.first().click();
		browser.sleep(1000);
		expect(menu.first().isPresent()).toBeTruthy();
	});

	it ("should have a create product menu", function(){
		var rightMenu = element.all(by.css('ul.mm-list > li')).filter(function(el){
			return el.isDisplayed();
		});

		rightMenu.get(7).click();
		browser.waitForAngular();
		browser.sleep(3000);

		expect(browser.getCurrentUrl()).toContain('/prop/create/single/product');

		element(by.id('mm-blocker')).click();
		browser.sleep(1000);
	});

	it ("should expose create product form", function(){
		// expect(createPollTab.getAttribute('class')).toContain('active');

		var proName = element(by.model('prop.name'));
		expect(proName.isPresent()).toBeTruthy();
		proName.sendKeys(conf.product.name);

		var proDesc = element(by.model('prop.desc'));
		expect(proDesc.isPresent()).toBeTruthy();
		//check rich text editor key
    	var proDescVal = element(by.id('prop-description'));
    	expect(proDescVal.isPresent()).toBeTruthy();
		proDescVal.sendKeys(conf.product.desc);

		saveBtn = element(by.buttonText('Save'));
		expect(saveBtn.isPresent()).toBeTruthy();

		addProductBtn = element(by.buttonText('+ Add Product'));
		expect(addProductBtn.isPresent()).toBeTruthy();

		var proList = element.all(by.repeater('answer in prop.answers')).filter(function(el){
			return el.isDisplayed();
		});
		expect(proList.count()).toEqual(1);
	});

	it ("should add/delete product successfull", function(){
		addProductBtn.click();
		var proList = element.all(by.repeater('answer in prop.answers')).filter(function(el){
			return el.isDisplayed();
		});
		expect(proList.count()).toEqual(2);

		//remove a product
		proList.get(1).element(by.linkText('Remove Product')).click();
		browser.sleep(1000);
		
		proList = element.all(by.repeater('answer in prop.answers')).filter(function(el){
			return el.isDisplayed();
		});
		expect(proList.count()).toEqual(1);

		addProductBtn.click();
		addProductBtn.click();
		addProductBtn.click();
	});

	it ("should input the product successfull", function(){
		var proList = element.all(by.repeater('answer in prop.answers')).filter(function(el){
			return el.isDisplayed();
		});
		expect(proList.count()).toEqual(4);

		proList.get(0).element(by.model('answer.name')).sendKeys(conf.product.items[0].name);

		//check upload button
	    var btnUpload = proList.get(0).element(by.css('.uploadcare-widget-button.uploadcare-widget-button-open'));
	    expect(btnUpload.isPresent()).toBeTruthy();
	    btnUpload.click();
	    browser.sleep(1000);

	    var absolutePath = path.resolve(__dirname, conf.product.items[0].image);
        //check uploadcare input file type
        var inputFile = element(by.xpath('//input[@type="file"]'));
        expect(inputFile.isPresent()).toBeTruthy();
        //upload an image
        inputFile.sendKeys(absolutePath);
        browser.sleep(15000);

        //check upload sucessfull
        var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
        expect(sucessBtn.isPresent()).toBeTruthy();
        sucessBtn.click();
        browser.waitForAngular();
        browser.sleep(2000);
	});

	it ("should save product sucessfull", function(){
		saveBtn.click();
		browser.waitForAngular();
		expect(browser.getCurrentUrl()).toContain('/edit/single');
	});
});