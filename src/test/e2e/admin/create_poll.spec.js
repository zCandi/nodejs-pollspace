var path = require('path');
var conf = require('../conf/config.js');
// spec.js
describe('Pollspace e2e test scripts - admin create image poll: ', function() {

  var createPollTab, allPollsTab, leaderboardTab, submittedPhotosTab;
  var mainMenu = element.all(by.xpath('//div[@ng-switch-when="true"]/ul/li'));
  var channelName = conf.channel;
  var saveBtn = element(by.id('create'));
  var propsTotal;

  beforeEach(function() {
    
  });

  afterEach(function() {   
  });

  it('should have an admin menu', function() {
    propsTotal = element.all(by.id('propList')).count();        
    expect(mainMenu.count()).toBe(4);

    createPollTab = mainMenu.get(0).element(by.tagName('a'));
    expect(createPollTab.isPresent()).toBeTruthy();
    expect(createPollTab.getAttribute('href')).toContain('/prop/create'); 

    allPollsTab = mainMenu.get(1).element(by.tagName('a'));
    expect(allPollsTab.isPresent()).toBeTruthy(); 

    leaderboardTab = mainMenu.get(2).element(by.tagName('a'));
    expect(leaderboardTab.isPresent()).toBeTruthy(); 

    submittedPhotosTab = mainMenu.get(3).element(by.tagName('a'));
    expect(submittedPhotosTab.isPresent()).toBeTruthy(); 
  });

  it('should go to admin create poll page', function() {
    createPollTab.click();
    browser.waitForAngular();
    expect(mainMenu.get(0).getAttribute('class')).toBe('active');
  });

  it('should have a create single form', function() {
    var containner = element.all(by.xpath('//section[@data-ng-controller="AdminPropsController"]')).filter(function(el){
      return el.isDisplayed();
    });
    expect(containner.first().isPresent()).toBeTruthy();
    expect(element(by.xpath('//h2/span')).getText()).toBe('Create Single Question Image Poll');
  });

  it('should disabled the save button', function() {
    expect(saveBtn.isPresent()).toBeTruthy();
    expect(saveBtn.getAttribute('class')).toContain('disabled');
  });

  it('should expose a warning if click on save button which the form is empty', function() {
    saveBtn.click();
    browser.waitForAngular();
    var warning = element.all(by.css('.has-error'));
    expect(warning.count()).toBe(3);
    expect(warning.first().element(by.css('div > ul > li')).getText()).toEqual('Name is required.');
  });

  it("should have the type's input hidden field", function() {
    var typeInput = element(by.model('type'));
    expect(typeInput.isPresent()).toBeTruthy();
    expect(typeInput.getAttribute('type')).toBe('hidden');
  });

  it("should have the name's input field", function() {
    //check model prop.name
    var nameInput = element(by.model('prop.name'));
    expect(nameInput.isPresent()).toBeTruthy();
    //input value for prop.name
    nameInput.sendKeys(conf.prop.name);
  });

  it('should expose a warning if click on save button if not input at least 2 contestants', function() {
    saveBtn.click();
    browser.waitForAngular();
    var warning = element.all(by.css('.has-error'));// > div > ul > li'));
    expect(warning.count()).toBeGreaterThan(0);
  });

  it('should have a form of rich text editor for description', function() {
    //check prop.desc model
    var descTextarea = element(by.model('prop.desc'));
    expect(descTextarea.isPresent()).toBeTruthy();
    //check rich text editor key
    var descVal = element(by.id('prop-description'));
    expect(descVal.isPresent()).toBeTruthy();
    //input value for prop.desc
    descVal.sendKeys(conf.prop.desc);
  });

  describe('Admin create poll: check week of section', function() {
    var weekofFrom = element(by.model('prop.weekof.from'));
    var calWeekofFrom = element(by.xpath('//div[@id="weeokof_from_date"]/span[@class="input-group-addon"]'));
    var weekofTo = element(by.model('prop.weekof.to'));
    var calWeekofTo = element(by.xpath('//div[@id="weeokof_to_date"]/span[@class="input-group-addon"]'));

    it('should have a form of calendar for week of section', function() {
      //week of from
      expect(weekofFrom.isPresent()).toBeTruthy();
      //check calendar button
      expect(calWeekofFrom.isPresent()).toBeTruthy();
      //week of to
      expect(weekofTo.isPresent()).toBeTruthy();
      //check calendar button
      expect(calWeekofTo.isPresent()).toBeTruthy();
    });

    it('should display a calendar when click on calendar button of input week_of_from', function() {
      calWeekofFrom.click();
      browser.sleep(2000);
      expect(weekofFrom.getAttribute('value')).not.toBe('');
    });

    it('should display a calendar when click on calendar button of input week_of_to', function() {
      calWeekofTo.click();
      browser.sleep(2000);
      expect(weekofTo.getAttribute('value')).not.toBe('');
    });
  });

  describe('Admin create poll: check upload file section', function() {
    var propImg = element(by.model('prop.image'));
    
    it('should have a form of upload image using uploadcare service', function() {
      expect(propImg.isPresent()).toBeTruthy();
      expect(propImg.getAttribute('value')).toEqual('/img/fp/ogx587x587.png');
      //check image wrapper
      var imgWrap = element(by.css('.prop-image-wrap'));
      expect(imgWrap.isPresent()).toBeTruthy();
      //check upload button
      var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).first();
      expect(btnUpload.isPresent()).toBeTruthy();
      btnUpload.click();
      browser.sleep(1000);
    });

    it('should expose uploadcare widget and upload an image successful', function() {
      var absolutePath = path.resolve(__dirname, conf.prop.image);
      //check uploadcare input file type
      var inputFile = element(by.xpath('//input[@type="file"]'));
      expect(inputFile.isPresent()).toBeTruthy();
      //upload an image
      inputFile.sendKeys(absolutePath);
      browser.sleep(15000);
    });

    it('should complete image processing of uploadcare widget and show on form', function() {    
      //check upload sucessfull
      var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
      expect(sucessBtn.isPresent()).toBeTruthy();
      sucessBtn.click();
      browser.waitForAngular();
      browser.sleep(2000);
      expect(propImg.getAttribute('value')).not.toBe('');
      expect(propImg.getAttribute('value')).not.toBe('/img/fp/ogx587x587.png');
    });
  });

  describe('Admin create poll: check contestants section', function() {
    
    var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open'));
    var contestants = element.all(by.css('.contesten-container'));

    it ('should have 4 contestants', function(){
      expect(contestants.count()).toBe(4);
      expect(contestants.get(0).element(by.tagName('h4')).getText()).toEqual('Contestant A');
      expect(contestants.get(1).element(by.tagName('h4')).getText()).toEqual('Contestant B');
      expect(contestants.get(2).element(by.tagName('h4')).getText()).toEqual('Contestant C');
      expect(contestants.get(3).element(by.tagName('h4')).getText()).toEqual('Contestant D');
    });

    it ("should have data input to the contestant's A successful", function(){
      var email = element(by.model('prop.answers[0].name'));
      expect(email.isPresent()).toBeTruthy();
      //input email
      email.sendKeys(conf.prop.contestants[0].email);
      // //upload image
      expect(btnUpload.get(1).isPresent()).toBeTruthy();
      btnUpload.get(1).click();
      browser.sleep(1000);
      //upload file
      var absolutePath = path.resolve(__dirname, conf.prop.contestants[0].image);
      //check uploadcare input file type
      var inputFile = element(by.xpath('//input[@type="file"]'));
      expect(inputFile.isPresent()).toBeTruthy();
      //upload an image
      inputFile.sendKeys(absolutePath);
      browser.sleep(15000);
      //check upload sucessfull
      var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
      expect(sucessBtn.isPresent()).toBeTruthy();
      sucessBtn.click();
      browser.waitForAngular();
    });

    it ("should have data input to the contestant's B successful", function(){
      var email = element(by.model('prop.answers[1].name'));
      expect(email.isPresent()).toBeTruthy();
      //input email
      email.sendKeys(conf.prop.contestants[1].email);
      // //upload image
      expect(btnUpload.get(2).isPresent()).toBeTruthy();
      btnUpload.get(2).click();
      browser.sleep(1000);
      //upload file
      var absolutePath = path.resolve(__dirname, conf.prop.contestants[1].image);
      //check uploadcare input file type
      var inputFile = element(by.xpath('//input[@type="file"]'));
      expect(inputFile.isPresent()).toBeTruthy();
      //upload an image
      inputFile.sendKeys(absolutePath);
      browser.sleep(15000);
      //check upload sucessfull
      var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
      expect(sucessBtn.isPresent()).toBeTruthy();
      sucessBtn.click();
      browser.waitForAngular();
    });

    it ("should have data input to the contestant's C successful", function(){
      var email = element(by.model('prop.answers[2].name'));
      expect(email.isPresent()).toBeTruthy();
      //input email
      email.sendKeys(conf.prop.contestants[2].email);
      // //upload image
      expect(btnUpload.get(3).isPresent()).toBeTruthy();
      btnUpload.get(3).click();
      browser.sleep(1000);
      //upload file
      var absolutePath = path.resolve(__dirname, conf.prop.contestants[2].image);
      //check uploadcare input file type
      var inputFile = element(by.xpath('//input[@type="file"]'));
      expect(inputFile.isPresent()).toBeTruthy();
      //upload an image
      inputFile.sendKeys(absolutePath);
      browser.sleep(15000);
      //check upload sucessfull
      var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
      expect(sucessBtn.isPresent()).toBeTruthy();
      sucessBtn.click();
      browser.waitForAngular();
    });

    it ("should have data input to the contestant's D successful", function(){
      var email = element(by.model('prop.answers[3].name'));
      expect(email.isPresent()).toBeTruthy();
      //input email
      email.sendKeys(conf.prop.contestants[3].email);
      // //upload image
      expect(btnUpload.get(4).isPresent()).toBeTruthy();
      btnUpload.get(4).click();
      browser.sleep(1000);
      //upload file
      var absolutePath = path.resolve(__dirname, conf.prop.contestants[3].image);
      //check uploadcare input file type
      var inputFile = element(by.xpath('//input[@type="file"]'));
      expect(inputFile.isPresent()).toBeTruthy();
      //upload an image
      inputFile.sendKeys(absolutePath);
      browser.sleep(15000);
      //check upload sucessfull
      var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
      expect(sucessBtn.isPresent()).toBeTruthy();
      sucessBtn.click();
      browser.waitForAngular();
    });
  });

  it ('should have a preview button', function(){
    var previewBtn = element(by.xpath('//button[@ng-click="showCreateModalPreview()"]'));
    expect(previewBtn.isPresent()).toBeTruthy();
    previewBtn.click();
    browser.sleep(1000);
  });

  it ('should expose the preview popup when click on preview button', function(){
    expect(element(by.css('.modal-dialog')).isPresent()).toBeTruthy();
    browser.sleep(1000);
  });

  it ('should close the preview popup when click on close button', function(){
    var closePopupBtn = element(by.css('.modal-header > .close'));
    expect(closePopupBtn.isPresent()).toBeTruthy();
    closePopupBtn.click();
    browser.sleep(1000);
  });

  it ('should be save successful and expose an edit form', function(){
    saveBtn.click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('/edit/single');    
  });

  it('should not publish', function (){
    var inactiveBtn = element.all(by.css('.prop-state-inactive'));    
    expect(inactiveBtn.count()).toEqual(3);
    expect(inactiveBtn.get(0).getText()).toEqual('Published');
  });

  it ('should have a publish button and publish a poll successful', function(){
    var publishBtn = element(by.xpath('//button[@ng-click="publish($event)"]'));
    expect(publishBtn.getText()).toBe('Publish');
    publishBtn.click();
    browser.waitForAngular(); 
    expect(element.all(by.css('.prop-state-inactive')).count()).toEqual(0);
    var activeBtn = element.all(by.css('.prop-state-active'));
    expect(activeBtn.count()).toEqual(4);
    expect(activeBtn.get(1).element(by.css('i.fa.fa-check-circle')).isPresent()).toBeTruthy();
  });

  it ('should expose new prop in props list', function(){
    mainMenu.get(1).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('/prop/manage/single/image');
    //check total of props
    expect(element.all(by.id('propList')).count()).toBeGreaterThan(propsTotal);
  });

  xit ('should have a remove button and delete successful', function(){
    var delBtn = element(by.xpath('//a[@data-ng-click="remove(channel._id, prop._id);"]'));
    expect(delBtn.getText()).toBe('Delete');
    delBtn.click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('/prop/manage/single/image');
    //check total of props not change
    expect(element.all(by.id('propList')).count()).toEqual(propsTotal);
  });

  //logout
  it('should have a menu link', function() {
    var menu = element.all(by.xpath("//a[@ng-click='openMenu()']")).filter(function(el){
        return el.isDisplayed();
    });

    menu.first().click();
    browser.sleep(1000);
    expect(menu.first().isPresent()).toBeTruthy();
  });

  it("should have the logout link and logout successful", function() {
    var logout = element.all(by.xpath("//a[@href='/logout']")).filter(function(el){
      return el.isDisplayed();
    });

    expect(logout.first().isPresent()).toBeTruthy();
    logout.first().click();
    browser.waitForAngular();
    browser.sleep(2000);
    expect(browser.getCurrentUrl()).toContain('/#!/login');
  });

});