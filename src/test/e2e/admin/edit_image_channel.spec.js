var path = require('path');
var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin edit image channel: ", function(){
	var menuBtn;

	beforeEach(function() {
    
  	});

	afterEach(function() {

	});

	it ("should have a list channels", function(){
		var url = conf.protocol + '://' + conf.domain + '/#!/admin/channel/list';
		browser.get(url);
		browser.waitForAngular();

		expect(element(by.repeater('channel in channels')).isPresent()).toBeTruthy();		
	});

	it ("should expose the channel page", function(){
		var channel = element(by.linkText(conf.channels.name));

		expect(channel.isPresent()).toBeTruthy();
		channel.click();
		browser.waitForAngular();
		browser.sleep(2000);
	});

	it('should have a menu link', function() {
		menuBtn = element.all(by.xpath("//a[@ng-click='openMenu()']")).filter(function(el){
		  return el.isDisplayed();
		}).first();

		menuBtn.click();
		browser.sleep(1000);
		expect(menuBtn.isPresent()).toBeTruthy();
	});

	it ("should have a menu of channel settings", function(){
		var rightMenu = element.all(by.css('ul.mm-list > li')).filter(function(el){
			return el.isDisplayed();
		});

		rightMenu.get(12).click();
		browser.waitForAngular();
		browser.sleep(3000);

		expect(browser.getCurrentUrl()).toContain('/edit');

		element(by.id('mm-blocker')).click();
		browser.sleep(1000);
	});

	it ("should have the select mode and selected image channel", function(){
		var selectMode = element(by.model('channel.mode'));

		expect(selectMode.getAttribute('value')).toEqual('image');
	});

	it ("should have the guid field", function(){
		var guid = element(by.model('channel.guid'));

		expect(guid.getAttribute('value')).toEqual(conf.channels.guid);
	});

	it ("should have the name field", function(){
		var name = element(by.model('channel.name'));

		expect(name.getAttribute('value')).toEqual(conf.channels.name);
	});

	it ("should have the description field", function(){
		var desc = element(by.model('channel.description'));

		expect(desc.getAttribute('value')).toEqual(conf.channels.desc);
	});

	it ("should have the logo section", function(){
		var logo = element(by.model('channel.logo'));
		var logobk = element(by.model('channel.logobk'));

		expect(logo.isPresent()).toBeTruthy();
		expect(logobk.isPresent()).toBeTruthy();    
	});

	it ("should have the picture section", function(){
		var picture = element(by.model('channel.picture'));
		var picturebk = element(by.model('picturebk'));

		expect(picture.isPresent()).toBeTruthy();
		expect(picturebk.isPresent()).toBeTruthy();
	});

	it('should have a form of rich text editor for the leaderboard description', function() {
	    //check prop.desc model
	    var descTextarea = element(by.model('channel.leaderboard_desc'));
	    expect(descTextarea.isPresent()).toBeTruthy();
	    //check rich text editor key
	    var descVal = element(by.id('leaderboard'));
	    expect(descVal.getText()).toEqual(conf.channels.leaderboard_desc);
	});

	it('should have a form of rich text editor for the photo page description', function() {
	    //check prop.desc model
	    var descTextarea = element(by.model('channel.photo_page.desc'));
	    expect(descTextarea.isPresent()).toBeTruthy();
	    //check rich text editor key
	    var descVal = element(by.id('photo-desc'));
	    expect(descVal.getText()).toEqual(conf.channels.photo_desc);
	});

	it ("should have the photo page image section", function(){
		var photo_page_image = element(by.model('channel.photo_page.image'));
		var photo_page_imagebk = element(by.model('channel.photo_page_imagebk'));

		expect(photo_page_image.isPresent()).toBeTruthy();
		expect(photo_page_imagebk.isPresent()).toBeTruthy();
	});

	it ("should have the text color field", function(){
		var textColor = element(by.model('channel.color.color_text'));

		expect(textColor.isPresent()).toBeTruthy();
		expect(textColor.getAttribute('value')).toEqual(conf.channels.color_t);
	});

	it ("should have the background color field", function(){
		var backgroundColor = element(by.model('channel.color.color1'));

		expect(backgroundColor.isPresent()).toBeTruthy();
		expect(backgroundColor.getAttribute('value')).toEqual(conf.channels.color_one);
	});

	it ("should have the highlight color field", function(){
		var highlightColor = element(by.model('channel.color.color2'));

		expect(highlightColor.isPresent()).toBeTruthy();
		expect(highlightColor.getAttribute('value')).toEqual(conf.channels.color_two);
	});

	it ("should have the header color field", function(){
		var  headerColor = element(by.model('channel.color.color3'));

		expect(headerColor.isPresent()).toBeTruthy();
		expect(headerColor.getAttribute('value')).toEqual(conf.channels.color_third);
	});

	it ('should have the controls button', function(){		
		//reset button
		var resetBtn = element.all(by.buttonText('Reset')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(resetBtn.isPresent()).toBeTruthy();

		//preview button
		var previewBtn = element.all(by.buttonText('Preview')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(previewBtn.isPresent()).toBeTruthy();

		//next button
		var nextBtn = element.all(by.buttonText('Next >>')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(nextBtn.isPresent()).toBeTruthy();
		nextBtn.click();
		browser.sleep(2000);
	});

	it ("should have the facebook name field", function(){
		expect(element(by.model('channel.social.facebook.name')).isPresent()).toBeTruthy();
	});

	it ("should have the twitter name field", function(){
		expect(element(by.model('channel.social.twitter.name')).isPresent()).toBeTruthy();
	});	

	it ("should have the facebook publishing radio field", function(){
		expect(element(by.model('channel.social.facebook.publishing')).isPresent()).toBeTruthy();
	});

	it ("should have the twitter publishing radio field", function(){
		expect(element(by.model('channel.social.twitter.publishing')).isPresent()).toBeTruthy();
	});

	it ("should have the tags field", function(){
		expect(element(by.model('channel.tags')).isPresent()).toBeTruthy();
	});

	it ("should have the voting field", function(){
		var voting = element(by.model('channel.points.voting'));

		expect(voting.isPresent()).toBeTruthy();
		expect(voting.getAttribute('value')).toEqual(conf.channels.points_voting);
	});

	it ("should have the completed poll field", function(){
		var points_completed = element(by.model('channel.points.completed'));

		expect(points_completed.isPresent()).toBeTruthy();
		expect(points_completed.getAttribute('value')).toEqual(conf.channels.points_completed);
	});

	it ("should have the share poll field", function(){
		var points_share_opinion = element(by.model('channel.points.share_opinion'));

		expect(points_share_opinion.isPresent()).toBeTruthy();
		expect(points_share_opinion.getAttribute('value')).toEqual(conf.channels.points_share_opinion);
	});

	it ("should have the poll selected field", function(){
		var points_suggest_opinion = element(by.model('channel.points.suggest_opinion'));

		expect(points_suggest_opinion.isPresent()).toBeTruthy();
		expect(points_suggest_opinion.getAttribute('value')).toEqual(conf.channels.points_suggest_opinion);
	});

	it ('should have the controls button', function(){		
		//reset button
		var resetBtn = element.all(by.buttonText('Reset')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(resetBtn.isPresent()).toBeTruthy();

		//reset button
		var resetPointsBtn = element.all(by.buttonText('Reset Points')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(resetPointsBtn.isPresent()).toBeTruthy();

		//preview button
		var previewBtn = element.all(by.buttonText('Preview')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(previewBtn.isPresent()).toBeTruthy();

		//back button
		expect(element(by.linkText('<< Back')).isPresent()).toBeTruthy();

		//next button
		var nextBtn = element.all(by.buttonText('Next >>')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(nextBtn.isPresent()).toBeTruthy();
		nextBtn.click();
		browser.waitForAngular();
	});
});
