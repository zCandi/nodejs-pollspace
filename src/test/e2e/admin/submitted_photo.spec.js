var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin submitted photos: ", function(){
	var mainMenu = element.all(by.xpath('//div[@ng-switch-when="true"]/ul/li'));
	var addBtn, photo, downloadBtn, delBtn;

	it('should have an admin menu', function() {      
		expect(mainMenu.count()).toBe(4);

		expect(mainMenu.get(0).element(by.tagName('a')).isPresent()).toBeTruthy();
		expect(mainMenu.get(1).element(by.tagName('a')).isPresent()).toBeTruthy();
		expect(mainMenu.get(2).element(by.tagName('a')).isPresent()).toBeTruthy();
		expect(mainMenu.get(3).element(by.tagName('a')).isPresent()).toBeTruthy(); 
	});

	it ("should expose the submitted photos's page when click on Submitted Photos tab", function(){
		mainMenu.get(3).click();
		browser.waitForAngular();
		browser.sleep(2000);

		expect(browser.getCurrentUrl()).toContain('/submitted/photos');
	});

	it ("should have the Add to Poll button", function(){
		addBtn = element(by.id('create'));
		expect(addBtn.isPresent()).toBeTruthy();
	});

	it ("should expose list submitted photos", function(){
		var list = element(by.repeater('(date, list) in submittedPhotos'));
		expect(list.isPresent()).toBeTruthy();
	});

	it ("should expose posted date", function(){
		var postedDate = element(by.repeater('(date, list) in submittedPhotos').row(0).column('date'));
		expect(postedDate.isPresent()).toBeTruthy();
	});

	it ("should expose list photos", function(){
		var listPhotos = element(by.repeater('(date, list) in submittedPhotos').row(0)).element(by.repeater('(key, photo) in list'));
		expect(listPhotos.isPresent()).toBeTruthy();
	});

	it ("should have a photo, download button and delete button in each photo section", function(){
		//selected photo
		photo = element(by.repeater('(date, list) in submittedPhotos').row(0)).element(by.repeater('(key, photo) in list').row(0));
		expect(photo.element(by.css('.contestent-img-round-container')).isPresent()).toBeTruthy();
		
		//download button
		downloadBtn = photo.element(by.linkText('Download'));
		expect(downloadBtn.isPresent()).toBeTruthy();

		//delete button
		delBtn = photo.element(by.linkText('Delete'));
		expect(delBtn.isPresent()).toBeTruthy();
	});

	it ("should download and delete successful", function(){
		downloadBtn.click();
		browser.sleep(2000);

		delBtn.click();
		browser.sleep(5000);
		//should delete successful
		expect(element(by.repeater('(date, list) in submittedPhotos').row(0)).element(by.repeater('(key, photo) in list').row(0))).not.toEqual(photo);
	});

	it ("should add to poll button works", function(){
		var listPhotos = element(by.repeater('(date, list) in submittedPhotos').row(0));

		//selected four photo
		listPhotos.element(by.repeater('(key, photo) in list').row(0)).click();
		//click add to poll button
		addBtn.click();
		browser.waitForAngular();
		browser.sleep(2000);
		expect(browser.getCurrentUrl()).toContain('/prop/create/single/image');
	});
});