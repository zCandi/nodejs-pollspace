var path = require('path');
var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin edit single poll: ", function(){
	var mainMenu = element.all(by.xpath('//div[@ng-switch-when="true"]/ul/li'));
	var saveBtn, publishBtn;

	beforeEach(function() {

	});

	afterEach(function() {

	});

	it ("should have the name's input field and contains the value just created", function(){
		var name = element(by.model('prop.name'));

		expect(name.isPresent()).toBeTruthy();
		expect(name.getAttribute('value')).toEqual(conf.prop_single.name);
	});

	it ("should have the details's textarea field and contains the value just created", function(){
		var details = element(by.model('prop.details'));

		expect(details.isPresent()).toBeTruthy();
		expect(details.getAttribute('value')).toEqual(conf.prop_single.details);
	});

	it ("should have the tags's input field", function(){
		var tags = element(by.model('prop.tags'));

		expect(tags.isPresent()).toBeTruthy();
	});

	it ("should expose the image's section", function(){
		var image = element(by.model('prop.image'));
		var imagebk = element(by.model('prop.imagebk'));

		expect(image.isPresent()).toBeTruthy();
		expect(imagebk.isPresent()).toBeTruthy();
	});

	it ("should have four options and contains the value just created", function(){
		var opt1 = element(by.model('prop.answers[0].name'));
		var opt2 = element(by.model('prop.answers[1].name'));
		var opt3 = element(by.model('prop.answers[2].name'));
		var opt4 = element(by.model('prop.answers[3].name'));

		expect(opt1.isPresent()).toBeTruthy();
		expect(opt2.isPresent()).toBeTruthy();
		expect(opt3.isPresent()).toBeTruthy();
		expect(opt4.isPresent()).toBeTruthy();

		expect(opt1.getAttribute('value')).toEqual(conf.prop_single.option1);
		expect(opt2.getAttribute('value')).toEqual(conf.prop_single.option2);
		expect(opt3.getAttribute('value')).toEqual(conf.prop_single.option3);
		expect(opt4.getAttribute('value')).toEqual(conf.prop_single.option4);
	});



	describe ("check the publish function", function(){
		var fbBtn, twBtn;

		it ("should inactive of the Published status", function(){
			expect(element(by.css('.prop-state-inactive')).getText()).toBe('Published');
		});

		it ("should have the publish button and publishing successfull", function(){
			publishBtn = element(by.buttonText('Publish'));

			expect(publishBtn.isPresent()).toBeTruthy();

			//publish the prop
			publishBtn.click();
			browser.sleep(2000);
		});
		
		it ("should expose the share on Facebook button and share on Twitter button", function(){
			fbBtn = element(by.buttonText('Share on facebook'));
			twBtn = element(by.buttonText('Share via twitter'));

			expect(fbBtn.isPresent()).toBeTruthy();
			expect(twBtn.isPresent()).toBeTruthy();
		});

		it ("should checked on the Published status", function(){

		});
	});

	it ("", function(){

	});
});