var path = require('path');
var conf = require('../conf/config.js');

describe ("Pollspace e2e test scripts - admin create image channel: ", function(){

	it ("should expose list channels", function(){
		expect(browser.getCurrentUrl()).toContain('/#!/admin/channel/list');

		expect(element(by.repeater('channel in channels')).isPresent()).toBeTruthy();
	});

	it ("should have create channel button", function(){
		var createChannelBtn = element(by.linkText('Create Channel'));

		expect(createChannelBtn.getTagName()).toBe('a');
		expect(createChannelBtn.getAttribute('href')).toContain('/#!/admin/channel/create');

		createChannelBtn.click();
		browser.waitForAngular();
	});

	it ("should expose the create channel form", function(){
		expect(browser.getCurrentUrl()).toContain('/#!/admin/channel/create');

		expect(element(by.id('channelcreateform')).isPresent()).toBeTruthy();
	});

	it ("should have the select mode and select image channel", function(){
		var selectMode = element(by.model('mode'));

		expect(selectMode.isPresent()).toBeTruthy();
		selectMode.element(by.cssContainingText('option', 'Image Channel')).click();
	});

	it ("should have the guid field", function(){
		var guid = element(by.model('guid'));

		expect(guid.isPresent()).toBeTruthy();
		guid.sendKeys(conf.channels.guid);
	});

	it ("should have the name field", function(){
		var name = element(by.model('name'));

		expect(name.isPresent()).toBeTruthy();
		name.sendKeys(conf.channels.name);
	});

	it ("should have the description field", function(){
		var desc = element(by.model('description'));

		expect(desc.isPresent()).toBeTruthy();
		desc.sendKeys(conf.channels.desc);
	});

	it ("should have the logo section using uploadcare to upload the logo and uploaded sucessfull", function(){
		var logo = element(by.model('logo'));

		expect(logo.isPresent()).toBeTruthy();
	    //check upload button
	    var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).first();
	    expect(btnUpload.isPresent()).toBeTruthy();
	    btnUpload.click();
	    browser.sleep(1000);

	    var absolutePath = path.resolve(__dirname, conf.channels.logo);
        //check uploadcare input file type
        var inputFile = element(by.xpath('//input[@type="file"]'));
        expect(inputFile.isPresent()).toBeTruthy();
        //upload an image
        inputFile.sendKeys(absolutePath);
        browser.sleep(15000);

        //check upload sucessfull
        var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
        expect(sucessBtn.isPresent()).toBeTruthy();
        sucessBtn.click();
        browser.waitForAngular();
	});
	
	it ("should have the picture section using uploadcare to upload the channel's picture and uploaded sucessfull", function(){
		var picture = element(by.model('picture'));

		expect(picture.isPresent()).toBeTruthy();
	    //check upload button
	    var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).get(1);
	    expect(btnUpload.isPresent()).toBeTruthy();
	    btnUpload.click();
	    browser.sleep(1000);

	    var absolutePath = path.resolve(__dirname, conf.channels.picture);
        //check uploadcare input file type
        var inputFile = element(by.xpath('//input[@type="file"]'));
        expect(inputFile.isPresent()).toBeTruthy();
        //upload an image
        inputFile.sendKeys(absolutePath);
        browser.sleep(15000);

        //check upload sucessfull
        var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
        expect(sucessBtn.isPresent()).toBeTruthy();
        sucessBtn.click();
        browser.waitForAngular();
	});

	it('should have a form of rich text editor for the leaderboard description', function() {
	    //check prop.desc model
	    var descTextarea = element(by.model('leaderboard_desc'));
	    expect(descTextarea.isPresent()).toBeTruthy();
	    //check rich text editor key
	    var descVal = element(by.id('leaderboard'));
	    expect(descVal.isPresent()).toBeTruthy();
	    //input value for prop.desc
	    descVal.sendKeys(conf.channels.leaderboard_desc);
	});

	it('should have a form of rich text editor for the photo page description', function() {
	    //check prop.desc model
	    var descTextarea = element(by.model('photo_desc'));
	    expect(descTextarea.isPresent()).toBeTruthy();
	    //check rich text editor key
	    var descVal = element(by.id('photo-desc'));
	    expect(descVal.isPresent()).toBeTruthy();
	    //input value for prop.desc
	    descVal.sendKeys(conf.channels.photo_desc);
	});

	it ("should have the photo page image section using uploadcare to upload the photo page's image and uploaded sucessfull", function(){
		var logo = element(by.model('photo_page_image'));

		expect(logo.isPresent()).toBeTruthy();
	    //check upload button
	    var btnUpload = element.all(by.css('.uploadcare-widget-button.uploadcare-widget-button-open')).get(2);
	    expect(btnUpload.isPresent()).toBeTruthy();
	    btnUpload.click();
	    browser.sleep(1000);

	    var absolutePath = path.resolve(__dirname, conf.channels.photo_page_image);
        //check uploadcare input file type
        var inputFile = element(by.xpath('//input[@type="file"]'));
        expect(inputFile.isPresent()).toBeTruthy();
        //upload an image
        inputFile.sendKeys(absolutePath);
        browser.sleep(15000);

        //check upload sucessfull
        var sucessBtn = element(by.css('.uploadcare-dialog-button-success.uploadcare-dialog-preview-done'));
        expect(sucessBtn.isPresent()).toBeTruthy();
        sucessBtn.click();
        browser.waitForAngular();
	});

	it ("should have the text color field", function(){
		var textColor = element(by.model('color_t'));

		expect(textColor.isPresent()).toBeTruthy();
		textColor.sendKeys(conf.channels.color_t);
	});

	it ("should have the background color field", function(){
		var backgroundColor = element(by.model('color_one'));

		expect(backgroundColor.isPresent()).toBeTruthy();
		backgroundColor.sendKeys(conf.channels.color_one);
	});

	it ("should have the highlight color field", function(){
		var highlightColor = element(by.model('color_two'));

		expect(highlightColor.isPresent()).toBeTruthy();
		highlightColor.sendKeys(conf.channels.color_two);
	});

	it ("should have the header color field", function(){
		var  headerColor = element(by.model('color_third'));

		expect(headerColor.isPresent()).toBeTruthy();
		headerColor.sendKeys(conf.channels.color_third);
	});

	it ('should have the controls button', function(){		
		//reset button
		var resetBtn = element.all(by.buttonText('Reset')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(resetBtn.isPresent()).toBeTruthy();

		//preview button
		var previewBtn = element.all(by.buttonText('Preview')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(previewBtn.isPresent()).toBeTruthy();

		//next button
		var nextBtn = element.all(by.buttonText('Next >>')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(nextBtn.isPresent()).toBeTruthy();
		nextBtn.click();
		browser.sleep(2000);
	});

	it ("should have the facebook name field", function(){
		expect(element(by.model('facebook_name')).isPresent()).toBeTruthy();
	});

	it ("should have the twitter name field", function(){
		expect(element(by.model('twitter_name')).isPresent()).toBeTruthy();
	});	

	it ("should have the facebook publishing radio field", function(){
		expect(element(by.model('facebook_publishing')).isPresent()).toBeTruthy();
	});

	it ("should have the twitter publishing radio field", function(){
		expect(element(by.model('twitter_publishing')).isPresent()).toBeTruthy();
	});

	it ("should have the tags field", function(){
		expect(element(by.model('tags')).isPresent()).toBeTruthy();
	});

	it ("should have the voting field", function(){
		var voting = element(by.model('points_voting'));

		expect(voting.isPresent()).toBeTruthy();
		voting.sendKeys(conf.channels.points_voting);
	});

	xit ("should have the correct answer field", function(){
		var points_correct = element(by.model('points_correct'));

		expect(points_correct.isPresent()).toBeTruthy();
		points_correct.sendKeys(conf.channels.points_correct);
	});

	xit ("should have the incorrect answer field", function(){
		var points_incorrect = element(by.model('points_incorrect'));

		expect(points_incorrect.isPresent()).toBeTruthy();
		points_incorrect.sendKeys(conf.channels.points_incorrect);
	});

	xit ("should have the streak reward field", function(){
		var points_streakbonus = element(by.model('points_streakbonus'));

		expect(points_streakbonus.isPresent()).toBeTruthy();
		points_streakbonus.sendKeys(conf.channels.points_streakbonus);
	});

	xit ("should have the share quiz field", function(){
		var points_share_classic = element(by.model('points_share_classic'));

		expect(points_share_classic.isPresent()).toBeTruthy();
		points_share_classic.sendKeys(conf.channels.points_share_classic);
	});

	xit ("should have the quiz selected field", function(){
		var points_suggest_classic = element(by.model('points_suggest_classic'));

		expect(points_suggest_classic.isPresent()).toBeTruthy();
		points_suggest_classic.sendKeys(conf.channels.points_suggest_classic);
	});

	it ("should have the completed poll field", function(){
		var points_completed = element(by.model('points_completed'));

		expect(points_completed.isPresent()).toBeTruthy();
		points_completed.sendKeys(conf.channels.points_completed);
	});

	it ("should have the share poll field", function(){
		var points_share_opinion = element(by.model('points_share_opinion'));

		expect(points_share_opinion.isPresent()).toBeTruthy();
		points_share_opinion.sendKeys(conf.channels.points_share_opinion);
	});

	it ("should have the poll selected field", function(){
		var points_suggest_opinion = element(by.model('points_suggest_opinion'));

		expect(points_suggest_opinion.isPresent()).toBeTruthy();
		points_suggest_opinion.sendKeys(conf.channels.points_suggest_opinion);
	});

	xit ("should have the leaderboard reset radio field", function(){
		expect(element(by.model('leaderboard_reset')).isPresent()).toBeTruthy();
	});

	it ('should have the controls button', function(){		
		//reset button
		var resetBtn = element.all(by.buttonText('Reset')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(resetBtn.isPresent()).toBeTruthy();

		//preview button
		var previewBtn = element.all(by.buttonText('Preview')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(previewBtn.isPresent()).toBeTruthy();

		//back button
		expect(element(by.linkText('<< Back')).isPresent()).toBeTruthy();

		//next button
		var nextBtn = element.all(by.buttonText('Next >>')).filter(function(el){
			return el.isDisplayed();
		}).first();
		expect(nextBtn.isPresent()).toBeTruthy();
		nextBtn.click();
		browser.waitForAngular();
	});

	it ("should create channel sucessfull", function(){
		expect(browser.getCurrentUrl()).toContain('prop/manage/single/image');
	});

	//logout
	xit('should have a menu link', function() {
		var menu = element.all(by.xpath("//a[@ng-click='openMenu()']")).filter(function(el){
	  		return el.isDisplayed();
		});

		menu.first().click();
		browser.sleep(1000);
		expect(menu.first().isPresent()).toBeTruthy();
	});

	xit("should have the logout link and logout successful", function() {
		var logout = element.all(by.xpath("//a[@href='/logout']")).filter(function(el){
			return el.isDisplayed();
		});

		expect(logout.first().isPresent()).toBeTruthy();
		logout.first().click();
		browser.waitForAngular();
		browser.sleep(2000);
		expect(browser.getCurrentUrl()).toContain('/#!/login');
	});
});