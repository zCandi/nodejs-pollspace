// spec.js
describe('Protractor e2e test scripts - logout function: ', function() {

  beforeEach(function() {
    // browser.wait(2000);  	
    // browser.get('http://bahdtest.pollspacedev.com');
  });

  afterEach(function() {   
    // browser.get('http://bahdtest.pollspacedev.com/logout');
  });

  it('should have a menu link', function() {
    var menu = element.all(by.xpath("//a[@ng-click='openMenu()']")).filter(function(el){
      return el.isDisplayed();
    });
    
    menu.first().click();
    browser.sleep(1000);
    expect(menu.first().isPresent()).toBeTruthy();
  });

  it("should have the logout link and logout successful", function() {
    var logout = element.all(by.xpath("//a[@href='/logout']")).filter(function(el){
      return el.isDisplayed();
    });
    expect(logout.first().isPresent()).toBeTruthy();
    logout.first().click();
    browser.waitForAngular();
    browser.sleep(2000);
    expect(browser.getCurrentUrl()).toContain('/#!/login');
  });

});