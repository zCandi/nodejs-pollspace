var conf = require('./conf/config.js');
// spec.js
describe("Protractor e2e test scripts - login function for user as super admin: ", function() {
  var url = conf.protocol + '://' + conf.domain;
  var loginBtn;

  beforeEach(function() {

  });

  afterEach(function() {   

  });

  it ("should expose a login page", function(){
    browser.get(url);
    browser.waitForAngular();

    expect(browser.getCurrentUrl()).toContain('/#!/login');
  });

  it ("should have a sign up form", function(){
    expect(element(by.model('firstname')).isPresent()).toBeTruthy();
    expect(element(by.model('lastname')).isPresent()).toBeTruthy();
    expect(element(by.model('company')).isPresent()).toBeTruthy();
    expect(element(by.model('email')).isPresent()).toBeTruthy();
    expect(element(by.id('create')).isPresent()).toBeTruthy();
  });

  it ("should have a login button", function(){
    var loginBtn = element(by.linkText('log in here.'));
    expect(loginBtn.getTagName()).toBe('a');
    expect(loginBtn.getAttribute('href')).toContain('/#!/admin_login');

    loginBtn.click();
    browser.waitForAngular();
  });

  it ("should expose admin login page and have a login form", function(){
    expect(browser.getCurrentUrl()).toContain('/#!/admin_login');

    var emailInput = element(by.id('email'));
    expect(emailInput.getTagName()).toBe('input');
    emailInput.sendKeys(conf.super_admin.email);

    var pwInput = element(by.id('password'));
    expect(pwInput.getTagName()).toBe('input');
    pwInput.sendKeys(conf.super_admin.pw);

    loginBtn = element(by.buttonText('Log In'));
    expect(loginBtn.getAttribute('type')).toBe('submit');
  });

  it ("should login successful with super admin user", function(){
    loginBtn.click();
    browser.waitForAngular();

    expect(browser.getCurrentUrl()).toContain('/#!/admin/channel/list');
  });

});