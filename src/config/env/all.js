'use strict';

var path = require('path');
var rootPath = path.normalize(__dirname + '/../..');

module.exports = {
	root: rootPath,
	//port: process.env.PORT || 3000,
    port: 3000,
	db: process.env.MONGOHQ_URL,
	templateEngine: 'swig',

	// The secret should be set to a non-guessable string that
	// is used to compute a session hash
	sessionSecret: 'pollspace',
	// The name of the MongoDB collection to store sessions in
	sessionCollection: 'sessions',

    //Global Points for Gloableaderboard
    global: {
        points: 5
    },

    email: {
        host: 'smtp.mandrillapp.com',
        port: 587,
        auth: {
            user: 'veroyv412@gmail.com',
            pass: 'GWwOVPVdNKxLpA-HzEYhXw'
        }
    }
};
