'use strict';

module.exports = {
    db: 'mongodb://testing:testing@c594.candidate.17.mongolayer.com:10594,c790.candidate.0.mongolayer.com:10790/testing?replicaSet=set-54ec9a1207c0e7feac000abd',
    app: {
        name: 'Pollspace'
    },
    cookie_domain: 'pollspacetest.com',
    port: 3000,
    guid: '',
    facebook: {
        clientID: 'APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: 'http://localhost:3000/auth/facebook/callback'
    },
    twitter: {
        clientID: 'CONSUMER_KEY',
        clientSecret: 'CONSUMER_SECRET',
        callbackURL: 'http://localhost:3000/auth/twitter/callback'
    },
    github: {
        clientID: 'APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: 'http://localhost:3000/auth/github/callback'
    },
    google: {
        clientID: 'APP_ID',
        clientSecret: 'APP_SECRET',
        callbackURL: 'http://localhost:3000/auth/google/callback'
    },
    linkedin: {
        clientID: 'API_KEY',
        clientSecret: 'SECRET_KEY',
        callbackURL: 'http://localhost:3000/auth/linkedin/callback'
    },
    viafo: {
        server: "https://vsg-live.appspot.com",
        return_url: "https://vsg-live.appspot.com/auth/1/UciApEiJ1Zfm5UHGMRbgVXpev0AZl5Jh",
        client_id: "sgWu3zA5kTJwCyQdxemaYzwJ6wl3GiPQ",
        client_secret: "LywWLuT4OclKVaLjLUMiLTwOnlz7Jbo2"
    }
};
