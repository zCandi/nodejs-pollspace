'use strict';

/**
 * Module dependencies.
 */
var express = require('express'),
    consolidate = require('consolidate'),
    mongoStore = require('connect-mongo')(express),
    flash = require('connect-flash'),
    helpers = require('view-helpers'),
    assetmanager = require('assetmanager'),
    uaparser = require('ua-parser'),
    http = require('http'),
    session = require('express-session'),
    //cookie_session = require('cookie-session'),
    sessionStore = new express.session.MemoryStore(),
    //FSStore = require('connect-fs2')(session),
    config = require('./config');

module.exports = function(app, passport, db) {
    var hostname = null;

    console.log('CONFIG');
    console.log(config.cookie_domain);
    console.log(config);

    app.set('showStackError', true);

    // Prettify HTML
    app.locals.pretty = true;
    // cache=memory or swig dies in NODE_ENV=production
    app.locals.cache = 'memory';

    // Should be placed before express.static
    // To ensure that all assets and data are compressed (utilize bandwidth)
    app.use(express.compress({
        filter: function(req, res) {
            return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
        },
        // Levels are specified in a range of 0 to 9, where-as 0 is
        // no compression and 9 is best compression, but slowest
        level: 9
    }));

    // Only use logger for development environment
    if (process.env.NODE_ENV === 'development') {
        app.use(express.logger('dev'));
    }



    // assign the template engine to .html files
    app.engine('html', consolidate[config.templateEngine]);

    // set .html as the default extension
    app.set('view engine', 'html');

    // Set views path, template engine and default layout
    app.set('views', config.root + '/app/views');

    // Enable jsonp
    app.enable('jsonp callback');

    app.configure(function() {
        // The cookieParser should be above session
        app.use(express.cookieParser());

        // Request body parsing middleware should be above methodOverride
        app.use(express.urlencoded());
        app.use(express.json());
        app.use(express.methodOverride());

        // Import your asset file
        var assets = require('./assets.json');
        assetmanager.init({
            js: assets.js,
            css: assets.css,
            debug: true,
            webroot: 'public'
        });
        // Add assets to local variables
        app.use(function (req, res, next) {
            hostname =  req.get('host');
            console.log('hostname');
            console.log(hostname);
            res.locals({
                assets: assetmanager.assets
            });
            next();
        });

        // Express/Mongo session storage
        app.use(

            //Store session in client browser and see if its faster
            /*cookie_session({
                name: 'pollspace',
                secret: config.sessionSecret,
                cookie: { domain: config.cookie_domain },
                domain: config.cookie_domain,
                store: sessionStore
            })*/

            session({
                secret: config.sessionSecret,
                cookie: { domain: config.cookie_domain },
                resave: true,
                domain: config.cookie_domain ,
                store: new mongoStore({
                    db: db.connection.db,
                    collection: config.sessionCollection
                })
            })

            /*
            For some reason Facebook doesnt like maxAge couse it does not store user into session.
            Need to investigate a little deeper.
            session({
                secret: config.sessionSecret,
                cookie: { domain: hostname, maxAge: 7 * 24 * 60 * 60 * 1000}, //1 week
                domain: hostname,
                store: new mongoStore({
                    db: db.connection.db,
                    collection: config.sessionCollection
                })
            })*/
        );

        // Dynamic helpers
        app.use(helpers(config.app.name));
        //app.use(require('prerender-node')); Do not put it back. I will ruined facebook and twitter scrapper.

        // Use passport session
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(express.logger('dev'))

        // Connect flash for flash messages
        app.use(flash());

        //app.use(express.vhost('*.localhost', this));

        // Routes should be at the last
        app.use(app.router);

        /*
         * Here's where I set the headers, make sure it's above `express.static()`.
         *
         * Note: You can safely ignore the rest of the code, (it's pretty much "stock").
         */
        app.use(function noCachePlease(req, res, next) {
            var useragent = uaparser.parseUA(req.get('User-Agent'));
            var os = uaparser.parseOS(req.get('User-Agent'));
            var device = uaparser.parseDevice(req.get('User-Agent'));

            if (!req.session.ua)
                req.session.ua = uaparser.parse(req.get('User-Agent'));

            // console.log('UA-parser');
            // console.log(useragent.family.replace(/\s+/g, "-"));
            // console.log(os.family);
            // console.log(device.family);

            //console.log('SESSION - EXPRESS');
            //console.log(req.session);

            // console.log('REQ');
            // console.log(req.originalUrl);
            // console.log(req.get('host'));

            //if ( useragent && os && useragent.family == 'Safari' && os.family == 'iOS' ){
            //    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
            //    res.header("Pragma", "no-cache");
            //    re
            //req.headers['if-none-ms.header("Expires", 0);
            //}atch'] = 'no-match-for-this';

            //res.header("Cache-Control", "no-cache, no-store, must-revalidate");
            //res.header("Pragma", "no-cache");
            //res.header("Expires", 0);
            //req.headers['if-none-match'] = 'no-match-for-this';
            next();
        });

        //app.disable('etag');
        app.use(express.errorHandler());

        app.configure('development', function(){
            app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
        });

        // app.configure('production', function(){
        //   app.use(express.errorHandler()); 
        // });

        /*
         I can use this to put any variable in locals and then this can be seen in swig
             app.use(function(req, res, next) {
             res.locals.ua = req.get('User-Agent');
             next();
         });*/


         app.use(function(req, res, next) {
             next();
         });

        // Setting the fav icon and static folder
        app.use(express.favicon());
        app.use(express.static(config.root + '/public'));

        // Assume "not found" in the error msgs is a 404. this is somewhat
        // silly, but valid, you can do whatever you like, set properties,
        // use instanceof etc.
        app.use(function(err, req, res, next) {
            // Treat as 404
            if (~err.message.indexOf('not found')) return next();

            // Log it
            console.error(err.stack);

            // Error page
            res.status(500).render('500', {
                error: err.stack
            });
        });

        // Assume 404 since no middleware responded
        app.use(function(req, res) {
            res.status(404).render('404', {
                url: req.originalUrl,
                error: 'Not found'
            });
        });

    });

    return app;
};
