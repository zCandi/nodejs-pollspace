'use strict';

var mongoose = require('mongoose'),
    LocalStrategy = require('passport-local').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    GitHubStrategy = require('passport-github').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    LinkedinStrategy = require('passport-linkedin').Strategy,
    User = mongoose.model('User'),
    request = require('request'),
    RandExp = require('randexp'),
    config = require('./config');

module.exports = function(passport) {

    // Serialize the user id to push into the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // Deserialize the user object based on a pre-serialized token
    // which is the user id
    passport.deserializeUser(function(id, done) {
        User.findOne({
            _id: id
        }, '-salt -hashed_password', function(err, user) {
            done(err, user);
        });
    });

    // Use local strategy
    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function(email, password, done) {
            User.findOne({email: email})
                .populate('subscriptions')
                .populate('props')
                .exec(function(err, user) {
                    if (err) {
                        return done(err);
                    }
                    if (!user) {
                        return done(null, false, {
                            message: 'User does not exists.'
                        });
                    }
                    if (!user.authenticate(password)) {
                        return done(null, false, {
                            message: 'Your email and or password is invalid.'
                        });
                    }
                    return done(null, user);
                });
        }
    ));

    // Use twitter strategy
    passport.use(new TwitterStrategy({
            consumerKey: config.twitter.clientKey,
            consumerSecret: config.twitter.clientSecret,
            callbackURL: config.twitter.callbackURL
        },
        function(token, tokenSecret, profile, done) {
            User.findOne({
                'twitter.id_str': profile.id
            }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    user = new User({
                        name: profile.displayName,
                        username: profile.username,
                        provider: 'twitter',
                        twitter: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user);
                }
            });
        }
    ));

    // Use facebook strategy
    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL,
            profileFields   : ['id', 'name', 'email'],
            passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, accessToken, refreshToken, profile, done) {

            console.log('accessToken', accessToken);
            console.log('profile', profile);

            // get channel guid from req.session.redirectTo parameter
            var guid = req.session.redirectTo.split('.')[0].split('//')[1];

            User.findOne({$or:[ {'email': profile._json.email}, {'facebook_data.email': profile._json.email} ]})
                        .populate('subscriptions')
                        .populate('props')
                        .exec(function(err, user) {
                            if (err) {
                                return done(err);
                            }

                            var url = 'https://graph.facebook.com/' + profile.id + '?access_token=' + accessToken;
                            url += '&fields=id,name,about,birthday,context,currency,email,first_name,gender,hometown,last_name,link,locale,location,political,quotes,relationship_status,religion,timezone,updated_time,work,education';

                            console.log('url', url);

                            request(url, function (error, response, body) {

                                if (error) {
                                    return done(error);
                                }

                                var fb_data = JSON.parse(body);
                                var facebook = {};
                                facebook.id = fb_data.id;
                                facebook.name = fb_data.name;
                                facebook.email = fb_data.email;
                                facebook.first_name = fb_data.first_name;
                                facebook.last_name = fb_data.last_name;
                                facebook.gender = fb_data.gender;
                                facebook.location = fb_data.location ? fb_data.location.name : (fb_data.hometown ? fb_data.hometown.name : null);
                                facebook.locale = fb_data.locale;
                                facebook.timezone = fb_data.timezone;
                                facebook.currency = fb_data.currency.user_currency;

                                if (fb_data.relationship_status)
                                    facebook.relationship_status = fb_data.relationship_status;

                                facebook.hometown = fb_data.hometown ? fb_data.hometown.name : null;                                

                                if ( fb_data.birthday ){
                                    facebook.birthday = fb_data.birthday;

                                    var dob = new Date(fb_data.birthday);
                                    var ageDifMs = new Date().getTime() - dob.getTime();
                                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                                    facebook.age = Math.abs(ageDate.getUTCFullYear() - 1970);
                                }

                                if (fb_data.education) 
                                    facebook.education = fb_data.education;

                                if (fb_data.work)
                                    facebook.work = fb_data.work;

                                if (fb_data.quotes)
                                    facebook.quotes = fb_data.quotes;

                                if (fb_data.political)
                                    facebook.political = fb_data.political.replace(/\(\)$/g, '');

                                if (fb_data.religion)
                                    facebook.religion = fb_data.religion.replace(/\(\)$/g, '');
                                // facebook.bio = body.bio;
                                if (fb_data.about)
                                    facebook.about = fb_data.about;

                                facebook.friends = fb_data.context.mutual_friends ? fb_data.context.mutual_friends.summary.total_count : 0;
                                facebook.likes = fb_data.context.mutual_likes ? fb_data.context.mutual_likes.summary.total_count : 0;

                                console.log('facebook', facebook);

                                if (!user) {
                                    user = new User();
                                    user.email = fb_data.email;
                                    user.firstname = fb_data.first_name;
                                    user.lastname =  fb_data.last_name;
                                    user.facebook_data = facebook;
                                    user.provider = 'facebook';
                                    user.password = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                                    user.is_admin = false;
                                    user.channel_logged_total = [{channel: guid, logged: 1}];              
                                } else {
                                    user.facebook_data = facebook;
                                    if (!user.channel_logged_total) {
                                        user.channel_logged_total = [{ channel: guid, logged: 1}];
                                    } else {
                                        if (user.channel_logged_total.length > 0) {
                                            var assign = false;
                                            user.channel_logged_total.forEach(function(logged, key){
                                                if (logged.channel == guid) {
                                                    assign = true;
                                                    logged.logged++;
                                                }
                                            });

                                            if (!assign) {
                                                user.channel_logged_total.push({ channel: guid, logged: 1});
                                            }
                                        } else {
                                            user.channel_logged_total.push({ channel: guid, logged: 1});
                                        }
                                    }
                                }

                                console.log('user', user);

                                user.save(function(err) {
                                    if (err) console.log(err);
                                    return done(err, user);
                                }); 
                            });
                        }); 
        }
    ));

    // Use github strategy
    passport.use(new GitHubStrategy({
            clientID: config.github.clientID,
            clientSecret: config.github.clientSecret,
            callbackURL: config.github.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'github.id': profile.id
            }, function(err, user) {
                if (!user) {
                    user = new User({
                        name: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.username,
                        provider: 'github',
                        github: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user);
                }
            });
        }
    ));

    // Use google strategy
    passport.use(new GoogleStrategy({
            clientID: config.google.clientID,
            clientSecret: config.google.clientSecret,
            callbackURL: config.google.callbackURL,
            passReqToCallback : true
        },
        function(req, accessToken, refreshToken, profile, done) {

            // get channel guid from req.session.redirectTo parameter
            var guid = req.session.redirectTo.split('.')[0].split('//')[1];

            User.findOne({$or:[ {'email': profile._json.email}, {'google.id': profile.id} ]})
                        .populate('subscriptions')
                        .populate('props')
                        .exec(function(err, user) {
                            if (err) {
                                return done(err);
                            }

                            var google = {};
                            google.id = profile._json.id;
                            google.name = profile._json.name;
                            google.email = profile._json.email;
                            google.first_name = profile._json.given_name;
                            google.last_name = profile._json.family_name;

                            if (profile._json.gender)
                                google.gender = profile._json.gender;

                            if (profile._json.link)
                                google.link = profile._json.link;

                            google.locale = profile._json.locale;
                            google.picture = profile._json.picture;

                            if (!user) {
                                user = new User();
                                user.email = profile._json.email;
                                user.firstname = profile._json.given_name;
                                user.lastname =  profile._json.family_name;
                                user.google = google;
                                user.provider = 'google';
                                user.password = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                                user.is_admin = false;
                                user.channel_logged_total = [{channel: guid, logged: 1}];               
                            } else {
                                user.google = google;
                                if (!user.channel_logged_total) {
                                    user.channel_logged_total = [{ channel: guid, logged: 1}];
                                } else {
                                    if (user.channel_logged_total.length > 0) {
                                        var assign = false;
                                        user.channel_logged_total.forEach(function(logged, key){
                                            if (logged.channel == guid) {
                                                assign = true;
                                                logged.logged++;
                                            }
                                        });

                                        if (!assign) {
                                            user.channel_logged_total.push({ channel: guid, logged: 1});
                                        }
                                    } else {
                                        user.channel_logged_total.push({ channel: guid, logged: 1});
                                    }
                                }
                            }

                            user.save(function(err) {
                                if (err) console.log(err);
                                return done(err, user);
                            }); 
                        }); 
        }
    ));

    // use linkedin strategy
    passport.use(new LinkedinStrategy({
            consumerKey: config.linkedin.clientID,
            consumerSecret: config.linkedin.clientSecret,
            callbackURL: config.linkedin.callbackURL,
            profileFields: ['id', 'first-name', 'last-name', 'email-address']
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'linkedin.id': profile.id
            }, function(err, user) {
                if (!user) {
                    user = new User({
                        name: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.emails[0].value,
                        provider: 'linkedin'
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user);
                }
            });
        }
    ));
};