'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    RandExp = require('randexp'),
    deepPopulate = require('mongoose-deep-populate'),
    _ = require('lodash'),
    Schema = mongoose.Schema;

var ChannelSchema = new Schema({
    guid: {
        type: String,
        index: { unique: true }
    },
    type: { type: String, enum: ['single', 'multiple'], default: 'single'},
    mode: { type: String, enum: ['standard', 'image'], default: 'standard'},
    name: {
        type: String,
        trim: true
    },
    description: {
        type: String,
        trim: true
    },
    logo: {
        type: String
    },
    picture: {
        type: String
    },
    color: {
        color_text: { type: String },
        color1: { type: String },
        color2: { type: String },
        color3: { type: String },
        palette: { type: Schema.Types.Mixed }
    },
    default_page: { type: String, default: 'poll/list' },
    reset_rolling: { type: Boolean },
    social: {
        facebook: {
            name: { type: String },
            publishing: { type: Boolean }
        },
        twitter: {
            name: { type: String },
            publishing: { type: Boolean }
        }
    },
    voted_products:[{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        prop: {
            type: Schema.ObjectId,
            ref: 'Prop'
        },
        answers:[{ type: String }],
        voted_date: { type: Date }
    }],
    subscribers: [{
        type: Schema.ObjectId,
        ref: 'User'
    }],
    admins: [{
        type: Schema.ObjectId,
        ref: 'User'
    }],
    tags: [{
        type: String
    }],
    submittedPhotos: [{
        type: Schema.ObjectId,
        ref: 'Photo'
    }],
    points: {
        voting: { type: Number, default: 0 },
        correct: { type: Number, default: 0 },
        streakbonus: { type: Number, default: 0 },
        incorrect: { type: Number, default: 0 },
        completed: { type: Number, default: 0 },
        share_classic: { type: Number, default: 0 },
        suggest_classic: { type: Number, default: 0 },
        share_opinion: { type: Number, default: 0 },
        suggest_opinion: { type: Number, default: 0 }
	// quiz_picture: {type: String},
	// poll_picture: {type: String}
    },
    signin_page_desc: { type: String },
    leaderboard_desc: { type: String },
    photo_page: {
        desc: { type: String },
        image: { type: String }
    },
    deleted: { type: Boolean, default: false },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        deleted: { type: Date, default: 0 }
    }
});


ChannelSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

/**
 * Pre-save hook
 */
ChannelSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    if ( _.isEmpty(this.guid)  ){
        this.guid = new RandExp(/([A-Za-z0-9]){6}/i).gen();
    }

    next();
});

/**
 * Methods
 */
ChannelSchema.methods = {
    /* isSubscribed */
    isSubscribed: function(user_id){
        return !!~this.subscribers.indexOf(user_id);
    }

};

ChannelSchema.plugin(deepPopulate);
mongoose.model('Channel', ChannelSchema);
