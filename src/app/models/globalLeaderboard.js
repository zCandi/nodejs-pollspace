'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GlobalLeaderboardSchema = new Schema({
    current_points: {
        type: Number,
        default: 0
    },
    points: {
        type: Number,
        default: 0
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        unique: true
    },
    actions: {
        voting: { type: Number, default: 0 },  // number of times a user has voted
        correct: { type: Number, default: 0 },
        streakbonus: { type: Number, default: 0 },
        incorrect: { type: Number, default: 0 },
        completed: { type: Number, default: 0 },
        share_classic: { type: Number, default: 0 },
        suggest_classic: { type: Number, default: 0 },
        share_opinion: { type: Number, default: 0 },
        suggest_opinion: { type: Number, default: 0 }
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now }
    }
});

mongoose.model('GlobalLeaderboard', GlobalLeaderboardSchema);