'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Schema = mongoose.Schema;

var ShareSchema = new Schema({
    guid: {
        type: String,
        index: { unique: true }
    },
    prop: {
        type: Schema.ObjectId,
        ref: 'Prop'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    count: { type: Number, default: 0 },
    counts: { 
        fb: { type: Number, default: 0 },
        tw: { type: Number, default: 0 }
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now }
    },
    histories: [{
        type: { type: String, enum: ['fb', 'tw'], default: 'fb'},
        created: { type: Date, default: Date.now }
    }]
});

/**
 * Pre-save hook
 */
ShareSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    this.guid = new RandExp(/([A-Za-z0-9]){6}/i).gen();
    next();
});


ShareSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

mongoose.model('Share', ShareSchema);