'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ImageSchema = new Schema({
    X: {
        type: Number
    },
    Y: {
        type: Number
    },
    W: {
        type: Number
    },
    H: {
        type: Number
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now }
    }
});

mongoose.model('Image', ImageSchema);