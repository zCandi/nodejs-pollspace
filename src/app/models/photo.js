'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    RandExp = require('randexp'),
    Schema = mongoose.Schema;

var PhotoSchema = new Schema({
    guid: {
        type: String,
        index: { unique: true }
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    channel: {
        type: Schema.ObjectId,
        ref: 'Channel'
    },
    instagram: { value: { type: String } },
    image: {
        type: String
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        deleted: { type: Date, default: 0 }
    },
    tags: [{
        type: String
    }],
   deleted: {type: Boolean, default: false}
});

PhotoSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    this.guid = new RandExp(/([A-Za-z0-9]){6}/i).gen();
    next();
});

mongoose.model('Photo', PhotoSchema);
