'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    RandExp = require('randexp'),
    deepPopulate = require('mongoose-deep-populate'),
    Schema = mongoose.Schema;

var PropSetSchema = new Schema({
    guid: {
        type: String,
        index: { unique: true }
    },
    title: {
        type: String
    },
    channel: {
        type: Schema.ObjectId,
        ref: 'Channel'
    },
    props: [{
        type: Schema.ObjectId,
        ref: 'Prop'
    }],
    image: {
        type: String
    },
    thankyoupage_text: {
        type: String
    },
    type: { type: String, enum: ['classic', 'quiz','opinion']},
    deleted: { type: Boolean, default: false },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        published: { type: Date, default: 0 },
        deleted: { type: Date, default: 0 }
    }
});

PropSetSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    this.guid = new RandExp(/([A-Za-z0-9]){6}/i).gen();
    next();
});

PropSetSchema.plugin(deepPopulate);
mongoose.model('PropSet', PropSetSchema);