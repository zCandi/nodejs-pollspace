'use strict';

// User routes use users controller
var users = require('../controllers/users');
var propset = require('../controllers/propsets');
var prop = require('../controllers/props');
var authorization = require('./middlewares/authorization');
var utils = require(__dirname + '/../lib/utils');


var hasAdminPrivileges = function(req, res, next) {
    if (req.user && !req.user.is_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

var isFPAdmin = function(req, res, next) {
    if (req.user && !req.user.fp_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
};


module.exports = function(app, passport) {

    //ABOUT
    app.get('/about', users.about);

    //Embedded
    app.get('/embedded/login', users.embeddedLogin); //@TODO: Move to users routing. It will be used in single questions too.

    /* REGULAR CALLS */
    app.post('/api/user/forgotpassword', users.forgotpassword);
    app.get('/resetpassword', users.getresetpassword);
    app.post('/api/user/resetpassword', users.postresetpassword);

    /* Admins */
    app.get('/api/user/admin/list', authorization.requiresLogin, users.adminList);

    /* API */
    app.get('/api/user/:user_id', users.show);
    app.get('/api/user/:user_id/channel/list', authorization.requiresLogin, users.subscribeChannelList);
    app.get('/api/user/:user_id/channel/suggested/list',authorization.requiresLogin,  users.suggestedChannelList);
    app.get('/api/user/:user_id/props', authorization.requiresLogin, users.votedPropsList);
    app.get('/api/user/:user_id/ranking', authorization.requiresLogin, users.ranking);

    /* FP API calls */
    app.get('/api/fp/user/list', authorization.requiresLogin, hasAdminPrivileges, users.all);
    app.get('/api/fp/user/autogenerate', authorization.requiresLogin, users.autogenerate);
    app.get('/api/fp/user/:user_id', authorization.requiresLogin, users.show);
    app.post('/api/fp/user', authorization.requiresLogin, users.createNoLogin);
    app.post('/api/fp/user/:user_id', authorization.requiresLogin, users.update);

    /* Search */
    app.post('/api/fp/search/demographic', users.searchDemograpchic);
    app.get('/api/fp/search/demographic', users.searchDemograpchicByQuery);

    app.post('/api/user', users.create);
    app.post('/api/user/:user_id', users.update);

    /* REGULAR CALLS */
    //app.get('/login', users.login);
    app.get('/logout', users.signout);
    app.get('/user/me', users.me);
    app.get('/my/channels', users.mychannels);
    app.get('/my/rankings', users.myrankings);
    app.get('/my/props', users.myprops);
    app.get('/my/account', users.myaccount);
    app.get('/admin/my/channels', users.adminchannelList)

    /* FACEBOOK */
    app.get('/fb_login/embedded/callback', users.fbEmbeddedCallback);
    app.get('/fb_login/:access_token', users.fbLogin);
    app.get('/viafo_login', users.viafoLogin);
    app.post('/viafo_login', users.viafoLogin);

    /* TWITTER */
    app.get('/twitter_login/callback', users.twitterCallback);

    // Setting up the userId param
    app.param('user_id', users.user);
    app.param('channel_id', users.channel);

    // Setting the local strategy route
    /*app.post('/user/admin/session', passport.authenticate('local', {
        failureRedirect: '/',
        failureFlash: true
    }), users.session);*/

    /*app.post('/user/session', passport.authenticate('local', {
        failureRedirect: '/',
        failureFlash: true
    }), users.session);*/

    app.post('/user/admin/session', function(req, res, next) {
        passport.authenticate('local', function(err, user, info) {
            if (err) {
                utils.setErrorMessage(req, err);
            };

            if (!user) {
                utils.setErrorMessage(req, info.message);
                res.redirect("/#!/admin_login");
            } else {
                req.logIn(user, function(err) {
                    if (err) { return next(err); }
                    users.session(req, res);
                });
            }
        })(req, res, next);
    });

    app.post('/user/session', function(req, res, next) {
        passport.authenticate('local', function(err, user, info) {
            if (err) {
                utils.setErrorMessage(req, err);
            };

            if (!user) {
                utils.setErrorMessage(req, info.message);
                res.redirect("/#!/login");
            } else {
                req.logIn(user, function(err) {
                    if (err) { return next(err); }
                    users.session(req, res);
                });
            }
        })(req, res, next);
    });

    app.get('/embedded/user/session', function(req, res, next) {
        var question_type = req.body.question_type;
        passport.authenticate('local', function(err, user, info) {
            if (err) {
                res.jsonp({ type: 'error', message: info.message });
            };

            if (!user) {
                res.jsonp({ type: 'error', message: info.message });
            } else {
                req.logIn(user, function(err) {
                    if (err) {
                        res.jsonp({ type: 'error', message: err });
                    }

                    res.jsonp({ type: 'success' });
                });
            }
        })(req, res, next);
    });

    /*
    // Setting the facebook oauth routes
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: ['email', 'user_about_me'],
        failureRedirect: '/signin'
    }), users.signin);

    app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/signin'
    }), users.authCallback);

    // Setting the github oauth routes
    app.get('/auth/github', passport.authenticate('github', {
        failureRedirect: '/signin'
    }), users.signin);

    app.get('/auth/github/callback', passport.authenticate('github', {
        failureRedirect: '/signin'
    }), users.authCallback);

    // Setting the twitter oauth routes
    app.get('/auth/twitter', passport.authenticate('twitter', {
        failureRedirect: '/signin'
    }), users.signin);

    app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        failureRedirect: '/signin'
    }), users.authCallback);

    // Setting the google oauth routes
    app.get('/auth/google', passport.authenticate('google', {
        failureRedirect: '/signin',
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ]
    }), users.signin);

    app.get('/auth/google/callback', passport.authenticate('google', {
        failureRedirect: '/signin'
    }), users.authCallback);

    // Setting the linkedin oauth routes
    app.get('/auth/linkedin', passport.authenticate('linkedin', {
        failureRedirect: '/signin',
        scope: [ 'r_emailaddress' ]
    }), users.signin);

    app.get('/auth/linkedin/callback', passport.authenticate('linkedin', {
        failureRedirect: '/siginin'
    }), users.authCallback);
*/

    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: ['email', 'user_hometown', 'user_location', 'user_relationships', 'user_relationship_details', 'user_religion_politics', 'user_birthday', 'user_education_history', 'user_work_history']
        // failureRedirect: '/signin'
    }));

    app.get('/auth/facebook/callback', function(req, res, next) {
        passport.authenticate('facebook', function(err, user, info) {
            if (err) {
                utils.setErrorMessage(req, err);
                res.redirect(req.session.redirectTo + '/#!/login');
            };

            if (!user) {
                utils.setErrorMessage(req, info);
                res.redirect(req.session.redirectTo + '/#!/login');
            } else {
                req.logIn(user, function(err) {
                    if (err) {
                        utils.setErrorMessage(req, err);
                        res.redirect(req.session.redirectTo + '/#!/login');
                    }  
                    res.redirect(req.session.redirectTo);                 
                });
            }
            
        })(req, res, next);
    });

    app.get('/auth/google', passport.authenticate('google', {
        scope: ['https://www.googleapis.com/auth/userinfo.profile', 
                'https://www.googleapis.com/auth/userinfo.email', 
                'https://www.googleapis.com/auth/plus.login', 
                'https://www.googleapis.com/auth/plus.me']
        // failureRedirect: '/signin'
    }));

    app.get('/auth/google/callback', function(req, res, next) {
        passport.authenticate('google', function(err, user, info) {
            if (err) {
                utils.setErrorMessage(req, err);
                res.redirect(req.session.redirectTo + '/#!/login');
            };

            if (!user) {
                utils.setErrorMessage(req, info);
                res.redirect(req.session.redirectTo + '/#!/login');
            } else {
                req.logIn(user, function(err) {
                    if (err) {
                        utils.setErrorMessage(req, err);
                        res.redirect(req.session.redirectTo + '/#!/login');
                    }  
                    res.redirect(req.session.redirectTo);                 
                });
            }
            
        })(req, res, next);
    });
};
