'use strict';

// Articles routes use articles controller
var groups = require('../controllers/groups');
var authorization = require('./middlewares/authorization');

var hasAdminPrivileges = function(req, res, next) {
    if (req.user && !req.user.is_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(app) {

    /* ADMIN SECTION */
    app.get('/api/admin/group/list', authorization.requiresLogin, hasAdminPrivileges, groups.all);
    app.get('/api/admin/group/list/:type', authorization.requiresLogin, hasAdminPrivileges, groups.allbytype);
    app.post('/api/admin/group', authorization.requiresLogin, hasAdminPrivileges, groups.create);
    app.post('/api/admin/group/:group_id', authorization.requiresLogin, hasAdminPrivileges, groups.update);
    app.get('/api/admin/group/:group_id', authorization.requiresLogin, hasAdminPrivileges, groups.show);
    app.get('/api/admin/group/:group_id/deactivate', authorization.requiresLogin, hasAdminPrivileges, groups.deactivate);

    app.param('group_id', groups.group);
    app.param('type', groups.bytype);

};