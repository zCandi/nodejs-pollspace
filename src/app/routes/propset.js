'use strict';

var propsets = require('../controllers/propsets');
var authorization = require('./middlewares/authorization');

var hasAdminPrivileges = function(req, res, next) {
    if (req.user && !req.user.is_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
}

module.exports = function(app) {

    //Embedded
    app.get('/q/:propsetguid/poll.js', propsets.renderMultipleEmbedPoll);
    app.get('/q/:propsetguid/embedded-test', propsets.embeddedTest);
    app.get('/q/:propsetguid/embedded-list', propsets.renderEmbeddedList);
    app.get('/q/:propsetguid/embedded-results', propsets.renderEmbeddedResults);

    /* APIs */
    app.get('/q/:propsetguid/view',  propsets.viewPropset);

    app.post('/q/:propsetguid/results', propsets.postResults);
    app.get('/q/:propsetguid/results',  propsets.getResults); //AFTER REDIRECT of being logged in
    app.get('/q/:propsetguid', propsets.all);

    app.get('/api/admin/propset/:propsetid', authorization.requiresLogin, hasAdminPrivileges, propsets.showpropset);
    app.post('/api/admin/propset/:propsetid', authorization.requiresLogin, hasAdminPrivileges, propsets.updatepropset);
    app.get('/api/admin/propset/:propsetid/deactivate',  authorization.requiresLogin, hasAdminPrivileges, propsets.deactivate);

    app.get('/api/propset/:propsetguid', authorization.requiresLogin, propsets.showpropset);

    app.param('propsetguid', propsets.getPropsetguid);
    app.param('propsetid', propsets.getPropset);
};