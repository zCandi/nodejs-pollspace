'use strict';

// Props routes use articles controller
var shares = require('../controllers/shares'),
    _ = require('lodash');

module.exports = function(app) {

    /* APIs */
    app.get('/s/:sguid', shares.share);

    app.param('sguid', shares.getShare);
};