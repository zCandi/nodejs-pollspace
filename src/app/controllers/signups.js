'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    async = require('async'),
    Signup = mongoose.model('Signup'),
    _ = require('lodash');

/**
 * Find Channel by id
 */
exports.getSignups = function(req, res) {
    Signup.find({})
        .exec(function(err, signups) {
            if (err) return next(err);
            if (!signups) return next(new Error('Failed to load signups'));
            res.jsonp(signups);
        });
};

exports.postSignups = function(req, res) {
    var form_data = req.body;
    var signup = new Signup(req.body);
    signup.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                signup: signup
            });
        } else {
            res.jsonp(signup);
        }
    });
};