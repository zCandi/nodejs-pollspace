'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Share    = mongoose.model('Share'),
    Channel    = mongoose.model('Channel'),
    Prop    = mongoose.model('Prop'),
    User    = mongoose.model('User'),
    GlobalLeaderboard = mongoose.model('GlobalLeaderboard'),
    ChannelLeaderboard = mongoose.model('ChannelLeaderboard'),
    config = require(__dirname + '/../../config/config'),
    async = require('async'),
    _ = require('lodash');

exports.getShare = function(req, res, next, id) {
    Share.findOne({ guid: id }).populate('prop').populate('user').exec(function(err, share) {
        if (err) return next(err);
        if (!share) return next(new Error('Failed to load share ' + id));
        req.share = share;

        next();
    });
};

exports.share = function(req, res, next) {
    var type = req.query.type;
    var share = req.share;
    var today = new Date();

    share.timestamps.update = today.getTime();

    var newshare = true;
    share.histories.forEach(function(his, key){
        var lastshare = new Date(his.created);
        if (his.type == type && lastshare.getDate() == today.getDate() && lastshare.getMonth() == today.getMonth() && lastshare.getUTCFullYear() == today.getUTCFullYear()) {
            newshare = false;
        }
    });

    if (newshare) {
        console.log('share vao day');
        share.count =  share.count + 1;

        if (type == 'fb') {
            if (share.counts.fb)
                share.counts.fb += 1;
            else
                share.counts.fb = 1;

            share.histories.push({type: 'fb'});
        }

        if (type == 'tw'){
            if (share.counts.tw)
                share.counts.tw += 1;
            else
                share.counts.tw = 1;

            share.histories.push({type: 'tw'});
        }
    } 

    share.save();

    var user = share.user;
    var prop = share.prop;
    var channel = null;

    async.series([
        function(callback){
            Prop.findOne({_id: prop._id} , function(error, _prop){
                if (newshare) {
                    if ( _prop.counts && _prop.counts.share ){
                        _prop.counts.share = _prop.counts.share + 1;

                    } else {
                        _prop.counts.share = 1;
                    }

                    if (type == 'fb'){
                        if (_prop.counts.share_fb)
                            _prop.counts.share_fb += 1;
                        else
                            _prop.counts.share_fb = 1;
                    }

                    if (type == 'tw'){
                        if (_prop.counts.share_tw)
                            _prop.counts.share_tw += 1;
                        else
                            _prop.counts.share_tw = 1;
                    }

                    _prop.save();
                }
                callback(null);
            });
        },
        function(callback){
            Prop.find({ channel: prop.channel }).exec(function(err, props){
                var sum = 0;
                props.forEach(function(prop, propkey){
                    sum = sum + prop.counts.share;
                });

                Prop.findOne({_id: prop._id} , function(error, _prop){
                    _prop.percentage.share = (_prop.counts.share * 100) / sum;
                    _prop.save();
                });

                callback(null);
            });
        },
        function(callback){ //need to create a new Share row in shares documents
            console.log('channel find before on');
            console.log(prop.channel);
            Channel.findOne({_id: prop.channel} , function(error, c){
                channel = c;
                callback(null);
            });
        },
        function(callback){ //increment user's global points based on global defaults >  GlobalLeaderboardSchema.points+5
            GlobalLeaderboard.findOne({user: user} , function(error, globalLeaderboard){
                console.log('GlobalLeaderboard');
                if(!error && globalLeaderboard == null){
                    globalLeaderboard  =  new GlobalLeaderboard();
                    globalLeaderboard.user  = user;
                    // globalLeaderboard.points = config.global.points;
                    // globalLeaderboard.current_points = config.global.points;

                    console.log('GlobalLeaderboard is null');
                    console.log(globalLeaderboard);

                } else {
                    globalLeaderboard.timestamps.update = new Date().getTime();
                    // globalLeaderboard.points            = globalLeaderboard.points + config.global.points;
                    // globalLeaderboard.current_points    = globalLeaderboard.current_points + config.global.points;
                    if ( prop.type == 'classic' ){
                        globalLeaderboard.actions.share_classic   = globalLeaderboard.actions.share_classic + 1;
                    } else {
                        globalLeaderboard.actions.share_opinion   = globalLeaderboard.actions.share_opinion + 1;
                    }

                    console.log(globalLeaderboard);
                }

                globalLeaderboard.save();

                /*User.findOne({_id: user._id} , function(error, _user){
                    if ( prop.type == 'classic' ){
                        _user.number.share_classic = globalLeaderboard.actions.share_classic;
                    } else {
                        _user.number.share_opinion = globalLeaderboard.actions.share_opinion;
                    }
                    _user.number.total_points = globalLeaderboard.points;
                    _user.save();

                    console.log('User');
                    console.log(_user);
                });*/

                callback(null);
            });
        },
        function(callback){//increment user's channel points based on channel's settings  > ChannelLeaderboardSchema.points + Channel.points.voting
            console.log('channel');
            console.log(channel);
            ChannelLeaderboard.findOne({user: user, channel: channel} , function(error, channelLeaderboard){
                console.log('ChannelLeaderboard');
                console.log(channelLeaderboard);
                if(!error && channelLeaderboard == null){
                    channelLeaderboard          =  new ChannelLeaderboard();
                    channelLeaderboard.user     = user;
                    channelLeaderboard.channel  = channel;
                    if ( prop.type == 'classic' ){
                        channelLeaderboard.points   = channel.points.share_classic;
                        channelLeaderboard.current_points = channel.points.share_classic;
                    } else {
                        channelLeaderboard.points   = channel.points.share_opinion;
                        channelLeaderboard.current_points = channel.points.share_opinion;
                    }

                    console.log('ChannelLeaderboard is null');
                    console.log(channelLeaderboard);

                } else {
                    channelLeaderboard.timestamps.update = new Date().getTime();
                    if ( prop.type == 'classic' ){
                        channelLeaderboard.points           = channelLeaderboard.points + channel.points.share_classic;
                        channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.share_classic;
                        channelLeaderboard.actions.share_classic   = channelLeaderboard.actions.share_classic + 1;
                    } else {
                        channelLeaderboard.points           = channelLeaderboard.points + channel.points.share_opinion;
                        channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.share_opinion;
                        channelLeaderboard.actions.share_opinion   = channelLeaderboard.actions.share_opinion + 1;
                    }

                    User.findOne({_id: user._id} , function(error, _user){
                        if ( prop.type == 'classic' ){
                            _user.number.share_classic = _user.number.share_classic + 1;
                            _user.number.total_points = _user.number.total_points + channel.points.share_classic;
                        } else {
                            _user.number.share_opinion = _user.number.share_opinion + 1;
                            _user.number.total_points = _user.number.total_points + channel.points.share_opinion;
                        }

                        _user.save();

                        console.log('User');
                        console.log(_user);
                    });

                    console.log('updated');
                    console.log(channelLeaderboard);
                }

                channelLeaderboard.save();

                callback(null);
            });
        }
    ],// optional callback
    function(err, results){
        // var hostname =  req.protocol + '://' + req.get('host');
        // req.session.redirectTo = hostname + '/#!/single/'+ prop.type + "/" + prop.guid; //used in case user needs to login

        // console.log('share');
        // console.log(req.session.redirectTo);
        // res.redirect('/prop/'+ prop.guid);
    });
};