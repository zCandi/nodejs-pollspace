'use strict';

var fs = require('fs'),
    cryptojs = require('cryptojs'),
    config = require(__dirname + '/../../config/config'),
    _ = require('lodash');

exports.getGuid = function(req, res){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = config.guid || '';

    if ( _.isEmpty(guid) && !exports.checkIsIPV4(hostname) ){
        guid = exports.subDomain(hostname);
    }

    if ( _.isEmpty(guid) ){
        req.session.subdomain = false;
    } else {
        req.session.subdomain = true;
    }

    console.log('UTILS GUID: ' + guid);
    return guid
};

exports.getChannelGuid = function(req, res){
    var hostname =  req.url;
    var guid = exports.getChannelSubDomain(hostname);
 
    if ( _.isEmpty(guid) ){
        guid = req.session.guid;
    }
    
    console.log('UTILS GUID - getChannelGuid: ' + guid);
    return guid
};

exports.checkIsIPV4 = function(entry) {
    entry = entry.replace(new RegExp(/^\s+/),""); // START
    entry = entry.replace(new RegExp(/\s+$/),""); // END
    // IF THERE, REMOVES 'http://', 'https://' or 'ftp://' FROM THE START
    entry = entry.replace(new RegExp(/^http\:\/\/|^https\:\/\/|^ftp\:\/\//i),"");

    // IF THERE, REMOVES 'www.' FROM THE START OF THE STRING
    entry = entry.replace(new RegExp(/^www\./i),"");

    var blocks = entry.split(".");
    if(blocks.length === 4) {
        return blocks.every(function(block) {
            return parseInt(block,10) >=0 && parseInt(block,10) <= 255;
        });
    }
    return false;
};

exports.subDomain = function(url) {

    // IF THERE, REMOVE WHITE SPACE FROM BOTH ENDS
    url = url.replace(new RegExp(/^\s+/),""); // START
    url = url.replace(new RegExp(/\s+$/),""); // END

    // IF FOUND, CONVERT BACK SLASHES TO FORWARD SLASHES
    url = url.replace(new RegExp(/\\/g),"/");

    // IF THERE, REMOVES 'http://', 'https://' or 'ftp://' FROM THE START
    url = url.replace(new RegExp(/^http\:\/\/|^https\:\/\/|^ftp\:\/\//i),"");

    // IF THERE, REMOVES 'www.' FROM THE START OF THE STRING
    url = url.replace(new RegExp(/^www\./i),"");

    // REMOVE COMPLETE STRING FROM FIRST FORWARD SLASH ON
    url = url.replace(new RegExp(/\/(.*)/),"");

    // REMOVES '.??.??' OR '.???.??' FROM END - e.g. '.CO.UK', '.COM.AU'
    if (url.match(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i))) {
        url = url.replace(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i),"");
        // REMOVES '.??' or '.???' or '.????' FROM END - e.g. '.US', '.COM', '.INFO'
    } else if (url.match(new RegExp(/\.[a-z]{2,4}$/i))) {
        url = url.replace(new RegExp(/\.[a-z]{2,4}$/i),"");
    }

    var subdomain = "";
    if (url.match(new RegExp(/\./g))){
        subdomain = url.split(".")[0]
    }

    return(subdomain);
};


exports.getChannelSubDomain = function(url) {

    // IF THERE, REMOVE WHITE SPACE FROM BOTH ENDS
    url = url.replace(new RegExp(/^\s+/),""); // START
    url = url.replace(new RegExp(/\s+$/),""); // END

    // IF FOUND, CONVERT BACK SLASHES TO FORWARD SLASHES
    url = url.replace(new RegExp(/\\/g),"/");

    // IF THERE, REMOVES 'http://', 'https://' or 'ftp://' FROM THE START
    url = url.replace(new RegExp(/^http\:\/\/|^https\:\/\/|^ftp\:\/\//i),"");

    // IF THERE, REMOVES 'www.' FROM THE START OF THE STRING
    url = url.replace(new RegExp(/^www\./i),"");

    var list = url.split("/");

    var subdomain = "";
    if (list){
        subdomain = list[2];
    }

    return(subdomain);
};


exports.getCustomFolderList = function(){
    var models_path = __dirname + '/../../public/views/custom';

    var getFiles = function (dir,files_){
        files_ = files_ || [];
        if (typeof files_ === 'undefined') files_=[];
        var files = fs.readdirSync(dir);
        for(var i in files){
            if (!files.hasOwnProperty(i)) continue;
            var name = dir+'/'+files[i];
            if (fs.statSync(name).isDirectory()){
                getFiles(name,files_);
            } else {
                name = name.replace(__dirname + "/../../public/", "");
                files_.push(cryptojs.Crypto.SHA1(name));
                //files_.push(name);
            }
        }
        return files_;
    }

    return getFiles(models_path);
};

exports.setErrorMessage = function(req, message){
    req.session.flash_message = { 'type': 'danger', 'message': message };
};

exports.setSuccessMessage = function(req, message){
    req.session.flash_message = { 'type': 'success', 'message': message };
};

exports.flushMessage = function(req){
    req.session.flash_message = null;
};

exports.getMessage = function(req){
    return req.session.flash_message;
};

exports.getChannelColor = function(channel){
    var ct = channel.color.color_text;
    var c1 = channel.color.color1;
    var c2 = channel.color.color2;
    var c3 = channel.color.color3;
    var palette = channel.color.palette;
    var colors = {};

    if ( !_.isEmpty(ct) ){
        colors['ct'] = ct;
    }
    if ( !_.isEmpty(c1) ){
        colors['c1'] = c1;
    }
    if ( !_.isEmpty(c2) ){
        colors['c2'] = c2;
    }
    if ( !_.isEmpty(c3) ){
        colors['c3'] = c3;
    }
    if ( !_.isEmpty(palette) ){
        colors['palette'] = palette;
    }

    return encodeURIComponent(JSON.stringify(colors));
};

Array.prototype.contains = function(k, callback) {
    var self = this;
    return (function check(i) {
        if (i >= self.length) {
            return callback(false);
        }

        if ( self[i].toString() === k.toString() ) {
            return callback(true);
        }

        return process.nextTick(check.bind(null, i+1));
    }(0));
}