'use strict';

// Props routes use articles controller
var props = require('../controllers/props');
var authorization = require('./middlewares/authorization');

var hasAdminPrivileges = function(req, res, next) {
    if (req.user && !req.user.is_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
}

module.exports = function(app) {

    //Embedded
    app.get('/prop/:guid/poll.js', props.renderSingleEmbedPoll);
    app.get('/prop/:guid/embedded-test', props.embeddedSingleQuestionTest);
    app.get('/prop/:guid/embedded-single', props.renderEmbeddedSingleQuestion);
    app.get('/prop/:guid/embedded-results', props.renderEmbeddedSingleQuestionResults);

    /* APIs */
    app.get('/prop/:guid', props.showprop);
    app.get('/prop/:guid/:hash', props.getPropGuidHash); //decrypts the hash and do the logic to see if the user expired
    app.get('/email/hash', props.getEmailHash); //renders page form to display email and get hash
    app.post('/email/hash', props.postEmailHash); //generates hash and display

    /* ADMIN SECTION */
    app.get('/api/admin/prop/:prop_id', authorization.requiresLogin, hasAdminPrivileges, props.show);
    app.post('/api/admin/prop/:prop_id', authorization.requiresLogin, hasAdminPrivileges, props.update);
    app.get('/api/admin/prop/:prop_id/deactivate',  authorization.requiresLogin, hasAdminPrivileges, props.deactivate);
    app.get('/api/admin/prop/:prop_id/results',  authorization.requiresLogin, hasAdminPrivileges, props.results);

    app.get('/api/prop/:guid/vote/stats',  props.voteStats);
    app.post('/api/prop/:guid/vote', authorization.requiresLogin, props.vote);
    app.get('/api/prop/:guid/vote/:answerid', authorization.requiresLogin, props.vote);
    app.post('/api/prop/:guid', props.update);
    app.get('/api/prop/:guid/others', props.findOtherPropsFromChannel);
    app.get('/api/prop/:guid',  props.show);
    app.get('/api/prop/:guid/vote/user',  props.voteUser);
    app.get('/api/prop/:guid/share',  authorization.requiresLogin, props.share);

    /* FP API calls */
    app.get('/api/fp/prop/:prop_id/autogeneratevotes', props.autogeneratevotes);

    /* CRON */
    app.get('/cron/prop/grade',  props.gradeAllOpinionProps);

    // Finish with setting up the channelId param
    app.param('prop_id', props.propp);
    app.param('guid', props.propguid);

};