'use strict';

// Articles routes use articles controller
var signups = require('../controllers/signups');
var authorization = require('./middlewares/authorization');

var hasAdminPrivileges = function(req, res, next) {
    if (req.user && !req.user.is_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
};


var isFPAdmin = function(req, res, next) {
    if (req.user && !req.user.fp_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(app) {

    /* ADMIN SECTION */
    app.get('/api/admin/signups', authorization.requiresLogin, isFPAdmin, signups.getSignups);
    app.post('/signup', signups.postSignups);

};