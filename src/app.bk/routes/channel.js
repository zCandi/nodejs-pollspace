'use strict';

// Articles routes use articles controller
var channels = require('../controllers/channels');
var authorization = require('./middlewares/authorization');

var hasAdminPrivileges = function(req, res, next) {
    if (req.user && !req.user.is_admin) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(app) {

    /* APIs */
    app.get('/channel/reset', channels.reset);
    app.get('/channel/:cguid', channels.subdomainRedirect);
    app.get('/api/admin/channel/list', authorization.requiresLogin, hasAdminPrivileges, channels.all);
    app.get('/admin/channel/:channel_id', channels.subdomainRedirect);

    app.get('/api/channel/list', authorization.requiresLogin, channels.all);
    app.get('/api/channel/:cguid', channels.show);
    app.get('/api/channel/:cguid/view', channels.show);
    app.get('/api/channel/:cguid/prop/:type/list', authorization.requiresLogin, channels.propList);
    app.get('/api/channel/:cguid/propset/:type/list', authorization.requiresLogin, channels.propsetList);
    app.get('/api/channel/:cguid/leaders', authorization.requiresLogin, channels.leaderboard);
    app.post('/api/channel/:cguid/prop/create', authorization.requiresLogin, channels.createProp);

    app.post('/api/channel/:cguid/upload/photo',  channels.postUploadPhoto);
    app.get('/api/channel/:cguid/uploaded/photo',  channels.getUploadedPhoto);
    app.get('/api/channel/:cguid/last/prop/:type',  channels.lastProp);
    app.post('/api/channel/:cguid/product/vote', authorization.requiresLogin,  channels.productVote);

    app.get('/api/channel/color/style.css', channels.getColorStylesheet);

    /* ADMIN SECTION */
    app.get('/api/admin/channel/list/all', channels.allChannels);
    app.get('/api/admin/channel/:channel_id', authorization.requiresLogin, hasAdminPrivileges, channels.showadmin);
    app.post('/api/admin/channel', authorization.requiresLogin, hasAdminPrivileges, channels.create);
    app.post('/api/admin/channel/:channel_id', authorization.requiresLogin, hasAdminPrivileges, channels.update);
    app.get('/api/admin/channel/:channel_id/deactivate', authorization.requiresLogin, hasAdminPrivileges, channels.deactivate);
    app.get('/api/admin/channel/:channel_id/leaderboard', authorization.requiresLogin, channels.adminleaderboard);
    app.post('/api/admin/channel/:channel_id/photos/:photo_id/deactivatephoto', authorization.requiresLogin, hasAdminPrivileges, channels.deactivatePhoto);

    app.post('/api/admin/channel/:channel_id/resetleaderboard', authorization.requiresLogin, channels.resetleaderboard);

    app.get('/api/admin/channel/:channel_id/prop/:type/list', authorization.requiresLogin, hasAdminPrivileges, channels.adminPropList);
    app.get('/api/admin/channel/:channel_id/propset/:type/list', authorization.requiresLogin, hasAdminPrivileges, channels.adminPropsetList);
    app.get('/api/admin/channel/:channel_id/prop/suggestedby/list', authorization.requiresLogin, hasAdminPrivileges, channels.suggestedbyPropList);
    // app.get('/api/admin/channel/:channel_id/tag/list', authorization.requiresLogin, hasAdminPrivileges, channels.tagList);
    app.post('/api/admin/channel/:channel_id/tag/list', authorization.requiresLogin, hasAdminPrivileges, channels.tagList);
    app.post('/api/admin/channel/:channel_id/prop/create', authorization.requiresLogin, hasAdminPrivileges, channels.createProp);
    app.post('/api/admin/channel/:channel_id/propset/create', authorization.requiresLogin, hasAdminPrivileges, channels.createPropset);

    app.get('/api/admin/channel/:channel_id/admin/list', authorization.requiresLogin, hasAdminPrivileges, channels.channelAdminList);
    app.post('/api/admin/channel/:channel_id/admin/:admin_id/add', authorization.requiresLogin, hasAdminPrivileges, channels.channelAdminAdd);
    app.post('/api/admin/channel/:channel_id/admin/:admin_id/remove', authorization.requiresLogin, hasAdminPrivileges, channels.channelAdminRemove);

    app.post('/api/admin/channel/:channel_id/analytics/users', authorization.requiresLogin, hasAdminPrivileges, channels.analyticsUsersSearch);
    app.post('/api/admin/channel/:channel_id/analytics/props', authorization.requiresLogin, hasAdminPrivileges, channels.analyticsPropsSearch);
    app.get('/api/admin/channel/:channel_id/analytics/channel', authorization.requiresLogin, hasAdminPrivileges, channels.analyticsChannelInsight);

    app.post('/api/channel/:cguid/contact', authorization.requiresLogin, channels.sendContactEmail);

    app.param('channel_id', channels.channel);
    app.param('cguid', channels.channelguid);
};
