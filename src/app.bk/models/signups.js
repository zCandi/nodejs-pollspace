'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto');

/**
 * User Schema
 */
var SignupsSchema = new Schema({
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    company: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        deleted: { type: Date, default: 0 }
    }
});

mongoose.model('Signup', SignupsSchema);