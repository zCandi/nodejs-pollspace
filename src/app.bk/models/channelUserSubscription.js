'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ChannelUserSubscriptionSchema = new Schema({
    channel: {
        type: Schema.ObjectId,
        ref: 'Channel'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('ChannelUserScubscription', ChannelUserSubscriptionSchema);