'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    Schema = mongoose.Schema;

var VotesSchema = new Schema({
    prop: {
        type: Schema.ObjectId,
        ref: 'Prop'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    choice: { type: String }, //This is option field 'id'
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now }
    }
});


VotesSchema.statics.searchVotingPropsAndAnswers = function(data, cb) {
    var channel_id = data.channel_id;
    var propList = (data.propList) ? data.propList : null;

    var search_data = {};
    var temp_search_data = [];

    if ( propList ){
        propList.forEach(function(propdata, propKey){
            if ( !_.isEmpty( propdata.prop ) ){
                temp_search_data[propKey] = {'prop': propdata.prop };
                if ( propdata.answer ){
                    temp_search_data[propKey] = { '$and' : [ {'prop': propdata.prop}, {'choice': propdata.answer} ] }
                }
            }
        });

        if ( !_.isEmpty(temp_search_data) && Object.keys(temp_search_data).length >= 2){ // should be at least to elements to $or
            search_data['$or'] = temp_search_data;
        } else {
            search_data = temp_search_data[0];
        }
    }

    this.find(search_data).populate('user').populate('prop').exec(cb);
};

mongoose.model('Votes', VotesSchema);