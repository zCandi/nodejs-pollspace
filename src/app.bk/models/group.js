'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var GroupSchema = new Schema({
    name: {
        type: String,
        trim: true
    },
    admin: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    users: [{
        type: Schema.ObjectId,
        ref: 'User'
    }],
    type: { type: String, enum: ['static', 'dynamic']},
    deleted: { type: Boolean, default: false },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        nextUpdate: { type: Date, default: 0 },
        deleted: { type: Date, default: 0 }
    }
});

GroupSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

mongoose.model('Group', GroupSchema);