'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var ChannelAdminSchema = new Schema({
    email: {
        type: String
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    company: {
        type: String
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        verified: { type: Date, default: 0 },
        updated: { type: Date, default: Date.now },
        deleted: { type: Date, default: 0 }
    }
});

mongoose.model('ChannelAdmin', ChannelAdminSchema);