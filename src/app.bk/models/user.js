'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    queryPlugin = require('mongoose-query'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    crypto = require('crypto');

/**
 * User Schema
 */
var UserSchema = new Schema({
    username: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    facebook_data: {
        id: { type: String },
        first_name: { type: String },
        last_name: { type: String },
        email: { type: String },
        gender: { type: String },
        relationship_status: { type: String },
        about: { type: String },
        age: { type: Number },
        birthday: { type: String },
        location: { type: String },
        locale: { type: String },
        timezone: { type: Number },
        currency: { type: String },
        hometown: { type: String },
        political: { type: String },
        religion: { type: String },
        friends: { type: Number },
        likes: { type: Number },
        quotes: { type: String },
        education: { type: Schema.Types.Mixed },
        work: { type: Schema.Types.Mixed }
    },
    twitter_data: {
        id: { type: String },
        name: { type: String },
        email: { type: String },
        screen_name: { type: String },
        location: { type: String },
        profile_picture: { type: String },
        followers_count: { type: String },
        friends_count: { type: String }
    },
    google: {
        id: { type: String },
        name: { type: String },
        email: { type: String },
        first_name: { type: String },
        last_name: { type: String },
        gender: { type: String },
        picture: { type: String },
        locale: { type: String },
        link: { type: String }
    },
    instagram_data: {
        username: { type: String }
    },
    fp_admin: {
        type: Boolean
    },
    is_admin: {
        type: Boolean
    },
    subscriptions: [{
        type: Schema.ObjectId,
        ref: 'Channel'
    }],
    number: {// number of times a user has voted
        voting: { type: Number, default: 0 },  //Should be the same as action: voting globalleaderboard
        correct: { type: Number, default: 0 },  //Should be the same as action: correct globalleaderboard
        streakbonus: { type: Number, default: 0 }, //Should be the same as action: streakbonus globalleaderboard
        incorrect: { type: Number, default: 0 },//Should be the same as action: incorrect globalleaderboard
        completed: { type: Number, default: 0 },//Should be the same as action: completed globalleaderboard
        share_classic: { type: Number, default: 0 },//Should be the same as action: share_classic globalleaderboard
        suggest_classic: { type: Number, default: 0 },//Should be the same as action: suggest_classic globalleaderboard
        share_opinion: { type: Number, default: 0 },//Should be the same as action: share_opinion globalleaderboard
        suggest_opinion: { type: Number, default: 0 },//Should be the same as action: suggest_opinion globalleaderboard
        total_points: { type: Number, default: 0 } //Should be the same as points globalleaderboard
    },
    channel_logged_totals: [{
        type: Number
    }],
    props:[
        {
            type: Schema.ObjectId,
            ref: 'Prop'
    }],
    correct_props:[{
        type: Schema.ObjectId,
        ref: 'Prop'
    }],
    incorrect_props:[{
        type: Schema.ObjectId,
        ref: 'Prop'
    }],
    completed_props:[{
        type: Schema.ObjectId,
        ref: 'Prop'
    }],
    hashed_password: String,
    salt: String,
    provider: String,
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        deleted: { type: Date, default: 0 }
    }
});

/**
 * Virtuals
 */
UserSchema.virtual('password').set(function(password) {
    if ( this.isNew && !_.isEmpty(password) ){
        this._password = password;
        this.salt = this.makeSalt();
        this.hashed_password = this.encryptPassword(password);
    } else {
        if ( !this.isNew && !_.isEmpty(password) ){
            this._password = password;
            this.salt = this.makeSalt();
            this.hashed_password = this.encryptPassword(password);
        }
    }

}).get(function() {
    return this._password;
});

/**
 * Validations
 */
var validatePresenceOf = function(value) {
    return value && value.length;
};

// The 4 validations below only apply if you are signing up traditionally.
UserSchema.path('firstname').validate(function(firstname) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof firstname === 'string' && firstname.length > 0);
}, 'First Name cannot be blank');

UserSchema.path('lastname').validate(function(lastname) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof lastname === 'string' && lastname.length > 0);
}, 'Last Name cannot be blank');

UserSchema.path('email').validate(function(email) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof email === 'string' && email.length > 0);
}, 'Email cannot be blank');

/*UserSchema.path('username').validate(function(username) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof username === 'string' && username.length > 0);
}, 'Username cannot be blank');*/

UserSchema.path('hashed_password').validate(function(hashed_password) {
    // If you are authenticating by any of the oauth strategies, don't validate.
    if (!this.provider) return true;
    return (typeof hashed_password === 'string' && hashed_password.length > 0);
}, 'Password cannot be blank');


/**
 * Pre-save hook
 */
UserSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    if (!validatePresenceOf(this.password) && !this.provider)
        next(new Error('Invalid password'));
    else
        next();
});

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function(password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
    }
};

UserSchema.statics.searchDemographicByChannel = function(data, cb) {
    var channel_id = data.channel_id;

    var gender =    (data.gender) ? data.gender : null;
    var age_from =  (data.age_from) ? data.age_from : null;
    var age_to =    (data.age_to) ? data.age_to : null;
    var relationship_status = (data.relationship_status) ? data.relationship_status : null;
    var location = (data.location) ? data.location : null;

    var search_data = {};
    search_data['subscriptions'] = { "$in" : [channel_id] };

    if ( gender ){
        search_data['facebook_data.gender'] = gender;
    }

    if ( age_from !== null && age_to !== null ) {
        search_data['facebook_data.age'] = { $gte: age_from, $lte: age_to };
    } else {
        if ( age_from ) {
            search_data['facebook_data.age'] = { $gte: age_from };
        } else {
            if ( age_to ) {
                search_data['facebook_data.age'] = { $lte: age_to };
            }
        }
    }

    if ( relationship_status ){
        search_data['facebook_data.relationship_status'] = relationship_status;
    }

    if ( location ){
        search_data['facebook_data.location'] =  eval("/"+location+"/i") ;
    }

    this.find(search_data).populate('props').exec(cb);
};

UserSchema.plugin(queryPlugin);
mongoose.model('User', UserSchema);