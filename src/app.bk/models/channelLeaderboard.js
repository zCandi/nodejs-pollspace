'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ChannelLeaderboardSchema = new Schema({
    current_points: {
        type: Number,
        default: 0
    },
    points: { // points and current points at some stage are the same. Difference is that at some point, current_points are wiped and points is used as historic points
        type: Number,
        default: 0
    },
    channel: {
        type: Schema.ObjectId,
        ref: 'Channel'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    actions: {
        voting: { type: Number, default: 0 },  // number of times a user has voted
        correct: { type: Number, default: 0 }, //Number of times a user is correct
        streakbonus: { type: Number, default: 0 },
        incorrect: { type: Number, default: 0 }, //Number of times a user is incorrect
        completed: { type: Number, default: 0 }, //Number of times a user completed a Poll (a poll is closed and this poll is 'graded'
        share_classic: { type: Number, default: 0 },  //Number of times a user shares a clasic
        share_opinion: { type: Number, default: 0 }, //Number of times a user shares an opinion
        suggest_classic: { type: Number, default: 0 }, //Number of times suggest a classic
        suggest_opinion: { type: Number, default: 0 } //Number of times suggest an opinion
    },
    timestamps: {
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now }
    }
});

mongoose.model('ChannelLeaderboard', ChannelLeaderboardSchema);