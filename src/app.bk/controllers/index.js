'use strict';
var fs = require('fs'),
    mongoose = require('mongoose'),
    Channel    = mongoose.model('Channel'),
    User    = mongoose.model('User'),
    Photo    = mongoose.model('Photo'),
    RandExp = require('randexp'),
    https = require('https'),
    request = require('request'),
    qs = require('querystring'),
    async = require('async'),
    cryptojs = require('cryptojs'),
    config = require(__dirname + '/../../config/config'),
    utils = require(__dirname + '/../lib/utils'),
    _ = require('lodash');


exports.render = function(req, res, next) {
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = utils.getGuid(req, res);

    var models_path = __dirname + '/../../public/views/custom';
    var isSubdomainHeader = false;
    var isSubdomainFooter = false;

    if ( _.isEmpty(guid) && !_.isEmpty(req.session.guid)){
        guid = req.session.guid;
    }

    var getCustomFolderList = utils.getCustomFolderList();
    var access_token = req.session.access_token;

    if (req.params.pguid) {
        Photo.findOne({ guid: req.params.pguid })
            .exec(function(err, photo) {
                if (err) return next(err);
                if ( photo ){
                    req.photo = photo;
                }
            });
    }

    async.series([
        function(callback){
            var viafo_server = config.viafo.server;
            var viafo_client_id = config.viafo.client_id;
            var viafo_client_secret = config.viafo.client_secret;

            var url = "/client/1/register.json?client_id=" + viafo_client_id + "&client_secret=" + viafo_client_secret   + "&uuid=" + new RandExp(/([A-Za-z0-9]){10}/i).gen();
            request(viafo_server + url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var jsonbody = JSON.parse(body);
                    var access_token = jsonbody.access_token;

                    req.session.access_token = access_token;
                }

                callback(null);
            });

            console.log('IN VIAFO CALL');
            console.log(req.user);
        },
        function (callback){
            var colors = null;
            Channel.findOne({ guid: guid })
                .populate('subscribers')
                .deepPopulate('submittedPhotos.user voted_products.user voted_products.prop')
                .exec(function(err, channel) {
                    if (err) return next(err);
                    if ( channel ){
                        req.channel = channel;
                    }
                    callback(null,channel);
                });
        },
        function (callback){
            console.log('************************');
            console.log(models_path + '/' + guid + '/header.html');

            fs.stat(models_path + '/' + guid + '/header.html', function (err, stats) {
                if (err) {
                    isSubdomainHeader = false
                } else {
                    isSubdomainHeader = true
                }

                console.log('************************');
                console.log(isSubdomainHeader);

                callback(null, isSubdomainHeader);
            });

        },
        function (callback){
            console.log('************************');
            console.log(models_path + '/' + guid + '/footer.html');
            fs.stat(models_path + '/' + guid + '/footer.html', function (err, stats) {
                if (err) {
                    isSubdomainFooter = false
                } else {
                    isSubdomainFooter = true
                }

                console.log('************************');
                console.log(isSubdomainFooter);

                callback(null, isSubdomainFooter);
            });

        },
        function(callback){
            var twitter_key = config.twitter.clientKey;
            var twitter_secret = config.twitter.clientSecret;
            var twitter_callback = config.twitter.callbackURL;

            var oauth = { oauth_callback: hostname + twitter_callback, consumer_key: twitter_key, consumer_secret: twitter_secret };
            request.post({url:"https://api.twitter.com/oauth/request_token", oauth:oauth }, function (error, response, body) {
                if ( !error && response.statusCode == 200 ) {
                    var req_data = qs.parse(body);
                    var uri = 'https://api.twitter.com/oauth/authenticate' + '?' + qs.stringify({oauth_token: req_data.oauth_token});

                    req.session.twitter_access_token = req_data.oauth_token;
                    req.session.twitter_token_secret = req_data.oauth_token_secret;
                    req.session.twitter_oauth_verifier = req_data.oauth_callback_confirmed;
                    req.session.twitter_url = uri;

                    if ( _.isEmpty(req.session.redirectTo) ){
                        if ( req.channel && req.channel.default_page){
                            req.session.redirectTo = hostname + "/#!/final/four"; // + req.channel.default_page;
                        } else {
                            req.session.redirectTo = hostname;
                        }
                    }
                }

                callback(null);
            });

            console.log('IN VIAFO CALL');
            console.log(req.user);
        }],
        function(err, results){
            var forgotPasswordHash = req.session.forgotPasswordHash;
            var access_token = req.session.access_token;
            var twitter_access_token = req.session.twitter_access_token;
            var twitter_url = req.session.twitter_url;
            var flash_message = utils.getMessage(req);
            var colors = null;
            var channel = null;

            if ( !_.isUndefined(results[1]) && !_.isEmpty(results[1]) ){
                channel = results[1];
                colors = utils.getChannelColor(channel);
            }
            utils.flushMessage(req);

            console.log('req query');
            console.log(req.query);

            res.render('index', {
                user: req.user ? JSON.stringify(req.user) : 'null',
                prop_image: req.photo ? req.photo.image : hostname + "/img/fp/ps40x40.png",
                hostname: req.params.pguid ? hostname + req.url : hostname,
                guid: guid,
                flash_message: flash_message,
                getCustomFolderList: getCustomFolderList,
                access_token: access_token,
                twitter_access_token: twitter_access_token,
                twitter_url: twitter_url,
                colors: colors,
                channel: channel ? JSON.stringify(channel) : 'null',
		        mode: channel ? channel.mode : 'null',
                prop: req.prop ? JSON.stringify(req.prop) : 'null',
                propset: req.propset ? JSON.stringify(req.propset) : 'null',
                forgotPasswordHash: forgotPasswordHash ? forgotPasswordHash : 'null',
                subdomain: req.session.subdomain,
                isSubdomainHeader: isSubdomainHeader,
                isSubdomainFooter: isSubdomainFooter
            });
        });

    req.session.message = null;
};

exports.admin = function(req, res) {
    res.render('admin', {
        user: req.user ? JSON.stringify(req.user) : 'null'
    });
};
