'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    PropSet    = mongoose.model('PropSet'),
    Channel    = mongoose.model('Channel'),
    Prop    = mongoose.model('Prop'),
    users = require('../controllers/users'),
    uploadcare = require('uploadcare')('05817a84d61d7339a3b1', '253a61bad6416902b5dd'),
    config = require(__dirname + '/../../config/config'),
    utils = require(__dirname + '/../lib/utils'),
    async = require('async'),
    RandExp = require('randexp'),
    request = require('request'),
    fs = require('fs'),
    _ = require('lodash');


/* EMBEDDED */
exports.renderMultipleEmbedPoll = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.propsetguid;

    req.session.loginCallback = '/q/'+guid+'/embedded-list';
    req.session.questionType = 'multiple';

    res.contentType("text/javascript");
    res.render('embedmultiple', {
        guid: req.params.propsetguid,
        hostname: hostname,
        containerName: 'psm_container_' +  req.params.propsetguid
    });
};

exports.embeddedTest = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.propsetguid;

    var data = {
        hostname: hostname,
        guid: guid,
    };

    res.render('embedded/test', data);
};

function userVote(prop, user){
    var flag = false;
    if ( user ){
        prop.votes.forEach(function(vote, key){
            var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
            if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                flag = true;
            }
        });
    }

    return flag;
};

function userVoteOption(prop, user){
    var option = '';
    if ( user ){
        prop.votes.forEach(function(vote, key){
            var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
            if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                option = vote.choice;
            }
        });
    }

    return option;
};

function parseUserPropVote(propset, user){
    propset.props.forEach(function(prop, propkey){
        prop.userVote = userVote(prop, user);
        prop.userVoteOption = userVoteOption(prop, user);
    });

    return propset;
}

exports.renderEmbeddedList = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.propsetguid;
    var user = req.user;
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var propset = req.propset;

    req.session.guid = guid;

    if ( user ){
        propset = parseUserPropVote(propset, user);
        var data = {
            guid: req.params.propsetguid,
            fullUrl: fullUrl,
            hostname: hostname,
            user: user ? JSON.stringify(user) : 'null',
            propset_json: propset ? JSON.stringify(propset) : 'null',
            propset: propset,
            props_length: propset.props.length
        };

        res.render('embedded/multiple/view', data, function(err, html) {
            res.jsonp(html);
        });
    } else {
        users.embeddedLogin(req, res, next);
    }

};

exports.renderEmbeddedResults = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.propsetguid;
    var user = req.user;
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var propset = req.propset;

    if ( user ){
        propset = parseUserPropVote(propset, user);
        var data = {
            guid: req.params.propsetguid,
            fullUrl: fullUrl,
            hostname: hostname,
            user: user ? JSON.stringify(user) : 'null',
            propset_json: propset ? JSON.stringify(propset) : 'null',
            propset: propset,
            props_length: propset.props.length
        };

        res.render('embedded/multiple/results', data, function(err, html) {
            res.jsonp(html);
        });
    } else {
        users.embeddedLogin(req, res, next);
    }
};

exports.viewPropset = function(req, res){
    var user = req.user;
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var hostname =  req.protocol + '://' + req.get('host');
    var propset = req.propset;

    res.render('embedded/multiple/view', {
        user: user,
        fullUrl: fullUrl,
        hostname: hostname,
        propset: propset,
        props_length: propset.props.length
    });
};

/* END embedded */

exports.all = function(req, res) {
    var user = req.user;
    if ( user ){
        res.redirect("/#!/q/"+req.params.propsetguid);
    } else {
        var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        var hostname =  req.protocol + '://' + req.get('host');
        var propset = req.propset;
        var channel = req.channel;

        req.session.guid = channel.guid;

        var image = null;
        if ( propset.image ){
            image = propset.image;
        } else {
            if ( channel.picture ){
                image = channel.picture;
            } else {
                image = hostname + "/img/fp/ps400x400.png"
            }
        }

        var props = {};
        props.title = channel.name + ", powered by Pollspace" + " - " + propset.title + " - Vote Now ";
        props.fullUrl = fullUrl;
        props.user = req.user ? JSON.stringify(req.user) : 'null';
        props.channel = req.channel ? JSON.stringify(req.channel) : 'null';
        props.propsetJSON = propset ? JSON.stringify(propset) : 'null';
        props.cguid = channel.guid;
        props.hostname = hostname;
        props.channel_picture = channel.picture ? channel.picture : hostname + "/img/fp/ps400x400.png";
        props.channel_logo = channel.logo ? channel.logo : hostname + "/img/fp/ps40x40.png";
        props.image = image;
        props.propset = propset;
        props.total_props = propset.props.length;
        props.colors = utils.getChannelColor(channel);

        res.render('propset/list', props);
    }
};

exports.getResults = function(req, res ){
    var user = req.user;
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var hostname =  req.protocol + '://' + req.get('host');
    var propset = req.propset;
    var channel = req.channel;
    var guid = utils.getGuid(req, res);

    var props = {};
    props.title = channel.name + ", powered by Pollspace" + " - " + propset.title + " - Vote Now ";
    props.fullUrl = fullUrl;
    props.user = req.user ? JSON.stringify(req.user) : 'null';
    props.channel = req.channel ? JSON.stringify(req.channel) : 'null';
    props.propsetJSON = propset ? JSON.stringify(propset) : 'null';
    props.cguid = channel.guid;
    props.hostname = hostname;
    props.channel_picture = channel.picture ? channel.picture : hostname + "/img/fp/ps400x400.png";
    props.channel_logo = channel.logo ? channel.logo : hostname + "/img/fp/ps40x40.png";
    props.propset = propset;
    props.total_props = propset.props.length;
    props.colors = utils.getChannelColor(channel);

    res.render('propset/results', props);
};

exports.postResults = function(req, res){
    var hostname =  req.protocol + '://' + req.get('host');
    req.session.redirectTo = hostname + '/#!/q/'+req.params.propsetguid;
    res.redirect(hostname + "/#!/login");
}

exports.getPropset = function(req, res, next, id) {

    PropSet.findOne({ _id : id})
        .populate('props')
        .populate('channel')
        .exec(function(err, propset) {
            if (err) return next(err);
            if (!propset) return next(new Error('Failed to load propset with id ' + id));
            req.propset = propset;
            req.channel = propset.channel;

            next();
    });
};

exports.getPropsetguid = function(req, res, next, id) {

    PropSet.findOne({ guid : id})
        .populate('props')
        .populate('channel')
        .exec(function(err, propset) {
            if (err) return next(err);
            if (!propset) return next(new Error('Failed to load propset with guid ' + id));
            req.propset = propset;
            req.channel = propset.channel;

            next();
        });
};

function _getFileId(url, index) {
    if ( url ) {
        return url.replace(/^https?:\/\//, '').split('/')[index];
    }
};

exports.showpropset = function(req, res){
    res.jsonp(req.propset);
};


exports.updatepropset = function(req, res){
    var propset = req.propset;
    propset = _.extend(propset, req.body);
    propset.timestamps.updated = new Date().getTime();

    if ( req.body.status_action == 'publish' ) {
        propset.timestamps.published = new Date().getTime();
    }

    if ( propset.image ){
        var imageId = _getFileId(propset.image, 1);
    }

    if ( propset.imagebk ){
        var imageIdbk = _getFileId(propset.imagebk, 1);
    }

    async.series([
        function(callback){
            if ( imageId ){
                if ( imageIdbk != imageId ){
                    uploadcare.files.remove(imageIdbk, function(error, response) {
                        if(error) {
                            console.log('Error: ' + JSON.stringify(response));
                        } else {
                            console.log('Response: ' + JSON.stringify(response));
                        }
                    });
                };
            }
            callback(null);
        },
        function(callback){
            if ( imageId ){
                uploadcare.files.store(imageId, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;

                });
            }
            callback(null);
        }
    ],
        function(err, results){
            propset.save(function(err) {
                if (err) {
                    res.jsonp(err);
                } else {
                    res.jsonp(propset);
                }
            });
        });
};

exports.deactivate = function(req, res) {
    var propset = req.propset;

    propset = _.extend(propset, req.body);
    propset.deleted = true;
    propset.timestamps.deleted = new Date().getTime();

    propset.save(function(err) {
        if (err) {
            res.jsonp([err]);
        } else {
            res.jsonp(propset);
        }
    });
};