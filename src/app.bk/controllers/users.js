'use strict';

/**
 * Module dependencies.
 */
var index = require('../controllers/index');

var mongoose = require('mongoose'),
    Channel = mongoose.model('Channel'),
    ChannelLeaderboard = mongoose.model('ChannelLeaderboard'),
    GlobalLeaderboard = mongoose.model('GlobalLeaderboard'),
    Prop = mongoose.model('Prop'),
    User = mongoose.model('User'),
    Share = mongoose.model('Share'),
    async = require('async'),
    cryptojs = require('cryptojs'),
    RandExp = require('randexp'),
    request = require('request'),
    qs = require('querystring'),
    nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    config = require(__dirname + '/../../config/config'),
    utils = require(__dirname + '/../lib/utils'),
    url = require('url'),
    phpjs = require('phpjs'),
    _ = require('lodash');


exports.about = function(req, res) {
    res.render('about', {
        title: 'About'
    });
};

exports.mychannels = function(req, res){
    req.session.guid = null;
    res.redirect('/#!/my/channels');
};

exports.myrankings = function(req, res){
    req.session.guid = null;
    res.redirect('/#!/my/rankings');
};

exports.myprops = function(req, res){
    req.session.guid = null;
    res.redirect('/#!/my/props');
};

exports.myaccount = function(req, res){
    req.session.guid = null;
    res.redirect('/#!/my/account');
};

exports.adminchannelList = function(req, res){
    req.session.guid = null;
    res.redirect('/#!/admin/channel/list');
}

/**
 * Auth callback
 */
exports.authCallback = function(req, res) {
    res.redirect('/');
};

/**
 * Show login form
 */
exports.login = function(req, res) {
    res.render('users/login', {
        title: 'Login',
        isAdmin: false,
        message: req.flash('error')
    });
};


exports.user_login = function(req, res) {
    res.render('users/signin', {
        title: 'Signin',
        isAdmin: false,
        message: req.flash('error')
    });
};

exports.admin_login = function(req, res) {
    res.render('users/admin_login', {
        title: 'Signin',
        isAdmin: true,
        message: req.flash('error')
    });
};

/**
 * Show sign up form
 */
exports.signupUser = function(req, res) {
    res.render('users/signup', {
        title: 'Sign up',
        isAdmin: false,
        user: new User()
    });
};

/**
 * Show sign up form
 */
exports.signupAdmin = function(req, res) {
    res.render('users/signup', {
        title: 'Sign up',
        isAdmin: true,
        user: new User()
    });
};

/**
 * Logout
 */
exports.signout = function(req, res) {
    req.logout();
    req.session.redirectTo = null;
    req.session.guid = null;
    res.redirect('/#!/logout');
};

exports.forgotpassword = function(req, res){
    var transporter = nodemailer.createTransport(smtpTransport(config.email));
    var hostname =  req.protocol + '://' + req.get('host');
    var date = new Date();
    date.setDate(date.getDate() + 7 );

    var email = req.body.email;
    var timestamp = date.getTime();
    var encrypated = cryptojs.Crypto.AES.encrypt(""+email+"/"+timestamp+"","flashprops");

    var link = hostname + "/resetpassword?link=" + phpjs.urlencode(encrypated);

    var mailOptions = {
        from: "Pollspace" + " <donotreply@pollspace.com>", // sender address
        to: email, // list of receivers
        subject: 'Forgot Password', // Subject line
        html: '<b> Please select this link '+ link +' to reset your password.</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log('MAILER ERROR');
            res.jsonp({'type' : 'error', 'message': 'There was an error sending your message. Please try again later.'});
        }else{
            console.log('MAILER SUCCESS');
            res.jsonp({'type' : 'success', 'message': 'An email has been sent to you to reset your password. Please visit your inbox for further instructions.'});
        }
    });
};

exports.getresetpassword = function(req, res){
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var link = query.link;

    req.session.forgotPasswordHash = link;
    res.redirect("/#!/resetpassword");
};

exports.postresetpassword = function(req, res){
    var hostname =  req.protocol + '://' + req.get('host');

    var hash = req.session.forgotPasswordHash;
    var decrypted = cryptojs.Crypto.AES.decrypt(hash, "flashprops");
    var list = decrypted.split("/");
    var email = list[0];
    var timestamp = parseInt(list[1]);
    var today = new Date().getTime();
    var password = req.body.password;

    if ( today >= timestamp  ){
        res.jsonp({'type' : 'error', 'message': "Your reset password has expired, please reset your password again."});
    } else {
        User.findOne({email: email})
            .exec(function(err, user) {
                if (err) {
                    res.jsonp({'type' : 'error', 'message': err.err});
                };

                if (user) {
                    user.password = password;
                    user.save();
                    res.jsonp({'type' : 'success', 'message': 'Thanks for visiting our website. Please login.'});
                }
            });
    }
};

exports.embeddedLogin = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.session.guid;

    async.series([
        function(callback){
            var viafo_server = config.viafo.server;
            var viafo_client_id = config.viafo.client_id;
            var viafo_client_secret = config.viafo.client_secret;

            var url = "/client/1/register.json?client_id=" + viafo_client_id + "&client_secret=" + viafo_client_secret   + "&uuid=" + new RandExp(/([A-Za-z0-9]){10}/i).gen();
            request(viafo_server + url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var jsonbody = JSON.parse(body);
                    var access_token = jsonbody.access_token;

                    req.session.access_token = access_token;
                    req.session.redirectTo = hostname + '/fb_login/embedded/callback';
                }

                callback(null);
            });
        }],
        function(err, results){
            var access_token = req.session.access_token;
            var flash_message = utils.getMessage(req);

            var data = {
                access_token: access_token,
                hostname: hostname,
                guid: guid,
                flash_message: flash_message,
                loginCallback: req.session.loginCallback,
                questionType: req.session.questionType
            };
            res.render('embedded/login', data, function(err, html) {
                res.jsonp(html);
            })
        });

}

exports.fbEmbeddedCallback = function(req, res){
    var hostname =  req.protocol + '://' + req.get('host');
    
    var data = {
        hostname: hostname
    };

    res.render('embedded/fb_callback', data);
};

exports.fbLogin = function(req, res){
    console.log('SESSION BEFORE GOING TO FACEBOOK');
    console.log(req.protocol + '://' + req.get('host'));
    console.log(req.session);

    var hostname =  req.protocol + '://' + req.get('host');
    var access_token = req.params.access_token;

    console.log('access_token', access_token);

    var viafo_server = config.viafo.server;
    var viafo_return_url = config.viafo.return_url;
    var url = viafo_server +  "/auth/1/facebook?access_token=" + access_token + "&return_url=" + viafo_return_url + "&scope=email,user_relationships,user_relationship_details,user_location,user_hometown,user_birthday,user_interests,user_religion_politics";

    if ( _.isEmpty(req.session.redirectTo) ){
        req.session.redirectTo = hostname;
    }

    console.log('SETING REDIRECT TO');
    console.log(req.session.redirectTo);

    res.redirect(url);
};

exports.twitterCallback = function(req, res){
    var logged_user = req.user;

    var twitter_key = config.twitter.clientKey;
    var twitter_secret = config.twitter.clientSecret;
    var twitter_callback = config.twitter.callbackURL;
    var _qs = qs;
    console.log('SESSION AFTER GOING TO TWITTER');
    console.log(req.protocol + '://' + req.get('host'));    
    console.log('SESSION REDIRECT TO'); //This comes from index.js page from twitter request_token URL
    var redirectTo = req.session.redirectTo;
    console.log(redirectTo);


    var oauth = { 
        consumer_key: twitter_key, 
        consumer_secret: twitter_secret, 
        token: req.session.twitter_access_token, 
        token_secret: req.session.twitter_token_secret, 
        verifier: req.query.oauth_verifier
    };
    var url = 'https://api.twitter.com/oauth/access_token';
    request.post({ url:url, oauth:oauth }, function (error, response, body) {
        if ( !error && response.statusCode == 200 ){
            // ready to make signed requests on behalf of the user
            var perm_data = _qs.parse(body);
            var oauth = { 
                    consumer_key: twitter_key, 
                    consumer_secret: twitter_secret, 
                    token: perm_data.oauth_token, 
                    token_secret: perm_data.oauth_token_secret
                };

            var url = 'https://api.twitter.com/1.1/users/show.json'
            var data =
                { 
                    screen_name: perm_data.screen_name, 
                    user_id: perm_data.user_id
                };
            request.get( {url:url, oauth:oauth, qs: data, json:true }, function (error, response, twitter_user) {
                if ( !error && response.statusCode == 200 ){
                    console.log('twitter user');
                    console.log(twitter_user);

                    if ( logged_user ){
                        console.log('logged_user');
                        console.log(logged_user);

                        logged_user.twitter_data = {};
                        logged_user.twitter_data.id = twitter_user.id_str;
                        logged_user.twitter_data.name = twitter_data.name;
                        logged_user.twitter_data.email = twitter_user.screen_name + "@user-pollspace-twitter.com";
                        logged_user.twitter_data.screen_name = twitter_user.screen_name;
                        logged_user.twitter_data.location = twitter_user.location;
                        logged_user.twitter_data.followers_count = twitter_user.followers_count;
                        logged_user.twitter_data.friends_count = twitter_user.friends_count;
                        logged_user.twitter_data.profile_picture = twitter_user.profile_image_url;
                        logged_user.save();

                        res.redirect(redirectTo);
                    } else {
                        User.findOne({'twitter_data.email' : twitter_user.screen_name + "@user-pollspace-twitter.com" }) //this is being added because twitter doesnt get the email back. so we need to add an email address when signin
                            .populate('subscriptions')
                            .populate('props')
                            .exec(function(err, user) {
                                if (err) {
                                    console.log('REDIRECTING TO...');
                                    console.log(redirectTo + '/#!/login');
                                    console.log(err);
                                    utils.setErrorMessage(req, err.err);
                                    res.redirect(redirectTo + '/#!/login');
                                };

                                if (user) {
                                    req.logIn(user, function(err) {
                                        if (err) {
                                            console.log('REDIRECTING TO...');
                                            console.log(redirectTo + '/#!/login');
                                            console.log(err);
                                            utils.setErrorMessage(req, err.err);
                                            res.redirect(redirectTo + '/#!/login');

                                        } else {
                                            console.log('REDIRECTING TO...');
                                            console.log(redirectTo);
                                            res.redirect(redirectTo);
                                        }
                                    });
                                } else {
                                    var user = new User();
                                    user.firstname = twitter_user.name;
                                    user.lastname = "."; //need to put something couse is required and twitter doesnt get the lastname back
                                    user.email = twitter_user.screen_name + "@user-pollspace-twitter.com";
                                    user.twitter_data.id = twitter_user.id_str;
                                    user.twitter_data.name = twitter_data.name;
                                    user.twitter_data.email = twitter_user.screen_name + "@user-pollspace-twitter.com";
                                    user.twitter_data.screen_name = twitter_user.screen_name;
                                    user.twitter_data.location = twitter_user.location;
                                    user.twitter_data.followers_count = twitter_user.followers_count;
                                    user.twitter_data.friends_count = twitter_user.friends_count;
                                    user.twitter_data.profile_picture = twitter_user.profile_image_url;

                                    user.provider = 'local';
                                    user.password = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                                    user.is_admin = false;
                                    console.log(user);
                                    user.save(function(err){
                                        if (err) {
                                            console.log('REDIRECTING TO...');
                                            console.log(redirectTo + '/#!/login');
                                            console.log(err);
                                            utils.setErrorMessage(req, err.err);
                                            res.redirect(redirectTo + '/#!/login');

                                        } else {
                                            req.logIn(user, function(err) {
                                                if (err) {
                                                    console.log('REDIRECTING TO...');
                                                    console.log(redirectTo + '/#!/login');
                                                    console.log(err);
                                                    utils.setErrorMessage(req, err.err);
                                                    res.redirect(redirectTo + '/#!/login');

                                                } else {
                                                    console.log('REDIRECTING TO...');
                                                    console.log(redirectTo);
                                                    res.redirect(redirectTo);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                    }
                }
            });
        }
    })



    //res.redirect(redirectTo);
};


exports.facebookConnect = function(req, res, next){


};

exports.twitterConnect = function(req, res, next){

};

exports.viafoLogin = function(req, res){
    var logged_user = req.user;

    console.log('SESSION AFTER GOING TO FACEBOOK');
    console.log(req.protocol + '://' + req.get('host'));
    console.log(req.session);

    console.log('SESSION REDIRECT TO');
    var redirectTo = req.session.redirectTo;
    
    var access_token = req.session.access_token;
    if ( access_token ){

        console.log('SESSION REDIRECT TO 2');
        console.log(redirectTo);

        var viafo_server = config.viafo.server;
        var viafo_return_url = config.viafo.return_url;
        var url = "/client/1/get_services.json?access_token=" + access_token + "&return_url=" + viafo_return_url + "&auth_info=true";

        request(viafo_server + url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var jsonbody = JSON.parse(body);
                console.log('viafologin body');
                console.log(jsonbody);
                
                var auth_info = jsonbody.authenticated;
                console.log('auth_info');
                console.log(auth_info);

                if ( !_.isEmpty(auth_info) ){
                    auth_info.forEach(function(service, key){
                        console.log('service');
                        console.log(service);
                        if( service['name'] == 'facebook') {
                            console.log('service facebook fb_id');
                            console.log(service['auth_info']['fb_id']);
                            var fb_id = service['auth_info']['fb_id'];
                            var facebookurl = viafo_server + "/client/1/proxy/Facebook/graph.facebook.com/get.json?access_token=" + access_token + "&_path=/me";
                            request(facebookurl, function (error, response, body) {
                            console.log('call to facebook API');
                            console.log(error);
                            console.log(body);
                            if (!error && response.statusCode == 200) {
                                var facebook_data = JSON.parse(body);
                                console.log('Facebook Data');
                                console.log(facebook_data);

                                if ( logged_user ){
                                    logged_user.facebook_data = {};
                                    logged_user.facebook_data.id = facebook_data.id;
                                    logged_user.facebook_data.gender = facebook_data.gender;
                                    logged_user.facebook_data.locale = facebook_data.locale;//'en_US',
                                    logged_user.facebook_data.birthday = facebook_data.birthday;
                                    logged_user.facebook_data.relationship_status = facebook_data.relationship_status;
                                    if ( facebook_data.email ){
                                        logged_user.facebook_data.email = facebook_data.email;
                                    } else {
                                        logged_user.facebook_data.email = facebook_data.username + "@user-pollspace-facebook.com";
                                    }

                                    if ( facebook_data.hometown ){
                                        logged_user.facebook_data.hometown = facebook_data.hometown.name;
                                    }
                                    if ( facebook_data.location ){
                                        logged_user.facebook_data.location = facebook_data.location.name;
                                    }
                                    // logged_user.facebook_data.relationship_status = facebook_data.relationship_status;

                                    if ( facebook_data.birthday ){
                                        var dob = new Date(facebook_data.birthday);
                                        var ageDifMs = new Date().getTime() - dob.getTime();
                                        var ageDate = new Date(ageDifMs); // miliseconds from epoch
                                        logged_user.facebook_data.age = Math.abs(ageDate.getUTCFullYear() - 1970);
                                    }
                                    logged_user.save();

                                    res.redirect(redirectTo);

                                } else {
                                    User.findOne({ $or:[ {'email':facebook_data.email}, {'facebook_data.email':facebook_data.email} ]})
                                        .populate('subscriptions')
                                        .populate('props')
                                        .exec(function(err, user) {
                                            if (err) {
                                                console.log('REDIRECTING TO...');
                                                console.log(redirectTo + '/#!/login');
                                                console.log(err);
                                                utils.setErrorMessage(req, err.err);
                                                res.redirect(redirectTo + '/#!/login');
                                            };

                                            if (user) {
                                                req.logIn(user, function(err) {
                                                    if (err) {
                                                        console.log('REDIRECTING TO...');
                                                        console.log(redirectTo + '/#!/login');
                                                        console.log(err);
                                                        utils.setErrorMessage(req, err.err);
                                                        res.redirect(redirectTo + '/#!/login');

                                                    } else {
                                                        console.log('REDIRECTING TO...');
                                                        console.log(redirectTo);
                                                        res.redirect(redirectTo);
                                                    }
                                                });
                                            } else {
                                                var user = new User();
                                                var email;
                                                user.firstname = facebook_data.first_name;
                                                user.lastname = facebook_data.last_name;
                                                if ( facebook_data.email ){
                                                    email = facebook_data.email;
                                                } else {
                                                    email = facebook_data.username + "@user-pollspace-facebook.com";
                                                }
                                                user.email = email;

                                                user.facebook_data.id = facebook_data.id;
                                                user.facebook_data.gender = facebook_data.gender;
                                                user.facebook_data.locale = facebook_data.locale;//'en_US',
                                                user.facebook_data.birthday = facebook_data.birthday;
                                                user.facebook_data.relationship_status = facebook_data.relationship_status;
                                                user.facebook_data.email = email;

                                                if ( facebook_data.hometown ){
                                                    user.facebook_data.hometown = facebook_data.hometown.name;
                                                }
                                                if ( facebook_data.location ){
                                                    user.facebook_data.location = facebook_data.location.name;
                                                }
                                                // user.facebook_data.relationship_status = facebook_data.relationship_status;

                                                if ( facebook_data.birthday ){
                                                    var dob = new Date(facebook_data.birthday);
                                                    var ageDifMs = new Date().getTime() - dob.getTime();
                                                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                                                    user.facebook_data.age = Math.abs(ageDate.getUTCFullYear() - 1970);
                                                }
                                                //we are missing: email, age, relationship status and location
                                                user.provider = 'local';
                                                user.password = new RandExp(/([A-Za-z0-9]){6}/i).gen();
                                                user.is_admin = false;
                                                console.log(user);
                                                user.save(function(err){
                                                    if (err) {
                                                        console.log('REDIRECTING TO...');
                                                        console.log(redirectTo + '/#!/login');
                                                        console.log(err);
                                                        utils.setErrorMessage(req, err.err);
                                                        res.redirect(redirectTo + '/#!/login');

                                                    } else {
                                                        req.logIn(user, function(err) {
                                                            if (err) {
                                                                console.log('REDIRECTING TO...');
                                                                console.log(redirectTo + '/#!/login');
                                                                console.log(err);
                                                                utils.setErrorMessage(req, err.err);
                                                                res.redirect(redirectTo + '/#!/login');

                                                            } else {
                                                                console.log('REDIRECTING TO...');
                                                                console.log(redirectTo);
                                                                res.redirect(redirectTo);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }

                            });
                        } else if(service['name'] == 'twitter') {
                            //$tw_profile = $service['auth_info'];
                            //AuthenticationController::getTwitterUser($tw_profile);
                        }
                    });
                } else {
                    console.log('REDIRECTING TO...');
                    console.log(redirectTo + '/#!/login');
                    console.log('Error from Viafo. Authenticated is empty.');
                    res.redirect(redirectTo + '/#!/login');
                }
            }
        });
    }
};

/**
 * Session
 */
exports.session = function(req, res) {

    var guid = utils.getGuid(req, res);
    var user = req.user;
    console.log('USER from LOGIN SESSION');
    console.log(user);
    if ( user.is_admin  ){
        if (_.isEmpty(guid) ){
            Channel.find({ deleted: false, admins: {$in: [user._id]} }).exec(function(err, channels) {
                if (_.isEmpty(channels)) {
                    res.redirect('/#!/admin/channel/create');
                } else {
                    if ( channels.length === 1){
                        var channel = channels[0];
                        res.redirect('/admin/channel/'+ channel._id);
                    } else {
                        res.redirect('/#!/admin/channel/list');
                    }
                }
            });
        } else {
            //Channel.findOne({ deleted: false,  guid: guid, admins: {$in: [user._id]} }).exec(function(err, channel) {
            Channel.findOne({ deleted: false,  guid: guid }).exec(function(err, channel) {
                if ( !_.isEmpty(channel)) {
                    channel.admins.contains(user._id, function(found) {
                        if (found) {
                            res.redirect('/admin/channel/'+ channel._id);
                        } else {
                            res.render('error', {
                                status: 404,
                                error: 'User not authorized to access this channel.'
                            });
                        }
                    });
                } else {
                    res.redirect('/#!/admin/channel/create');
                }
            });
        }

    } else {
        if ( req.session.redirectTo ){
            console.log('user come for voting or quiz');
            console.log(req.session.redirectTo);
            res.redirect(req.session.redirectTo);
        } else {
            var guid = utils.getGuid(req, res);
            if ( _.isEmpty(guid) ){
                console.log('user my/channels');
                res.redirect('/my/channels');
            } else {
                Channel.findOne({ deleted: false, guid: guid }).exec(function(err, channel) {
                    if ( _.isEmpty(channel) ) {
                        console.log('user my/channels');
                        res.redirect('/my/channels');
                    } else {
                        console.log('user channel ' + channel.name);
                        if (_.isEmpty(channel.default_page) ){
                            console.log('user my/channels');
                            res.redirect('/my/channels');
                        } else {
                            // if ( channel.default_page ){
                            //     console.log(channel.default_page);
                            //     res.redirect('/#!/' + channel.default_page );
                            // } else {
                            //     console.log(channel.default_page);
                            //     res.redirect('/#!/poll/list');
                            // }
                            res.redirect('/#!/final/four');
                        }
                    }
                });
            }
        }
    }

};

/**
 * Create user
 */
exports.create = function(req, res, next) {
    var user = new User(req.body);

    //user.username = user.email;
    user.provider = 'local';
    user.save(function(err) {
        if (err) {
            return res.send('/', {
                errors: err.errors,
                user: user
            });
        } else {
            req.logIn(user, function(err) {
                if (err) return next(err);
                res.jsonp(user);
            });
        }
    });
};

exports.createNoLogin = function(req, res, next) {
    var user = new User(req.body);
    var guid = utils.getGuid(req, res);

    //user.username = user.email;
    user.provider = 'local';
    user.save(function(err) {
        if (err) {
            return res.send('/', {
                errors: err.errors,
                user: user
            });
        } else {
            if ( _.isEmpty(guid) ){
                res.jsonp(user);
            } else {
                if ( !user.is_admin ){
                    Channel.findOne({ guid: guid } , function(error, _channel){
                        _channel.subscribers.push(user);
                        _channel.save();

                        User.findOne({ _id: user._id } , function(error, _user){
                            _user.subscriptions.push(_channel);
                            _user.save();

                            res.jsonp(_user);
                        });
                    });
                } else {
                    res.jsonp(user);
                }
            }
        }
    });
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

/**
 * Show User Data
 */
exports.show = function(req, res) {
    Share.find({ user: req.user._id}).exec(function(err, shares) {
        if (err) {
            return res.jsonp( {"message" : "There is been an error in fetching data"});
        }

        var results = {};
        results.user = req.user;
        results.shares = shares;
        res.jsonp(results);
    });
};

exports.all = function(req, res) {
    User.find().sort('-created').exec(function(err, users) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(users);
        }
    });
};

exports.adminList = function(req, res){
    User.find({ is_admin: true })
        .sort('-created')
        .exec(function(err, users) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(users);
        }
    });
};

exports.subscribeChannelList = function(req, res) {
    var user = req.user;
    Channel.find({ deleted: false, subscribers: { "$in" : [user]}  }).sort('-created').exec(function(err, channels) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(channels);
        }
    });
};

exports.suggestedChannelList = function(req, res) {
    var user = req.user;
    Channel.find({ deleted: false, subscribers: { "$nin" : [user]} }).sort('-created').exec(function(err, channels) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(channels);
        }
    });
}

exports.votedPropsList = function(req, res) {
    var user = req.user;
    User.findOne({_id: user._id}).populate('props').sort('-created').exec(function(err, user) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(user.props);
        }
    });
};


exports.subscribeToChannel = function(req, res) {
    var channel = req.channel;
    var user = req.user;
    if ( !channel.isSubscribed(user._id) ){
        var channelReturn = channel.update({ $push: { subscribers: user }},
            { safe: true, upsert: true },
            function( err, channels ) {
                if (err) {
                    res.jsonp( {"message" : "There is been an error in subscribing to channel"});
                } else {
                    res.jsonp( {"message" : "User has been Successfully Subscribed to channel"});
                }
            });
    } else {
        res.jsonp({"message" : "User is already Subscribed to this channel."});
    }

};


/**
 * Update User
 */
exports.update = function(req, res) {
    var user = req.user;
    user = _.extend(user, req.body);

    user.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                user: user
            });
        } else {
            res.jsonp(user);
        }
    });
};

/**
 * User Rankings
 */
exports.ranking = function(req, res) {
    var user = req.user;
    var rankings = [];
    //https://github.com/caolan/async#each
    async.eachSeries(user.subscriptions, function(channel, callback) {
        var rank = {};
        //We need to find all channel rows order DESC by points so we will know where the user stands according to points
        ChannelLeaderboard.find({channel: channel}).populate('user').populate('channel').sort('-points').exec(function(err, channelleaderboardlist) {
            //now we need to find where the user stands in that channel
            var position = 0;
            var userfound = false;
            async.eachSeries(channelleaderboardlist, function(channelleaderboard, callback){
                position++;
                if ( channelleaderboard.user && channelleaderboard.user._id.equals(user._id)){
                    userfound = true;
                    rank.channel_name = channelleaderboard.channel.name;
                    rank.channel_logo = channelleaderboard.channel.logo;
                    rank.channel_guid = channelleaderboard.channel.guid;
                    rank.currentranking = position;
                    rank.currentpoints = channelleaderboard.current_points;
                    callback();
                }
            });

            if ( !userfound ){
                rank.currentranking = position+1;
                rank.currentpoints = 0;
            }
            rank.currentrankingtotal = channelleaderboardlist.length;
            callback();
        });
        rank.alltimepoints = user.number.total_points;
        rankings.push(rank);

    }, function(err){
        res.jsonp(rankings);
    });
};


/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
    User.findOne({
            _id: id
        })
        .populate("subscriptions props")
        .exec(function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('Failed to load User ' + id));
            req.profile = user;
            req.user = user;
            next();
        });
};

/**
 * Find Channel by id
 */
exports.channel = function(req, res, next, id) {
    Channel.load(id, function(err, channel) {
        if (err) return next(err);
        if (!channel) return next(new Error('Failed to load channel ' + id));
        req.channel = channel;
        next();
    });
};

exports.autogenerate = function(req, res, next){

    for ( var i=0; i < 100; i++ ){
        var user_data = {};
        var autogeneratedString = new RandExp(/([a-z]){6}/i).gen();
        var autogeneratedGender = new RandExp(/(male|female|other)/).gen();
        var autogeneratedAge = new RandExp(/([1-9][0-9])/).gen();
        var autogeneratedLocation = new RandExp(/([0-9]){5}/i).gen();
        var autogeneratedRelationship = new RandExp(/(single|married|other|null)/).gen();

        user_data.firstname = autogeneratedString;
        user_data.lastname = autogeneratedString;
        //user_data.username = autogeneratedString + "@flashprops.com";
        user_data.email = autogeneratedString + "@flashprops.com";
        user_data.password = autogeneratedString;
        user_data.provider = 'local';
        user_data.is_admin = false;

        user_data.facebook_data = {};
        user_data.facebook_data.gender = autogeneratedGender;
        user_data.facebook_data.age = autogeneratedAge;
        user_data.facebook_data.location = autogeneratedLocation;
        user_data.facebook_data.relationship_status = (autogeneratedRelationship == "null" ? null : autogeneratedRelationship);

        var user = new User(user_data);
        console.log(user);
        user.save(function(err) {
            console.log(err);
        });

        //res.jsonp(user);
    }

    res.jsonp("Generation Complete!");
};

exports.searchDemograpchic = function(req, res, next){
    var gender =    (req.body.gender) ? req.body.gender : null;
    var age_from =  (req.body.age_from) ? req.body.age_from : null;
    var age_to =    (req.body.age_to) ? req.body.age_to : null;
    var relationship_status = (req.body.relationship_status) ? req.body.relationship_status : null;

    var search_data = {};
    //search_data['subscriptions'] = { "$in" : ['5340b768e0d6ae45630f8daa'] }

    if ( gender ){
        search_data['facebook_data.gender'] = gender;
    }

    if ( age_from !== null && age_to !== null ) {
        search_data['facebook_data.age'] = { $gte: age_from, $lte: age_to };
    } else {
        if ( age_from ) {
            search_data['facebook_data.age'] = { $gte: age_from };
        } else {
            if ( age_to ) {
                search_data['facebook_data.age'] = { $lte: age_to };
            }
        }
    }
    if ( relationship_status ){
        search_data['facebook_data.relationship_status'] = relationship_status;
    }

    User.find(search_data).exec(function(err, users) {
        if (err) {
            res.jsonp([err]);
        } else {
            console.log(users);
            res.jsonp(users);
        }
    });
};

exports.searchDemograpchicByQuery = function(req, res, next){

    User.query(req.query, function(error, data){
        if (error) {
            res.jsonp(error);
        } else {
            res.jsonp(data);
        }
    });

    //https://www.npmjs.org/package/mongoose-query
    //http://www.myserver.com/query?[q=<query>][&t=<type>]
    // [&f=<fields>]
    // [&s=<order>]
    // [&sk=<skip>]
    // [&l=<limit>]
    // [&p=<populate>]
    // [&fl=<boolean>]
    // [&map=<mapFunction>]
    // [&reduce=<reduceFunction>]


    /*
    *  http://localhost:3000/api/fp/search/demographic?q={"facebook_data.gender":"male", "facebook_data.age":{"$gte": 13}}&f=facebook_data&t=find
    * OR
    *
    * http://localhost:3000/api/fp/search/demographic?q={"facebook_data.gender":"male", "facebook_data.age":{"$gte": 13, "$lte": 60}}&f=facebook_data&t=find
    * */

}

