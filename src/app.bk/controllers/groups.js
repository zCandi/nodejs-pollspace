'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    async = require('async'),
    Group = mongoose.model('Group'),
    User = mongoose.model('User'),
    _ = require('lodash');

/**
 * Find Channel by id
 */
exports.group = function(req, res, next, id) {
    Group.load(id, function(err, group) {
        if (err) return next(err);
        if (!group) return next(new Error('Failed to load group ' + id));
        req.group = group;
        next();
    });
};

/**
 * Create an Channel
 */
exports.create = function(req, res) {
    var group = new Group(req.body);
    var user = req.user;
    group.deleted = false;
    group.admin = user;
    group.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                group: group
            });
        } else {
            res.jsonp(group);
        }
    });
};

/**
 * Update an Channel
 */
exports.update = function(req, res) {
    var group = req.group;
    group = _.extend(group, req.body);
    group.timestamps.updated = new Date().getTime();

    group.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                group: group
            });
        } else {
            res.jsonp(group);
        }
    });
};

exports.deactivate = function(req, res) {
    var group = req.group;

    group = _.extend(group, req.body);
    group.deleted = true;
    group.timestamps.deleted = new Date().getTime();

    group.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                group: group
            });
        } else {
            res.jsonp(group);
        }
    });
};

/**
 * Show an channel
 */
exports.show = function(req, res) {
    res.jsonp(req.group);
};

exports.all = function(req, res) {
    var user = req.user;
    Group.find({ deleted: false, admin: user }).populate('users').sort('-created').exec(function(err, groups) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(groups);
        }
    });
};

exports.allbytype = function(req, res) {
    res.jsonp(req.groups);
};

exports.bytype = function(req, res, next, type) {
    var user = req.user;
    Group.find({ deleted: false, admin: user, type: type }).populate('users').sort('-created').exec(function(err, groups) {
        if (err) return next(err);
        if (!groups) return next(new Error('Failed to load groups ' + type));
        req.groups = groups;
        next();
    });
};