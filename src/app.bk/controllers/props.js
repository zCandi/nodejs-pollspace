'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Prop    = mongoose.model('Prop'),
    Channel = mongoose.model('Channel'),
    Share = mongoose.model('Share'),
    GlobalLeaderboard = mongoose.model('GlobalLeaderboard'),
    ChannelLeaderboard = mongoose.model('ChannelLeaderboard'),
    config = require(__dirname + '/../../config/config'),
    Votes = mongoose.model('Votes'),
    User    = mongoose.model('User'),
    Photo    = mongoose.model('Photo'),
    users = require('../controllers/users'),
    helpers = require('view-helpers'),
    async = require('async'),
    uploadcare = require('uploadcare')('05817a84d61d7339a3b1', '253a61bad6416902b5dd'),
    fs = require('fs'),
    cryptojs = require('cryptojs'),
    RandExp = require('randexp'),
    phpjs = require('phpjs'),
    request = require('request'),
    utils = require(__dirname + '/../lib/utils'),
    _ = require('lodash');

exports.embeddedSingleQuestionTest = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.guid;

    var data = {
        hostname: hostname,
        guid: guid
    };

    res.render('embedded/single_test', data);
}

exports.renderSingleEmbedPoll = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.guid;

    req.session.loginCallback = '/prop/'+guid+'/embedded-single'
    req.session.questionType = 'single';

    res.contentType("text/javascript");
    res.render('embedsingle', {
        guid: req.params.guid,
        hostname: hostname,
        containerName: 'pss_container_' +  req.params.guid
    });
};


function userVote(prop, user){
    var flag = false;
    if ( user ){
        prop.votes.forEach(function(vote, key){
            var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
            if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                flag = true;
            }
        });
    }

    return flag;
};

function userAnswer(prop, user){
    var option = null;
    if ( user ){
        prop.votes.forEach(function(vote, key){
            var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
            if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                prop.answers.forEach(function(answer, key){
                    if ( _.isEqual(answer.id.toString(),  vote.choice.toString())){
                        option = answer;
                    }
                });
            }
        });
    }

    return option;
};

function userVoteOptionId(prop, user){
    var option = '';
    if ( user ){
        prop.votes.forEach(function(vote, key){
            var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
            if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                option = vote.choice;
            }
        });
    }

    return option;
};

function parseUserPropVote(prop, user){
    prop.userVote = userVote(prop, user);
    prop.userVoteOption = userVoteOptionId(prop, user);
    prop.userAnswer = userAnswer(prop, user);

    return prop;
}

exports.renderEmbeddedSingleQuestion = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.guid;
    var user = req.user;
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var prop = req.prop;

    req.session.guid = guid;

    if ( user ){
        prop = parseUserPropVote(prop, user);
        var data = {
            guid: guid,
            fullUrl: fullUrl,
            hostname: hostname,
            user: user ? JSON.stringify(user) : 'null',
            prop_json: prop ? JSON.stringify(prop) : 'null',
            prop: prop
        };

        res.render('embedded/single/view', data, function(err, html) {
            res.jsonp(html);
        });
    } else {
        users.embeddedLogin(req, res, next);
    }

}

exports.renderEmbeddedSingleQuestionResults = function(req, res, next){
    var hostname =  req.protocol + '://' + req.get('host');
    var guid = req.params.guid;
    var user = req.user;
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var prop = req.prop;

    if ( user ){
        prop = parseUserPropVote(prop, user);
        var data = {
            guid: guid,
            fullUrl: fullUrl,
            hostname: hostname,
            user: user ? JSON.stringify(user) : 'null',
            prop_json: prop ? JSON.stringify(prop) : 'null',
            prop: prop
        };

        res.render('embedded/single/results', data, function(err, html) {
            res.jsonp(html);
        });
    } else {
        users.embeddedLogin(req, res, next);
    }
};

exports.propp = function(req, res, next, id) {

    if ( req.user ){
        Prop.findOne({ _id: id })
            .populate('channel')
            .populate('suggested_by')
            .populate("votes.user", "_id facebook_data")
            .exec(function(err, prop) {
                if (!prop) return next(new Error('Failed to load prop ' + id));

                req.prop = prop;
                req.prop = parseUserPropVote(prop, req.user);
                req.channel = prop.channel;
                next();
            });
    } else {
        Prop.findOne({ _id: id })
            .populate('channel', '_id guid name logo picture')
            .populate('suggested_by')
            .exec(function(err, prop) {
                if (err) return next(err);
                if (!prop) return next(new Error('Failed to load prop ' + id));
                req.prop = prop;
                req.prop = parseUserPropVote(prop, req.user);
                req.channel = prop.channel;

                next();
            });
    }

};

exports.propguid = function(req, res, next, id) {

    Prop.findOne({ guid: id })
        .populate('channel')
        .populate("votes.user", "_id facebook_data")
        .populate('suggested_by')
        .exec(function(err, prop) {
            if (err) return next(err);
            if (!prop) return next(new Error('Failed to load prop with guid ' + id));
            req.prop = prop;
            req.prop = parseUserPropVote(prop, req.user);
            req.channel = prop.channel;
            next();
        });
};


function _getFileId(url, index) {
    if ( url ) {
        return url.replace(/^https?:\/\//, '').split('/')[index];
    }
}

exports.giveSuggestedPropUserPoints = function(_prop) {
    var prop = _prop;
    var user = _prop.suggested_by;
    var channel = _prop.channel;

    async.series([
        function(callback){ //increment user's global points based on global defaults >  GlobalLeaderboardSchema.points+5
            GlobalLeaderboard.findOne({user: user} , function(error, globalLeaderboard){
                console.log('GlobalLeaderboard');
                if(!error && globalLeaderboard == null){
                    globalLeaderboard  =  new GlobalLeaderboard();
                    globalLeaderboard.user  = user;
                    // globalLeaderboard.points = config.global.points;
                    // globalLeaderboard.current_points = config.global.points;

                    console.log('GlobalLeaderboard is null');
                    console.log(globalLeaderboard);

                } else {
                    globalLeaderboard.timestamps.update = new Date().getTime();
                    // globalLeaderboard.points            = globalLeaderboard.points + config.global.points;
                    // globalLeaderboard.current_points    = globalLeaderboard.current_points + config.global.points;
                    if ( prop.type == 'classic' ){
                        globalLeaderboard.actions.suggest_classic   = globalLeaderboard.actions.suggest_classic + 1;
                    } else {
                        globalLeaderboard.actions.suggest_opinion   = globalLeaderboard.actions.suggest_opinion + 1;
                    }

                    console.log(globalLeaderboard);
                }

                globalLeaderboard.save();


                /*User.findOne({_id: user._id} , function(error, _user){
                    if ( prop.type == 'classic' ){
                        _user.number.suggest_classic = globalLeaderboard.actions.suggest_classic;
                    } else {
                        _user.number.suggest_opinion = globalLeaderboard.actions.suggest_opinion;
                    }
                    _user.number.total_points = globalLeaderboard.points;
                    _user.save();

                    console.log('User');
                    console.log(_user);
                });*/

                callback(null);
            });
        },
        function(callback){//increment user's channel points based on channel's settings  > ChannelLeaderboardSchema.points + Channel.points.voting
            ChannelLeaderboard.findOne({user: user, channel: channel} , function(error, channelLeaderboard){
                console.log('ChannelLeaderboard');
                if(!error && channelLeaderboard == null){
                    channelLeaderboard          =  new ChannelLeaderboard();
                    channelLeaderboard.user     = user;
                    channelLeaderboard.channel  = channel;
                    if ( prop.type == 'classic' ){
                        channelLeaderboard.points   = channel.points.suggest_classic;
                        channelLeaderboard.current_points = channel.points.suggest_classic;
                    } else {
                        channelLeaderboard.points   = channel.points.suggest_opinion;
                        channelLeaderboard.current_points = channel.points.suggest_opinion;
                    }

                    console.log('ChannelLeaderboard is null');
                    console.log(channelLeaderboard);

                } else {
                    channelLeaderboard.timestamps.update = new Date().getTime();
                    if ( prop.type == 'classic' ){
                        channelLeaderboard.points           = channelLeaderboard.points + channel.points.share_classic;
                        channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.share_classic;
                        channelLeaderboard.actions.suggest_classic   = channelLeaderboard.actions.suggest_classic + 1;
                    } else {
                        channelLeaderboard.points           = channelLeaderboard.points + channel.points.share_opinion;
                        channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.share_opinion;
                        channelLeaderboard.actions.suggest_opinion   = channelLeaderboard.actions.suggest_opinion + 1;
                    }

                    console.log(channelLeaderboard);
                }

                User.findOne({_id: user._id} , function(error, _user){
                    if ( prop.type == 'classic' ){
                        _user.number.suggest_classic = channelLeaderboard.actions.suggest_classic;
                        _user.number.total_points = _user.number.total_points + channel.points.share_classic;
                    } else {
                        _user.number.suggest_opinion = channelLeaderboard.actions.suggest_opinion;
                        _user.number.total_points = _user.number.total_points + channel.points.share_classic;
                    }

                    _user.save();

                    console.log('User');
                    console.log(_user);
                });

                channelLeaderboard.save();

                callback(null);
            });
        }
    ]);
};

exports.gradeAllOpinionProps = function(req, res, next){
    Prop.find({ type: 'opinion' }).populate('votes.user').populate('channel').exec(function(err, props) {
        if (err) {
            console.log(err);
        }

        if ( props ){
            props.forEach(function(prop, key){
                exports.grade(prop);
            });

            next();
        }
    });
}

exports.grade = function(prop){
    if ( prop.type == 'classic' ){
        prop.votes.forEach(function(vote, key){
            var user = vote.user;
            var choice = vote.choice;
            var channel = prop.channel;
            var isCorrect = prop.isCorrect(choice);

            async.series([
                function(callback){ //increment user's global points based on global defaults >  GlobalLeaderboardSchema.points+5
                    GlobalLeaderboard.findOne({user: user} , function(error, globalLeaderboard){
                        if(!error && globalLeaderboard == null){
                            globalLeaderboard  =  new GlobalLeaderboard();
                            globalLeaderboard.user  = user;
                            // globalLeaderboard.points = config.global.points;
                            // globalLeaderboard.current_points = config.global.points;

                        } else {
                            globalLeaderboard.timestamps.update = new Date().getTime();
                            // globalLeaderboard.points            = globalLeaderboard.points + config.global.points;
                            // globalLeaderboard.current_points    = globalLeaderboard.current_points + config.global.points;
                            if ( isCorrect ){
                                globalLeaderboard.actions.correct   = globalLeaderboard.actions.correct + 1;
                            } else {
                                globalLeaderboard.actions.incorrect   = globalLeaderboard.actions.incorrect + 1;
                            }
                        }

                        globalLeaderboard.save();

                        console.log('GlobalLeaderboard');
                        console.log(globalLeaderboard);

                        /*User.findOne({_id: user._id} , function(error, _user){
                            if ( isCorrect ){
                                _user.number.correct = globalLeaderboard.actions.correct;
                                _user.correct_props.push(prop);
                            } else {
                                _user.number.incorrect = globalLeaderboard.actions.incorrect;
                                _user.incorrect_props.push(prop);
                            }
                            _user.number.total_points = globalLeaderboard.points;
                            _user.save();

                            console.log('User');
                            console.log(_user);
                        });*/

                        callback(null);
                    });
                },
                function(callback){//increment user's channel points based on channel's settings  > ChannelLeaderboardSchema.points + Channel.points.voting
                    ChannelLeaderboard.findOne({user: user, channel: channel} , function(error, channelLeaderboard){
                        if(!error && channelLeaderboard == null){
                            channelLeaderboard          =  new ChannelLeaderboard();
                            channelLeaderboard.user     = user;
                            channelLeaderboard.channel  = channel;
                            if ( isCorrect ){
                                channelLeaderboard.points   = channel.points.correct;
                                channelLeaderboard.current_points = channel.points.correct;
                            } else {
                                channelLeaderboard.points   = channel.points.incorrect;
                                channelLeaderboard.current_points = channel.points.incorrect;
                            }

                        } else {
                            channelLeaderboard.timestamps.update = new Date().getTime();
                            if ( isCorrect ){
                                channelLeaderboard.points           = channelLeaderboard.points + channel.points.correct;
                                channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.correct;
                                channelLeaderboard.actions.correct   = channelLeaderboard.actions.correct + 1;
                            } else {
                                channelLeaderboard.points           = channelLeaderboard.points + channel.points.incorrect;
                                channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.incorrect;
                                channelLeaderboard.actions.incorrect   = channelLeaderboard.actions.incorrect + 1;
                            }

                            console.log('ChannelLeaderboard');
                            console.log(channelLeaderboard);
                        }

                        User.findOne({_id: user._id} , function(error, _user){
                            if ( isCorrect ){
                                _user.number.correct = channelLeaderboard.actions.correct;
                                _user.number.total_points = _user.number.total_points + channel.points.correct;
                                _user.correct_props.push(prop);
                            } else {
                                _user.number.incorrect = channelLeaderboard.actions.incorrect;
                                _user.number.total_points = _user.number.total_points + channel.points.incorrect;
                                _user.incorrect_props.push(prop);
                            }
                            _user.save();

                            console.log('User');
                            console.log(_user);
                        });

                        channelLeaderboard.save();

                        callback(null);
                    });
                }
            ]);
        });
    } else { //will be also called by cron
        prop.votes.forEach(function(vote, key){
            var user = vote.user;
            var choice = vote.choice;
            var channel = prop.channel;

            async.series([
                function(callback){ //increment user's global points based on global defaults >  GlobalLeaderboardSchema.points+5
                    GlobalLeaderboard.findOne({user: user} , function(error, globalLeaderboard){
                        if(!error && globalLeaderboard == null){
                            globalLeaderboard  =  new GlobalLeaderboard();
                            globalLeaderboard.user  = user;
                            // globalLeaderboard.points = config.global.points;
                            // globalLeaderboard.current_points = config.global.points;
                        } else {
                            globalLeaderboard.timestamps.update = new Date().getTime();
                            // globalLeaderboard.points            = globalLeaderboard.points + config.global.points;
                            // globalLeaderboard.current_points    = globalLeaderboard.current_points + config.global.points;
                            globalLeaderboard.actions.completed   = globalLeaderboard.actions.completed + 1;
                        }

                        globalLeaderboard.save();

                        console.log('GlobalLeaderboard');
                        console.log(globalLeaderboard);

                        /*User.findOne({_id: user._id} , function(error, _user){
                            _user.number.completed = globalLeaderboard.actions.completed;
                            _user.completed_props.push(prop);

                            _user.number.total_points = globalLeaderboard.points;
                            _user.save();

                            console.log('User');
                            console.log(_user);
                        });*/

                        callback(null);
                    });
                },
                function(callback){//increment user's channel points based on channel's settings  > ChannelLeaderboardSchema.points + Channel.points.voting
                    ChannelLeaderboard.findOne({user: user, channel: channel} , function(error, channelLeaderboard){
                        if(!error && channelLeaderboard == null){
                            channelLeaderboard          =  new ChannelLeaderboard();
                            channelLeaderboard.user     = user;
                            channelLeaderboard.channel  = channel;
                            channelLeaderboard.points   = channel.points.completed;
                            channelLeaderboard.current_points = channel.points.completed;
                        } else {
                            channelLeaderboard.timestamps.update = new Date().getTime();
                            channelLeaderboard.points           = channelLeaderboard.points + channel.points.completed;
                            channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.completed;
                            channelLeaderboard.actions.completed   = channelLeaderboard.actions.completed + 1;
                        }

                        User.findOne({_id: user._id} , function(error, _user){
                            _user.number.completed = channelLeaderboard.actions.completed;
                            _user.completed_props.push(prop);

                            _user.number.total_points = _user.number.total_points + channel.points.completed;
                            _user.save();

                            console.log('User');
                            console.log(_user);
                        });

                        channelLeaderboard.save();

                        console.log('ChannelLeaderboard');
                        console.log(channelLeaderboard);

                        callback(null);
                    });
                }
            ]);
        });
    }
};

/**
 * Update an Channel
 */
exports.update = function(req, res) {
    var prop = req.prop;
    prop = _.extend(prop, req.body);
    prop.timestamps.updated = new Date().getTime();

    if ( req.body.status == 'suggested' && req.body.status_action == null ){
        prop.timestamps.created = new Date().getTime(); //it was a suggested prop and now the admin approved it.
        exports.giveSuggestedPropUserPoints(prop);
    }

    if ( req.body.status == 'suggested' && req.body.status_action == 'publish' ){
        prop.timestamps.created = new Date().getTime(); //it was a suggested prop and now the admin approved it.
        exports.giveSuggestedPropUserPoints(prop);
    }

    if ( req.body.status_action == 'publish' ) {
        prop.timestamps.published = new Date().getTime();
        prop.timestamps.unpublished = 0;
    }

    if ( req.body.status_action == 'unpublish' ) {
        prop.timestamps.unpublished = new Date().getTime();
        prop.timestamps.published = 0;
    }

    if ( req.body.status_action == 'close' ) {
        prop.timestamps.closed = new Date().getTime();
    }

    if ( req.body.status_action == 'grade' ) {
        prop.timestamps.graded = new Date().getTime();
        exports.grade(prop);
    }

    if ( prop.image ){
        var imageId = _getFileId(prop.image, 1);
    }

    if ( prop.imagebk ){
        var imageIdbk = _getFileId(prop.imagebk, 1);
    }

    async.series([
        function(callback){
            if ( imageId ){
                if ( imageIdbk != imageId ){
                    uploadcare.files.remove(imageIdbk, function(error, response) {
                        if(error) {
                            console.log('Error: ' + JSON.stringify(response));
                        } else {
                            console.log('Response: ' + JSON.stringify(response));
                        }
                    });
                };
            }
            callback(null);
        },
        function(callback){
            if ( imageId ){
                uploadcare.files.store(imageId, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;

                });
            }
            callback(null);
        }
    ],
    function(err, results){
        prop.save(function(err) {
            if (err) {
                return res.send('users/signup', {
                    errors: err.errors,
                    prop: prop
                });
            } else {
                res.jsonp(prop);
            }
        });
    });


};

exports.deactivate = function(req, res) {
    var prop = req.prop;

    prop = _.extend(prop, req.body);
    prop.deleted = true;
    prop.timestamps.deleted = new Date().getTime();

    prop.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                prop: prop
            });
        } else {
            res.jsonp(prop);
        }
    });
};

/**
 * Show a Prop
 */
exports.show = function(req, res) {
    var prop = req.prop;

    //tracking traffic access from
    var accessor = req.user._id + '-' + req.channel._id;
    accessor = accessor + '-' + req.session.ua.os.family.replace(/\s+/g, "-");
    accessor = accessor + '-' + req.session.ua.device.family.replace(/\s+/g, "-");
    accessor = accessor + '-' + req.session.ua.family.replace(/\s+/g, "-");

    if (!req.session[accessor]){
        req.session[accessor] = true;

        //tracking
        if (req.get('host') == req.get('referer').replace(/.*?:\/\/|\/$/g, "")) {        
            prop.counts.direct_from_browser += 1;             
        } else {
            if (req.session.ua.device.family != 'Other') {
                prop.counts.ext_embedded_app +=1;
            } 
            prop.counts.ext_embedded_url +=1;
        }

        prop.save(function(err) {
            if (err) 
                console.log('prop save err', err);
        });
    }

    res.jsonp(req.prop);
};

exports.voteUser = function(req, res){
    var prop = req.prop;
    var user = req.user;

    Votes.findOne({ user: user, prop: prop })
        .exec(function(err, vote) {
        if (err) return next(err);
        if ( vote ){
            res.jsonp({ data: true });
        } else {
            res.jsonp({ data: false });
        }
    });
};

exports.voteStats = function(req, res) {
    var user = req.user;
    var prop = req.prop;
    var choice = null;
    var prop_data = {
        stats: {},
        vote: false
    };

    if ( user ){
        prop.votes.forEach(function(item){
            if ( item.user && user && _.isEqual(item.user._id.toString(), user._id.toString()) ){
                choice = item.choice;
                prop_data.vote = true;
            }
        });

        if ( choice ){
            prop_data.total_votes = prop.votes.length;
            prop.answers.forEach(function(option){
                if ( _.isEqual(option._id.toString(), choice.toString()) ){
                    prop_data.vote_option = option.name;
                }
            });

        }
    }

    res.jsonp(prop_data);
};

exports.share = function(req, res, next) {
    var user = req.user;
    var prop = req.prop;

    Share.findOne({ user: user, prop: prop}).exec(function(err, share) {
        if (err) return next(err);
        res.jsonp(share);
    });
};

exports.findOtherPropsFromChannel = function(req, res) {
    var prop = req.prop;
    var channel = req.channel;

    Prop.find(
        {
            deleted: false,
            channel: req.channel,
            _id: { "$nin" : [prop._id]},
            type: prop.type,
            "$or": [
                { "timestamps.published" : { $ne: 0 } },
                { "timestamps.closed" : { $ne: 0 } },
                { "timestamps.graded" : { $ne: 0 } }
            ]
        })
        .select('_id guid image name votes.user timestamps')
        .populate('votes.user', '_id')
        .sort('-created')
        .exec(function(err, props) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(props);
        }
    });
}

/**
 * List of Props
 */
exports.all = function(req, res) {
    Prop.find({ deleted: false, channel: req.channel }).sort('-created').exec(function(err, props) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(props);
        }
    });
};

exports.vote = function(req, res){
    myLog('VOTE FUNCTION User: ' + req.user.email + ' - Prop: ' + req.prop.guid );
    var channel = req.channel;
    var user = req.user;
    var prop = req.prop;
    var choice = !_.isUndefined(req.body.choice) ? req.body.choice : req.params.answerid;
    
    myLog('req:', req);
    myLog('res:', res);

    Votes.findOne({user: user, prop: prop}).limit(1).exec(function(err, vote) {
        myLog('error', err);
        myLog('vote', vote);

        if (!err && vote == null) {
            async.series([
                function(callback){ //Subscribe to channel if its not subscribed yet
                    if ( !channel.isSubscribed(user._id) ){
                        myLog('is not subscribed');
                        Channel.findOne({_id: channel._id} , function(error, _channel){
                            _channel.subscribers.push(user);
                            _channel.save();
                        });

                        User.findOne({_id: user._id} , function(error, _user){
                            _user.subscriptions.push(channel);
                            _user.save();
                        });

                    };

                    callback(null);
                },
                function(callback){ // insert vote row into votes collection
                    myLog('adding new votes to Votes collection');
                    var votes = new Votes();
                    votes.user = user;
                    votes.prop = prop;
                    votes.choice = choice;
                    votes.save();

                    myLog('Counting + 1 in _prop.counts.vote');
                    Prop.findOne({_id: prop._id} , function(error, _prop){
                        if ( _prop.counts && _prop.counts.vote ){
                            _prop.counts.vote = _prop.counts.vote + 1;
                        } else {
                            _prop.counts.vote = 1;
                        }

                        _prop.save();
                    });

                    Prop.find({ channel: channel }).exec(function(err, props){
                        var sum = 0;
                        props.forEach(function(prop, propkey){
                            sum = sum + prop.counts.vote;
                        });

                        Prop.findOne({_id: prop._id} , function(error, _prop){
                            _prop.percentage.vote = (_prop.counts.vote * 100) / sum;
                            _prop.save();
                        });

                    });


                    callback(null);
                },
                function(callback){ //pushes prop votes with a new user
                    Prop.findOne({_id: prop._id} , function(error, _prop){
                        myLog('PUSHING USER TO PROP.VOTES - BEFORE', _prop);
                        _prop.votes.push({user: user, choice: choice});
                        _prop.save();
                        myLog('PUSHING USER TO PROP.VOTES - AFTER', _prop);
                    });

                    callback(null);
                },
                function(callback){//pushes user props with a new prop
                    User.findOne({_id: user._id} , function(error, _user){
                        _user.props.push(prop);
                        _user.save();
                        myLog('PUSHING VOTES TO USER.PROPS', _user);
                    });
                    //tuanbui added
                    /*User.findOne({_id: user._id} , function(error, _user){
                      //var prod_prop = Prop.findOne({type: 'product', channel: user.channel});
                       myLog("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!duplicate!!!!!", prop);
                       var randexp = new RandExp('([A-Za-z0-9]){16}', 'i'); 
                       prop.name = 'product_' + randexp.gen();
                        _user.props.push(prop);
                        _user.save();
                        myLog('PUSHING VOTE PRODUCT TO USER.PROPS', _user);
                    });*/
                    //tuanbui added
                    /*User.findOne({_id: user._id} , function(error, _user){
                      var prod_prop = new Prop();
                      var randexp = new RandExp('([A-Za-z0-9]){16}', 'i');

                      prod_prop.type = 'product';
                      prod_prop.channel = user.channel;
                      prod_prop.name = 'product_' + randexp.gen();
                      prod_prop.timestamps = { created: new Date().getTime(), published: new Date().getTime() };
                      
                      var prop2 = prod_prop.save();
                      myLog('save prod_prop!!!!!!!!!!!!!!!!!!!!!!!!', prop2);
                      
                        _user.props.push(prop2);
                        _user.save();
                        myLog('PUSHING VOTE PRODUCT TO USER.PROPS', _user);
                    });*/

                    callback(null);
                },
                function(callback){ //increment user's global points based on global defaults >  GlobalLeaderboardSchema.points+5
                    GlobalLeaderboard.findOne({user: user} , function(error, globalLeaderboard){
                        myLog('GlobalLeaderboard');
                        if(!error && globalLeaderboard == null){
                            globalLeaderboard  =  new GlobalLeaderboard();
                            globalLeaderboard.user  = user;
                            // globalLeaderboard.points = config.global.points;
                            // globalLeaderboard.current_points = config.global.points;

                            myLog('GlobalLeaderboard is null', globalLeaderboard);

                        } else {
                            globalLeaderboard.timestamps.update = new Date().getTime();
                            // globalLeaderboard.points            = globalLeaderboard.points + config.global.points;
                            // globalLeaderboard.current_points    = globalLeaderboard.current_points + config.global.points;
                            globalLeaderboard.actions.voting   = globalLeaderboard.actions.voting + 1;
                            myLog('GlobalLeaderboard is NOT null', globalLeaderboard);
                        }

                        User.findOne({_id: user._id} , function(error, _user){
                            _user.number.voting = globalLeaderboard.actions.voting;
                            _user.number.total_points = _user.number.total_points + globalLeaderboard.points;
                            _user.save();

                            console.log('User');
                            console.log(_user);
                        });

                        globalLeaderboard.save();

                        callback(null);
                    });
                },
                function(callback){//increment user's channel points based on channel's settings  > ChannelLeaderboardSchema.points + Channel.points.voting
                    ChannelLeaderboard.findOne({user: user, channel: channel} , function(error, channelLeaderboard){
                        myLog('ChannelLeaderboard');
                        if(!error && channelLeaderboard == null){
                            channelLeaderboard          =  new ChannelLeaderboard();
                            channelLeaderboard.user     = user;
                            channelLeaderboard.channel  = channel;
                            channelLeaderboard.points   = channel.points.voting;
                            channelLeaderboard.current_points = channel.points.voting;

                            myLog('ChannelLeaderboard is null', channelLeaderboard);

                        } else {
                            channelLeaderboard.timestamps.update = new Date().getTime();
                            channelLeaderboard.points           = channelLeaderboard.points + channel.points.voting;
                            channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.voting;
                            channelLeaderboard.actions.voting   = channelLeaderboard.actions.voting + 1;

                            myLog('ChannelLeaderboard is NOT null', channelLeaderboard);
                        }

                        User.findOne({_id: user._id} , function(error, _user){
                            _user.number.voting = channelLeaderboard.actions.voting;
                            _user.number.total_points = _user.number.total_points + channel.points.voting;
                            _user.save();

                            console.log('User');
                            console.log(_user);
                        });

                        channelLeaderboard.save();

                        callback(null);
                    });
                },
                function(callback){ //need to create a new Share row in shares documents
                    var share = new Share();
                    share.user = user;
                    share.prop = prop;
                    share.save();

                    myLog('Creating new Share', share);
                    callback(null, share);
                }
            ],// optional callback
            function(err, results){
                //5403ad5e2650e09c2cdd6246
                Prop.findOne({ _id: prop._id })
                    .populate('channel')
                    .populate("votes.user")
                    .exec(function(err, prop) {
                        if (err) return next(err);
                        myLog('RESULTS - NEW PROP', prop);

                        var hostname =  req.protocol + '://' + req.get('host');
                        req.session.redirectTo = hostname + '/#!/single/'+ prop.type + "/" + prop.guid; //used in case user needs to login
                        myLog('REDIRECT TO ', req.session.redirectTo);

                        prop = parseUserPropVote(prop, user);
                        res.jsonp({"message" : "User Vote Successfully", "prop": prop});
                    });
            });
        } else {
            myLog('USER ALREADY VOTE', prop);

            prop = parseUserPropVote(prop, user);
            res.jsonp({"message" : "The User has already vote this prop.", "prop": prop});
        }
    });


    //5403ad5e2650e09c2cdd6246
    /*Prop.findOne({ _id: prop._id })
        .populate('channel')
        .populate("votes.user")
        .exec(function(err, prop) {
            if (err) return next(err);
            res.jsonp({"message" : "User Vote Successfully", "prop": prop});
        });*/
};

exports.showprop = function(req, res) {
    var prop = req.prop;
    var user = req.user;

    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var hostname =  req.protocol + '://' + req.get('host');
    var optionsCount = 0;

    async.series([
        function(callback){
            Prop.findOne({_id: prop._id} , function(error, _prop){
                if ( _prop.counts && _prop.counts.view ){
                    _prop.counts.view = _prop.counts.view + 1;
                } else {
                    _prop.counts.view = 1;
                }

                _prop.save();
            });

            Prop.find({ channel: prop.channel }).exec(function(err, props){
                var sum = 0;
                props.forEach(function(prop, propkey){
                    sum = sum + prop.counts.view;
                });

                Prop.findOne({_id: prop._id} , function(error, _prop){
                    _prop.percentage.view = (_prop.counts.view * 100) / sum;
                    _prop.save();
                });

            });

            callback(null);
        },
        function(callback){
            if ( user && _.isEmpty(user.props) ){ //Means user didnt vote. FIRST time user. New Voter
                Prop.findOne({_id: prop._id} , function(error, _prop){
                    if ( _prop.counts && _prop.counts.new_voters ){
                        _prop.counts.new_voters = _prop.counts.new_voters + 1;
                    } else {
                        _prop.counts.new_voters = 1;
                    }

                    if ( _prop.percentage && !_prop.percentage.new_voters ){
                        _prop.percentage = {};
                        _prop.percentage.new_voters = 0;
                    }
                    _prop.save();
                });

                Prop.find({ channel: prop.channel }).exec(function(err, props){
                    var sum = 0;
                    props.forEach(function(prop, propkey){
                        sum = sum + prop.counts.new_voters;
                    });

                    Prop.findOne({_id: prop._id} , function(error, _prop){
                        _prop.percentage.new_voters = (_prop.counts.new_voters * 100) / sum;
                        _prop.save();
                    });
                });
            }

            callback(null);
        },
        function (callback){
            Share.findOne({ user: user, prop: prop}).exec(function(err, share) {
                if (err) return next(err);
                callback(null, share);
            });
        }
    ],
    function(err, results){
        prop.answers.forEach(function(option){
            if ( option.name ){
                optionsCount = optionsCount + 1;
            }
        });
        console.log('showprop');
        console.log(prop);
        var singleUrl = hostname + '/#!/single/'+ prop.type + "/" + prop.guid;
        if ( user ){ //means is logged in.. so we need to redirect
            res.redirect(singleUrl);
        } else {
            req.session.redirectTo = singleUrl;
            req.session.guid = prop.channel.guid;

            var data = {
                title: prop.name,
                prop_image: prop.image ? prop.image : hostname + "/img/fp/ps40x40.png",
                channel_picture: prop.channel.picture ? prop.channel.picture : hostname + "/img/fp/ps400x400.png",
                channel_logo: prop.channel.logo ? prop.channel.logo : hostname + "/img/fp/ps40x40.png",
                prop_guid: prop.guid,
                hostname: hostname,
                fullUrl: fullUrl,
                facebook_url: results && results[2] ? hostname + '/s/'+ results[2].guid : hostname + '/prop/'+ prop.guid,
                prop: prop,
                channel: prop.channel,
                optionsCount: optionsCount,
                colors: utils.getChannelColor(prop.channel),
                user: null
            };

            if ( prop.type == 'image' ){
                res.render('prop/view_image', data);
            } else {
                res.render('prop/view', data);
            }
        }
    });
};

exports.results = function(req, res) {
    var _prop = req.prop;
    Prop.findOne({ _id: _prop._id })
        .populate("votes.user", "facebook_data")
        .exec(function(err, prop) {
            if (err) {
                res.render('error', {
                    status: 500
                });
            } else {
                prop.answers.forEach(function(answer, answerKey){
                    answer.correct = null;
                });

                res.jsonp(prop);
            }
        });
}

exports.autogeneratevotes = function(req, res){

    var prop = req.prop;
    // myLog('prop',prop);

/*
    User.find().limit(10).exec(function(err, users) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            // res.jsonp(users);
            // myLog('users',users);
            users.forEach(function(member){
                myLog('member',member);
                var data = {
                                "channel": prop.channel,
                                "user": member,
                                "prop": prop,
                                "choice": Math.floor(Math.random()*4)
                            };
                exports.vote(data, res);
            });
        }
    });

*/

    res.jsonp({"message" : "This is the autogeneratevotes function"});

};

exports.getEmailHash = function(req, res){
    res.render('users/hash', {
        title: 'Get Hash'
    });
};

exports.postEmailHash = function(req, res){
    var date = new Date();
    date.setDate(date.getDate() + 7 );

    var email = req.body.email;
    var timestamp = date.getTime();
    var encrypated = cryptojs.Crypto.AES.encrypt(""+email+"/"+timestamp+"","flashprops");

    res.render('users/hash', {
        title: 'Get Hash',
        hash: phpjs.urlencode(encrypated)
    });
};

exports.getPropGuidHash = function(req, res){
    var guid = req.params.guid;
    var hash = req.params.hash;
    var prop = req.prop;

    //var encrypated = cryptojs.Crypto.AES.encrypt("user@gmail.com/1409250955011","flashprops");
    //14QW2uNxs%2BYmqafD3mHcNEnFVFkQ1LnBCNpUPbL%2BL1ELfkidCgY1KTwqh7Y%3D   > time passed existing user
    //cCCAqB%2BH3djI%2F8KN6Z8LPPKedl2w2NOmb4uR65dsJ3UCK%2FG4YujipYOlww0%3D > time has not passed existing user
    //1KMWiI5ezwwVT6d4IvpOCjRhywb70pNlXF%2FmeOjUu3lyRue5A8tAFyH70Q%3D%3D > new user

    var decrypted = cryptojs.Crypto.AES.decrypt(hash, "flashprops");
    var list = decrypted.split("/");
    var email = list[0];
    var timestamp = parseInt(list[1]);
    var today = new Date().getTime();
    var password = new RandExp(/([A-Za-z0-9]){6}/i).gen();

    if ( today >= timestamp  ){
        utils.setErrorMessage(req, "Your given user and password has expired, please signup.");
        res.redirect('/#!/signup');
    } else {
        User.findOne({email: email})
            .populate('subscriptions')
            .populate('props')
            .exec(function(err, user) {
                if (err) {
                    utils.setErrorMessage(req, err.err);
                    res.redirect('/#!/login');
                };

                if (user) {
                    utils.setSuccessMessage(req, "Thanks for visiting our website. Please login.");
                    res.redirect('/#!/login');
                } else {
                    var user = new User();
                    user.firstname = email;
                    user.lastname = "-";
                    user.email = email;
                    //user.username = email;
                    user.provider = 'local';
                    user.password = password;
                    user.is_admin = false;
                    user.save(function(err) {
                        if (err) {
                            utils.setErrorMessage(req, err.err);
                            res.redirect('/#!/login');
                        } else {
                            req.logIn(user, function(err) {
                                if (err) {
                                    utils.setErrorMessage(req, err.err);
                                    res.redirect('/#!/login');
                                } else {
                                    res.redirect("/#!/prop/" + prop.guid);
                                }
                            });
                        }
                    });
                }
            });
    }
};


function myLog(title, data) {
    console.log('-------------------------------');
    console.log(title);
    console.log('-------------------------------');
    if ( data ){
        console.log(data);
    }
};
