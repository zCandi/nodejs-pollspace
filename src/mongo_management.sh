#!/bin/bash

LOADING=false
DEBUG=/dev/null

usage()
{
    cat << EOF
    usage: $0 [options] <DBNAME>

    Example:
    1. Export Channel from a certain 'guid'
    sh mongo_management.sh -H c790.candidate.0.mongolayer.com:10790 -u production -p production -c channels -q "{guid: 'apimetrics'}” production

    2. Export props from a certain channel id
    sh mongo_management.sh -H c790.candidate.0.mongolayer.com:10790 -u production -p production -c props -q "{channel: ObjectId('54e7ca88390a4a147896cb68
')}}” production

    3. Import Channel
    sh mongo_management.sh -H kahana.mongohq.com:10011 -u development -p development -c channels -l development

    4. Import props
    sh mongo_management.sh -H kahana.mongohq.com:10011 -u development -p development -c props -l development

    5. No Query and No Collection. Will export everything and do a tar.gz
    sh mongo_management.sh -H c790.candidate.0.mongolayer.com:10790 -u production -p production production


    OPTIONS:
        -h      Show this help.
        -l      Load instead of export
        -u      Mongo username
        -p      Mongo password
        -c      Mongo Collection
        -H      Mongo host string (ex. localhost:27017)
        -q      Mongo query for export (JSON string)
        -L      Limit exported results to this amount
        -d      Show (debug) error messages in console
EOF
}

while getopts "hldu:c:p:H:q:L:" opt; do
    MAXOPTIND=$OPTIND

    case $opt in
        h)
            usage
            exit
            ;;
        l)
            LOADING=true
            ;;
        d)
            DEBUG=/dev/stderr
            ;;
        u)
            USERNAME="$OPTARG"
            ;;
        p)
            PASSWORD="$OPTARG"
            ;;
        H)
            HOST="$OPTARG"
            ;;
        q)
            QUERY="$OPTARG"
            ;;
        c)
            COLLECTION="$OPTARG"
            ;;
        L)
            LIMIT="$OPTARG"
            ;;
        \?)
            echo "Invalid option $opt"
            exit 1
            ;;
    esac
done

shift $(($MAXOPTIND-1))

if [ -z "$1" ]; then
    echo "Usage: $0 [options] <DBNAME>"
    exit 1
fi

DB="$1"
if [ -z "$HOST" ]; then
    HOST="localhost:27017"
    CONN="localhost:27017/$DB"
else
    CONN="$HOST/$DB"
fi

ARGS=""
if [ -n "$USERNAME" ]; then
    ARGS="-u $USERNAME"
fi
if [ -n "$PASSWORD" ]; then
    ARGS="$ARGS -p $PASSWORD"
fi

if $LOADING ; then
    echo "*************************** Mongo Import ************************"
    echo "**** Host:      $HOST"
    echo "**** Database:  $DB"
    echo "**** Username:  $USERNAME"
    echo "**** Password:  $PASSWORD"
    echo "*****************************************************************"

    if [[ ! -f $DB.tar.gz ] || [ ! -f $COLLECTION.tar.gz ]]; then
        echo "Archive $DB.tar.gz to import from could not be found! Aborting ..."
        exit
    fi

    tar -xzf $DB.tar.gz 2>$DEBUG
    pushd $DB 2>$DEBUG

    for path in *.json; do
        collection=${path%.json}
        echo "Loading into $DB/$collection from $path"
        mongoimport --host $HOST $ARGS -d $DB -c $collection $path --jsonArray
    done

    popd 2>$DEBUG
    rm -rf $DB 2>$DEBUG
else
    echo "*************************** Mongo Export ************************"
    echo "**** Host:        $HOST"
    echo "**** Database:    $DB"
    echo "**** Username:    $USERNAME"
    echo "**** Password:    $PASSWORD"
    echo "**** Query:       $QUERY"
    echo "**** Collection:  $COLLECTION"
    echo "**** Limit:       $LIMIT"
    echo "*****************************************************************"

    mkdir -p tmp/$DB 2>$DEBUG
    pushd tmp/$DB  2>$DEBUG

    if  [ "$COLLECTION" != "" ]; then
        echo "Exporting collection $COLLECTION ..."
        mongoexport --host $HOST $ARGS -db $DB -c $COLLECTION -q "$QUERY" -o $COLLECTION.json >$DEBUG
        if [ "$LIMIT" != "" ]; then
            head -n $LIMIT $COLLECTION.json > $COLLECTION.json.tmp && mv $COLLECTION.json.tmp $COLLECTION.json
            echo "limited result set to $LIMIT records"
        fi

        pushd .. 2>$DEBUG
        tar -czf "$COLLECTION.tar.gz" $DB 2>$DEBUG
        popd 2>$DEBUG
        popd 2>$DEBUG
        mv tmp/$COLLECTION.tar.gz ./ 2>$DEBUG
        rm -rf tmp 2>$DEBUG
    else
        DATABASE_COLLECTIONS=$(mongo $CONN $ARGS --quiet --eval 'db.getCollectionNames()' | sed 's/,/ /g')

        for collection in $DATABASE_COLLECTIONS; do
            echo "Exporting collection $collection ..."
            mongoexport --host $HOST $ARGS -db $DB -c $collection -q "$QUERY" -o $collection.json >$DEBUG
            if [ "$LIMIT" != "" ]; then
                head -n $LIMIT $collection.json > $collection.json.tmp && mv $collection.json.tmp $collection.json
                echo "limited result set to $LIMIT records"
            fi
        done

        pushd .. 2>$DEBUG
        tar -czf "$DB.tar.gz" $DB 2>$DEBUG
        popd 2>$DEBUG
        popd 2>$DEBUG
        mv tmp/$DB.tar.gz ./ 2>$DEBUG
        rm -rf tmp 2>$DEBUG
    fi


fi