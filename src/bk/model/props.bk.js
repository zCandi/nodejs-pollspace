'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    RandExp = require('randexp'),
    deepPopulate = require('mongoose-deep-populate'),
    _ = require('lodash'),
    Schema = mongoose.Schema;

var PropSchema = new Schema({
    guid: {
        type: String,
        index: { unique: true }
    },
    name: {
        type: String
    },
    details: {
        type: String
    },
    image: {
        type: String
    },
    suggested_by: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    answers:  [
        {
            id:  { type: String },
            name: { type: String },
            image: { type: String},
            correct: { type: Boolean },
            meta: {
                instagram: { value: { type: String } }
            }
        }
    ], /*I wasnt able to put options as a field, seems to be a reserved word*/
    correct_description:{
        type: String
    },
    channel: {
        type: Schema.ObjectId,
        ref: 'Channel'
    },
    tags: [{
        type: String
    }],
    type: { type: String, enum: ['classic', 'quiz','opinion', 'product', 'image']}, //classic: prop, quiz: quiz, opinion: poll
    deleted: { type: Boolean, default: false },
    hidden: { type: Boolean, default: false },
    votes:[{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        choice: { type: String },
        created: { type: Date, default: Date.now }
    }],
    counts: {
        vote: { type: Number, default: 0 },
        share: { type: Number, default: 0 },
        view: { type: Number, default: 0 },
        new_voters: { type: Number, default: 0 }
    },
    percentage:{
        vote: { type: Number, default: 0 },
        share: { type: Number, default: 0 },
        view: { type: Number, default: 0 },
        new_voters: { type: Number, default: 0 }
    },
    weekof: {
        from: { type: Date, default: 0 },
        to: { type: Date, default: 0 }
    },
    timestamps: {
        suggested: { type: Date, default: 0 },
        created: { type: Date, default: 0 },
        published: { type: Date, default: 0 },
        unpublished: { type: Date, default: 0 },
        distributed: { type: Date, default: 0 },
        closed: { type: Date, default: 0 },
        graded: { type: Date, default: 0 },
        updated: { type: Date, default: Date.now },
        deleted: { type: Date, default: 0 }
    },
    //this is just for displaying purposes, we are not using these fields in backend. The prop will hold the
    //voting information for the logged user. As logged user is in session, this data will always delivered the right
    //information for the selected prop.
    userVote: { type: Boolean },
    userVoteOption: { type: String },
    userAnswer: { type: Object }
});

PropSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).exec(cb);
};

/**
 * Pre-save hook
 */
PropSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    this.guid = new RandExp(/([A-Za-z0-9]){6}/i).gen();
    this.answers.forEach(function(item, key){
        if (_.isUndefined(item.id) ){
            item.id = new RandExp(/([A-Za-z0-9]){16}/i).gen();
        }
    });
    next();
});

PropSchema.statics.searchTagsByChannel = function(data, cb) {
    var channel_id = data.channel_id;

    var tagList =    (data.tagList) ? data.tagList : null;
    var search_data = {};

    if ( !_.isEmpty(tagList) ){
        search_data['channel'] = channel_id;
        search_data['hidden'] = false;
        search_data['tags'] = { "$in" : data.tagList };
    }

    this.find(search_data).populate("votes.user", "_id facebook_data").exec(cb);
};

PropSchema.statics.searchSharingByChannel = function(data, cb) {
    var channel_id = data.channel_id;

    var numberOfVotes_from =  (data.numberOfVotes_from) ? data.numberOfVotes_from : null;
    var numberOfVotes_to =    (data.numberOfVotes_to) ? data.numberOfVotes_to : null;
    var numberOfShares_from =  (data.numberOfShares_from) ? data.numberOfShares_from : null;
    var numberOfShares_to =    (data.numberOfShares_to) ? data.numberOfShares_to : null;
    var numberOfViews_from =  (data.numberOfViews_from) ? data.numberOfViews_from : null;
    var numberOfViews_to =    (data.numberOfViews_to) ? data.numberOfViews_to : null;
    var numberOfNewVoters_from = (data.numberOfNewVoters_from) ? data.numberOfNewVoters_from : null;
    var numberOfNewVoters_to = (data.numberOfNewVoters_to) ? data.numberOfNewVoters_to : null;

    var votePercent_from =  (data.votePercent_from) ? data.votePercent_from : null;
    var votePercent_to =    (data.votePercent_to) ? data.votePercent_to : null;
    var sharePercent_from =  (data.sharePercent_from) ? data.sharePercent_from : null;
    var sharePercent_to =    (data.sharePercent_to) ? data.sharePercent_to : null;
    var viewPercent_from =  (data.viewPercent_from) ? data.viewPercent_from : null;
    var viewPercent_to =    (data.viewPercent_to) ? data.viewPercent_to : null;
    var newVotersPerShare_from =  (data.newVotersPerShare_from) ? data.newVotersPerShare_from : null;
    var newVotersPerShare_to =    (data.newVotersPerShare_to) ? data.newVotersPerShare_to : null;

    var search_data = {};
    search_data['channel'] = channel_id;
    search_data['hidden'] = false;
    //#of votes
    if ( numberOfVotes_from !== null && numberOfVotes_to !== null ) {
        search_data['counts.vote'] = { $gte: numberOfVotes_from, $lte: numberOfVotes_to };
    } else {
        if ( numberOfVotes_from ) {
            search_data['counts.vote'] = { $gte: numberOfVotes_from };
        } else {
            if ( numberOfVotes_to ) {
                search_data['counts.vote'] = { $lte: numberOfVotes_to };
            }
        }
    }

    //#of shares
    if ( numberOfShares_from !== null && numberOfShares_to !== null ) {
        search_data['counts.share'] = { $gte: numberOfShares_from, $lte: numberOfShares_to };
    } else {
        if ( numberOfShares_from ) {
            search_data['counts.share'] = { $gte: numberOfShares_from };
        } else {
            if ( numberOfShares_to ) {
                search_data['counts.share'] = { $lte: numberOfShares_to };
            }
        }
    }

    //#of Views
    if ( numberOfViews_from !== null && numberOfViews_to !== null ) {
        search_data['counts.view'] = { $gte: numberOfViews_from, $lte: numberOfViews_to };
    } else {
        if ( numberOfViews_from ) {
            search_data['counts.view'] = { $gte: numberOfViews_from };
        } else {
            if ( numberOfViews_to ) {
                search_data['counts.view'] = { $lte: numberOfViews_to };
            }
        }
    }

    //#of New Voters
    if ( numberOfNewVoters_from !== null && numberOfNewVoters_to !== null ) {
        search_data['counts.new_voters'] = { $gte: numberOfNewVoters_from, $lte: numberOfNewVoters_to };
    } else {
        if ( numberOfNewVoters_from ) {
            search_data['counts.new_voters'] = { $gte: numberOfNewVoters_from };
        } else {
            if ( numberOfNewVoters_to ) {
                search_data['counts.new_voters'] = { $lte: numberOfNewVoters_to };
            }
        }
    }

    //--- Percentage
    //#of votes
    if ( votePercent_from !== null && votePercent_to !== null ) {
        search_data['percentage.vote'] = { $gte: votePercent_from, $lte: votePercent_to };
    } else {
        if ( votePercent_from ) {
            search_data['percentage.vote'] = { $gte: votePercent_from };
        } else {
            if ( votePercent_to ) {
                search_data['percentage.vote'] = { $lte: votePercent_to };
            }
        }
    }

    //#of Shares
    if ( sharePercent_from !== null && sharePercent_to !== null ) {
        search_data['percentage.share'] = { $gte: sharePercent_from, $lte: sharePercent_to };
    } else {
        if ( sharePercent_from ) {
            search_data['percentage.share'] = { $gte: sharePercent_from };
        } else {
            if ( sharePercent_to ) {
                search_data['percentage.share'] = { $lte: sharePercent_to };
            }
        }
    }

    //#of Views
    if ( viewPercent_from !== null && viewPercent_to !== null ) {
        search_data['percentage.view'] = { $gte: viewPercent_from, $lte: viewPercent_to };
    } else {
        if ( viewPercent_from ) {
            search_data['percentage.view'] = { $gte: viewPercent_from };
        } else {
            if ( viewPercent_to ) {
                search_data['percentage.view'] = { $lte: viewPercent_to };
            }
        }
    }

    //#of new voters
    if ( newVotersPerShare_from !== null && newVotersPerShare_to !== null ) {
        search_data['percentage.new_voters'] = { $gte: newVotersPerShare_from, $lte: newVotersPerShare_to };
    } else {
        if ( newVotersPerShare_from ) {
            search_data['percentage.new_voters'] = { $gte: newVotersPerShare_from };
        } else {
            if ( newVotersPerShare_to ) {
                search_data['percentage.new_voters'] = { $lte: newVotersPerShare_to };
            }
        }
    }

    console.log('searchSharingByChannel');
    console.log(search_data);
    this.find(search_data).populate("votes.user", "_id facebook_data").exec(cb);
};


PropSchema.methods = {
    getCorrectAnswer: function(){
        var correct = null;
        this.answers.forEach(function(item, key){
            if ( item.correct ){
                correct = item.id;
            }
        });

        return correct;
    },

    isCorrect: function( answer ) {
        var correct = this.getCorrectAnswer();
        if (correct && answer && _.isEqual(correct.toString(), answer.toString()) ){
            return true;
        }
        return false;
    }
};

PropSchema.plugin(deepPopulate);
mongoose.model('Prop', PropSchema);
