'use strict';

angular.module('mean').provider('Template', function() {
    // SECTION 1: code to initialize/configure the PROVIDER goes here (executed during `config` phase)
    this.getTemplate = function(template){
        var customTemplate = template.replace("views", "views/custom/" + window.cguid);
        if ( this.file_exists(customTemplate)){
            return customTemplate;
        } else {
            if (window.channel != null && window.channel.mode == 'image') {
                customTemplate = template.replace("views", "views/custom/" + window.cguid);
                if (this.file_exists(customTemplate))
                    return customTemplate;
                else if (this.file_exists(template.replace("views", "views/custom/bahd")))
                    return template.replace("views", "views/custom/bahd");
            }

            if ( window.subdomain && template == 'views/login.html' ){
                if ( this.file_exists("views/custom/" + window.cguid + "/subdomain_login.html")){
                    return 'views/custom/' + window.cguid + '/subdomain_login.html';
                } else if (window.channel != null && window.channel.mode == 'image') {
                    if ( this.file_exists("views/custom/bahd/subdomain_login.html")){
                        return 'views/custom/bahd/subdomain_login.html';
                    }
                } else {
                    return 'views/subdomain_login.html';
                }
            }
            return template;
        }
    }

    this.file_exists = function(url) {
        var fileList = window.customFolderList;
        var SHA1 = window.Crypto.SHA1;
        if ( _.indexOf(fileList, SHA1(url)) !== -1 ){
            return true
        }

        return false;
    }

    this.$get = function() {
        // code to initialize/configure the SERVICE goes here (executed during `run` stage)
        var service = {};
        service.getTemplate = function(template){
            return this.getTemplate(template);
        }
        return service;
    };
});


//Setting up route
angular.module('mean').config(['$stateProvider', '$urlRouterProvider', '$httpProvider','TemplateProvider',
    function($stateProvider, $urlRouterProvider, $httpProvider, Template) {
        // interceptors
        $httpProvider.interceptors.push('myHttpResponseInterceptor');

        /*$routeProvider.when('channels', {templateUrl: 'views/channels/list.html', controller: 'ChannelsController'});*/
        /* prepend with: http://162.243.228.248:4000/#!/ to view on server */
        $urlRouterProvider.otherwise('/');

        // states for my app
        $stateProvider

            //ADMIN USER
            //Admin home
            .state('admin_home', {
                url: '/admin',
                templateUrl: Template.getTemplate('views/admin/index.html')
            })

            //Channels
            .state('admin_list_channels', {
                url: '/admin/channel/list',
                templateUrl: Template.getTemplate('views/admin/channel/list.html'),
                data:{
                    activeTab:  "channel_list"
                }
            })
            .state('admin_channel_manage_admins_list', {
                url: '/admin/channel/manage/admins/list',
                templateUrl: Template.getTemplate('views/admin/channel/manage_admins_list.html'),
                data:{
                    activeTab:  "channel_settings"
                }
            })
            .state('admin_channel_manage_admins', {
                url: '/admin/channel/:channel_id/manage/admins',
                templateUrl: Template.getTemplate('views/admin/channel/manage_admins.html'),
                data:{
                    activeTab:  "channel_settings"
                }
            })
            .state('admin_create_channel', {
                url: '/admin/channel/create',
                templateUrl: Template.getTemplate('views/admin/channel/create.html'),
                data:{
                    activeTab:  "create"
                }
            })
            .state('admin_edit_channel', {
                url: '/admin/channel/:channel_id/edit',
                templateUrl: Template.getTemplate('views/admin/channel/edit.html'),
                data:{
                    activeTab:  "channel_settings"
                }
            })
            .state('admin_view_channel', {
                url: '/admin/channel/:channel_id',
                templateUrl: Template.getTemplate('views/admin/channel/view.html'),
                data:{
                    activeTab:  "channel_settings"
                }
            })
            .state('admin_channel_leaderboard', {
                url: '/admin/channel/:channel_id/leaderboard',
                templateUrl: Template.getTemplate('views/admin/channel/leaderboard.html'),
                data:{
                    activeTab:  "leaderboard"
                }
            })
            .state('admin_deactivate_channel', { //do we need a page?
                url: '/admin/channel/:channel_id/deactivate',
                templateUrl: Template.getTemplate('views/admin/channel/deactivate.html'),
                data:{
                    activeTab:  "leaderboard"
                }
            })

            .state('admin_manage_channel_props', {
                url: '/admin/channel/:channel_id/prop/manage',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/manage.html')
                    }
                }
            })

            .state('admin_manage_prop_single', {
                url: '/admin/channel/:channel_id/prop/manage/single/:type',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/manage-single.html')
                    },
                    'list@admin_manage_prop_single': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    }
                }
            })
            .state('admin_manage_prop_multiple', {
                url: '/admin/channel/:channel_id/prop/manage/multiple/:type',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/manage-multiple.html')
                    },
                    'list@admin_manage_prop_multiple': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    }
                }
            })
            .state('admin_channel_suggestedby_list', {
                url: '/admin/channel/:channel_id/prop/suggestedby/list',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/suggestedby/list.html')
                    },
                    'list@admin_channel_suggestedby_list': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    }
                }
            })

            .state('admin_create_prop', {
                url: '/admin/channel/:channel_id/prop/create',
                templateUrl: Template.getTemplate('views/admin/channel/prop/create.html'),
                data:{
                    activeTab:  "createprop"
                }
            })
            .state('admin_create_prop_single', {
                url: '/admin/channel/:channel_id/prop/create/single/:type',
                data:{
                    activeTab:  "createprop"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/create-single.html')
                    },
                    'contestent_triangles@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_triangles.html')
                    },
                    'contestent_options@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_options.html')
                    },
                    'regular_poll@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_create_regular_poll_form.html')
                    },
                    'image_poll@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_create_image_poll_form.html')
                    },
                    'product_poll@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_create_product_poll_form.html')
                    },
                    'opinion_options@admin_create_prop_single': {
                        templateUrl:Template.getTemplate('views/admin/channel/prop/_opinion-options.html')
                    },
                    'classic_options@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_classic-options.html')
                    },
                    'quiz_options@admin_create_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_quiz-options.html')
                    }
                }
            })
            .state('admin_create_prop_multiple', {
                url: '/admin/channel/:channel_id/prop/create/multiple/:type',
                data:{
                    activeTab:  "createprop"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/create-multi.html')
                    },
                    'new@admin_create_prop_multiple': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_multiquestion-new.html')
                    },
                    'preedit@admin_create_prop_multiple': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_multiquestion-pre-edit.html')
                    },
                    'edit@admin_create_prop_multiple': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_multiquestion-edit.html')
                    }
                }
            })
            .state('admin_edit_prop_single', {
                url: '/admin/channel/:channel_id/prop/:prop_id/edit/single',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/edit-single.html')
                    },
                    'contestent_triangles@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_triangles.html')
                    },
                    'contestent_options@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_options.html')
                    },
                    'regular_poll@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_edit_regular_poll_form.html')
                    },
                    'image_poll@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_edit_image_poll_form.html')
                    },
                    'product_poll@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_edit_product_poll_form.html')
                    },
                    'opinion_options@admin_edit_prop_single': {
                        templateUrl:Template.getTemplate('views/admin/channel/prop/_opinion-options.html')
                    },
                    'classic_options@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_classic-options.html')
                    },
                    'quiz_options@admin_edit_prop_single': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_quiz-options.html')
                    }
                }
            })
            .state('admin_edit_prop_multiple', {
                url: '/admin/channel/:channel_id/prop/:propset_id/edit/multiple',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/edit-multiple.html')
                    },
                    'new@admin_edit_prop_multiple': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_multiquestion-new.html')
                    },
                    'preedit@admin_edit_prop_multiple': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_multiquestion-pre-edit.html')
                    },
                    'edit@admin_edit_prop_multiple': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_multiquestion-edit.html')
                    }
                }
            })
            .state('admin_preview_prop', {
                url: '/admin/channel/:channel_id/prop/:prop_id/preview',
                templateUrl: Template.getTemplate('views/admin/channel/prop/preview.html'),
                data:{
                    activeTab:  "manage_props"
                }
            })
            .state('admin_submited_photos', {
                url: '/admin/channel/:channel_id/submitted/photos',
                data:{
                    activeTab:  "submitted_photos"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/submitted_photos.html')
                    }
                }
            })
            .state('admin_results_prop', {
                url: '/admin/channel/:channel_id/prop/:prop_id/results',
                data:{
                    activeTab:  "manage_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/results.html')
                    },
                    'view@admin_results_prop': {
                        templateUrl: Template.getTemplate('views/admin/channel/prop/_view.html')
                    },
                    'contestent_triangles@admin_results_prop': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_triangles.html')
                    },
                    'contestent_options@admin_results_prop': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_options.html')
                    }
                }
            })
            .state('admin_view_prop', {
                url: '/admin/channel/:channel_id/prop/:prop_id/view',
                templateUrl: Template.getTemplate('views/admin/channel/prop/view.html'),
                data:{
                    activeTab:  "manage_props"
                }
            })
            //Props
            .state('admin_deactivate_prop', { //do we need a page?
                url: '/admin/prop/:prop_id/deactivate',
                templateUrl: Template.getTemplate('views/admin/prop/deactivate.html'),
                data:{
                    activeTab:  "manage_props"
                }
            })

            //Users
            .state('admin_user_profile', { //do we need a page?
                url: '/admin/user/:user_id',
                templateUrl: Template.getTemplate('views/admin/user/profile.html')
            })
            //Analytics
            .state('admin_analytics_channel', {
                url: '/admin/channel/:channel_id/analytics/channel',
                templateUrl: Template.getTemplate('views/admin/analytics/channel.html')
            })
            .state('admin_analytics_prop', {
                url: '/admin/channel/:channel_id/analytics/prop',
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/analytics/prop.html')
                    }
                }
            })
            .state('admin_analytics_product', {
                url: '/admin/channel/:channel_id/analytics/product',
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/admin/analytics/product.html')
                    }
                }
            })
            .state('admin_analytics_user', {
                url: '/admin/channel/:channel_id/analytics/user',
                templateUrl: Template.getTemplate('views/admin/analytics/user.html')
            })
            //REGULAR USER
            .state('user_channel_profile_subdomain', {
                url: '/profile',
                templateUrl: Template.getTemplate('views/channel/about.html'),
                data:{
                    activeTab:  "profile"
                }
            })
            .state('user_channel_terms_subdomain', {
                url: '/terms',
                templateUrl: Template.getTemplate('views/channel/terms.html'),
                data:{
                    activeTab:  "profile"
                }
            })
            .state('user_channel_privacy_subdomain', {
                url: '/privacy',
                templateUrl: Template.getTemplate('views/channel/privacy.html'),
                data:{
                    activeTab:  "profile"
                }
            })
            .state('user_channel_contact', {
                url: '/channel/:cguid/contact',
                templateUrl: Template.getTemplate('views/channel/contact.html'),
                data:{
                    activeTab:  "profile"
                }
            })
            .state('user_channel_contact_subdomain', {
                url: '/contact',
                templateUrl: Template.getTemplate('views/channel/contact.html'),
                data:{
                    activeTab:  "profile"
                }
            })
            /* User - BAHD - START */
            .state('user_upload_photo', {
                url: '/upload/photo',
                data:{
                    activeTab:  "submit_photo"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/upload_photo.html')
                    }
                }
            })
            .state('user_upload_photo_thanks', {
                url: '/upload/photo/thanks',
                data:{
                    activeTab:  "submit_photo"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/upload_photo_thanks.html')
                    }
                }
            })
            .state('user_final_four', {
                url: '/final/four',
                data:{
                    activeTab:  "final_four"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/final_four.html')
                    }
                }
            })
            .state('user_previous_winners', {
                url: '/previous/winners',
                data:{
                    activeTab:  "our_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/prop/previous_winners.html')
                    },
                    'poll_list@user_previous_winners': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    }
                }
            })
            .state('user_product_vote', {
                url: '/product/vote',
                data:{
                    activeTab:  "final_four"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/product_vote.html')
                    }
                }
            })
            /* User - BAHD - END*/
            .state('user_channel_poll_list_subdomain', {
                url: '/poll/list',
                data:{
                    activeTab:  "our_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/prop/list.html')
                    },
                    'poll_list@user_channel_poll_list_subdomain': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    },
                    'quiz_list@user_channel_poll_list_subdomain': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    }
                }
            })
            .state('user_channel_prop_list_subdomain', {
                url: '/prop/list',
                data:{
                    activeTab:  "our_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/prop/list.old.html')
                    }
                }
            })
            .state('user_channel_quiz_list_subdomain', {
                url: '/quiz/list',
                data:{
                    activeTab:  "our_quizzes"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/propset/list.html')
                    },
                    'poll_list@user_channel_quiz_list_subdomain': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    },
                    'quiz_list@user_channel_quiz_list_subdomain': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    }
                }
            })

            .state('user_view_quiz_thanks', {
                url: '/quiz/:qguid/thanks',
                data:{
                    activeTab:  "our_quizzes"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/propset/vote_thanks.html')
                    },
                    'vote_options@user_view_quiz_thanks': {
                        templateUrl: Template.getTemplate('views/channel/propset/_vote_options.html')
                    },
                    'facebook_stats@user_view_quiz_thanks': {
                        templateUrl: Template.getTemplate('views/channel/propset/_facebook_stats.html')
                    },
                    'vote_answers@user_view_quiz_thanks': {
                        templateUrl: Template.getTemplate('views/channel/propset/_vote_answers.html')
                    },
                    'multiple_list@user_view_quiz_thanks': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    },
                    'single_list@user_view_quiz_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    }
                }
            })
            .state('user_view_poll_thanks', {
                url: '/poll/:qguid/thanks',
                data:{
                    activeTab:  "our_quizzes"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/propset/vote_thanks.html')
                    },
                    'vote_options@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/channel/propset/_vote_options.html')
                    },
                    'contestent_triangles@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_triangles.html')
                    },
                    'contestent_options@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_options.html')
                    },
                    'facebook_stats@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/channel/propset/_facebook_stats.html')
                    },
                    'vote_answers@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/channel/propset/_vote_answers.html')
                    },
                    'multiple_list@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    },
                    'single_list@user_view_poll_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    }
                }
            })
            .state('user_channel_leaderboard_subdomain', {
                url: '/leaders',
                templateUrl: Template.getTemplate('views/channel/leaders.html'),
                data:{
                    activeTab:  "user_leaderboard"
                }
            })
            .state('user_suggest_prop_subdomain', {
                url: '/prop/suggest',
                templateUrl: Template.getTemplate('views/channel/prop/suggest.html'),
                data:{
                    activeTab:  "suggest_prop"
                }
            })
            //VIEW SINGLE
            .state('user_view_single', {
                url: '/single/:type/:pguid',
                data:{
                    activeTab:  "our_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/prop/view.html')
                    },
                    'multiple_list@user_view_single': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    },
                    'single_list@user_view_single': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    },
                    'contestent_triangles@user_view_single': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_triangles.html')
                    },
                    'contestent_options@user_view_single': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_options.html')
                    },
                    'regular_vote_thanks@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_regular_vote_thanks.html')
                    },
                    'image_vote_thanks@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_image_vote_thanks.html')
                    },
                    'facebook_stats@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_facebook_stats.html')
                    },
                    'view@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_view.html')
                    },
                    'regular_vote_options@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_regular_vote_options.html')
                    },
                    'image_vote_options@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_image_vote_options.html')
                    },
                    'analytics@user_view_single': {
                        templateUrl: Template.getTemplate('views/channel/prop/_analytics.html')
                    }
                }
            })
            //ViEW MULTIPLE
            .state('user_view_multiple', {
                url: '/multiple/:type/:qguid',
                data:{
                    activeTab:  "our_quizzes"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/propset/view.html')
                    },
                    'vote_options@user_view_multiple': {
                        templateUrl: Template.getTemplate('views/channel/propset/_vote_options.html')
                    },
                    'facebook_stats@user_view_multiple': {
                        templateUrl: Template.getTemplate('views/channel/propset/_facebook_stats.html')
                    },
                    'vote_answers@user_view_multiple': {
                        templateUrl: Template.getTemplate('views/channel/propset/_vote_answers.html')
                    }
                }
            })

            .state('user_suggest_prop_thanks', {
                url: '/channel/prop/:pguid/suggest/thanks',
                templateUrl: Template.getTemplate('views/channel/prop/suggest_thanks.html'),
                data:{
                    activeTab:  "our_props"
                }
            })
            .state('user_vote_prop_thanks', {
                url: '/channel/single/:type/:pguid/vote/thanks',
                data:{
                    activeTab:  "our_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/channel/prop/vote_thanks.html')
                    },
                    'multiple_list@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/propset/_list.html')
                    },
                    'single_list@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    },
                    'contestent_triangles@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_triangles.html')
                    },
                    'contestent_options@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/prop/_contestent_options.html')
                    },
                    'regular_vote_thanks@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/channel/prop/_regular_vote_thanks.html')
                    },
                    'image_vote_thanks@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/channel/prop/_image_vote_thanks.html')
                    },
                    'facebook_stats@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/channel/prop/_facebook_stats.html')
                    },
                    'view@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/channel/prop/_view.html')
                    },
                    'analytics@user_vote_prop_thanks': {
                        templateUrl: Template.getTemplate('views/channel/prop/_analytics.html')
                    }
                }
            })
            .state('user_view_prop_results', {
                url: '/prop/:pguid/results',
                templateUrl: Template.getTemplate('views/prop/results.html'),
                data:{
                    activeTab:  "our_props"
                }
            })
            .state('user_channel_list', {
                url: '/my/channels',
                templateUrl: Template.getTemplate('views/my/channels.html'),
                data:{
                    activeTab:  "my_channels"
                }
            })
            .state('user_prop_list', {
                url: '/my/props',
                data:{
                    activeTab:  "my_props"
                },
                views: {
                    '': {
                        templateUrl: Template.getTemplate('views/my/props.html')
                    },
                    'list@user_prop_list': {
                        templateUrl: Template.getTemplate('views/prop/_list.html')
                    }
                }
            })
            .state('user_rankings', {
                url: '/my/rankings',
                templateUrl: Template.getTemplate('views/my/rankings.html'),
                data:{
                    activeTab:  "my_rankings"
                }
            })
            .state('user_account', {
                url: '/my/account',
                templateUrl: Template.getTemplate('views/my/account.html'),
                data:{
                    activeTab:  "my_account"
                }
            })
            .state('user_account_edit', {
                url: '/user/:user_id/edit',
                templateUrl: Template.getTemplate('views/user/edit.html'),
                data:{
                    activeTab:  "my_account"
                }
            })
            //FLASHPROP USER VIEWS
            .state('fp_user_list', {
                url: '/fp/user/list',
                templateUrl: Template.getTemplate('views/admin/user/list.html')
            })
            .state('fp_user_view', {
                url: '/fp/user/:user_id/view',
                templateUrl: Template.getTemplate('views/admin/user/view.html')
            })
            .state('fp_user_create', {
                url: '/fp/user/create',
                templateUrl: Template.getTemplate('views/admin/user/create.html')
            })
            .state('fp_user_update', {
                url: '/fp/user/:user_id/edit',
                templateUrl: Template.getTemplate('views/admin/user/edit.html')
            })
            //ERROR
            .state('error', {
                url: '/error',
                templateUrl: Template.getTemplate('views/error.html')
            })
            //ANONYMOUS USER
            .state('home', {
                url: '/',
                templateUrl: Template.getTemplate('views/index.html')
            })
            .state('login', {
                url: '/login',
                templateUrl: Template.getTemplate('views/login.html')
            })
            .state('logout', {
                url: '/logout',
                templateUrl: Template.getTemplate('views/logout.html')
            })
            .state('admin_login', {
                url: '/admin_login',
                templateUrl: Template.getTemplate('views/admin_login.html')
            })
            .state('user_login', {
                url: '/user_login',
                templateUrl: Template.getTemplate('views/user_login.html')
            })
            .state('admin_login_failure', {
                url: '/admin_login/failure',
                templateUrl: Template.getTemplate('views/admin_login_failure.html')
            })
            .state('user_logi_failuren', {
                url: '/user_login/failure',
                templateUrl: Template.getTemplate('views/user_login_failure.html')
            })
            .state('forgotpassword', {
                url: '/forgotpassword',
                templateUrl: Template.getTemplate('views/forgotpassword.html')
            })
            .state('resetpassword', {
                url: '/resetpassword',
                templateUrl: Template.getTemplate('views/resetpassword.html')
            })
            .state('signup', {
                url: '/signup',
                templateUrl: Template.getTemplate('views/signup.html')
            })
            .state('signup_user', {
                url: '/signup/user',
                templateUrl: Template.getTemplate('views/signup_user.html')
            })
            .state('anonymous_view_channel', {
                url: '/channel/:cguid',
                templateUrl: Template.getTemplate('views/channel/view.html')
            })
            .state('anonymous_view_channel_short', { //can't we just make this do a redirect?
                url: '/c/:cguid',
                templateUrl: Template.getTemplate('views/channel/view.html')
            })
            .state('anonymous_view_prop', {
                url: '/prop/:pguid',
                templateUrl: Template.getTemplate('views/prop/view.html')
            })
            .state('anonymous_view_prop_short', {
                url: '/p/:pguid',
                templateUrl: Template.getTemplate('views/prop/view.html')
            })
            //Charts examples
            .state('charts_examples', {
                url: '/charts',
                templateUrl: 'views/chart.html' //[done]
            })
            //FindUsers
            .state('find_users', {
                url: '/find_users',
                templateUrl: 'views/find_users.html' //[done]
            })

            //FP-COLOR update
            .state('fp_color', {
                url: '/fpcolor',
                templateUrl: 'views/fpcolor.html' //[done]
            })
            .state('admin_fp_color', {
                url: '/admin/fpcolor',
                templateUrl: 'views/admin/fpcolor.html' //[done]
            })
            .state('admin_signups', {
                url: '/admin/signups',
                templateUrl: 'views/admin/signups.html' //[done]
            });
    }
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

//@todo: I should have search for channel and prop here and assign to $rootScope so each controller can access it via $rootScope
angular.module('mean').run(function($rootScope, $state, $http, $templateCache, $location, $localStorage,
    ChannelAdminFactory, ChannelFactory, CurrentChannel, PropsAdminFactory, PropsFactory, PropsetAdminFactory, PropsetFactory, Global) {
    $rootScope.$on( "$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        console.log("RUN");
        console.log(toState);

        console.log("fromParams");
        console.log(fromParams);

        console.log("toParams");
        console.log(toParams);


        //set Active tab
        if ( toState.data ){
            $rootScope.activeTab = toState.data.activeTab;
        };

        if ( toState.name != 'user_view_short_quiz' || toState.name != 'user_view_quiz'  ){
            $rootScope.quiz_title = null; //This is to go back to regular header. Not quiz one!
        };

        if ((
            toState.url == "/" ||
            toState.url == "/login" ||
            toState.url == "/admin/channel/list" ||
            toState.url == "/admin/channel/create"

        ) && ( _.isEmpty(Global.cguid) )){
            console.log("NULL CONFIG");
            CurrentChannel.setCurrentChannel(null);
            //CurrentChannel.setDefaultColor(null);
        }

        if ( !_.isEmpty(Global.channel) ){
            CurrentChannel.setCurrentChannel(Global.channel);
            //CurrentChannel.changeColorChannel(Global.channel);
        } else {
            CurrentChannel.setCurrentChannel(null);
            //CurrentChannel.setDefaultColor(null);
        }

        // if ( Global.user &&  !Global.user.is_admin && !$localStorage.displayProductVote && !CurrentChannel.userVoteChannelProducts() ){
        //     $localStorage.displayProductVote = true;
        //     $location.path('/product/vote');
        // }
        if ( Global.user &&  !Global.user.is_admin){
            if (!$localStorage.displayProductVote && !CurrentChannel.userVoteChannelProducts()) {
                $localStorage.displayProductVote = true;
                $location.path('/product/vote');
            } else if (!$localStorage.displayProductVote) {
                ChannelFactory.lastProp( { guid: Global.channel.guid, type: 'product' }, function(prop) {
                    $localStorage.displayProductVote = true;
                    
                    if ( prop ){
                        var loggedTotal;
                        Global.user.channel_logged_total.forEach(function(logged, key){
                            if (logged.channel == Global.channel.guid) {
                                loggedTotal = logged.logged;
                            }
                        });

                        if (prop.publishing == 1 || loggedTotal % prop.publishing == 1) {
                            $location.path('/product/vote');
                        }
                    }
                });
            }
        }
    });

});


angular.module('mean').filter('range', function() {
    return function(input, min, max, step) {

        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i=min; i<max; i++)
            input.push(i);
        return input;
       /* var a = [];
        a['0'] = 0;
        a['2'] = 2;
        return a;*/
    };
});

angular.module('mean')
    .constant('config', {
        'flag_thankyoupageredirect': true
    })

angular.module('mean').filter('ucfirst', function() {
    return function(input, scope) {
        return input ? input.substring(0,1).toUpperCase() : "";
    }
});


angular.module('mean').filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
});

angular.module('mean').filter('rawMustache', ['$sce', function($sce){
    return function(val) {
        return $sce.trustAsHtml("{{"+val+"}}");
    };
}]);

angular.module('mean').filter('rawColorPaletteMustache', ['$sce', function($sce){
    return function(val) {
        return $sce.trustAsHtml("{{palette."+val+"}}");
    };
}]);

// Upgrade for JSON.stringify, updated to allow arrays
(function(){
    // Convert array to object
    var convArrToObj = function(array){
        var thisEleObj = new Object();
        if(typeof array == "object"){
            for(var i in array){
                var thisEle = convArrToObj(array[i]);
                thisEleObj[i] = thisEle;
            }
        }else {
            thisEleObj = array;
        }
        return thisEleObj;
    };
    var oldJSONStringify = JSON.stringify;
    JSON.stringify = function(input){
        if(oldJSONStringify(input) == '[]')
            return oldJSONStringify(convArrToObj(input));
        else
            return oldJSONStringify(input);
    };
})();
