'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    async = require('async'),
    Channel = mongoose.model('Channel'),
    User = mongoose.model('User'),
    Votes = mongoose.model('Votes'),
    ChannelLeaderboard = mongoose.model('ChannelLeaderboard'),
    Prop    = mongoose.model('Prop'),
    PropSet    = mongoose.model('PropSet'),
    Photo    = mongoose.model('Photo'),
    User    = mongoose.model('User'),
    uploadcare = require('uploadcare')('05817a84d61d7339a3b1', '253a61bad6416902b5dd'),
    nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    config = require(__dirname + '/../../config/config'),
    utils = require(__dirname + '/../lib/utils'),
    fs = require('fs'),
    phpjs = require('phpjs'),
    _ = require('lodash');


/**
 * Find Channel by id
 */
exports.channel = function(req, res, next, id) {
    var select = "";
    if ( !req.user ){
        select = "_id guid name logo picture";
    }

    Channel.findOne({ deleted: false, _id: id })
        .select(select)
        .populate('admins', 'id firstname lastname')
        .populate('subscribers')
        .populate('submittedPhotos')
        .deepPopulate('submittedPhotos.user')
        .exec(function(err, channel) {
            if (err) return next(err);
            if (!channel) return next(new Error('Failed to load channel guid' + id));
            req.channel = channel;
            next();
    });
};

exports.channelguid = function(req, res, next, id) {
    var select = "";
    if ( !req.user ){
        select = "_id guid name logo picture";
    }

    Channel.findOne({ deleted: false, guid: id })
        .select(select)
        .deepPopulate('submittedPhotos.user')
        .sort('-created')
        .exec(function(err, channel) {
            if (err) return next(err);
            if (!channel) return next(new Error('Failed to load channel guid' + id));
            req.channel = channel;
            next();
    });

};


function _getFileId(url, index) {
    if ( url ) {
        return url.replace(/^https?:\/\//, '').split('/')[index];
    }
}

exports.lastProp = function(req, res, next){
    var channel = req.channel;
    var type = req.params.type;

    Prop.findOne(
            {
                channel: channel._id,
                type: type, deleted: false,
                "$or": [
                    { "timestamps.published" : { "$ne": 0 } },
                    { "timestamps.closed" : { "$ne": 0 } },
                    { "timestamps.graded" : { "$ne": 0 } }
                ]
            }
        )
        .sort({ 'timestamps.created' : -1 })
        .populate('channel')
        .exec(function(err, prop) {
            if (err) return next(err);
            if (!err) res.jsonp(prop);
        });
};

exports.productVote = function(req, res, next){
    var channel = req.channel;
    var prop_id = req.body.prop_id;
    var user = req.user;
    var answers = req.body.answers;
    var voted_date = Date.now();

    channel.voted_products.push({
        user: user._id,
        prop: prop_id,
        answers: answers,
        voted_date: voted_date        
    });
    channel.save(function(err) {
        if (err) {
            res.jsonp({});
        } else {
            res.jsonp(channel);
        }
    });
};

exports.getUploadedPhoto = function(req, res, next){
    var channel = req.channel;
    var user = req.user;

    Photo.findOne(
        {
            channel: channel._id,
            user: user._id
        }
    )
    .sort({ 'timestamps.created' : -1 })
    .populate('user')
    .exec(function(err, photo) {
        if (err) res.jsonp({});
        if ( photo ){
            res.jsonp(photo);
        } else {
            res.jsonp({});
        }
    });
};

exports.postUploadPhoto = function(req, res){
    var channel = req.channel;
    var user = req.user;

    user.instagram_data.username = req.body.instagram;
    user.save();

    var photo = new Photo();
    photo.channel = channel;
    photo.user = user;
    photo.instagram = req.body.instagram;
    photo.image = req.body.image;
    photo.save();

    if ( photo.image  ){
        var imageId = _getFileId(photo.image , 1);
    }

    async.series([
        function(callback){
            if ( imageId ){
                uploadcare.files.store(imageId, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }
            console.log("================================@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            ChannelLeaderboard.findOne({user: user, channel: channel} , function(error, channelLeaderboard){
                myLog('ChannelLeaderboard call for uploading photo!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                if(!error && channelLeaderboard != null){
                    
                    channelLeaderboard.timestamps.update = new Date().getTime();
                    // channelLeaderboard.points           = channelLeaderboard.points + channel.points.suggest_opinion;
                    // channelLeaderboard.current_points   = channelLeaderboard.current_points + channel.points.suggest_opinion;
                    channelLeaderboard.actions.suggest_opinion   = channelLeaderboard.actions.suggest_opinion + 1;

                    myLog('ChannelLeaderboard is NOT null', channelLeaderboard);
                    User.findOne({_id: user._id} , function(error, _user){
                        _user.number.suggest_opinion = channelLeaderboard.actions.suggest_opinion;
                        _user.number.total_points = _user.number.total_points + channel.points.suggest_opinion;
                        _user.save();

                        console.log('User');
                        console.log(_user);
                    });

                    channelLeaderboard.save();
                }
            });
            console.log("end================================@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

            callback(null);
        }
    ],
    function(err, results){
        Channel.findOne({_id: channel._id} , function(error, _channel){
            myLog('PUSHING PHOTO into PHOTO COLLECTION - BEFORE', _channel);
            _channel.submittedPhotos.push(photo);
            _channel.save();
            myLog('PUSHING PHOTO into PHOTO COLLECTION - AFTER', _channel);

            res.jsonp(_channel);
        });
    });
};


/**
 * Create an Channel
 */
exports.create = function(req, res) {
    var channel = new Channel(req.body);
    var user = req.user;
    var guid = utils.getGuid(req, res);

    channel.deleted = false;
    channel.admins.push(user);
    channel.default_page = 'poll/list';

    if ( _.isEmpty(channel.guid)  ){
        channel.guid = guid;
    }

    if ( channel.logo ){
        var logoId = _getFileId(channel.logo, 1);
    }
    if ( channel.picture ){
        var pictureId = _getFileId(channel.picture, 1);
    }

    if (channel.photo_page.image) {
        var photoPageImageId = _getFileId(channel.photo_page.image, 1);
    }

    async.series([
        function(callback){
            if ( logoId ){
                uploadcare.files.store(logoId, function(error, response) {
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //channel.logo = response.original_file_url;
                });
            }
            callback(null);
        },
        function(callback){
            if ( pictureId ){
                uploadcare.files.store(pictureId, function(error, response) {
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //channel.picture = response.original_file_url;
                });
            }
            callback(null);
        },
        function(callback){
            if ( photoPageImageId ){
                uploadcare.files.store(photoPageImageId, function(error, response) {
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //channel.picture = response.original_file_url;
                });
            }
            callback(null);
        }
    ],
        function(err, results){
            if ( !channel.logo ){
                channel.logo = "img/fp/ps40x40.png";
            }

            if ( !channel.picture ){
                channel.picture = "img/fp/ps400x400.png";
            }

            if (!channel.photo_page.image) {
                channel.photo_page.image = "/img/BAHDBanner.png";
            }

            channel.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        channel: channel
                    });
                } else {
                    res.jsonp(channel);

                    /*Channel.findById(channel, function (err, channel) {
                        if (err) {
                            return res.send('users/signup', {
                                errors: err.errors,
                                channel: channel
                            });
                        } else {
                            if ( !_.isEmpty(guid) ){
                                channel.guid = guid;
                                channel.save();
                            }
                        }
                    })*/
                }
            });
        });
};


/**
 * Update an Channel
 */
exports.update = function(req, res) {
    var channel = req.channel;
    var logoId = _getFileId(channel.logo, 1);
    var pictureId = _getFileId(channel.picture, 1);
    var logoIdbk = _getFileId(channel.logobk, 1);
    var pictureIdbk = _getFileId(channel.picturebk, 1);
    var photoPageImageId = _getFileId(channel.photo_page.image, 1);
    var photoPageImageIdbk = _getFileId(channel.photo_page_imagebk, 1);

    async.series([
        function(callback){
            if ( logoIdbk != logoId ){
                uploadcare.files.remove(logoIdbk, function(error, response) {
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                });
            };
            callback(null);
        },
        function(callback){
            if ( pictureIdbk != pictureId ){
                uploadcare.files.remove(pictureIdbk, function(error, response) {
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                });
            };
            callback(null);
        },
        function(callback){
            if ( photoPageImageIdbk != photoPageImageId ){
                uploadcare.files.remove(photoPageImageIdbk, function(error, response) {
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                });
            };
            callback(null);
        },
        function(callback){
            uploadcare.files.store(logoId, function(error, response) {
                if(error) {
                    console.log('Error: ' + JSON.stringify(response));
                } else {
                    console.log('Response: ' + JSON.stringify(response));
                }
                //channel.logo = response.original_file_url;
                callback(null);
            })
        },
        function(callback){
            uploadcare.files.store(pictureId, function(error, response) {
                if(error) {
                    console.log('Error: ' + JSON.stringify(response));
                } else {
                    console.log('Response: ' + JSON.stringify(response));
                }
                //channel.picture = response.original_file_url;
                callback(null);
            })
        },
        function(callback){
            uploadcare.files.store(photoPageImageId, function(error, response) {
                if(error) {
                    console.log('Error: ' + JSON.stringify(response));
                } else {
                    console.log('Response: ' + JSON.stringify(response));
                }
                //channel.picture = response.original_file_url;
                callback(null);
            })
        }
    ],
    function(err, results){
        Channel.findOne({ deleted: false, _id: channel._id })
            .exec(function(err, _channel) {
                if (err) {
                    res.jsonp(err.message);
                } else {
                    _channel.guid = req.body.guid;
                    _channel.mode = req.body.mode;
                    _channel.name = req.body.name;
                    _channel.description = req.body.description;
                    _channel.logo = req.body.logo;
                    _channel.picture = req.body.picture;
                    _channel.color = req.body.color;
                    _channel.reset_rolling = req.body.reset_rolling;
                    _channel.social = req.body.social;
                    _channel.tags = req.body.tags;
                    _channel.points = req.body.points;
                    _channel.leaderboard_desc = req.body.leaderboard_desc;
                    _channel.photo_page = req.body.photo_page;
                    _channel.reset_rolling = req.body.reset_rolling;
                    _channel.timestamps.updated = new Date().getTime();
                    _channel.save(function(err) {
                        if (err) {
                            res.jsonp(err.message);
                        } else {
                            res.jsonp(_channel);
                        }
                    });
                }
            });
    });
};

exports.deactivate = function(req, res) {
    var channel = req.channel;

    channel = _.extend(channel, req.body);
    channel.deleted = true;
    channel.timestamps.deleted = new Date().getTime();

    channel.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                channel: channel
            });
        } else {
            res.jsonp(channel);
        }
    });
};

exports.deactivatePhoto = function(req, res){
    console.log("app/controllers/channels.js deactivatePhoto!!!!!!!!!!!!!!!!");
    var params = req.params;
    console.log(params);

    Photo.findOne({_id: params.photo_id} , function(error, _photo){
        _photo.deleted = true;
        _photo.timestamps.deleted = new Date().getTime();
        _photo.save();
        res.jsonp(_photo);
        
        //res.jsonp({"message" : "Photo removed successfully."});

    });

};

/**
 * Show an channel
 */
exports.show = function(req, res) {
    res.jsonp(req.channel);
};

exports.showadmin = function(req, res){
    var select = "";
    if ( !req.user ){
        select = "_id guid name logo picture";
    }

    var id = req.params.channel_id;
    Channel.findOne({ deleted: false, _id: id })
        .select(select)
        .populate('admins', '_id firstname lastname')
        .populate('subscribers')
        .exec(function(err, channel) {
            if (err) return next(err);
            if (!channel) return next(new Error('Failed to load channel id' + id));
            res.jsonp(channel);
        });
};

exports.subdomainRedirect = function(req, res){
    //req.session.guid = utils.getChannelGuid(req, res);
    req.session.guid = req.channel.guid;
    req.session.subdomain = false;

    if ( req.user && req.user.is_admin ){
        // res.redirect('/#!/admin/channel/'+req.channel._id+'/prop/manage');
        res.redirect('/#!/admin/channel/'+req.channel._id+'/prop/manage/single/' + (req.Channel.type == 'single' ? 'image' : 'opinion'));
    } else {
        // if ( req.channel && req.channel.default_page ){
        //     res.redirect('/#!/' + req.channel.default_page );
        // } else {
        //     res.redirect('/#!/poll/list');
        // }
        res.redirect('/#!/final/four');
    }
};

exports.reset = function(req, res){
    req.session.guid = null;
};

exports.getColorStylesheet = function(req, res) {
    var colors = req.query.colors;
    colors = JSON.parse(colors);

    var ct = !_.isEmpty(colors.ct) ? colors.ct : "#EF6D54";
    var c1 = !_.isEmpty(colors.c1) ? colors.c1 : "#FFFFFF";
    var c2 = !_.isEmpty(colors.c2) ? colors.c2 : "#57A8D4";
    var c3 = !_.isEmpty(colors.c3) ? colors.c3 : "#4C4C4C";
    var palette = !_.isEmpty(colors.palette) ? colors.palette : {};

    var hexToRgb = function(hex) {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ?
            parseInt(result[1], 16)+","+parseInt(result[2], 16) + ","+parseInt(result[3], 16)
            : null;
    }

    //req.query.color
    res.setHeader("Content-Type", "text/css");
    res.render('color', {
        color_text: ct,
        color_1: c1,
        color_2: c2,
        color_3: c3,
        rgb_text: hexToRgb(ct),
        rgb_1: hexToRgb(c1),
        rgb_2: hexToRgb(c2),
        rgb_3: hexToRgb(c3),
        palette: palette
    });
};

exports.propList = function(req, res) {
    var type = req.params.type;
    Prop.find(
        {   deleted: false,
            channel: req.channel,
            type: type,
            "$or": [
                { "timestamps.published" : { "$ne": 0 } },
                { "timestamps.closed" : { "$ne": 0 } },
                { "timestamps.graded" : { "$ne": 0 } }
            ]
        })
        .populate("votes.user", "_id facebook_data")
        .sort('-timestamps.created')
        .exec(function(err, props) {
        if (err) {
            res.jsonp([err]);
        } else {
            res.jsonp(props);
        }
    });
};


exports.adminPropList = function(req, res) {
    var type = req.params.type;

    Prop.find(
        {   deleted: false,
            hidden: false,
            channel: req.channel,
            type: type
        })
        .populate("votes.user", "_id facebook_data")
        .sort('-timestamps.created')
        .exec(function(err, props) {
            if (err) {
                res.jsonp([err]);
            } else {
                res.jsonp(props);
            }
        });
};

exports.propsetList = function(req, res){
    var type = req.params.type;

    PropSet.find(
        {   channel: req.channel,
            deleted: false,
            type: type,
            "timestamps.published" : { $ne: 0 }
        })
        .populate("props")
        .sort('-timestamps.created')
        .exec(function(err, propsets) {
            if (err) {
                res.jsonp([err]);
            } else {
                res.jsonp(propsets);
            }
        });
}

exports.adminPropsetList = function(req, res){
    var type = req.params.type;

    PropSet.find(
        {
            channel: req.channel._id,
            deleted: false,
            type: type
        })
        .populate("props")
        .sort('-timestamps.created')
        .exec(function(err, propsets) {
            if (err) {
                res.jsonp([err]);
            } else {
                res.jsonp(propsets);
            }
        });
}

exports.suggestedbyPropList = function(req, res) {
    var channel = req.channel;
    var usersList = [];
    channel.subscribers.forEach(function(user, userKey){
        usersList.push(user);
    });

    Prop.find({ deleted: false, suggested_by: {$in: usersList} })
        .sort('-timestamps.created')
        .exec(function(err, props) {
        if (err) {
            res.jsonp([err]);
        } else {
            res.jsonp(props);
        }
    });
};

exports.tagList = function(req, res) {
    Prop.find({ deleted: false, channel: req.channel._id, "timestamps.published" : { $ne: 0 } }).sort('-created').exec(function(err, props) {
        if (err) {
            res.jsonp([err]);
        } else {
            var tags = [];
            props.forEach(function(propdata, propKey){
                propdata.tags.forEach(function(tagsdata, tagKey){
                    tags.push(tagsdata);
                });
            });
            res.jsonp(_.union(tags));
        }
    });
};

exports.channelAdminList = function(req, res) {
    Channel.findOne({ deleted: false, _id: req.channel._id })
        .populate('admins')
        .sort('-created')
        .exec(function(err, channel) {
        if (err) {
            res.jsonp([err]);
        } else {
            res.jsonp(channel.admins);
        }
    });
};

exports.channelAdminAdd = function(req, res){
    var params = req.params;
    var channel = req.channel;

    Channel.findOne({ deleted: false, _id: channel._id })
        .sort('-created')
        .exec(function(err, channel) {
            if (err) {
                res.jsonp([err]);
            } else {
                User.findOne({_id: params.admin_id} , function(error, _user){
                    channel.admins.push(_user);
                    channel.save();

                    res.jsonp({"message" : "Admin added successfully."});
                });
            }
        });
    console.log(params);
};

exports.channelAdminRemove = function(req, res){
    var params = req.params;
    var channel = req.channel;

    Channel.findOne({ deleted: false, _id: channel._id })
        .sort('-created')
        .exec(function(err, channel) {
            if (err) {
                res.jsonp([err]);
            } else {
                User.findOne({_id: params.admin_id} , function(error, _user){
                    channel.admins.pull(_user);
                    channel.save();

                    res.jsonp({"message" : "Admin removed successfully."});

                });
            }
        });
};

exports.sendContactEmail = function(req, res){
    var channel = req.channel;
    var user = req.user;
    var body = req.body;

    var email = body.email;
    var subject = body.subject;
    var message = body.message;

    var transporter = nodemailer.createTransport(smtpTransport(config.email));

    var mailOptions = {
        from: user.firstname + " " + user.lastname + " <" + email + ">", // sender address
        to: 'veronica@flashprops.com, toyin@flashprops.com, jay@flashprops.com, fred@flashprops.com', // list of receivers
        subject: subject, // Subject line
        html: '<b>'+ message +'</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log('MAILER ERROR');
            res.jsonp({'type' : 'error', 'message': 'There was an error sending your message. Please try again later.'});
        }else{
            console.log('MAILER SUCCESS');
            res.jsonp({'type' : 'success', 'message': 'Your message has been sent succesfully. We will contact you soon.'});
        }
    });
};

exports.createPropset = function(req, res){
    var user = req.user;
    var propset = new PropSet(req.body);
    propset.channel = req.channel;

    if ( propset.image ){
        var imageId = _getFileId(propset.image, 1);
    }

    async.series([
        function(callback){
            if ( imageId ){
                uploadcare.files.store(imageId, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }

            callback(null);
        }
    ],
        function(err, results){
            if ( !propset.image ){
                propset.image = "img/fp/ps740x280.png";
            }

            propset.save(function(err) {
                if (err) {
                    res.jsonp(err);
                } else {
                    res.jsonp(propset);
                }
            });
        });
};

exports.createProp = function(req, res) {
    var user = req.user;

    var prop = new Prop(req.body);
    prop.channel = req.channel;
    prop.deleted = false;
    prop.counts = {};
    prop.counts.view = 0;
    prop.counts.share = 0;
    prop.counts.share_fb = 0;
    prop.counts.share_tw = 0;
    prop.counts.ext_embedded_url = 0;
    prop.counts.ext_embedded_app = 0;
    prop.counts.direct_from_browser = 0;
    prop.counts.vote = 0;
    prop.counts.new_voters = 0;
    prop.percentage = {};
    prop.percentage.view = 0;
    prop.percentage.share = 0;
    prop.percentage.vote = 0;
    prop.percentage.new_voters = 0;

    if ( prop.image ){
        var imageId = _getFileId(prop.image, 1);
    }

    if ( prop.answers && prop.answers[0] && prop.answers[0].image ){
        var imageAnswer0 = _getFileId(prop.answers[0].image, 1);
    }

    if ( prop.answers && prop.answers[1] && prop.answers[1].image ){
        var imageAnswer1 = _getFileId(prop.answers[1].image, 1);
    }

    if ( prop.answers && prop.answers[2] && prop.answers[2].image ){
        var imageAnswer2 = _getFileId(prop.answers[2].image, 1);
    }

    if ( prop.answers && prop.answers[3] && prop.answers[3].image ){
        var imageAnswer3 = _getFileId(prop.answers[3].image, 1);
    }

    var suggested = new Date(prop.timestamps.suggested).getTime();
    if ( suggested > 0 ){
        prop.suggested_by = user;
    }

    async.series([
        function(callback){
            if ( imageId ){
                uploadcare.files.store(imageId, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }

            if ( imageAnswer0 ){
                uploadcare.files.store(imageAnswer0, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }

            if ( imageAnswer1 ){
                uploadcare.files.store(imageAnswer1, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }

            if ( imageAnswer2 ){
                uploadcare.files.store(imageAnswer2, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }

            if ( imageAnswer3 ){
                uploadcare.files.store(imageAnswer3, function(error, response) {
                    console.log(error);
                    console.log(response);
                    if(error) {
                        console.log('Error: ' + JSON.stringify(response));
                    } else {
                        console.log('Response: ' + JSON.stringify(response));
                    }
                    //prop.image = response.original_file_url;
                });
            }

            callback(null);
        }
    ],
        function(err, results){
            if ( !prop.image ){
                prop.image = "img/fp/ps740x280.png";
            }

            prop.save(function(err) {
                if (err) {
                    res.jsonp(err);
                } else {
                    res.jsonp(prop);
                }
            });
        });

};

/**
 * Channel leaderboard
 */
exports.leaderboard = function(req, res) {
    var user = req.user;
    var channel = req.channel;

    var _channelleaderboardlist = [];
    var _leaderboard = {};
    _leaderboard.currentranking = 0;
    _leaderboard.currentpoints = 0;
    _leaderboard.alltimepoints = 0;

    ChannelLeaderboard.find({channel: channel._id})
        .populate('user', 'firstname lastname username _id number.total_points')
        .sort('-points')
        .exec(function(error, channelleaderboardlist) {
            if(!error && channelleaderboardlist){
                //now we need to find where the user stands in that channel
                var position = 0;
                var userfound = false;
                channelleaderboardlist.forEach(function(channelleaderboard, userkey){
                    position++;
                    if ( channelleaderboard.user && channelleaderboard.user._id.equals(user._id)){
                        userfound = true;
                        _leaderboard.currentranking = position;
                        _leaderboard.currentpoints = channelleaderboard.current_points;
                        _leaderboard.alltimepoints = channelleaderboard.points;
                    }

                    _channelleaderboardlist.push( {
                        user: channelleaderboard.user,
                        current_points: channelleaderboard.current_points,
                        total_points:  channelleaderboard.points
                    });
                });
                if ( !userfound ){
                    _leaderboard.currentranking = position+1;
                    _channelleaderboardlist.push( { user: user, current_points: 0, total_points: 0  });
                }

                _leaderboard.currentrankingtotal = channelleaderboardlist.length;
            }

            res.jsonp({leaderboard: _leaderboard, leaderboardlist: _channelleaderboardlist});
        });
};

exports.resetleaderboard = function(req, res) {
    var channel = req.channel;
  //  console.log("lalalalallal app/controller/channels.js resetleaderboard");

    ChannelLeaderboard.find({channel: channel._id})
        .exec(function(error, channelleaderboardlist) {
            if(!error && channelleaderboardlist){
                channelleaderboardlist.forEach(function(channelleaderboard, userkey){
                    if(channelleaderboard.user){
                        console.log("forEach channelleaderboard: ", channelleaderboard);
                        channelleaderboard.points = 0;
                        channelleaderboard.current_points = 0;
                        channelleaderboard.save();
                    }
                });
            }
        });

};
exports.adminleaderboard = function(req, res) {
    var channel = req.channel;

    ChannelLeaderboard.find({channel: channel._id})
        .populate('user', 'firstname lastname username _id number.total_points')
        .sort('-points')
        .exec(function(error, channelleaderboardlist) {
            if(!error && channelleaderboardlist){
                res.jsonp(channelleaderboardlist);
            }
        });
};

/**
 * List of Channels
 */
exports.all = function(req, res) {
    var user = req.user;

    Channel.find({ deleted: false, admins: { "$in" : [user]}   }).sort('-created').exec(function(err, channels) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(channels);
        }
    });
};

exports.allChannels = function(req, res) {
    var user = req.user;

    Channel.find({ deleted: false }).sort('-created').exec(function(err, channels) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(channels);
        }
    });
};

exports.subscribe = function(req, res){
    var channel = req.channel;
    var user = req.user;

    if ( !user.isSubscribed(channel._id) ){
        channel.update({ $push: { subscribers: user }},
            { safe: true, upsert: true },
            function( err, channels ) {
                if (err) {
                    res.jsonp({"subscribed": false, "message" : "There is been an error in subscribing to channel"});
                } else {
                    res.jsonp({"subscribed": true, "message" : "User has been Successfully Subscibed to channel"});
                }
            });

        user.update({ $push: { subscriptions: channel }},
            { safe: true, upsert: true },
            function( err, users ) {
                if (err) {
                    res.jsonp({"subscribed": false, "message" : "There is been an error in subscribing to channel"});
                } else {
                    res.jsonp({"subscribed": true, "message" : "User has been Successfully Subscibed to channel"});
                }
            });
    } else {
        res.jsonp({"subscribed": true, "message" : "The User is already subscribed to the channel. "});
    }

};

function inSubscriptions(channelId, subscriptions){
    var isEqual = false;
    subscriptions.forEach(function(subscription, key){
        if ( _.isEqual(subscription, channelId) ){
            isEqual = true;
        }
    });
    return isEqual;
}

exports.analyticsUsersSearch = function(req, res, next){
    var channel = req.channel;
    var demographic = {};
    var voting = {};

    demographic.channel_id = channel._id;
    demographic.gender =    req.body.gender;
    demographic.age_from =  req.body.age_from;
    demographic.age_to =    req.body.age_to;
    demographic.relationship_status = req.body.relationship_status;
    demographic.location = req.body.location;

    voting.channel_id = channel._id;
    voting.propList = req.body.propList;

    async.series([
        function(callback){
            User.searchDemographicByChannel(demographic, function(err, users) {
                //console.log('users');
                //console.log(users);
                var userList = [];
                if ( users ){
                    users.forEach(function(user, userkey){
                        userList[user._id] = user;
                    });
                }

                callback(null, userList);
            });
        },
        function(callback){
            Votes.searchVotingPropsAndAnswers(voting, function(err, votes) {
                var userList = [];
                //console.log('votes results');
                //console.log(votes);
                if ( votes ){
                    votes.forEach(function(vote, propkey){
                        if ( vote.user && inSubscriptions(channel._id, vote.user.subscriptions )){
                            userList[vote.user._id] = vote.user;
                        }
                    });
                }

                callback(null, userList);
            });
        }
    ],
        // optional callback
        function(err, results){
            res.jsonp(phpjs.array_values(phpjs.array_intersect_key(results[0], results[1])));
        });
};

exports.analyticsPropsSearch = function(req, res, next){
    var channel = req.channel;
    var demographic = {};
    var tagging = {};
    var propsharing = {};

    demographic.channel_id = channel._id;
    demographic.gender =    req.body.gender;
    demographic.age_from =  req.body.age_from;
    demographic.age_to =    req.body.age_to;
    demographic.relationship_status = req.body.relationship_status;
    demographic.location = req.body.location;

    tagging.channel_id = channel._id;
    tagging.tagList = req.body.tagList;

    propsharing.channel_id = channel._id;
    propsharing.numberOfVotes_from = req.body.numberOfVotes_from;
    propsharing.numberOfVotes_to = req.body.numberOfVotes_to;
    propsharing.votePercent_from = req.body.votePercent_from;
    propsharing.votePercent_to = req.body.votePercent_to;
    propsharing.numberOfShares_from = req.body.numberOfShares_from;
    propsharing.numberOfShares_to = req.body.numberOfShares_to;
    propsharing.sharePercent_from = req.body.sharePercent_from;
    propsharing.sharePercent_to = req.body.sharePercent_to;
    propsharing.numberOfViews_from = req.body.numberOfViews_from;
    propsharing.numberOfViews_to = req.body.numberOfViews_to;
    propsharing.viewPercent_from = req.body.viewPercent_from;
    propsharing.viewPercent_to = req.body.viewPercent_to;
    propsharing.numberOfNewVoters_from = req.body.numberOfNewVoters_from;
    propsharing.numberOfNewVoters_to = req.body.numberOfNewVoters_to;
    propsharing.newVotersPerShare_from = req.body.newVotersPerShare_from;
    propsharing.newVotersPerShare_to = req.body.newVotersPerShare_to;

    async.series([
        function(callback){
            User.searchDemographicByChannel(demographic, function(err, users) {
                var propList = [];
                if ( users ){
                    users.forEach(function(user, userkey){
                        if ( user.props ){
                            user.props.forEach(function(prop, propkey){
                                propList[prop._id] = prop;
                            });
                        }
                    });
                }

                callback(null, propList);
            });
        },
        function(callback){
            Prop.searchTagsByChannel(tagging, function(err, props) {
                var propList = [];
                if ( props ){
                    props.forEach(function(prop, propkey){
                        propList[prop._id] = prop;
                    });
                }

                //console.log(propList);
                callback(null, propList);
            });
        },
        function(callback){
            Prop.searchSharingByChannel(propsharing, function(err, props) {
                var propList = [];
                if ( props ){
                    props.forEach(function(prop, propkey){
                        propList[prop._id] = prop;
                    });
                }

                //console.log(propList);
                callback(null, propList);
            });
        }
    ],
    // optional callback
    function(err, results){
        //console.log(phpjs.array_keys(results[0]));
        //console.log(phpjs.array_keys(results[1]));
        //console.log(phpjs.array_keys(results[2]));
        console.log(phpjs.array_keys(phpjs.array_intersect_key(results[0], results[1], results[2])));
        res.jsonp(phpjs.array_values(phpjs.array_intersect_key(results[0], results[1], results[2])));
    });
};

exports.analyticsChannelInsight = function(req, res, next){
    var channel = req.channel;
    var insight = {};

    async.series([
        function(callback){
            Prop.find( { deleted: false, channel: channel  }).exec(function(err, props) {//total shares for all props and polls
                if ( props ){
                    var total_shares = 0, total_shares_fb = 0, total_shares_tw = 0,
                        total_direct_browser = 0, ext_embedded_url = 0, ext_embedded_app = 0;
                    props.forEach(function(prop, propkey){
                        total_shares =  total_shares + prop.counts.share;
                        total_shares_fb += prop.counts.share_fb;
                        total_shares_tw += prop.counts.share_tw;
                        total_direct_browser += prop.counts.direct_from_browser;
                        ext_embedded_url += prop.counts.ext_embedded_url;
                        ext_embedded_app += prop.counts.ext_embedded_app;
                    });

                    insight.total_shares = total_shares;
                    insight.total_shares_fb = total_shares_fb;
                    insight.total_shares_tw = total_shares_tw;
                    insight.total_direct_browser = total_direct_browser;
                    insight.ext_embedded_url = ext_embedded_url;
                    insight.ext_embedded_app = ext_embedded_app;
                }

                callback(null);
            });
        },
        function(callback){
            var propList = [];
            Prop.find({ deleted: false, channel: channel, type: 'classic'   }).exec(function(err, props) {
                if ( props ){
                    var total_prop_shares = 0;
                    insight.total_props = props.length;
                    props.forEach(function(prop, propkey){
                        total_prop_shares =  total_prop_shares + prop.counts.share;
                        propList.push(prop._id);
                    });

                    insight.total_prop_shares = total_prop_shares;
                    insight.avg_shares_per_prop = insight.total_prop_shares / insight.total_props;
                }

                Votes.count({ prop: { $in: propList }}).exec(function(err, count) {
                    if ( count ){
                        insight.total_prop_votes = count;
                        insight.avg_votes_per_prop = insight.total_prop_votes / insight.total_props;
                    }

                    callback(null);
                });
            });
        },
        function(callback){
            var propList = [];
            Prop.find({ deleted: false, channel: channel, type: 'opinion'   }).exec(function(err, props) {
                if ( props ){
                    var total_poll_shares = 0;
                    insight.total_polls = props.length;
                    props.forEach(function(prop, propkey){
                        total_poll_shares =  total_poll_shares + prop.counts.share;
                        propList.push(prop._id);
                    });

                    insight.total_poll_shares = total_poll_shares;
                    insight.avg_shares_per_poll = insight.total_poll_shares / insight.total_polls;
                }

                Votes.count({ prop: { $in: propList }}).exec(function(err, count) {
                    if ( count ){
                        insight.total_poll_votes = count;
                        insight.avg_votes_per_poll = insight.total_poll_votes / insight.total_polls;
                    }

                    callback(null);
                });
            });
        }
    ],
    // optional callback
    function(err, results){
        insight.total_subscribers = channel.subscribers.length;
        res.jsonp(insight);
    });
}

function myLog(title, data) {
    console.log('-------------------------------');
    console.log(title);
    console.log('-------------------------------');
    if ( data ){
        console.log(data);
    }
}
