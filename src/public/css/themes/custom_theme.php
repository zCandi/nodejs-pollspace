<?php
header("Content-type: text/css");

$theme = file_get_contents('flashprop_theme.css');

$old_color_1 = "#ed1c24";
$old_color_2 = "#33ccff";
$old_color_3 = "#fcee21";
$old_color_text = "#000000";
$old_colors = array($old_color_1, $old_color_2, $old_color_3, $old_color_text);

if(isset($_GET['ct'])) {
	$new_color_text = "#" . $_GET['ct'];
} else {
	$new_color_text = "#000000";
}

if(isset($_GET['c1'])) {
	$new_color_1 = "#" . $_GET['c1'];
} else {
	$new_color_1 = "#ffffff";
}

if(isset($_GET['c2'])) {
	$new_color_2 = "#" . $_GET['c2'];
} else {
	$new_color_2 = "#cccccc";
}

if(isset($_GET['c3'])) {
	$new_color_3 = "#" . $_GET['c3'];
} else {
	$new_color_3 = "#999999";
}


$new_colors = array($new_color_1, $new_color_2, $new_color_3, $new_color_text);

$new_theme = (str_replace($old_colors, $new_colors, $theme));

echo $new_theme;