Single.prototype.vote = function(){
    var $this = this;
    var answerid = $("input[name=answer_"+this.guid+"]:checked").val();
    var propguid = this.guid;

    $(".vote-image").hide();

    Ajax.get('/api/prop/'+propguid+'/vote/'+answerid, {}, function(response){
        var prop = response.prop;
        $this.prop = prop;

        $this.viewResults();
    });
};

Single.prototype.viewResults = function(){
    var propguid = this.guid;

    Ajax.get('/prop/'+propguid+'/embedded-results', {}, function(response){
        $('#pss_container_'+propguid).html(response);
    });
};

