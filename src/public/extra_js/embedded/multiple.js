Multiple.prototype.votingprops = [];
Multiple.prototype.finish = false;
Multiple.prototype.votes = [];
Multiple.prototype.currentQuestion = 0;


Multiple.prototype.goToNextQuestion = function(){
    this.currentQuestion = this.currentQuestion + 1;

    if ( this.currentQuestion <= Object.keys(this.props).length ){
        $(".question-wrapper-"+this.guid).each(function(key, item){
            $(item).hide();
        });

        $(".question-wrapper-"+this.guid+'-'+this.currentQuestion).show();
    }
};

Multiple.prototype.initQuestions = function(){
    var $this = this;
    var voteCount = 0;
    this.props.forEach(function(prop, key){
        if ( prop.userVote ){
            voteCount++;
            $this.goToNextQuestion();
        }
    });

    if ( voteCount == 0 ){
        $this.goToNextQuestion(); //will go to first question
    }

    if ( $this.currentQuestion == Object.keys($this.props).length ){
        $(".view-results-"+$this.guid).show();
    }
};

Multiple.prototype.viewResults = function(){
    var $this = this;
    var propsetguid = this.guid;
    Ajax.get('/q/'+propsetguid+'/embedded-results', {}, function(response){
        $('#psm_container_'+propsetguid).parent().html(response);
    });
};

Multiple.prototype.vote = function(propguid){
    var $this = this;
    var answerid = $("input[name=answer_"+this.guid+"]:checked").val();

    $(".vote-image-"+this.guid+"-"+this.currentQuestion).hide();

    Ajax.get('/api/prop/'+propguid+'/vote/'+answerid, {}, function(response){
        var prop = response.prop;

        $this.votingprops.push({prop: prop, answer: answerid})

        if ( prop.type == 'quiz' ){
            $this.calculatePercentage();
        }

        if ( $this.currentQuestion == Object.keys($this.props).length ){
            $(".view-results-"+$this.guid).show();
        }

        $this.goToNextQuestion();
    });
};

Multiple.prototype.getCorrectPropAnswer = function(prop){
    var correct = null;
    prop.answers.forEach(function(answer, key){
        if (answer.correct) {
            correct = answer.id;
        }
    });

    return correct;
};

Multiple.prototype.calculatePercentage = function(){
    var $this = this;
    var percent = 0;
    var totalCorrect = 0;
    var totalIncorrect = 0;
    this.votingprops.forEach(function(data, key){
        var prop = data.prop;
        var answerid = data.answer;
        var correct = $this.getCorrectPropAnswer(prop);
        if ( correct.toString() == answerid.toString()){
            totalCorrect = totalCorrect + 1;
        } else {
            totalIncorrect = totalIncorrect + 1;
        }
    });

    percent = Math.round((totalCorrect / this.votingprops.length) * 100);

    $('.quiz-score-'+this.guid).html(percent + "%");
};

Multiple.prototype.getTotalPropAnswer = function(prop, answerid){
    var total = 0;
    prop.votes.forEach(function(vote, key){
        if ( vote.choice == answerid ){
            total = total + 1;
        }
    });

    return total;
};

