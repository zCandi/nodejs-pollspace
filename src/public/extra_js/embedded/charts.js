var charts = {};
charts.getPiechartConfiguration = function(){
    return {
        "options": {
            "chart": {
                "type": "pie",
                "backgroundColor": "transparent",
                "className": "results_piechart",
                "marginTop": "0"
            },
            "plotOptions": {
                "pie": {
                    "allowPointSelect": true,
                    "cursor": "pointer",
                    "clip": false,
                    "dataLabels": {
                        "enabled": false
                    }

                }
            },
            "tooltip": {
                "formatter": function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.point.y.toFixed (2);
                }
            }
        },
        "series": [
            {
                "type": "pie",
                "data": []
            }
        ],
        "title": {
            "style": {"display": "none"}
        },
        "credits": {
            "enabled": false
        },
        "size": {},
        "exporting": {
            "buttons":{
                "exportButton": {
                    "enabled": false
                }
            },
            "enabled": false
        },
        "legend": {
            "enabled": true,
            "x": 0,
            "y": 20
        }
    };
};

charts.getBarchartConfigurationAge = function(){
    return {
        "options": {
            "chart": {
                "type": "column",
                "backgroundColor": "transparent",
                "className": "barcharts results_barchartAge",
                "width": "150",
                "marginTop": "0"
            },
            "plotOptions": {
                "column": {
                    "stacking": "normal",
                    "dataLabels": {
                        "enabled": "true",
                        "style": {
                            "fontWeight": "",
                            "color": "#FFFFFF",
                            "fontSize": "15px"
                        }
                    }
                },
                "series":{
                    "borderColor": "transparent"
                }
            }
        },
        "xAxis": {
            "categories": ["Age"],
            "gridLineWidth": 0,
            "opposite": true,
            "lineColor": "transparent",
            "labels": {
                "style": {
                    "display": "none"
                }
            }

        },
        "yAxis": {
            "min": 0,
            "gridLineWidth": 0,
            "gridLineColor": "transparent",
            "title": {
                "style": {
                    "display": "none"
                }
            },
            "labels":{
                "style": {
                    "display": "none"
                }
            }
        },
        "series": [],
        "title": {
            "text": "Age",
            "align": "center",
            "style": {
                "color": "#FFFFFF",
                "fontWeight": "400",
                "fontSize": "15px",
                "padding": "0px 0px -20px 0px"
            }
        },
        "credits": {
            "enabled": false
        },
        "legend": {
            "enabled": false,
            "x": 0,
            "y": 10
        },
        "loading": false,
        "size": {}
    };
};

charts.getBarchartConfigurationGender = function(){
    return {
        "options": {
            "chart": {
                "type": "column",
                "backgroundColor": "transparent",
                "className": "barcharts results_barchartAge",
                "width": "150",
                "marginTop": "0"
            },
            "plotOptions": {
                "column": {
                    "stacking": "normal",
                    "dataLabels": {
                        "enabled": "true",
                        "style": {
                            "fontWeight": "",
                            "color": "#FFFFFF",
                            "fontSize": "15px"
                        }
                    }
                },
                "series":{
                    "borderColor": "transparent"
                }
            }
        },
        "xAxis": {
            "categories": ["Gender"],
            "gridLineWidth": 0,
            "opposite": true,
            "lineColor": "transparent",
            "labels": {
                "style": {
                    "display": "none"
                }
            }

        },
        "yAxis": {
            "min": 0,
            "gridLineWidth": 0,
            "gridLineColor": "transparent",
            "title": {
                "style": {
                    "display": "none"
                }
            },
            "labels":{
                "style": {
                    "display": "none"
                }
            }
        },
        "series": [],
        "legend":{
            "enabled": false
        },
        "title": {
            "text": "Gender",
            "align": "center",
            "style": {
                "color": "#FFFFFF",
                "fontWeight": "400",
                "fontSize": "15px",
                "padding": "0px 0px -20px 0px"}
        },
        "credits": {
            "enabled": false
        },
        "loading": false,
        "size": {}
    };
};

charts.getBarchartConfigurationRelationshipStatus = function(){
    return {
        "options": {
            "chart": {
                "type": "column",
                "backgroundColor": "transparent",
                "className": "barcharts results_barchartRelationshipStatus",
                "width": "150",
                "marginTop": "0"
            },
            "plotOptions": {
                "column": {
                    "stacking": "normal",
                    "dataLabels": {
                        "enabled": "true",
                        "style": {
                            "fontWeight": "",
                            "color": "#FFFFFF",
                            "fontSize": "15px"
                        }
                    }
                },
                "series":{
                    "borderColor": "transparent"
                }
            }
        },
        "xAxis": {
            "categories": ["Relationship"],
            "gridLineWidth": 0,
            "opposite": true,
            "lineColor": "transparent",
            "labels": {
                "style": {
                    "display": "none"
                }
            }
        },
        "yAxis": {
            "min": 0,
            "gridLineWidth": 0,
            "gridLineColor": "transparent",
            "title": {
                "style": {
                    "display": "none"
                }
            },
            "labels":{
                "style": {
                    "display": "none"
                }
            }
        },
        "series": [],
        "legend":{
            "enabled": false
        },
        "title": {
            "text": "Relationship",
            "align": "center",
            "style": {
                "color": "#FFFFFF",
                "fontWeight": "400",
                "fontSize": "15px",
                "padding": "0px 0px -20px 0px"}
        },
        "credits": {
            "enabled": false
        },
        "loading": false,
        "size": {}
    };
};

charts.getLinechartConfiguration = function(){
    return {
        "options": {
            "chart": {
                "type": "line",
                "backgroundColor": "transparent",
                "className": "barcharts results_linechart"
            },
            "plotOptions": {
                "size": "90%",
                "line": {
                    "dataLabels": {
                        "enabled": true
                    },
                    "enableMouseTracking": false
                }
            }
        },
        "series": [
            {
                "data": [],
                "name": false,
                "type": "line"
            }
        ],
        "legend": {
            "enabled": "false",
            "style": {"display": "none"}
        },
        "xAxis": {
            "categories": ["5/7", "5/8", "5/9", "5/10", "5/11", "5/12"],
            "title": {
                "style": {"display": "none"}
            },
            "labels": {
                "style": {
                    "color": "#FFFFFF",
                    "fontWeight": "bold",
                    "fontSize": "10px"
                }
            }
        },
        "yAxis":{
            "title": {
                "style": {"display": "none"}
            },
            "min": 0
        },
        "title": {
            "style": {"display": "none"}
        },
        "credits": {
            "enabled": false
        },
        "loading": false,
        "size": {}
    }
};

charts.dec2html = function(d){
    // Converts from decimal color value (0-255) to HTML-style (00-FF)
    var hch="0123456789ABCDEF";
    var a=d%16;
    var b=(d-a)/16;
    return hch.charAt(b)+hch.charAt(a);
}

charts.html2dec = function (h){
    return parseInt(h,16);
}

charts.rgb2html = function (rgb){
    // Converts from RGB color object to HEX/HTML-style color (AABBCC)
    return this.dec2html(rgb.r) + this.dec2html(rgb.g)+ this.dec2html(rgb.b);
}

charts.rgb2hsv = function(rg){
    // Converts an RGB color object to a HSV color object
    var hs=new Object();
    var m=rg.r;
    if(rg.g<m){m=rg.g};
    if(rg.b<m){m=rg.b};
    var v=rg.r;
    if(rg.g>v){v=rg.g};
    if(rg.b>v){v=rg.b};
    var value=100*v/255;
    var delta=v-m;
    if(v==0.0){hs.s=0}else{hs.s=100*delta/v};
    if(hs.s==0){hs.h=0}else{
        if(rg.r==v){hs.h=60.0*(rg.g-rg.b)/delta}
        else if(rg.g==v){hs.h=120.0+60.0*(rg.b-rg.r)/delta}
        else if(rg.b=v){hs.h=240.0+60.0*(rg.r-rg.g)/delta}
        if(hs.h<0.0){hs.h=hs.h+360.0}
    }
    hs.h=Math.round(hs.h);
    hs.s=Math.round(hs.s);
    hs.v=Math.round(value);
    return(hs);
}

charts.hsv2rgb = function(hsx){
    // Converts an HSV color object to a RGB color object
    var rg=new Object();

    var ls = hsx.s;
    var lh = hsx.h;
    var lv = hsx.v;

    if(ls==0){
        rg.r=rg.g=rg.b=Math.round(lv*2.55); return(rg);
    }
    ls=ls/100;
    lv=lv/100;
    lh/=60;
    var i=Math.floor(lh);
    var f=lh-i;
    var p=lv*(1-ls);
    var q=lv*(1-ls*f);
    var t=lv*(1-ls*(1-f));
    switch(i){
        case 0:rg.r=lv; rg.g=t; rg.b=p; break;
        case 1:rg.r=q; rg.g=lv; rg.b=p; break;
        case 2:rg.r=p; rg.g=lv; rg.b=t; break;
        case 3:rg.r=p; rg.g=q; rg.b=lv; break;
        case 4:rg.r=t; rg.g=p; rg.b=lv; break;
        default: rg.r=lv; rg.g=p; rg.b=q;
    }
    rg.r=Math.round(rg.r*255);
    rg.g=Math.round(rg.g*255);
    rg.b=Math.round(rg.b*255);
    return(rg);
}

charts.html2rgb = function(htmlcol){
    var rgb = new Object();
    rgb.r=this.html2dec(htmlcol.substr(0,2));
    rgb.g=this.html2dec(htmlcol.substr(2,2));
    rgb.b=this.html2dec(htmlcol.substr(4,2));
    return rgb;
};

charts.hueToWheel = function(h) {
    if(h<=120){
        return(Math.round(h*1.5));
    }else{
        return(Math.round(180+(h-120)*0.75));
    }
}

charts.wheelToHue = function(w) {
    if(w<=180){
        return(Math.round(w/1.5));
    }else{
        return(Math.round(120+(w-180)/0.75));
    }
}

charts.getMatchingColorPalette = function(initialColor, total){
    var matchingList = [];
    var weelStartingValue = 45;
    for ( var i = 0; i < total; i++ ){
        var hsv = this.rgb2hsv(this.html2rgb(initialColor));
        var w = this.hueToWheel(hsv.h);
        var z = new Object();

        z.h = this.wheelToHue((w+weelStartingValue)%360);
        z.s = hsv.s;
        z.v = hsv.v;

        matchingList[i] = this.rgb2html(this.hsv2rgb(z));
        weelStartingValue = weelStartingValue + 90;
    }

    return matchingList;
};

charts.matchingColorList = [];

charts.putInRange = function(n,l,h)
{
    return (n<l)?l:((n>h)?h:n);
}

charts.RGB2Hex = function(rgb) {
    return charts.Dec2Hex(rgb.r)+charts.Dec2Hex(rgb.g)+charts.Dec2Hex(rgb.b);
}

charts.Dec2Hex = function(d) {
    var hexdig='0123456789ABCDEF';
    return hexdig.charAt((d-(d%16))/16)+hexdig.charAt(d%16);
}

charts.setColors = function(r,g,b,a) {
    var rgb = {};
    rgb.r = r;
    rgb.g = g;
    rgb.b = b;

    var rgba = {};
    rgba.r =Math.round(r+(255-r)*charts.grade1);
    rgba.g =Math.round(g+(255-g)*charts.grade1);
    rgba.b =Math.round(b+(255-b)*charts.grade1);

    var rgbb = {};
    rgbb.r =Math.round(r+(255-r)*charts.grade2);
    rgbb.g =Math.round(g+(255-g)*charts.grade2);
    rgbb.b =Math.round(b+(255-b)*charts.grade2);

    var rgbc = {};
    rgbc.r =Math.round(r*charts.grade3);
    rgbc.g =Math.round(g*charts.grade3);
    rgbc.b =Math.round(b*charts.grade3);

    var rgbd = {};
    rgbd.r =Math.round(r*charts.grade4);
    rgbd.g =Math.round(g*charts.grade4);
    rgbd.b =Math.round(b*charts.grade4);

    var colora= "#" + charts.rgb2html(rgba);
    var colorb= "#" + charts.rgb2html(rgbb);
    var colorc= "#" + charts.rgb2html(rgb);
    var colord= "#" + charts.rgb2html(rgbc);
    var colore= "#" + charts.rgb2html(rgbd);

    for (var i=0;i<a.length;i+=2) {
        charts.matchingColorList[a[i]+'_'+a[i+1]+'_0'] = colora;
        charts.matchingColorList[a[i]+'_'+a[i+1]+'_1'] = colorb;
        charts.matchingColorList[a[i]+'_'+a[i+1]+'_2'] = colorc;
        charts.matchingColorList[a[i]+'_'+a[i+1]+'_3'] = colord;
        charts.matchingColorList[a[i]+'_'+a[i+1]+'_4'] = colore;
    }
}

charts.hueShift = function(h,s) {
    h+=s;
    while (h>=360.0) h-=360.0;
    while (h<0.0) h+=360.0;
    return h;
}

charts.getMatchingColorList = function(hex) {
    charts.grade1 = charts.putInRange(parseFloat(0.8),0.0,1.0);
    charts.grade2 = charts.putInRange(parseFloat(0.4),0.0,1.0);
    charts.grade3 = charts.putInRange(parseFloat(0.6),0.0,1.0);
    charts.grade4 = charts.putInRange(parseFloat(0.3),0.0,1.0);

    var rgb = charts.html2rgb(hex);
    var r = rgb.r;
    var g = rgb.g;
    var b = rgb.b;

    charts.setColors(r,g,b,Array('monochrome','1','complementary','1','analogous','2','split_complementary','2','triadic','1'));
    charts.setColors(g,b,r,Array('triadic','2'));

    // complement
    var temphsv = charts.rgb2hsv(rgb);
    temphsv.h = charts.hueShift(temphsv.h, 180.0);
    var temprgb = charts.hsv2rgb(temphsv);
    charts.setColors(temprgb.r,temprgb.g,temprgb.b, Array('complementary','2'));

    // analogous
    var temphsv = charts.rgb2hsv(rgb);
    temphsv.h = charts.hueShift(temphsv.h, 30.0);
    var temprgb = charts.hsv2rgb(temphsv);
    charts.setColors(temprgb.r,temprgb.g,temprgb.b,Array('analogous','1'));

    var temphsv = charts.rgb2hsv(rgb);
    temphsv.h = charts.hueShift(temphsv.h, 0.0 - 30.0);
    var temprgb = charts.hsv2rgb(temphsv);
    charts.setColors(temprgb.r,temprgb.g,temprgb.b,Array('analogous','3'));

    // split complementary
    var temphsv = charts.rgb2hsv(rgb);
    temphsv.h = charts.hueShift(temphsv.h, 180.0 - 30.0);
    var temprgb = charts.hsv2rgb(temphsv);
    charts.setColors(temprgb.r,temprgb.g,temprgb.b,Array('split_complementary','1'));

    var temphsv = charts.rgb2hsv(rgb);
    temphsv.h = charts.hueShift(temphsv.h, 180.0 + 30.0);
    var temprgb = charts.hsv2rgb(temphsv);
    charts.setColors(temprgb.r,temprgb.g,temprgb.b,Array('split_complementary','3'));

    return charts.matchingColorList;
}