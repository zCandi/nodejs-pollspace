'use strict';

angular.module('mean').service('CurrentChannel',function ($compile, $rootScope, $localStorage, Global) {
    this.getCurrentChannel = function(){
        console.log('getCurrentChannel');
        console.log($localStorage.currentChannel);
        return $localStorage.currentChannel;
    };

    this.setCurrentChannel = function(currentChannel){
        $localStorage.currentChannel = currentChannel;
        console.log('setCurrentChannel');
        console.log($localStorage.currentChannel);
        if ( !_.isEmpty(currentChannel) ){
            Global.cguid = currentChannel.guid;
        }

        $rootScope.$broadcast('updateTopAndSideBarEvent');
    };


    this.userVoteChannelProducts = function(){
        var channel = Global.channel;
        var user = Global.user;
        if ( _.isEmpty( channel.voted_products ) ){
            return false;
        } else {
            var flag = false
            channel.voted_products.forEach(function(_product, key){
		if (_product.user && user){

                if (_product.user._id.toString() == user._id.toString()) {
                    flag = true
                }
}
            });
            return flag;
        }
    };

    this.setCurrentProp = function(prop){
        $localStorage.currentProp = prop;
    };

    this.getCurrentProp = function(){
        return $localStorage.currentProp;
    };

    this.setCurrentPropset = function(propset){
        $localStorage.currentPropset = propset;
    };

    this.getCurrentPropset = function(){
        return $localStorage.currentPropset;
    };

    this.changeColorChannel = function(currentChannel){
        var head = angular.element(document.querySelector('head')); // TO make the code IE < 8 compatible, include jQuery in your page and replace "angular.element(document.querySelector('head'))" by "angular.element('head')"                
        if ( currentChannel != null && currentChannel.color != null){
            var ct = currentChannel.color.color_text;
            var c1 = currentChannel.color.color1;
            var c2 = currentChannel.color.color2;
            var c3 = currentChannel.color.color3;
            var palette = currentChannel.color.palette;
            var colors = {};

            if ( !_.isEmpty(ct) ){
                colors['ct'] = ct;
            }
            if ( !_.isEmpty(c1) ){
                colors['c1'] = c1;
            }
            if ( !_.isEmpty(c2) ){
                colors['c2'] = c2;
            }
            if ( !_.isEmpty(c3) ){
                colors['c3'] = c3;
            }
            if ( !_.isEmpty(palette) ){
                colors['palette'] = palette;
            }

            if ( Object.keys(colors).length > 1 ){
                var url = "?colors="+encodeURIComponent(JSON.stringify(colors));
                if ( $("#channelColor").size() == 1 ){
                    console.log('ENTTRE');
                    $("#channelColor").remove();
                }
                head.append($compile("<link id='channelColor' rel='stylesheet' data-ng-href='/api/channel/color/style.css"+url+"' >")($rootScope));
            }
        } else {
            console.log('ENTTREs');
            $compile($("#channelColor").remove());
        }
    };

    this.setDefaultColor = function(){
        $compile($("#channelColor").remove());
    };

    this.removeCurrentChannel = function(){
        //$localStorage.clearAll();
        this.setCurrentChannel(null);
    }
});

//Channels service used for articles REST endpoint
angular.module('mean.channels').factory('ChannelAdminFactory', ['$resource', function($resource) {
    return $resource('/api/admin/channel/:id',
        {
            id: "@id",
            user_id: "@user_id",
            type: "@type",
            photo_id: "@photo_id"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            update: {
                method: 'PUT'
            },
            subscribe: {
                url: '/api/channel/:id/subscribe',
                method: 'POST',
                isArray: false
            },
            deactivate: {
                url: '/api/admin/channel/:id/deactivate',
                method: 'GET',
                isArray: false
            },
            deactivatePhoto: {
                url: '/api/admin/channel/:id/photos/:photo_id/deactivatePhoto',
                method: 'POST',
                isArray: false
            },
            list: {
                url: '/api/admin/channel/list',
                method: 'GET',
                isArray: true
            },
            listAll: {
                url: '/api/admin/channel/list/all',
                method: 'GET',
                isArray: true
            },
            propList: {
                url: '/api/admin/channel/:id/prop/:type/list',
                method: 'GET',
                isArray: true
            },
            admins:{
                url: '/api/admin/channel/:id/admin/list',
                method: 'GET',
                isArray: true
            },
            adminAdd:{
                url: '/api/admin/channel/:id/admin/:user_id/add',
                method: 'POST',
                isArray: false
            },
            adminRemove:{
                url: '/api/admin/channel/:id/admin/:user_id/remove',
                method: 'POST',
                isArray: false
            },
            leaderboard:{
                url: '/api/admin/channel/:id/leaderboard',
                method: 'GET',
                isArray: true
            },
            resetleaderboard:{
                url: '/api/admin/channel/:id/resetleaderboard',
                method: 'POST',
                isArray: true
            },
            tagList: {
                url: '/api/admin/channel/:id/tag/list',
                method: 'POST',
                isArray: true
            },
            searchAnalyticsUsers:{
                url: '/api/admin/channel/:id/analytics/users',
                method: 'POST',
                isArray: true
            },
            searchAnalyticsProps:{
                url: '/api/admin/channel/:id/analytics/props',
                method: 'POST',
                isArray: true
            },
            getChannelInsights:{
                url: '/api/admin/channel/:id/analytics/channel',
                method: 'GET',
                isArray: false
            }
        }
    );
}]);

angular.module('mean.channels').factory('ChannelFactory', ['$resource', function($resource) {
    return $resource('/api/channel/:id',
        {
            id: "@id",
            user_id: "@user_id",
            guid: "@guid"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            getByGuid:{
                url: '/api/channel/:guid/view',
                cache: false,
                method: 'GET',
                isArray: false
            },
            update: {
                method: 'PUT'
            },
            list: {
                url: '/api/channel/:guid/list',
                method: 'GET',
                isArray: true
            },
            leaderboard:{
                url: '/api/channel/:guid/leaders',
                method: 'GET',
                isArray: false
            },
            contact:{
                url: '/api/channel/:guid/contact',
                method: 'POST',
                isArray: false
            },
            reset:{
                url: '/channel/reset',
                method: 'GET',
                isArray: false
            },
            uploadPhoto:{
                url: '/api/channel/:guid/upload/photo',
                method: 'POST',
                isArray: false
            },
            getUploadedPhoto:{
                url: '/api/channel/:guid/uploaded/photo',
                method: 'GET',
                isArray: false
            },
            lastProp: {
                url: '/api/channel/:guid/last/prop/:type',
                method: 'GET',
                isArray: false
            }
        }
    );
}]);


angular.module('mean').service('Channel',function (Global) {
    this.getCurrentChannel = function(){

    };

});




/**
 * Example as they were Articles from mean.io
 *
 * It creates a new factory in angular called Articles. The Articles factory has the $resource service injected.
 * The $resource object is used to setup an object for communicating with a RESTful service,
 * in this case "articles/:articleId" the articleId will be pulled from the _id of the resource objects that are
 * returned from queries using this $resource. When you call to update on one of the resources it will use the PUT HTTP Verb.

 By itself this just defines the factory but doesn't actually make any calls you would need to inject this and use
 it somewhere like Articles.query();

 From the docs

 If the parameter value is prefixed with @ then the value of that parameter is extracted from the data object
 (useful for non-GET operations).
 *
 *
 */
