'use strict';

/* Reference: http://docs.angularjs.org/api/ngResource/service/$resource */
angular.module('mean.system').factory('GroupsAdminFactory', ['$resource', function($resource) {
    return $resource('/api/admin/group/:id',
        {
            id: "@id",
            type: "@type"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            list: {
                url: '/api/admin/group/list/:type',
                method: 'GET',
                isArray: true
            }
        }
    );
}]);
