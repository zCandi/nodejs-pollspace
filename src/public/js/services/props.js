'use strict';

/* Reference: http://docs.angularjs.org/api/ngResource/service/$resource */
angular.module('mean.props').factory('PropsAdminFactory', ['$resource', function($resource) {
    return $resource('/api/admin/prop/:id',
        {
            channel_id: "@channel_id",
            id: "@id",
            type: "@type"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            update: {
                method: 'PUT'
            },
            deactivate: {
                url: '/api/admin/prop/:id/deactivate',
                method: 'GET',
                isArray: false
            },
            list: {
                url: '/api/admin/channel/:channel_id/prop/:type/list',
                method: 'GET',
                isArray: true
            },
            suggestedbylist: {
                url: '/api/admin/channel/:channel_id/prop/suggestedby/list',
                method: 'GET',
                isArray: true
            },
            results: {
                url: '/api/admin/prop/:id/results',
                method: 'GET',
                isArray: false
            },
            create:{
                url: '/api/admin/channel/:channel_id/prop/create',
                method: 'POST',
                isArray: false
            }
        }
    );
}]);

angular.module('mean.props').factory('PropsFactory', ['$resource', function($resource) {
    return $resource('/api/prop/:guid',
        {
            cguid: "@cguid",
            channel_id: "@channel_id",
            guid: "@guid",
            type: "@type"
        },
        {
            get:{
                method: 'GET'
            },
            update: {
                method: 'PUT'
            },
            vote: {
                url: '/api/prop/:guid/vote',
                method: 'POST',
                isArray: false
            },
            voteProduct: {
                url: '/api/channel/:cguid/product/vote',
                method: 'POST',
                isArray: false
            },
            votestats: {
                url: '/api/prop/:guid/vote/stats',
                method: 'GET',
                isArray: false
            },
            voteuser: {
                url: '/api/prop/:guid/vote/users',
                method: 'GET',
                isArray: false
            },
            sharingdata: {
                url: '/api/prop/:guid/share',
                method: 'GET',
                isArray: false
            },
            list: {
                url: '/api/channel/:cguid/prop/:type/list',
                method: 'GET',
                isArray: true
            },
            otherprops: {
                url: '/api/prop/:guid/others',
                method: 'GET',
                isArray: true
            },
            create:{
                url: '/api/channel/:cguid/prop/create',
                method: 'POST',
                isArray: false
            }
        }
    );
}]);


angular.module('mean').service('Single',function (Global, User) {
    this.getBanner = function(prop){
        var bannerPrefix = 'img/prop_status';
        var propStatus = this.propStatus(prop);

        if ( prop.suggested_by ) {
            if(_.isEqual(prop.suggested_by.toString(), $scope.global.user._id.toString())) {
                return  { banner: bannerPrefix +  "/prop-selected.png", mobile_banner: bannerPrefix +  "/prop-selected.png" };
            }
        }

        if ( propStatus ){
            if (_.isEqual(propStatus.toString(), 'published') || _.isEqual(propStatus.toString(), 'created') ){
                if ( User.userVote(prop) ){
                    if ( prop.type == 'opinion' ){
                        return { banner: bannerPrefix + "/poll-voted.png", mobile_banner: bannerPrefix + "/poll-voted-mobile.png"};
                    } else {
                        return { banner: bannerPrefix + "/quiz-complete.png", mobile_banner: bannerPrefix + "/quiz-complete-mobile.png"};
                    }
                } else {
                    return { banner: false, mobile_banner: false};
                }
            }


            if (_.isEqual(propStatus.toString(), 'closed') ){
                if ( User.userVote(prop) ){
                    return { banner: bannerPrefix + "/voted-full-closed-voted.png", mobile_banner: bannerPrefix + "/voted-mobile-closed-voted.png"};
                } else {
                    return { banner: bannerPrefix + "/voted-full-closed-novote.png", mobile_banner: bannerPrefix + "/voted-mobile-no-vote.png" };
                }
            }

            if (_.isEqual(propStatus.toString(), 'graded') ){
                if ( User.userVote(prop) ){
                    if ( this.userWon(prop) ){
                        return { banner: bannerPrefix + "/voted-full-won.png", mobile_banner: bannerPrefix + "/voted-mobile-won.png"};
                    } else if ( this.userLost(prop) ){
                        return { banner: bannerPrefix + "/voted-full-lost.png", mobile_banner: bannerPrefix + "/voted-mobile-lost.png"};
                    }
                } else {
                    return { banner: false, mobile_banner: false};
                }
            }
        } else {
            return { banner: false, mobile_banner: false};
        }
    };

    this.propStatus = function(prop){
        //created > published > closed > graded
        if ( this.isSet(prop.timestamps.graded)) return 'graded';
        if ( this.isSet(prop.timestamps.closed)) return 'closed';
        if ( this.isSet(prop.timestamps.published)) return 'published';
        if ( this.isSet(prop.timestamps.created)) return 'created';
    };

    this.isSet = function(date){
        var ts = new Date(date).getTime();
        return ts !== 0;
    };

    this.findChoiceName = function(choices, choice){
        var result = null;
        choices.forEach(function(_choice, key){
            if (angular.equals(_choice.id, choice)){
                result =  _choice.name;
            }
        });
        return result;
    };

    this.getCorrectPropAnswer = function(prop){
        var correct = null;
        prop.answers.forEach(function(answer, key){
            if (answer.correct) {
                correct = answer.id;
            }
        });

        return correct;
    };

    this.validateCorrectAnswer = function(answers){
        var valid = false;
        answers.forEach(function(value, key){
            if ( value.correct === true ){
                valid = true;
            }
        });

        return valid;
    };

    this.getPropVotes_bk20160512 = function(prop){
        var votes = prop.votes;
        var propVotes = [];

        for (var i in prop.answers) {
            var answer = prop.answers[i];
            propVotes[answer.id] = 0;
            for (var k in votes) {
                if ( votes[k].choice == answer.id ){
                    propVotes[answer.id]++;
                }
            }

            propVotes[answer.id] = parseInt(( propVotes[answer.id] * 100 ) / votes.length) + "%";
        }

        return propVotes;
    }
   
   this.getPropVotes = function(prop, channel){
        var votes = prop.votes;
        var propVotes = [];
        var prod_votes = [];
        var total_prod_votes = 0;

        console.log("public/js/service/props.js this.getPropVotes !!!!!!!!!!!!!!!!!!!!!!!!!");
        console.log("channel: ", channel);
        console.log("prop: ", prop);

        console.log("prop.votes: ", prop.votes);

        console.log("prop.answers: ", prop.answers);
        console.log("prop.type: ", prop.type);
        if( prop.type && prop.type == 'product' ) {
            prod_votes = channel.voted_products;
            console.log("channel.voted_products: ", channel.voted_products);

            for (var i in prop.answers) {
                var answer = prop.answers[i];
                
                console.log("answer: ", answer);
                propVotes[answer.id] = 0;

                for (var k in prod_votes) {
                    var prod_answers = prod_votes[k].answers;
                    
                    console.log("prod_answers: ", prod_answers);

                    for (var m in prod_answers ){
                        if(prod_answers[m] == answer.id) {
                            propVotes[answer.id]++;
                            total_prod_votes++;
                        }
                    }
                }
            }

            for (var n in propVotes) {
                console.log("propVotes[n]:" , propVotes[n]);
                //propVotes[n] = parseInt(( propVotes[n] * 100 ) / total_prod_votes ) + "%";
                if(total_prod_votes > 0){
                  propVotes[n] = parseInt(( propVotes[n] * 100 ) / total_prod_votes ) + "%";
                }else {
                  propVotes[n] = "0%";
               }
            }

        } else {
            //prop.type is image

            for (var i in prop.answers) {
                var answer = prop.answers[i];
                propVotes[answer.id] = 0;
                for (var k in votes) {
                    if ( votes[k].choice == answer.id ){
                        propVotes[answer.id]++;
                    }
                }
                if (votes.length > 0){
                    propVotes[answer.id] = parseInt(( propVotes[answer.id] * 100 ) / votes.length) + "%";   
                }
                else {
                    propVotes[answer.id] = "0%";
                }
               // propVotes[answer.id] = parseInt(( propVotes[answer.id] * 100 ) / votes.length) + "%";
            }
        }

        return propVotes;
    }

});
