'use strict';

//Global service for global variables
angular.module('mean.system').factory('Global',
    function() {
        var _this = this;
        _this._data = {
            user: window.user,
            cguid: window.cguid || null,
            access_token: window.access_token || null,
            twitter_access_token: window.twitter_access_token || null,
            twitter_url: window.twitter_url || null,
            hostname: window.hostname || null,
            colors: window.colors || null,
            channel: window.channel || null,
            prop: window.prop || null,
            propset: window.propset || null,
            authenticated: !! window.user,
            quiz: window.quiz,
            forgotPasswordHash: window.forgotPasswordHash || null,
            subdomain: window.subdomain
        };

        return _this._data;
    }
);

angular.module('mean').factory("flash", function($rootScope) {
    var currentMessage = "";

    return {
        setMessage: function(message) {
            currentMessage = message;
        },
        getMessage: function() {
            return currentMessage;
        }
    };
});

//http://djds4rce.wordpress.com/2013/08/13/understanding-angular-http-interceptors/
angular.module('mean').factory('myHttpResponseInterceptor',['$q','$location','Global', 'flash',function($q,$location, Global, flash){
    return {
        // On response success
        response: function (response) {
            return response || $q.when(response);
        },

        // On response failture
        responseError: function (rejection) {
            if ( rejection.status == '401' ){
                flash.setMessage("You are not allow to see that page!");
                $location.path("/");
            }
            // Return the promise rejection.
            return $q.reject(rejection);
        }
    };

}]);

//I had to add this one to be used in src/public/js/controllers/admin/props.js since providers cant be used at this point
//and services or factories cant be used in config section at that time
angular.module('mean').factory('Tpl',function(){
    return {
        getTemplate: function(template){
            var customTemplate = template.replace("views", "views/custom/" + window.cguid);
            if ( this.file_exists(customTemplate)){
                return customTemplate;
            } else {
                if ( window.subdomain && template == 'views/login.html' ){
                    return 'views/subdomain_login.html';
                }
                return template;
            }
        },

        file_exists: function(url) {
            var fileList = window.customFolderList;
            var SHA1 = window.Crypto.SHA1;
            if ( _.indexOf(fileList, SHA1(url)) !== -1 ){
                return true
            }

            return false;
        }
    }
});
