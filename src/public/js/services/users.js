'use strict';

angular.module('mean.users').factory('UsersFactory', ['$resource', function($resource) {
    return $resource('api/user/:id',
        {
            id: "@id"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            proplist: {
                url: 'api/user/:id/props',
                method: 'GET',
                isArray: true
            },
            channellist:{
                url: 'api/user/:id/channel/list',
                method: 'GET',
                isArray: true
            },
            suggestedchannellist:{
                url: 'api/user/:id/channel/suggested/list',
                method: 'GET',
                isArray: true
            },
            ranking:{
                url: 'api/user/:id/ranking',
                method: 'GET',
                isArray: true
            },
            forgotpassword:{
                url: '/api/user/forgotpassword',
                method: 'POST',
                isArray: false
            },
            resetpassword:{
                url: '/api/user/resetpassword',
                method: 'POST',
                isArray: false
            },
            signup: {
                url: '/signup',
                method: 'POST',
                isArray: false
            },
            getsignup: {
                url: '/api/admin/signups',
                method: 'GET',
                isArray: true
            }
        }
    );
}]);


angular.module('mean.users').factory('UserFPAdminFactory', ['$resource', function($resource) {
    return $resource('api/fp/user/:id',
        {
            id: "@id"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            list: {
                url: 'api/fp/user/list',
                method: 'GET',
                isArray: true
            },
            searchDemographic: {
                url: '/api/fp/search/demographic',
                method: 'POST',
                isArray: true
            },
            save: {
                url: '/api/fp/user/:id',
                method: 'POST',
                isArray: false
            }
        }
    );
}]);


angular.module('mean.users').factory('UserAdminFactory', ['$resource', function($resource) {
    return $resource('api/user/:id',
        {
            id: "@id"
        },
        {
            adminList: {
                url: 'api/user/admin/list',
                method: 'GET',
                isArray: true
            }
        }
    );
}]);


angular.module('mean').service('User',function (Global, CurrentChannel, Charts) {
    this.user = Global.user;

    this.userVote = function(prop){
        var flag = false;
        var user = this.user;
        if ( user ){
            prop.votes.forEach(function(vote, key){
                var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                    flag = true;
                }
            });
        }
        return flag;
    };

    this.getUserChoice = function(user, prop){
        var choice = null;
        if ( user ){
            prop.votes.forEach(function(vote, key){
                var vote_user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                var user_id = !_.isUndefined(user._id) ? user._id : user;
                if ( user && vote.user && _.isEqual(vote_user_id.toString(), user_id.toString()) ){
                    choice = vote.choice;
                }
            });
        };

        return choice;
    };

    this.userVoteOption = function(prop){
        var flag = false;
        var user = this.user;
        var voteOption = "";
        var $this =  this;

        if ( user ){
            prop.votes.forEach(function(vote, key){
                if ( user && vote.user && _.isEqual(vote.user._id.toString(), user._id.toString())  ){
                    var option = vote.choice;
                    voteOption = $this.findChoiceName(prop.answers, option);
                }
            });
        }

        return voteOption;
    };

    this.findChoiceName = function(choices, choice){
        var result = null;
        choices.forEach(function(_choice, key){
            if (angular.equals(_choice.id, choice)){
                result =  _choice.name;
            }
        });
        return result;
    };

    this.findUsersDataForPieChart = function(prop, type){
        var votes = prop.votes;
        var tempChartData = [];
        var $this = this;

        votes.forEach(function(vote, voteKey){
            var choice = vote.choice;
            if ( tempChartData[choice] ){
                tempChartData[choice]['count']++;
            } else {
                tempChartData[choice] = [];
                tempChartData[choice]['count'] = 1;
            }
            tempChartData[choice]['name'] = $this.findChoiceName(prop.answers, choice);
        });

        var dataList = [];
        for (var k in tempChartData) {
            var data = {};
            data.name = tempChartData[k].name;
            data.y = tempChartData[k].count;
            dataList.push(data);
        }

        var total = votes.length;
        var channel = CurrentChannel.getCurrentChannel();
        var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
        var palette = Charts.getMatchingColorList(initialColor);
        var i = 0;
        dataList.forEach(function(data, dataKey){
            if ( i == 2 ){
                i++;
            }
            data.y = (data.y * 100) / total;
            i++;
        });

        if ( type == 'single' ){
            if ( dataList[0] ){
                dataList[0].color = palette['monochrome_1_0'];
            }
            if ( dataList[1] ){
                dataList[1].color = palette['monochrome_1_4'];
            }
            if ( dataList[2] ){
                dataList[2].color = palette['monochrome_1_1'];
            }
            if ( dataList[3] ){
                dataList[3].color = palette['monochrome_1_3'];
            }
        } else if ( type == 'multiple' ){
            if ( dataList[0] ){
                dataList[0].color = palette['monochrome_1_2'];
            }
            if ( dataList[1] ){
                dataList[1].color = palette['monochrome_1_4'];
            }
            if ( dataList[2] ){
                dataList[2].color = palette['monochrome_1_3'];
            }
            if ( dataList[3] ){
                dataList[3].color = palette['monochrome_1_1'];
            }
        }



        return dataList;

    };

    this.getGenderChartData = function(prop){
        var votes = prop.votes;
        var tempChartData = [];

        votes.forEach(function(vote, voteKey){
            if ( vote.user && vote.user.facebook_data ){
                var gender = vote.user.facebook_data.gender;
                if ( tempChartData[gender] ){
                    tempChartData[gender]['count']++;
                } else {
                    tempChartData[gender] = [];
                    tempChartData[gender]['count'] = 1;
                }
            }
        });

        return tempChartData;

    };

    this.getAgeChartData = function(prop){
        var votes = prop.votes;
        var tempChartData = [];

        votes.forEach(function(vote, voteKey){
            if ( vote.user && vote.user.facebook_data ){
                var age = vote.user.facebook_data.age;
                if ( age < 21 ){
                    if ( tempChartData['a'] ){
                        tempChartData['a']['count']++;
                    } else {
                        tempChartData['a'] = [];
                        tempChartData['a']['count'] = 1;
                        tempChartData['a']['name'] = "-21";
                    }
                } else {
                    if ( age > 21 && age <= 25 ){
                        if ( tempChartData['b'] ){
                            tempChartData['b1']['count']++;
                        } else {
                            tempChartData['b'] = [];
                            tempChartData['b']['count'] = 1;
                            tempChartData['b']['name'] = "21-25";
                        }
                    } else {
                        if ( age > 26 && age <= 35 ){
                            if ( tempChartData['c'] ){
                                tempChartData['c']['count']++;
                            } else {
                                tempChartData['c'] = [];
                                tempChartData['c']['count'] = 1;
                                tempChartData['c']['name'] = "26-35";
                            }
                        } else {
                            if ( age > 36 && age <= 49 ){
                                if ( tempChartData['d'] ){
                                    tempChartData['d']['count']++;
                                } else {
                                    tempChartData['d'] = [];
                                    tempChartData['d']['count'] = 1;
                                    tempChartData['d']['name'] = "36-49";
                                }
                            } else {
                                if ( age >= 50  ){
                                    if ( tempChartData['e'] ){
                                        tempChartData['e']['count']++;
                                    } else {
                                        tempChartData['e'] = [];
                                        tempChartData['e']['count'] = 1;
                                        tempChartData['e']['name'] = "+50";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        return tempChartData;
    };


    this.findUsersDataForBarChartGender = function(prop){
        var tempChartData = this.getGenderChartData(prop);

        var series = [];
        var channel = CurrentChannel.getCurrentChannel();
        var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
        var palette = Charts.getMatchingColorList(initialColor);
        var pos = 0;
        for (var k in tempChartData) {
            if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                pos++;
            }

            var serie = {};
            serie.data = [{ y: tempChartData[k].count, color: palette['analogous_1_'+ pos] }];
            serie.type = "column";
            serie.name = k;
            series.push(serie);
            pos++;
        }

        return series;
    };

    this.findUsersDataForBarChartAge = function(prop){
        var tempChartData = this.getAgeChartData(prop);
        var series = [];
        var channel = CurrentChannel.getCurrentChannel();
        var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
        var palette = Charts.getMatchingColorList(initialColor);
        var pos = 0;
        for (var k in tempChartData) {
            if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                pos++;
            }

            var serie = {};
            serie.data = [{ y: tempChartData[k].count, color: palette['split_complementary_1_'+pos] }];
            serie.type = "column";
            serie.name = tempChartData[k].name;
            series.push(serie);
            pos++;
        }

        return series;
    };
});