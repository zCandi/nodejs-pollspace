'use strict';

/* Reference: http://docs.angularjs.org/api/ngResource/service/$resource */
angular.module('mean.propset').factory('PropsetAdminFactory', ['$resource', function($resource) {
    return $resource('/api/admin/propset/:id',
        {
            channel_id: "@channel_id",
            id: "@id",
            type: "@type"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            create:{
                url: '/api/admin/channel/:channel_id/propset/create',
                method: 'POST',
                isArray: false
            },
            update:{
                url: '/api/admin/propset/:id',
                method: 'POST',
                isArray: false
            },
            list: {
                url: '/api/admin/channel/:channel_id/propset/:type/list',
                method: 'GET',
                isArray: true
            },
            deactivate: {
                url: '/api/admin/propset/:id/deactivate',
                method: 'GET',
                isArray: false
            }
        }
    );
}]);

angular.module('mean.propset').factory('PropsetFactory', ['$resource', function($resource) {
    return $resource('/api/propset/:id',
        {
            channel_id: "@cguid",
            id: "@id",
            type: "@type",
            guid: "@guid"
        },
        {
            get:{
                cache: false,
                method: 'GET'
            },
            list: {
                url: '/api/channel/:cguid/propset/:type/list',
                method: 'GET',
                isArray: true
            },
            getByGuid: {
                url: '/api/propset/:guid',
                method: 'GET',
                isArray: false
            }
        }
    );
}]);