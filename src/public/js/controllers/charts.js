'use strict';

angular.module('mean.charts').controller('ChartController', ['$scope', '$http', '$rootScope','Global',
    function ($scope, $http, $rootScope, Global) {
    $scope.global = Global;
    $scope.pieChartLegend = [];

    $scope.pieChartConfig = {
        options: {
            chart: {
                type: 'pie'
            },
            plotOptions: {
                series: {
                    stacking: ''
                }
            }
        },
        series: [],
        title: {
            text: 'Pie Chart'
        },
        credits: {
            enabled: true
        },
        loading: false,
        size: {}
    };

    $scope.lineChartConfig = {
        options: {
            chart: {
                type: 'line'
            },
            plotOptions: {
                series: {
                    stacking: ''
                }
            }
        },
        series: [],
        title: {
            text: 'Line Chart'
        },
        credits: {
            enabled: true
        },
        loading: false,
        size: {}
    };

    $scope.barChartConfig = {
        options: {
            chart: {
                type: 'bar'
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            }
        },
        series: [],
        title: {
            text: 'Bar Chart'
        },
        credits: {
            enabled: true
        },
        loading: false,
        size: {}
    };

    $scope.pieChartOption1 = function(){
        $http.get("views/chart_data/piechartOption1.json").success(function(data) {
            $scope.pieChartConfig = data;
            angular.forEach($scope.pieChartConfig.series[0].data, function(value, key) {
                var colors = { name: value.name, color: value.color, number: value.y };
                $scope.pieChartLegend[key] = colors;
            });
        });
    };

    $scope.pieChartOption2 = function(){
        $http.get("views/chart_data/piechartOption2.json").success(function(data) {
            $scope.pieChartConfig = data;
            angular.forEach($scope.pieChartConfig.series[0].data, function(value, key) {
                var colors = { name: value.name, color: value.color, number: value.y };
                $scope.pieChartLegend[key] = colors;
            });
        });
    };

    $scope.pieChartOption3 = function(){
        $http.get("views/chart_data/piechartOption3.json").success(function(data) {
            $scope.pieChartConfig = data;
            angular.forEach($scope.pieChartConfig.series[0].data, function(value, key) {
                var colors = { name: value.name, color: value.color, number: value.y };
                $scope.pieChartLegend[key] = colors;
            });
        });
    };

    $scope.changePieColor = function(){
        angular.forEach($scope.pieChartConfig.series[0].data, function(value, key) {
            value.color = "#" + Math.floor(Math.random()*16777215).toString(16);

            var colors = { name: value.name, color: value.color, number: value.y };
            $scope.pieChartLegend[key] = colors;
        });
    };

    $scope.barChartOption1 = function(){
        $http.get("views/chart_data/barchartOption1.json").success(function(data) {
            $scope.barChartConfig = data;
        });
    };

    $scope.barChartOption2 = function(){
        $http.get("views/chart_data/barchartOption2.json").success(function(data) {
            $scope.barChartConfig = data;
        });
    };

    $scope.barChartOption3 = function(){
        $http.get("views/chart_data/barchartOption3.json").success(function(data) {
            $scope.barChartConfig = data;
        });
    };

    $scope.lineChartOption1 = function(){
        $http.get("views/chart_data/linechartOption1.json").success(function(data) {
            $scope.lineChartConfig = data;
        });
    };

    $scope.lineChartOption2 = function(){
        $http.get("views/chart_data/linechartOption2.json").success(function(data) {
            $scope.lineChartConfig = data;
        });
    };

    $scope.lineChartOption3 = function(){
        $http.get("views/chart_data/linechartOption3.json").success(function(data) {
            $scope.lineChartConfig = data;
        });
    };

    $scope.pieChartOption1();
    $scope.barChartOption1();
    $scope.lineChartOption1();
}]);
