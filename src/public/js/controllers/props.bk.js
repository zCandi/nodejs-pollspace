'use strict';

angular.module('mean.props').controller('PropsController', ['$window', '$resource', '$scope', '$state','$rootScope',
    '$stateParams', '$location', '$http', '$alert', '$timeout', '$sce', 'Global', 'PropsFactory', 'ChannelFactory',
    'UsersFactory', 'CurrentChannel',  'Charts', 'Single', 'User', 'UserFPAdminFactory','PropsetFactory',
    function ($window, $resource, $scope, $state, $rootScope, $stateParams, $location, $http, $alert, $timeout, $sce,
              Global, PropsFactory, ChannelFactory, UsersFactory, CurrentChannel, Charts, Single, User, UserFPAdminFactory,
              PropsetFactory) {

        $scope.global = Global;
        $scope.isVoteButtonDisabled = true;
        $scope.votebuttontext = 'VOTE';
        $scope.user = Global.user;
        $scope.options = [];
        $scope.facebookGenderStats = [];
        $scope.facebookAgeStats = [];
        $scope.selectedOption = "";
        $scope.answers = [
            {name: null, correct:false},
            {name: null, correct:false},
            {name: null, correct:false},
            {name: null, correct:false}]
        ; //for now we need to harcode to 4 answers
        $scope.currentchannel = CurrentChannel.getCurrentChannel();
        $scope.channel = CurrentChannel.getCurrentChannel();
        $scope.prop = CurrentChannel.getCurrentProp();
        $scope.type = $location.$$url.indexOf('poll') > -1 ? 'opinion' : 'quiz';
        $scope.suggest_type = 'opinion';
        $scope.mobileOptionsExpand = false;

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };

        /*********** RESULTS CHARTS ***********/
        $scope.pieChartLegend = [];
        $scope.pieChartConfig = Charts.getPiechartConfiguration();
        $scope.barChartConfigGender = Charts.getBarchartConfigurationGender();
        $scope.barChartConfigAge = Charts.getBarchartConfigurationAge();

        $scope.findMyProps = function(){
            UsersFactory.proplist({id: Global.user._id},function(props) {
                $scope.props = props;
                $scope.props.forEach(function(prop){
                    var bannerinfo = Single.getBanner(prop);

                    prop.banner = bannerinfo && bannerinfo.banner ? bannerinfo.banner : "";
                    prop.mobile_banner = bannerinfo && bannerinfo.mobile_banner ? bannerinfo.mobile_banner : "";
                });
            });
        };

        $scope._findProps = function(props){
            $scope.props = props;
            $scope.props.forEach(function(prop){
                var bannerinfo = Single.getBanner(prop);

                prop.banner = bannerinfo && bannerinfo.banner ? bannerinfo.banner : "";
                prop.mobile_banner = bannerinfo && bannerinfo.mobile_banner ? bannerinfo.mobile_banner : "";
                prop.vote = User.userVote(prop);
            });
        };

        $scope.findProps = function() {
            var cguid = $scope.channel.guid;
            PropsFactory.list({cguid: cguid, type: $stateParams.type ? $stateParams.type : $scope.type },function(props) {
                $scope._findProps(props);
            });
        };

        $scope.findPreviousWinners = function(){
            var cguid = $scope.channel.guid;
            PropsFactory.list({cguid: cguid, type: 'image' },function(props) {
                $scope._findProps(props);
            });
        };

        $scope.suggestPropImageCallback = function(file){
            $scope.image = file.cdnUrl;
        };

        $scope.userWon = function(prop){
            var flag = false;
            var user = $scope.user;
            user.correct_props.forEach(function(propid, key){
                if (_.isEqual(propid.toString(),prop._id.toString()) ){
                    flag = true;
                }
            });

            return flag;
        };

        $scope.userLost = function(prop){
            var flag = false;
            var user = $scope.user;
            user.incorrect_props.forEach(function(propid, key){
                if (_.isEqual(propid.toString(),prop._id.toString()) ){
                    flag = true;
                }
            });

            return flag;
        };

        $scope.getBaseUrl = function() {
            return $location.protocol() + "://" + $location.host() +
                ($location.port() && ":" + $location.port()) + "/";
        };

        $scope.findProp = function(pguid, cb){
            var prop;
            var prop_data;
            PropsFactory.get({
                guid: pguid
            }, function(prop) {
                $scope.prop = prop;

                CurrentChannel.setCurrentProp(prop);

                $scope.prop.imagebk = prop.image;

                $scope.options = prop.answers;
                $scope.optionsCount = 0;
                $scope.options.forEach(function(option){
                    delete option.correct;
                    if ( option.name ){
                        $scope.optionsCount = $scope.optionsCount + 1;
                    }
                });

                $scope.prop.status = Single.propStatus(prop);
                $scope.prop.isClosed = function(){
                    return Single.propStatus(prop) == 'closed';
                };
                $scope.prop.isGraded = function(){
                    return Single.propStatus(prop) == 'graded';
                };
                $scope.prop.isCreated = function(){
                    return Single.propStatus(prop) == 'created';
                };
                $scope.prop.isPublished = function(){
                    return Single.propStatus(prop) == 'published';
                };

                $scope.$root.ogTitle = prop.name;
                $scope.$root.ogImg = prop.image;

                //SET THE TAB to show
                if ( !_.isEmpty(prop) ){
                    if ( prop.type == 'opinion' ){
                        $rootScope.activeTab = 'our_props';
                    } else if (prop.type == 'quiz'){
                        $rootScope.activeTab = 'our_quizzes';
                    }
                }

                if ( cb ){ cb(); }
            });


            $scope.selectedOption = $scope.options[0];
        };

        $scope.propStatus = function(prop){
            return Single.propStatus(prop);
        };

        $scope.findChannelAndFindProp = function(){
            var guid = $stateParams.pguid;
            $scope.findProp(guid);
        };


        $scope.getThanksData = function(prop){
            var guid = $stateParams.pguid;

            PropsFactory.sharingdata({guid: guid},function(sharing_data) {
                var url = Global.hostname;
                url = url + "/s/"+sharing_data.guid;
                //$scope.facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url);
                //$scope.twitter_url = "https://twitter.com/intent/tweet?text=" + prop.name + "&url=" + encodeURIComponent(url);

                $scope.facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" + url;
                $scope.twitter_url = "https://twitter.com/intent/tweet?text=" + urlencode(prop.name) + "&url=" + url;

                /* CHARTS */
                var dataList = User.findUsersDataForPieChart(prop, 'single');
                $scope.pieChartConfig.series[0].data = dataList;
                $scope.pieChartLegend = dataList;
                $scope.propVotes = Single.getPropVotes(prop);

                /*var series = User.findUsersDataForBarChartGender(prop);
                $scope.barChartConfigGender.series = series;

                var series = User.findUsersDataForBarChartAge(prop);
                $scope.barChartConfigAge.series = series;*/

                $scope.findUsersDataForBarChartGender(prop);
                $scope.findUsersDataForBarChartAge(prop);
            });
        };

        $scope.findUsersDataForBarChartGender = function(prop){
            var votes = prop.votes;
            var tempChartData = User.getGenderChartData(prop);

            $scope.facebookGenderStats[prop.guid] = [];
            for (var k in tempChartData) {
                var obj = {};
                obj.id = prop.guid + "_" + k;
                obj.total = Math.round((tempChartData[k].count * 100) / Object.keys(prop.votes).length) + "%";
                obj.name = k;
                $scope.facebookGenderStats[prop.guid].push(obj);

            }
        };

        $scope.findUsersDataForBarChartAge = function(prop){
            var tempChartData = User.getAgeChartData(prop);

            $scope.facebookAgeStats[prop.guid] = [];
            for (var k in tempChartData) {
                var obj = {};
                obj.id = prop.guid + "_" + k;
                obj.total = Math.round((tempChartData[k].count * 100) / Object.keys(prop.votes).length) + "%";
                obj.name = tempChartData[k].name;
                $scope.facebookAgeStats[prop.guid].push(obj);
            }
        };

        $scope.findPropCallback = function(){
            var prop = $scope.prop;
            var user = Global.user;

            //need to save a count for being viewed
            if ( prop.counts && prop.counts.view ){
                prop.counts.view = prop.counts.view + 1;
            } else {
                prop.counts.view = 1;
            }
            prop.$save();

            //TODO: For some reason, the status is not reflected in the $scope, so is lost in the html.
            prop.status = Single.propStatus(prop);

            //This is for when the answers are displayed in the thank you page
            if ( prop.userAnswer ){
                $scope.selectedOption = prop.userAnswer.id;
            }
            $scope.propVotes = Single.getPropVotes(prop);

            //Timeout is used to WAIT till the DOM is finished loading..
            $timeout(function(){
                if ( User.userVote(prop) == true ){
                    $scope.getThanksData(prop);
                }
            });
        }

        $scope.getBAHDViewData = function(){
            var guid = $stateParams.pguid;
            $scope.findProp(guid, function(){
                $rootScope.activeTab = 'final_four';
                $scope.findPropCallback();
            });
        };

        $scope.getViewData = function(){
            var guid = $stateParams.pguid;
            $scope.findProp(guid, function(){
                $scope.findPropCallback();
            });
        };

        $scope.userIsCorrect = function(prop){
            var isCorrect = false;
            var user = Global.user;
            var correct = Single.getCorrectPropAnswer(prop);
            var userChoice = User.getUserChoice(user, prop);
            if ( correct && userChoice && _.isEqual(correct.toString(), userChoice.toString()) ){
                isCorrect = true;
            }
            return isCorrect;
        };


        $scope.getUserChoice = function(user, prop){
            return User.getUserChoice(user, prop);
        }

        $scope.loginToVote = function(){
            $state.transitionTo('login');
        };

        $scope.findOtherPropsFromChannel = function(prop){
            var temp = [];
            PropsFactory.list({cguid: cguid, type: $stateParams.type },function(props) {
                $scope.props = props;
                $scope.props.forEach(function(_prop, key){
                    if (prop._id != _prop._id) {
                        temp.push(_prop);
                    }
                });
                $scope.props = temp;
                $scope.props.forEach(function(prop){
                    var bannerinfo = Single.getBanner(prop);

                    prop.banner = bannerinfo && bannerinfo.banner ? bannerinfo.banner : "";
                    prop.mobile_banner = bannerinfo && bannerinfo.mobile_banner ? bannerinfo.mobile_banner : "";
                    prop.vote = User.userVote(prop);
                })
            });
        };

        $scope.selectItem = function(option_id){
            if ( $scope.selectedOption == option_id ){
                $scope.selectedOption = null;
                $scope.isVoteButtonDisabled = true;
            } else {
                $scope.selectedOption = option_id;
                $scope.isVoteButtonDisabled = false;
            }
        };

        $scope.vote = function(channel_id, guid){
            if (_.isEmpty($scope.selectedOption) ){
                $alert({
                    title: 'Error!',
                    content: 'Please select a valid Option to vote.',
                    placement: 'top',
                    type: 'danger',
                    duration: '8',
                    show: true,
                    container: '#alerts-container',
                    animation: 'am-fade-and-slide-top'
                });
            } else {
                var data = {
                    choice: $scope.selectedOption
                };
                $scope.isVoteButtonDisabled = false;
                $scope.votebuttontext = 'THANKS FOR VOTING, LOADING RESULTS...';

                PropsFactory.vote({guid: guid},data, function(response) {
                    $state.transitionTo('user_vote_prop_thanks', { pguid: guid, type: response.prop.type });
                });
            }

        };

        $scope.toggleMobileOptions = function(){
            $scope.mobileOptionsExpand = !$scope.mobileOptionsExpand;
        };

        $scope.optionChecked = function(option){
            for (var key in $scope.answers) {
                $scope.answers[key].correct = false;
            }

            option.correct = true;
        };

        $scope.create = function() {
            if ( this.suggest_type == 'quiz' && !Single.validateCorrectAnswer($scope.answers) ){
                $alert({
                    title: 'Error!',
                    content: 'Please select a correct answer for question: "' + this.name + '"',
                    placement: 'top',
                    type: 'danger',
                    duration: '8',
                    show: true,
                    container: '#alerts-container',
                    animation: 'am-fade-and-slide-top'
                });
            } else {
                var data = {
                    name:           this.name ,
                    details:        this.details,
                    image:          this.image,
                    answers:        $scope.answers,
                    timestamps:     { suggested: new Date().getTime() },
                    type:           this.suggest_type
                };

                var currentchannel = CurrentChannel.getCurrentChannel();
                PropsFactory.create({cguid: currentchannel.guid }, data,function(response) {
                    $state.transitionTo('user_suggest_prop_thanks',  {pguid: response.guid });
                });
            }

        };
}]);
