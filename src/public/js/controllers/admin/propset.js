'use strict';

angular.module('mean.propset').controller('AdminPropsetController', ['$scope', '$rootScope', '$resource', '$stateParams',
    '$location', '$alert', '$timeout','PropsAdminFactory', 'PropsetAdminFactory','Global', 'CurrentChannel',
    function ($scope, $rootScope, $resource, $stateParams, $location, $alert, $timeout,
              PropsAdminFactory, PropsetAdminFactory, Global, CurrentChannel) {
        $scope.propset = {};
        $scope.propset.props = [];
        $scope.globalCountQuestions = 1;
        $scope.globalTotalCountQuestions = 0;
        $scope.props = [];
        $scope.status_action = null;
        $scope.type = { classic: 'classic', opinion: 'opinion', quiz: 'quiz' };
        $scope.propType = $stateParams.type;
        $scope.channel = CurrentChannel.getCurrentChannel();
        $scope.user = Global.user;
        $scope.publish_text = 'Publish';
        $scope.save_text = 'Save';
        $scope.embedded = false;

        $scope.sortableOptions = {
            handle: ".glyphicon-resize-vertical",
            opacity: 0.5,
            stop: function(e, ui){
                //$scope.editPoll();
            }
        };

        $scope.setPropsetImageCallback = function(file){
            $scope.propset.image = file.cdnUrl;
        };

        $scope.findChannel = function(channel_id){
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            var Channel = $resource('/api/admin/channel/:channel_id', { channel_id: channel_id });
            var channel = Channel.get(function(channel){
                $scope.channel = channel;
            });
        };

        $scope.findPropsets = function(channel_id){
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            PropsetAdminFactory.list({channel_id: channel_id, type: $stateParams.type},function(propsets) {
                $scope.channel = CurrentChannel.getCurrentChannel();
                $scope.propsets = propsets;
            });
        };

        $scope.findPropset = function(propset_id){
            if ( !propset_id ){
                propset_id = $stateParams.propset_id
            }
            PropsetAdminFactory.get({
                id: propset_id
            }, function(propset) {
                var url = Global.hostname;
                $scope.propset = propset;

                CurrentChannel.setCurrentPropset(propset);

                $scope.propset.imagebk = propset.image;
                $scope.propset.status = $scope.propsetStatus(propset);
                $scope.propType = propset.type;
                $scope.propset.isCreated = function(){
                    return $scope.propsetStatus(propset) == 'created';
                };
                $scope.propset.isPublished = function(){
                    return $scope.propsetStatus(propset) == 'published';
                };
                $scope.resetPropsList();
                $scope.globalTotalCountQuestions = Object.keys($scope.propset.props).length;

                $scope.propset.props.forEach(function(_prop, key){
                    _prop.showPanelBody = true;
                    _prop.status = 'preedit';
                });

                var quiz_url = url + "/q/"+propset.guid;
                $scope.facebook_quiz_url = "https://www.facebook.com/sharer/sharer.php?u=" + quiz_url;
                $scope.twitter_quiz_url = "https://twitter.com/intent/tweet?text=" + urlencode(propset.title) + "&url=" + quiz_url;

                var embedded_code = '<div class="pollspace-mq"><script src="'+url+ "/q/"+propset.guid+'/poll.js"></script></div>';
                $scope.pollspace_embedded_code = embedded_code;
            });
        };

        $scope.getNewQuestion = function(){
            var question = {};
            question.name = "";
            question.type = $scope.propType;
            question.order = $scope.globalCountQuestions;
            question.correct_description = "";
            question.hidden = true;
            question.answers = [];
            question.answers[0] = {};
            question.answers[1] = {};
            question.answers[2] = {};
            question.answers[3] = {};
            question.showPanelBody = true;
            question.status = 'new';
            question.new = true;

            question.answers.forEach(function(answer, key){
                var randexp = new RandExp('([A-Za-z0-9]){16}', 'i');
                answer.id = randexp.gen();
            });

            $scope.globalCountQuestions = $scope.globalCountQuestions + 1;
            return question;
        };

        $scope.addQuestion = function(){
            var question = $scope.getNewQuestion();
            $scope.propset.props.push(question);
            $scope.globalTotalCountQuestions = Object.keys($scope.propset.props).length;
        };

        $scope.editQuestion = function(question){
            question.status = 'edit';
        };

        $scope.saveEditQuestion = function(question){
            question.answers.forEach(function(answer, key){
                if ( _.isUndefined(answer.id) ){
                    var randexp = new RandExp('([A-Za-z0-9]){16}', 'i');
                    answer.id = randexp.gen();
                }
            });

            PropsAdminFactory.save({id: question._id }, question, function(response) {
                if ( !question.new ){
                    $scope.findPropset($stateParams.propset_id);
                } else {
                    question.status = 'preedit';
                }
            });
        }

        $scope.deleteQuestion = function(question){
            PropsAdminFactory.deactivate({ id: question._id },function(prop) {
                $scope.removePropsetQuestion(question);
            });
        };

        $scope.validateCorrectAnswer = function(answers){
            var valid = false;
            answers.forEach(function(value, key){
                if ( value.correct === true ){
                    valid = true;
                }
            });

            return valid;
        };

        $scope.saveQuestion = function(question){
            $scope.globalCountQuestions = $scope.globalCountQuestions - 1;

            if ( question.type == 'quiz' && !$scope.validateCorrectAnswer(question.answers) ){
                $alert({
                    title: 'Error!',
                    content: 'Please select a correct answer for question: "' + question.name + '"',
                    placement: 'top',
                    type: 'danger',
                    duration: '8',
                    show: true,
                    container: '#alerts-container',
                    animation: 'am-fade-and-slide-top'
                });
            } else {
                var data = {
                    name:       question.name,
                    active:     true,
                    answers:    question.answers,
                    type:       question.type,
                    correct_description: question.correct_description,
                    hidden:     true,
                    timestamps:     { created: new Date().getTime() }
                };

                PropsAdminFactory.create({channel_id: $stateParams.channel_id }, data,function(prop) {
                    $scope.props.push(prop._id);
                    question.guid = prop.guid;
                    question._id = prop._id;
                    question.status = 'preedit';
                });
            }

        };

        $scope.createMultipleQuestion = function(e){
            e.preventDefault();
            e.stopPropagation();

            if ( _.isEmpty($scope.props) || $scope.props.length == 1){
                $alert({
                    title: 'Error!',
                    content: 'Please select and save 2 questions at least before saving.',
                    placement: 'top',
                    type: 'danger',
                    duration: '8',
                    show: true,
                    container: '#alerts-container',
                    animation: 'am-fade-and-slide-top'
                });

            } else {
                $scope.propset.timestamps = { created: new Date().getTime() };
                $scope.propset.props = $scope.props;
                $scope.propset.type = $scope.propType;

                var isDisabled = $("#create").hasClass('disabled');
                if ( !isDisabled ){
                    PropsetAdminFactory.create({channel_id: $stateParams.channel_id }, $scope.propset,function(response) {
                        $location.path('admin/channel/' + $stateParams.channel_id + '/prop/' + response._id + '/edit/multiple');
                    });
                }

            }

        };

        $scope.publishMultipleQuestion = function (e){
            $scope.status_action = 'publish';
            $scope.publish_text = 'Publishing ';
            $scope.editMultipleQuestion(e);
        };

        $scope.editMultipleQuestion = function(e){
            e.preventDefault();
            e.stopPropagation();

            if ( _.isEmpty($scope.props) || $scope.props.length == 1){
                $alert({
                    title: 'Error!',
                    content: 'Please select and save 2 questions at least before saving.',
                    placement: 'top',
                    type: 'danger',
                    duration: '8',
                    show: true,
                    container: '#alerts-container',
                    animation: 'am-fade-and-slide-top'
                });

            } else {
                var isDisabled = $("#update").hasClass('disabled');
                if ( !isDisabled ){
                    $scope.save_text = 'Saving ';

                    $scope.resetPropsList();

                    $scope.propset.status_action = $scope.status_action;
                    $scope.propset.props = $scope.props;
                    if (_.isEmpty($scope.propset.type) ){
                        $scope.propset.type = $scope.propType;
                    }


                    PropsetAdminFactory.update({ id: $stateParams.propset_id }, $scope.propset,function(propset) {
                        $scope.findPropset($stateParams.propset_id);

                        $scope.publish_text = 'Publish';
                        $scope.save_text = 'Save';
                    });
                }

            }

        };

        $scope.findChannelAndFindPropset = function(){
            $scope.findChannel($stateParams.channel_id);
            $scope.findPropset($stateParams.propset_id);
        };

        $scope.optionChecked = function(question, option){
            question.answers.forEach(function (_option) {
                _option.correct = false;
            });

            option.correct = true;
        };

        $scope.removePropsetQuestion = function(question){
            for (var key in $scope.propset.props) {
                if ($scope.propset.props[key]._id == question._id) {
                    $scope.propset.props.splice(key, 1);
                }
            }

            for (var key in $scope.props) {
                if ($scope.props[key] == question._id) {
                    $scope.props.splice(key, 1);
                }
            }
        };

        $scope.removeMultipleQuestion = function() {
            PropsetAdminFactory.deactivate({ id: $scope.propset._id },function(prop) {
                $location.path('admin/channel/' + $scope.channel._id + "/prop/manage/multiple/"+ $scope.propset.type);
            });
        };

        $scope.propsetStatus = function(propset){
            //created > published
            if ( $scope.isSet(propset.timestamps.published)) return 'published';
            if ( $scope.isSet(propset.timestamps.created)) return 'created';
        };

        $scope.isSet = function(date){
            var ts = new Date(date).getTime();
            return ts !== 0;
        };

        $scope.resetPropsList = function(){
            $scope.props = [];
            $scope.propset.props.forEach(function(_prop, key){
                $scope.props.push(_prop._id);
            });
        };

        $scope.togglePanelBody = function(question){
            question.showPanelBody = !question.showPanelBody;
        }

        $scope.toggleEmbedded = function(){
            $scope.embedded = !$scope.embedded;
        }
    }]);
