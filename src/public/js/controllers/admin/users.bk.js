'use strict';

angular.module('mean.channels').controller('AdminUsersController', ['$resource', '$scope', '$stateParams',
    '$location', 'Global', 'UserFPAdminFactory', 'ChannelAdminFactory', 'CurrentChannel',
    function ($resource, $scope, $stateParams, $location, Global, UserFPAdminFactory, ChannelAdminFactory, CurrentChannel) {
        $scope.global = Global;
        $scope.show_facebook = true;
        $scope.filtertext = 'Filter';
        $scope.currentchannel = CurrentChannel.getCurrentChannel();

        $scope.toggleFacebookInformation = function(){
            $scope.show_facebook = !$scope.is_admin;
        };

        $scope.find = function() {
            var channel = CurrentChannel.getCurrentChannel();
            if ( channel ){
                $scope.users = channel.subscribers;
            } else {
                UserFPAdminFactory.list(function(users) {
                    $scope.users = users;
                    ChannelAdminFactory.list(function(channels) {
                        $scope.channels = channels;
                    });
                });
            }
        };

        $scope.filterUsers = function(){
            var channel_id = $scope.channel;
            if ( !_.isEmpty($scope.currentchannel) && _.isObject($scope.currentchannel) ){
                channel_id = $scope.currentchannel._id;
            }

            if (_.isEmpty(channel_id) ){
                alert('Please Select Channel before Filtering');
            } else {
                $scope.filtertext = 'Filtering...'
                ChannelAdminFactory.get({ id: channel_id },function(channel) {
                    $scope.users = channel.subscribers;

                    $scope.filtertext = 'Filter'
                });
            }
        };

        $scope.findOne = function() {
            var channel = CurrentChannel.getCurrentChannel();
            UserFPAdminFactory.get({ id: $stateParams.user_id },function(user) {
                $scope.user = user;
	console.log('view user', user);
                if ( channel ){
                    var props = [];
                    $scope.user.props.forEach(function(prop, key){
                        //tuanbui commented
                        if ( prop.channel == channel._id ){
                            props.push(prop);
                        }
                    });
                    $scope.user.props = props;
                }
                $scope.channel = channel;
                console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               
                if (channel) {
                    var  voted_products = [];
                    $scope.channel.voted_products.forEach(function(prod){
                        if ( prod.user && user._id.valueOf() == prod.user._id.valueOf() ){
                            voted_products.push(prod);
                          console.log(prod.user._id.valueOf()); 
                        }
                       console.log(user._id.valueOf());
                       
                       console.log('end !!!!!!!!!');
                    });
                    $scope.user.voted_products = voted_products;
                   console.log("voted products!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                   console.log($scope.user.voted_products);

                }

                $scope.show_facebook = !user.is_admin;
            });
        };

        $scope.userVoteOption = function(prop){
            var user = $stateParams.user_id;
            var voteOption = "";

            if ( user ){
                prop.votes.forEach(function(vote, key){
                    var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                    if ( user && vote.user && _.isEqual(user_id.toString(), user.toString()) ){
                        var option = vote.choice;
                        voteOption =  $scope.findChoiceName(prop.answers, option);
                    }
                });
            }

            return voteOption;
        };

       $scope.userVoteProductOption = function(prod){
            var user = $stateParams.user_id;
            var voteOption = [];

            if ( user ){
                /*prod.answers.forEach(function(ans){
                    if ( ans != 'undefined' ){
                        
                        voteOption.push(ans);
                    }
                });*/
               prod.prop.answers.forEach(function(prop_ans) {
                  prod.answers.forEach(function(ans){
                    if ( ans == prop_ans.id ){
                      voteOption.push(prop_ans.name);
                    }
                  });
                });
            }
            
            return voteOption.join(', ');
        };

        $scope.findChoiceName = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };

        $scope.userVoteOption_bk = function(prop){
            var user = $stateParams.user_id;
            var voteOption = "";

            if ( user ){
                prop.votes.forEach(function(vote, key){
                    var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                    if ( user && vote.user && _.isEqual(user_id.toString(), user.toString()) ){
                        var option = vote.choice;
                        voteOption = $scope.findChoiceName(prop.answers, option);
                    }
                });
            }

            return voteOption;
        };
        $scope.findChoiceName_bk = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };

        $scope.create = function() {
            var data = {
                firstname:  this.firstname,
                lastname:   this.lastname,
                email:      this.email,
                password:   this.password,
                is_admin:   this.is_admin,
                facebook_data:   {
                    id: this.facebook_data_id,
                    gender: this.facebook_data_gender,
                    relationship_status: this.facebook_data_relationship_status,
                    birthday: this.facebook_data_birthday,
                    age: this.facebook_data_age,
                    locations: this.facebook_data_location
                }
            };

            UserFPAdminFactory.save(data, function(response) {
                $location.path('fp/user/list');
            });
        };

        $scope.update = function() {
            var user = $scope.user;

            user.$save({ id: $stateParams.user_id },function() {
                $location.path('fp/user/list');
            });
        };

        /* Harcoded for now. This function works. Use it when we get data from facebook */
        $scope.getAge = function(date){
            var dob = new Date(date);
            var currentDate = new Date();
            var ageDifMs = currentDate.getTime() - dob.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };

        $scope.changeAge = function(){
            $scope.user.facebook_data.age = $scope.getAge($scope.user.facebook_data.birthday);
        };

    }]);
