'use strict';

angular.module('mean.channels').controller('AdminUsersController', ['$resource', '$scope', '$stateParams',
    '$location', '$modal', '$alert', '$timeout', '$sce','$localStorage', 'Global', 'UserFPAdminFactory', 'ChannelAdminFactory', 'CurrentChannel', 'Charts', 
    function ($resource, $scope, $stateParams, $location, $modal, $alert, $timeout, $sce, $localStorage, Global, UserFPAdminFactory, ChannelAdminFactory, CurrentChannel, Charts) {
        $scope.global = Global;
        $scope.show_facebook = true;
        $scope.filtertext = 'Filter';
        $scope.btn_fb = 1;
        $scope.btn_tw = 0;
        $scope.btn_detailed = 0;
        $scope.avatar = '';
        $scope.currentchannel = CurrentChannel.getCurrentChannel();
        $scope.lineChartConfig = Charts.getLinechartConfiguration();

        $scope.toggleFacebookInformation = function(){
            $scope.show_facebook = !$scope.is_admin;
        };

        $scope.find = function() {
            var channel = CurrentChannel.getCurrentChannel();
            if ( channel ){
                $scope.users = channel.subscribers;
            } else {
                UserFPAdminFactory.list(function(users) {
                    $scope.users = users;
                    ChannelAdminFactory.list(function(channels) {
                        $scope.channels = channels;
                    });
                });
            }
        };

        $scope.filterUsers = function(){
            var channel_id = $scope.channel;
            if ( !_.isEmpty($scope.currentchannel) && _.isObject($scope.currentchannel) ){
                channel_id = $scope.currentchannel._id;
            }

            if (_.isEmpty(channel_id) ){
                alert('Please Select Channel before Filtering');
            } else {
                $scope.filtertext = 'Filtering...'
                ChannelAdminFactory.get({ id: channel_id },function(channel) {
                    $scope.users = channel.subscribers;

                    $scope.filtertext = 'Filter'
                });
            }
        };

        $scope.findOne = function() {
            var channel = CurrentChannel.getCurrentChannel();
            UserFPAdminFactory.get({ id: $stateParams.user_id },function(res) {
                $scope.user = res.user;

                if ( channel ){
                    var props = [];
                    var total_points = 0;
                    var total_shares = 0;
                    var _response = ['A', 'B', 'C', 'D'];
                    $scope.user.props.forEach(function(prop, key){
                        //tuanbui commented
                        if ( prop.channel == channel._id ){
                            $.map(prop.votes, function(o){
                                if (o.user === $scope.user._id) {
                                    //caculate total points with the voting action
                                    total_points += channel.points.voting != null ? channel.points.voting : 0;

                                    //caculate total points with result of the answers
                                    $.map(prop.answers, function(ob, key){
                                        if (ob.id == o.choice) {
                                            prop.response = _response[key];
                                            if (ob.correct) {
                                                total_points += channel.points.correct != null ? channel.points.correct : 0;
                                            }else {
                                                total_points += channel.points.incorrect != null ? channel.points.incorrect : 0;    
                                            }
                                        }
                                    });

                                    //caculate total points with share's option
                                    $.map(res.shares, function(s){
                                        if (prop._id == s.prop) {
                                            total_shares++;
                                            if (prop.type == 'classic')
                                                total_points += channel.points.share_classic != null ? channel.points.share_classic : 0;
                                            else
                                                total_points += channel.points.share_opinion != null ? channel.points.share_opinion : 0;
                                        }
                                    });

                                    //caculate total points with suggest's option
                                    if (prop.suggested_by && prop.suggested_by == $scope.user._id) {
                                        if (prop.type == 'classic')
                                            total_points += channel.points.suggest_classic != null ? channel.points.suggest_classic : 0;
                                        else
                                            total_points += channel.points.suggest_opinion != null ? channel.points.suggest_opinion : 0;
                                    }

                                    //caculate total points with completed the game
                                    $.map($scope.user.completed_props, function (co){
                                        if (co._id == prop._id) {
                                            total_points += channel.points.completed != null ? channel.points.completed : 0;
                                        }
                                    });

                                    return props.push(prop); 
                                }
                            });                                                           
                        }
                    });
                    $scope.user.props = props;
                    $scope.user.total_points = total_points;
                    $scope.user.total_shares = total_shares;

                    //getting the list of products histories
                    var  voted_products = [];
                    if (channel.voted_products) {                        
                        channel.voted_products.forEach(function(prod){
                            if ( prod.user && $scope.user._id.valueOf() == prod.user._id.valueOf() ){
                                //get the image of current product
                                $.map(prod.prop.answers, function (a){
                                    if (a.id == prod.answers[prod.answers.length - 1]) {
                                        prod.name = a.name;
                                        return prod.image = a.image;
                                    }
                                });
                                voted_products.push(prod);
                            }
                        });
                    }
                    $scope.user.voted_products = voted_products;

                    //getting the list of photos histories
                    var photos = [];
                    if (channel.submittedPhotos){
                        channel.submittedPhotos.forEach(function(photo){
                            if ( photo.user && $scope.user._id.valueOf() == photo.user._id.valueOf() ){
                                photos.push(photo);
                            }
                        });
                    }
                    $scope.user.photos = photos;
                }
                $scope.channel = channel;

                $scope.show_facebook = !user.is_admin;
                if ($scope.user.facebook_data)
                    $scope.avatar = "https://graph.facebook.com/"+ $scope.user.facebook_data.id +"/picture?width=250&height=250";

                $scope.votesAndSharesPerfomanceForLineChart(res.shares);
            });
        };

        $scope.votesAndSharesPerfomanceForLineChart = function(shares){
            var channel = CurrentChannel.getCurrentChannel();
            var chart = $scope.lineChartConfig;
            var votesData = [], sharesData = [];

            var currentDate = new Date();
            var currentMonth = currentDate.getMonth() + 1;
            var currentYear = currentDate.getUTCFullYear();
            var month;
            var i =12;

            while (i > 0 ) {
                if (i == 12)
                    month = currentMonth;
                else {
                    month = month - 1;
                    if (month == 0) {
                        month = 12;
                        currentYear--;
                    }
                }

                var node = month + "/" + currentYear;
                if ( month < 10 ){
                    node = "0" + month + "/" + currentYear;
                }
                
                votesData[node] = 0;
                sharesData[node] = 0;
                i--;
            }

            //count total votes & shares
            if ( channel ){
                $scope.user.props.forEach(function(prop, key){
                    if ( prop.channel == channel._id ) {
                        $.map(prop.votes, function(o){
                            if (o.user === $scope.user._id) {
                                var voteTime = new Date(o.created);
                                var voteMonth = voteTime.getMonth() + 1;
                                var voteYear = voteTime.getUTCFullYear();
                                var node = (voteMonth < 10 ? '0' + voteMonth : voteMonth) + '/' + voteYear;

                                if (!_.isNaN(votesData[node])) {
                                    votesData[node]++;
                                }
                            }
                        });

                        $.map(shares, function(s){
                            if (prop._id == s.prop) {
                                var shareTime = new Date(s.timestamps.created);
                                var shareMonth = shareTime.getMonth() + 1;
                                var shareYear = shareTime.getUTCFullYear();
                                var node = (shareMonth < 10 ? '0' + shareMonth : shareMonth) + '/' + shareYear;

                                if (!_.isNaN(sharesData[node])) {
                                    sharesData[node]++;
                                }
                            }
                        });
                    }
                });

                if (channel.voted_products) {                        
                    channel.voted_products.forEach(function(prod){
                        if ( prod.user && $scope.user._id.valueOf() == prod.user._id.valueOf() ){
                            var voteTime = new Date(prod.voted_date);
                            var voteMonth = voteTime.getMonth() + 1;
                            var voteYear = voteTime.getUTCFullYear();
                            var node = (voteMonth < 10 ? '0' + voteMonth : voteMonth) + '/' + voteYear;

                            if (!_.isNaN(votesData[node])) {
                                votesData[node]++;
                            }
                        }
                    });
                }
            }

            chart.options = {
                "chart": {
                    "type": "line",
                    "backgroundColor": "transparent",
                    "className": "barcharts results_linechart"
                },
                "plotOptions": {
                    "size": "90%",
                    "line": {
                        "dataLabels": {
                            "enabled": true
                        },
                        "enableMouseTracking": true
                    }
                }
            };

            chart.legend = {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                borderWidth: 0
            };

            chart.series = [{
                name: 'Votes',
                data: array_values(votesData).reverse()
            },
            {
                name: 'Shares',
                data: array_values(sharesData).reverse()
            }];

            chart.xAxis.categories = array_keys(votesData).reverse();
        };

        $scope.userView = function (mode) {
            if (mode == 'fb') {
                $scope.btn_fb = true;
                $scope.btn_tw = false;
                $scope.btn_detailed = false;
                $scope.avatar = "https://graph.facebook.com/"+ $scope.user.facebook_data.id +"/picture?width=250&height=250";
                return;
            }

            if (mode == 'tw') {
                $scope.btn_tw = true;
                $scope.btn_fb = false;
                $scope.btn_detailed = false;
                $scope.avatar = $scope.user.twitter_data.profile_picture;
                return;
            }

            if (mode == 'detailed') {
                $scope.btn_tw = false;
                $scope.btn_fb = false;
                $scope.btn_detailed = true;

                if ($scope.user.facebook_data)
                    $scope.avatar = "https://graph.facebook.com/"+ $scope.user.facebook_data.id +"/picture?width=250&height=250";
                else if ($scope.user.twitter_data)
                    $scope.avatar = $scope.user.twitter_data.profile_picture;
                return;
            }
        };

        $scope.userVoteOption = function(prop){
            var user = $stateParams.user_id;
            var voteOption = "";

            if ( user ){
                prop.votes.forEach(function(vote, key){
                    var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                    if ( user && vote.user && _.isEqual(user_id.toString(), user.toString()) ){
                        var option = vote.choice;
                        voteOption =  $scope.findChoiceName(prop.answers, option);
                    }
                });
            }

            return voteOption;
        };

       $scope.userVoteProductOption = function(prod){
            var user = $stateParams.user_id;
            var voteOption = [];

            if ( user ){
                /*prod.answers.forEach(function(ans){
                    if ( ans != 'undefined' ){
                        
                        voteOption.push(ans);
                    }
                });*/
               prod.prop.answers.forEach(function(prop_ans) {
                  prod.answers.forEach(function(ans){
                    if ( ans == prop_ans.id ){
                      voteOption.push(prop_ans.name);
                    }
                  });
                });
            }
            
            return voteOption.join(', ');
        };

        $scope.findChoiceName = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };

        $scope.userVoteOption_bk = function(prop){
            var user = $stateParams.user_id;
            var voteOption = "";

            if ( user ){
                prop.votes.forEach(function(vote, key){
                    var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                    if ( user && vote.user && _.isEqual(user_id.toString(), user.toString()) ){
                        var option = vote.choice;
                        voteOption = $scope.findChoiceName(prop.answers, option);
                    }
                });
            }

            return voteOption;
        };
        $scope.findChoiceName_bk = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };

        $scope.create = function() {
            var data = {
                firstname:  this.firstname,
                lastname:   this.lastname,
                email:      this.email,
                password:   this.password,
                is_admin:   this.is_admin,
                facebook_data:   {
                    id: this.facebook_data_id,
                    gender: this.facebook_data_gender,
                    relationship_status: this.facebook_data_relationship_status,
                    birthday: this.facebook_data_birthday,
                    age: this.facebook_data_age,
                    locations: this.facebook_data_location
                }
            };

            UserFPAdminFactory.save(data, function(response) {
                $location.path('fp/user/list');
            });
        };

        $scope.update = function() {
            var user = $scope.user;

            UserFPAdminFactory.save({ id: $stateParams.user_id }, user,function() {
                $location.path('fp/user/list');
            });
        };

        /* Harcoded for now. This function works. Use it when we get data from facebook */
        $scope.getAge = function(date){
            var dob = new Date(date);
            var currentDate = new Date();
            var ageDifMs = currentDate.getTime() - dob.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };

        $scope.changeAge = function(){
            $scope.user.facebook_data.age = $scope.getAge($scope.user.facebook_data.birthday);
        };

    }]);
