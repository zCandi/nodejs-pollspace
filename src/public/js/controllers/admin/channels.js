'use strict';

angular.module('mean.channels').controller('AdminChannelsController',
    ['$compile', '$resource', '$rootScope','$scope', '$state', '$stateParams',
    '$window','$location', '$modal', '$alert', '$timeout', '$sce','$localStorage','Global', 'ChannelAdminFactory',
    'PropsAdminFactory', 'UserAdminFactory','CurrentChannel', 'Charts',
    function ($compile, $resource, $rootScope, $scope, $state, $stateParams,
              $window, $location, $modal, $alert, $timeout, $sce, $localStorage, Global, ChannelAdminFactory,
              PropsAdminFactory, UserAdminFactory, CurrentChannel, Charts) {
        $scope.global = Global;
        $scope.currentChannel = CurrentChannel.getCurrentChannel();
        $scope.channel = CurrentChannel.getCurrentChannel();
        $scope.showFirstStep = true;
        $scope.tags = [];
        $scope.submit = false;
        $scope.addAdminTitle = 'Admin';
        $scope.loadingtext = "";
        $scope.color_text = "color_text";
        $scope.color1 = "color_1";
        $scope.color2 = "color_2";
        $scope.color3 = "color_3";
        $scope.photosToSubmit = [];
        $scope.submittedPhotoError = false;
        $scope.signin_page_desc = "";
        $scope.leaderboard_desc = "";
        $scope.photo_desc = "";

        //This also happens to be the default menu options.
        $scope.customMenu = [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['format-block'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['code', 'quote', 'paragraph'],
            ['link', 'image']
        ];

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };

        /* @todo: YUP! I know.. this could have been bind to a channel variable so I do not have to do this.  */
        $scope.create = function() {
            var initialColor = this.color_two && this.color_two ? this.color_two.replace("#", "") : "57A8D4"
            var _palette = Charts.getMatchingColorList(initialColor);
            var palette = JSON.parse(JSON.stringify(_palette));

            var data = {
                name: this.name,
                guid: this.guid,
                mode: this.mode,
                description: this.description,
                logo: this.logo,
                picture: this.picture,
                color: {
                    color_text: this.color_t,
                    color1: this.color_one,
                    color2: this.color_two,
                    color3: this.color_third,
                    palette: palette
                },
                tags: this.tags,
                social:{
                    facebook:{
                        name: this.facebook_name,
                        publishing: this.facebook_publishing
                    },
                    twitter:{
                        name: this.twitter_name,
                        publishing: this.twitter_publishing
                    }
                },
                reset_rolling: this.leaderboard_reset ? this.leaderboard_reset : false,
                points: {
                    voting: this.points_voting,
                    correct: this.points_correct ? this.points_correct : null,
                    streakbonus: this.points_streakbonus ? this.points_streakbonus : null,
                    incorrect: this.points_incorrect ? this.points_incorrect : null,
                    completed: this.points_completed,
                    share_classic: this.points_share_classic ? this.points_share_classic : null,
                    suggest_classic: this.points_suggest_classic ? this.points_suggest_classic : null,
                    share_opinion: this.points_share_opinion,
                    suggest_opinion: this.points_suggest_opinion
                },
                signin_page_desc: this.signin_page_desc,
                leaderboard_desc: this.leaderboard_desc,
                photo_page: {
                    desc: this.photo_desc,
                    image: this.photo_page_image
                }
            };

            var isDisabled = $("#create").hasClass('disabled');
            if ( !isDisabled ){
                ChannelAdminFactory.save(data, function(response) {
                    //$location.path("/admin/channel/"+response._id);
                    $window.location.href = "/admin/channel/"+response._id;
                });
            }
        };

        $scope.checkSpaces = function(e){
            e.currentTarget.value = e.currentTarget.value.replace(/ /g, '_');
        };

        $scope.checkApostrophes = function(e){
            e.currentTarget.value = e.currentTarget.value.replace(/'/g, '');
        };

        $scope.checkGuid = function(e){
            e.currentTarget.value = e.currentTarget.value.replace(/ /g, '');
            e.currentTarget.value = e.currentTarget.value.replace(/'/g, '');
            e.currentTarget.value = e.currentTarget.value.replace(/_/g, '');
            e.currentTarget.value = e.currentTarget.value.replace(/-/g, '');
            e.currentTarget.value = e.currentTarget.value.toLowerCase();
        };

        $scope.remove = function(channel_id) {

            ChannelAdminFactory.deactivate({ id: channel_id },function(channel) {
                $scope.find();
            });
        };

        $scope.update = function() {
            var channel = $scope.channel;
            channel.timestamps.updated = new Date().getTime();

            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            channel.color.palette = JSON.parse(JSON.stringify(palette));

            /* This { channelId: $stateParams.channelId } is going to MAP node router PUT channels/:channelId */
            var isDisabled = $("#update").hasClass('disabled');
            if ( !isDisabled ){
                ChannelAdminFactory.save({id: $stateParams.channel_id }, channel, function(response) {
                    Global.channel = channel;
                    $window.location.reload();
                });
            }

        };

        $scope.resetPoints = function(){
          var channel = $scope.channel;
          channel.timestamps.updated = new Date().getTime();
          // console.log("reset points !!!!!!!!!!!!!!!!!!!!!!!");
          // console.log(channel);

          ChannelAdminFactory.resetleaderboard({ id: channel._id },function(leaderboard) {
            // console.log("reset leaderboard: ", leaderboard);
          });

        };        

        $scope.findOne = function() {
            ChannelAdminFactory.get({ id: $stateParams.channel_id },function(channel) {
                $scope.channel = channel;
                $scope.channel.mode = channel.mode ? channel.mode : 'standard';
                $scope.channel.logobk = channel.logo;
                $scope.channel.picturebk = channel.picture;
                // $scope.channel.photo_page_imagebk = channel.photo_page.image;
                $scope.channel.photo_page_imagebk = (channel.photo_page && channel.photo_page.image ? channel.photo_page.image : null);
                $scope.channel.signin_page_desc = channel.signin_page_desc ? channel.signin_page_desc : "";
                
                if (!channel.photo_page) {
                    $scope.channel.photo_page = { desc: null, image: null };
                }
            });
        };

        $scope.find = function() {
            ChannelAdminFactory.list(function(channels) {
                $scope.channels = channels;
            });
        };

        $scope.findAll = function(){
            ChannelAdminFactory.listAll(function(channels) {
                $scope.channels = channels;
            });
        };

        $scope.findChannelAdmins = function(){
            ChannelAdminFactory.get({ id: $stateParams.channel_id },function(channel) {
                $scope.channel = channel;
                ChannelAdminFactory.admins({ id: channel._id },function(channel_admins) {
                    $scope.channel_admins = channel_admins;
                });
            });
        };

        $scope.$watch('selectedAdmin', function(newValue, oldValue) {
            $scope.selectedAdminHidden = newValue;
        });

        $scope.findAdmins = function(){
            UserAdminFactory.adminList({},function(admins) {
                var _admins = [];
                $scope.adminsList = [];
                admins.forEach(function(admin){
                    var label = admin.firstname + " " + admin.lastname + " - " + admin.email;
                    _admins.push( { value: admin._id, label: label });
                    $scope.adminsList[label] = admin._id;
                });
                $scope.admins = _admins;
            });
        };

        $scope.addAdminToChannel = function(){
            var channel = $scope.channel;
            var admin_id = null;

            if (_.isObject(this.selectedAdmin) ){
                admin_id = this.selectedAdmin.value;
            } else {
                admin_id = $scope.adminsList[this.selectedAdmin];
            }

            $scope.addAdminTitle = 'Adding Admin to database..'
            ChannelAdminFactory.adminAdd({ id: channel._id, user_id: admin_id },function(result) {
                $scope.findChannelAdmins();
                $scope.selectedAdmin = '';

                $scope.addAdminTitle = 'Admin';
            });
        };

        $scope.removeAdminToChannel = function(admin_id){
            var channel = $scope.channel;

            ChannelAdminFactory.adminRemove({ id: channel._id, user_id: admin_id },function(result) {
                $scope.findChannelAdmins();
            });
        };

        $scope.getLeaderboardPoints = function(){
            ChannelAdminFactory.leaderboard({ id: $stateParams.channel_id },function(leaderboard) {
                $scope.leaderboardList = leaderboard;
            });
        };

        $scope.submit = function(){
            $scope.submit = true;
        };

        $scope.editChannelLogoCallback = function(file){
            $scope.channel.logo = file.cdnUrl;
        };

        $scope.editChannelPictureCallback = function(file){
            $scope.channel.picture = file.cdnUrl;
        };

        $scope.createChannelLogoCallback = function(file){
            $scope.logo = file.cdnUrl;
        };

        $scope.createChannelPictureCallback = function(file){
            $scope.picture = file.cdnUrl;
        };

        $scope.createChannelPhotoPageImageCallback = function(file){
            $scope.photo_page_image = file.cdnUrl;
        };

        $scope.editChannelPhotoPageImageCallback = function(file){
            $scope.channel.photo_page.image = file.cdnUrl;
        };

        $scope.appendCss = function(){
            var ct = $scope.channel.color.color_text;
            var c1 = $scope.channel.color.color1;
            var c2 = $scope.channel.color.color2;
            var c3 = $scope.channel.color.color3;
            var colors = {};

            if ( !_.isEmpty(ct) ){
                colors['ct'] = ct;
            }
            if ( !_.isEmpty(c1) ){
                colors['c1'] = c1;
            }
            if ( !_.isEmpty(c2) ){
                colors['c2'] = c2;
            }
            if ( !_.isEmpty(c3) ){
                colors['c3'] = c3;
            }

            var url = "?colors="+encodeURIComponent(JSON.stringify(colors));
            var head = angular.element(document.querySelector('head')); // TO make the code IE < 8 compatible, include jQuery in your page and replace "angular.element(document.querySelector('head'))" by "angular.element('head')"
            if ( $("#channelColor").size() == 1 ){
                $("#channelColor").remove();
            }
            head.append($compile("<link id='channelColor' rel='stylesheet' data-ng-href='/api/channel/color/style.css"+url+"' >")($scope));
        };

        /* Preview Channel */
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        $scope.showEditModalPreview = function() {
            PropsAdminFactory.list({channel_id: $scope.channel._id},function(props) {
                $scope.props = props;
                var modal = $modal({scope: $scope, contentTemplate: 'views/admin/channel/preview.html', show: false, title: "Preview Channel"});
                modal.$promise.then(modal.show);
            });
        };

        $scope.showCreateModalPreview = function() {
            $scope.channel = {};
            $scope.channel._id = null;
            $scope.channel.name = this.name;
            $scope.channel.description = this.description,
            $scope.channel.logo = this.logo;
            $scope.channel.picture = this.picture;
            $scope.channel.color = {
                color_text: _.isEmpty(this.color_text) ? "#EF6D54" : this.color_text,
                color1:  _.isEmpty(this.color_one) ? "#FFFFFF" : this.color_one,
                color2: _.isEmpty(this.color_two) ? "#57A8D4" : this.color_two,
                color3: _.isEmpty(this.color_third) ? "#4C4C4C" : this.color_third
            };
            $scope.props = [];
            var modal = $modal({scope: $scope, contentTemplate: 'views/admin/channel/preview.html', show: false, title: "Preview Channel"});
            modal.$promise.then(modal.show);
        };


        $scope.fpColorInit = function(){
            ChannelAdminFactory.list(function(channels) {
                $scope.channels = channels;
            });
        };

        $scope.updatePaletteByChannel = function(){
            var channel_id = this.channel_id;
            $scope.loadingtext = "Loading channel colors...";
            ChannelAdminFactory.get({ id: channel_id },function(channel) {

                var initialColor = channel.color.color2 && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"

                $scope.palette = $scope.getPaletteList(initialColor);
                $scope.color = "#"+initialColor;
                $scope.channel = channel;

                $scope.loadingtext = "";
            });
        };

        $scope.updatePaletteByColor = function(){
            var color = this.color;
            $scope.palette = $scope.getPaletteList(color);
        };

        $scope.getPaletteList = function( color ){
            var palette = Charts.getMatchingColorList(color);

            var palettelist = [];
            for (var key in palette) {
                var obj = {
                    'name': key,
                    'color': palette[key]
                };

                if ( ( key.match(/^complementary_1/i) ) || key.match(/^analogous_2/i) || key.match(/^split_complementary_2/i) || key.match(/^triadic_1/i)) {
                    obj.show = false;
                } else {
                    obj.show = true;
                }

                palettelist.push(obj);
            }

            return palettelist;
        }

        $scope.submittedPhotos = function(){
            var channel = $scope.channel;

            //We need to split submitedPhotos into a date array
            if ( channel.submittedPhotos ){
                $scope.submittedPhotos = {};

                angular.forEach(channel.submittedPhotos, function(photo, key) {
                    if (typeof photo.deleted == 'undefined' || !photo.deleted) {
                        var format = moment(photo.timestamps.created).utc().format("MM/DD/YYYY");
                        if ( $scope.submittedPhotos[format] ){
                            $scope.submittedPhotos[format].push(photo);
                        } else {
                            $scope.submittedPhotos[format] = [];
                            $scope.submittedPhotos[format].push(photo);
                        }
                    }
                });
            }
        }
       
        $scope.removePhoto = function( photo_id){
            ChannelAdminFactory.deactivatePhoto({id: $scope.channel._id, photo_id: photo_id },function(photo) {
                var format = moment(photo.timestamps.created).utc().format("MM/DD/YYYY");
                for (var i = $scope.submittedPhotos[format].length - 1; i >= 0; i--) {
                    if ($scope.submittedPhotos[format][i]._id == photo._id) {
                        $scope.submittedPhotos[format].splice(i, 1);
                        if ($scope.submittedPhotos[format].length == 0) {
                            delete $scope.submittedPhotos[format];
                        }
                        break;
                    }
                }
                //update channel
                angular.forEach($scope.channel.submittedPhotos, function(photo, key) {
                    if (photo_id == photo._id) {
                        $scope.channel.submittedPhotos.splice(key, 1);                        
                        return;
                    }
                });                
            });
        };

        $scope.validateSubmitedPhotos = function(){
            var count = $scope.getSelectedPhotosLength();
            if ( count > 0 && count <= 4 ){
                $scope.submittedPhotoError = false;
            } else {
                $scope.submittedPhotoError = true;
            }
        };

        $scope.addSubmittedPhotosToPoll = function(){
            var channel = CurrentChannel.getCurrentChannel();
            $scope.validateSubmitedPhotos();
            if ( !$scope.submittedPhotoError ){
                $localStorage.submittedPhotosList = $scope.getSelectedPhotosList();
                $state.transitionTo('admin_create_prop_single', { channel_id: channel._id, type: 'image' });
            }

        };

        $scope.selectPhoto = function(photo_id){
            if ( $scope.photosToSubmit[photo_id] ){
                $scope.photosToSubmit[photo_id] = false;
            } else {
                $scope.photosToSubmit[photo_id] =  true;
            }

            $scope.validateSubmitedPhotos();
        };

        $scope.getSelectedPhotosLength = function(){
            var count = 0;
            for (var key in $scope.photosToSubmit) {
                if ( $scope.photosToSubmit[key] == true ){
                    count++;
                }
            }

            return count;
        };

        $scope.getSelectedPhotosList = function(){
            //We need to create the list with the photos IDs
            var list = [];
            for (var key in $scope.photosToSubmit) {
                if ( $scope.photosToSubmit[key] == true ){
                    list.push(key);
                }
            }

            //Now we need to build photo ids array
            var newList = [];
            for (var key in $scope.channel.submittedPhotos) {
                var photo = $scope.channel.submittedPhotos[key];
                if ( list.indexOf(photo._id.toString()) != -1 ){
                    newList.push(photo);
                }
            }

            return newList;
        };
}]);
