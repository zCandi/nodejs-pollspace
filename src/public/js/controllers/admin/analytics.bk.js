'use strict';

angular.module('mean.analytics').controller('AdminAnalyticsController', ['$resource', '$scope', '$stateParams',
    '$location', '$http', '$modal','Global', 'ChannelAdminFactory', 'PropsAdminFactory','CurrentChannel', 'Charts',
    function ($resource, $scope, $stateParams, $location, $http, $modal, Global, ChannelAdminFactory, PropsAdminFactory, CurrentChannel, Charts) {
        $scope.global = Global;
        $scope.currentChannel = CurrentChannel.getCurrentChannel();
        $scope.filterGenderList = { 'male': 'Male', 'female': 'Female', 'other': 'Other'};
        $scope.relationshipStatusList = { 'single': 'Single', 'married': 'Married', 'other': 'Other'};
        $scope.propListCount = 0;
        $scope.propListHolder = [$scope.propListCount];
        $scope.propListModel = [{
            prop: null,
            answer: null
        }];
        $scope.propListResponseModel = [];
        $scope.propListResponseList = [];
        $scope.tagListModel = [];
        $scope.tagListCount = 0;
        $scope.tagListHolder = [$scope.tagListCount];
        $scope.runLabel = 'Run Test';

	$scope.arrChannelInsights = {
            total_shares: 'Total Shares',
            total_polls: 'Total Polls',
            total_poll_votes: 'Total Poll Votes',
            avg_votes_per_poll: 'Avg. Votes per Poll',
            avg_shares_per_poll: 'Avg. Shares per Poll',
            total_subscribers: 'Total Subscribers',
            total_props: 'Total Quizzes',
            total_prop_votes: 'Total Quiz Votes',
            avg_votes_per_prop: 'Avg. Votes per Quiz',
            avg_shares_per_prop: 'Avg. Shares per Quiz'
        };

        $scope.pieChartConfig = {
            options: {
                chart: {
                    type: 'pie',
                    "backgroundColor": "transparent"
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: '#fff'
                            },
                        },
                        showInLegend: false
                    }
                }
            },
            series: [{
                name: 'Brands',
                data: [
                    
                ]
            }],
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            loading: false,
            size: {}
        };

        $scope.findOne = function() {
            ChannelAdminFactory.get({ id: $stateParams.channel_id },function(channel) {
                $scope.channel = channel;
                $scope.channel.logobk = channel.logo;
                $scope.channel.picturebk = channel.picture;
            });
        };

        $scope.findProps = function() {
            ChannelAdminFactory.propList({ id: $stateParams.channel_id, type: 'classic' },function(props) {
                $scope.props = props;
            });
        };

        $scope.updateResponse = function(index){
            var propId = this.propListModel[index].prop;

            angular.forEach($scope.props, function(prop, key) {
                if ( angular.equals(prop._id, propId) ){
                    var answers = [];
                    angular.forEach(prop.answers, function(answer, key) {
                        if ( !_.isEmpty(answer.name) ){
                            answers.push(answer);
                        }
                    });
                    $scope.propListResponseList[index] = answers;
                }
            });
        };

        $scope.addVotingHistory = function(){
            $scope.propListCount++;
            $scope.propListHolder.push($scope.propListCount);
            $scope.propListModel[$scope.propListCount] = { prop: null, answer: null };
        };

        $scope.runUserTest = function(){
            $scope.runLabel = 'Running Test...';

            var data = {};

            //Demotraphics
            data.gender = this.gender;
            data.age_from = this.age_from;
            data.age_to = this.age_to;
            data.relationship_status = this.relationship_status;
            data.location = this.location;

            //Voting history
            data.propList = this.propListModel;

            ChannelAdminFactory.searchAnalyticsUsers({ id: $stateParams.channel_id },data,function(users) {
                $scope.runLabel = 'Run Test';

                $scope.users = users;
            });
        };

        /* Analytic Prop */

        $scope.findTags = function() {
            ChannelAdminFactory.tagList({ id: $stateParams.channel_id },function(tags) {
                $scope.tags = tags;
            });
        };

        $scope.addTag = function(){
            $scope.tagListCount++;
            $scope.tagListHolder.push($scope.tagListCount);
        };

        $scope.runPropTest = function(){
            $scope.runLabel = 'Running Test...';
            var data = {};
            //Demotraphics
            data.gender = this.gender;
            data.age_from = this.age_from;
            data.age_to = this.age_to;
            data.relationship_status = this.relationship_status;
            data.location = this.location;

            //Tagging
            data.tagList = _.without(this.tagListModel, "");

            //Prop Sharing and Analytics
            data.numberOfVotes_from = this.numberOfVotes_from;
            data.numberOfVotes_to = this.numberOfVotes_to;
            data.votePercent_from = this.votePercent_from;
            data.votePercent_to = this.votePercent_to;
            data.numberOfShares_from = this.numberOfShres_from;
            data.numberOfShares_to = this.numberOfShres_to;
            data.sharePercent_from = this.sharePercent_from;
            data.sharePercent_to = this.sharePercent_to;
            data.numberOfViews_from = this.numberOfViews_from;
            data.numberOfViews_to = this.numberOfViews_to;
            data.viewPercent_from = this.viewPercent_from;
            data.viewPercent_to = this.viewPercent_to;
            data.numberOfNewVoters_from = this.numberOfNewVoters_from;
            data.numberOfNewVoters_to = this.numberOfNewVoters_to;
            data.newVotersPerShare_from = this.newVotersPerShare_from;
            data.newVotersPerShare_to = this.newVotersPerShare_to;

            ChannelAdminFactory.searchAnalyticsProps({ id: $stateParams.channel_id },data,function(props) {
                $scope.runLabel = 'Run Test';

                props.forEach(function(prop, propKey){
                    if ( !_.isUndefined(prop) && !_.isEmpty(prop) ){
                        prop.status = $scope.propStatus(prop);
                    }
                });

                $scope.props = props;
            });
        };

        $scope.getChannelInsights = function(){
            ChannelAdminFactory.getChannelInsights({ id: $stateParams.channel_id },function(insights) {
                $scope.insights = insights;
		angular.forEach($scope.arrChannelInsights, function (value, key){
                    let data = {name: value, y: $scope.insights[key] ? $scope.insights[key] : 0};
                    $scope.pieChartConfig.series[0].data.push(data);
                });
            });
        };

        $scope.getLeaderboardPoints = function(){
            ChannelAdminFactory.leaderboard({ id: $stateParams.channel_id },function(leaderboard) {
                $scope.leaderboardList = leaderboard.leaderboardlist || [];
                $scope.leaderboard = leaderboard.leaderboard || {};
            });
        };

        $scope.getBaseUrl = function() {
            return $location.protocol() + "://" + $location.host() +
                ($location.port() && ":" + $location.port()) + "/";
        };

        $scope.resizePieChart = function(pieChartConfig){
            $(window).resize(function(){
                var w = $(".pie-chart-pos").filter(":visible").width();
                var h = $(".pie-chart-pos").filter(":visible").height();

                pieChartConfig.size.width = w;
                pieChartConfig.size.height = h;
                $scope.$apply();
            });
        };

        $scope.propStatus = function(prop){
            //created > published > closed > graded
            if ( $scope.isSet(prop.timestamps.graded)) return 'graded';
            if ( $scope.isSet(prop.timestamps.closed)) return 'closed';
            if ( $scope.isSet(prop.timestamps.published)) return 'published';
            if ( $scope.isSet(prop.timestamps.created)) return 'created';
            if ( $scope.isSet(prop.timestamps.suggested)) return 'suggested';
        };

        $scope.isSet = function(date){
            var ts = new Date(date).getTime();
            return ts !== 0;
        };

        $scope.getAnalyticsPropResults = function(){
            $scope.results = [];

            ChannelAdminFactory.propList({ id: $stateParams.channel_id },function(props) {
                props.forEach(function(prop, key){
                    var data = {};
                    data.prop = prop;
                    data.prop.status = $scope.propStatus(prop);

                    $scope.results.push(data);
                });
            });
        };

        $scope.showResultsModalPreview = function(prop){

            //CHARTS
            $scope.pieChartLegend = [];
            $scope.pieChartConfig = Charts.getPiechartConfiguration();
            $scope.barChartConfigGender = Charts.getBarchartConfigurationGender();
            $scope.barChartConfigAge = Charts.getBarchartConfigurationAge();

            $scope.pieChartConfig.size.width = 300;
            $scope.pieChartConfig.size.height = 300;

            PropsAdminFactory.get({
                id: prop._id
            }, function(prop) {
                $scope.prop = prop;

                $scope.findUsersDataForPieChart(prop);
                $scope.findUsersDataForBarChartGender(prop);
                $scope.findUsersDataForBarChartAge(prop);

                var modal = $modal(
                    {   scope: $scope,
                        contentTemplate: 'views/admin/analytics/prop_results.html',
                        show: false,
                        title: "Poll Results"
                    });

                modal.$promise.then(modal.show);
            });

        };


        $scope.findUsersDataForPieChart = function(prop){
            var votes = prop.votes;
            var tempChartData = [];
            var pieChartConfig = $scope.pieChartConfig;
            var pieChartLegend = [];

            votes.forEach(function(vote, voteKey){
                var choice = vote.choice;
                if ( tempChartData[choice] ){
                    tempChartData[choice]['count']++;
                } else {
                    tempChartData[choice] = [];
                    tempChartData[choice]['count'] = 1;
                }
                tempChartData[choice]['name'] = $scope.findChoiceName(prop.answers, choice);
            });

            var dataList = [];
            for (var k in tempChartData) {
                var chartData = {};
                chartData.name = tempChartData[k].name;
                chartData.y = tempChartData[k].count;
                dataList.push(chartData);
            }

            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var total = votes.length;
            var i = 0;
            dataList.forEach(function(_data, dataKey){
                if ( i == 2 ){
                    i++;
                }
                _data.y = (_data.y * 100) / total;
                _data.color = palette['complementary_2_'+i];
                i++;
            });

            $scope.pieChartConfig.series[0].data = dataList;
            $scope.pieChartLegend = dataList;
        };

        $scope.findChoiceName = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };

        $scope.findUsersDataForBarChartGender = function(prop){
            var votes = prop.votes;
            var chart = $scope.barChartConfigGender;
            var tempChartData = [];

            votes.forEach(function(vote, voteKey){
                if ( vote.user && vote.user.facebook_data ){
                    var gender = vote.user.facebook_data.gender;
                    if ( tempChartData[gender] ){
                        tempChartData[gender]['count']++;
                    } else {
                        tempChartData[gender] = [];
                        tempChartData[gender]['count'] = 1;
                    }
                }

            });

            var series = [];
            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var pos = 0;
            for (var k in tempChartData) {
                if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                    pos++;
                }

                var serie = {};
                serie.data = [{ y: tempChartData[k].count, color: palette['analogous_1_'+pos] }];
                serie.type = "column";
                serie.name = k;
                series.push(serie);
                pos++;
            }

            chart.series = series;
        };

        $scope.findUsersDataForBarChartAge = function(prop){
            var votes = prop.votes;
            var chart = $scope.barChartConfigAge;
            var tempChartData = [];

            votes.forEach(function(vote, voteKey){
                if ( vote.user && vote.user.facebook_data ){
                    var age = vote.user.facebook_data.age;
                    if ( age < 21 ){
                        if ( tempChartData['a'] ){
                            tempChartData['a']['count']++;
                        } else {
                            tempChartData['a'] = [];
                            tempChartData['a']['count'] = 1;
                            tempChartData['a']['name'] = "-21";
                        }
                    } else {
                        if ( age > 21 && age <= 25 ){
                            if ( tempChartData['b'] ){
                                tempChartData['b1']['count']++;
                            } else {
                                tempChartData['b'] = [];
                                tempChartData['b']['count'] = 1;
                                tempChartData['b']['name'] = "21-25";
                            }
                        } else {
                            if ( age > 26 && age <= 35 ){
                                if ( tempChartData['c'] ){
                                    tempChartData['c']['count']++;
                                } else {
                                    tempChartData['c'] = [];
                                    tempChartData['c']['count'] = 1;
                                    tempChartData['c']['name'] = "26-35";
                                }
                            } else {
                                if ( age > 36 && age <= 49 ){
                                    if ( tempChartData['d'] ){
                                        tempChartData['d']['count']++;
                                    } else {
                                        tempChartData['d'] = [];
                                        tempChartData['d']['count'] = 1;
                                        tempChartData['d']['name'] = "36-49";
                                    }
                                } else {
                                    if ( age >= 50  ){
                                        if ( tempChartData['e'] ){
                                            tempChartData['e']['count']++;
                                        } else {
                                            tempChartData['e'] = [];
                                            tempChartData['e']['count'] = 1;
                                            tempChartData['e']['name'] = "+50";
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

            });

            var series = [];
            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var pos = 0;
            for (var k in tempChartData) {
                if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                    pos++;
                }

                var serie = {};
                serie.data = [{ y: tempChartData[k].count, color: palette['split_complementary_1_'+pos] }];
                serie.type = "column";
                serie.name = tempChartData[k].name;
                series.push(serie);
                pos++;
            }

            chart.series = series;
        };

    }]);
