'use strict';

angular.module('mean.system').controller('AdminHeaderController', ['$scope', 'Global', function ($scope, Global) {
    $scope.global = Global;

    $scope.menu = [{
        'title': 'Channels',
        'link': 'admin/channels'
    }];
    
    $scope.isCollapsed = false;
}]);