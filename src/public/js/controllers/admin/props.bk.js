'use strict';

/* use of underscore: http://underscorejs.org */

angular.module('mean.props').controller('AdminPropsController', ['$resource', '$scope', '$state', '$localStorage','$http','$stateParams',
    '$location', '$alert','$modal', '$timeout','$sce','Global', 'PropsAdminFactory', 'GroupsAdminFactory','CurrentChannel', 'Charts',
    'Tpl','Single',
    function ($resource, $scope, $state, $localStorage, $http, $stateParams, $location, $alert, $modal, $timeout, $sce, Global,
              PropsAdminFactory, GroupsAdminFactory, CurrentChannel, Charts, Template, Single) {
        $scope.global = Global;
        $scope.user = Global.user;
        $scope.options = [];
        $scope.status_action = null;
        $scope.type = { classic: 'classic', opinion: 'opinion', quiz: 'quiz', image: 'image', product: 'product' };
        $scope.propType = $stateParams.type;
        $scope.channel = CurrentChannel.getCurrentChannel();
        $scope.prop = {};
        $scope.embedded = false;

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };

        $scope.addOption = function() {
            $scope.options.push( { name: $scope.newOption  } );
            $scope.newOption = "";
        };

        $scope.removeOption = function(option){
            $scope.options.splice($scope.options.indexOf(option), 1);
        };

        $scope.optionChecked = function(option){
            for (var key in $scope.prop.answers) {
                $scope.prop.answers[key].correct = false;
            }

            option.correct = true;
        };

        $scope.findChannel = function(channel_id){
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            var Channel = $resource('/api/admin/channel/:channel_id', { channel_id: channel_id });
            var channel = Channel.get(function(channel){
                $scope.channel = channel;
            });
        };

        $scope.getBaseUrl = function() {
            return $location.protocol() + "://" + $location.host() +
                ($location.port() && ":" + $location.port()) + "/";
        };

        $scope.findProp = function(){
            PropsAdminFactory.get({
                id: $stateParams.prop_id
            }, function(prop) {
                $scope.prop = prop;

                CurrentChannel.setCurrentProp(prop);

                //This is necessary for waiting dom elements to load, then set the data
                $timeout(function() {
                    $("#weekof_from_date_input").val(moment(prop.weekof.from).utc().format("MM/DD/YYYY"));
                    $("#weekof_to_date_input").val(moment(prop.weekof.to).utc().format("MM/DD/YYYY"));
                }, 1000);

                $scope.prop.imagebk = prop.image;
                $scope.prop.status = $scope.propStatus(prop);
                $scope.prop.isClosed = function(){
                    return $scope.propStatus(prop) == 'closed';
                };
                $scope.prop.isGraded = function(){
                    return $scope.propStatus(prop) == 'graded';
                };
                $scope.prop.isCreated = function(){
                    return $scope.propStatus(prop) == 'created';
                };
                $scope.prop.isSuggested = function(){
                    return $scope.propStatus(prop) == 'suggested';
                };
                $scope.prop.isPublished = function(){
                    return $scope.propStatus(prop) == 'published';
                };
                $scope.prop.isUnpublished = function(){
                    return $scope.propStatus(prop) == 'unpublished';
                };
                $scope.prop.isDistributed = function(){
                    return $scope.propStatus(prop) == 'distributed';
                };

                $scope.options = prop.answers;

                var url = Global.hostname;
                url = url + "/prop/"+prop.guid;
                //$scope.facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url);
                //$scope.twitter_url = "https://twitter.com/intent/tweet?text=" + prop.name + "&url=" + encodeURIComponent(url);

                $scope.facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" + url;
                $scope.twitter_url = "https://twitter.com/intent/tweet?text=" + prop.name + "&url=" + url;

                var embedded_code = '<div class="pollspace-sq"><script src="'+url+'/poll.js"></script></div>';
                $scope.pollspace_embedded_code = embedded_code;
            });
        };

        $scope.findChannelAndFindProp = function(){
            $scope.findChannel();
            $scope.findProp();
        };

        $scope.find = function(channel_id) {
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            PropsAdminFactory.list({channel_id: channel_id},function(props) {
                $scope.channel = CurrentChannel.getCurrentChannel();
                $scope.props = props;
            });
        };

        $scope.findSinglePolls = function(channel_id){
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            PropsAdminFactory.list({channel_id: channel_id, type: $stateParams.type},function(props) {
                $scope.channel = CurrentChannel.getCurrentChannel();
                $scope.props = props;
            });
        };

        $scope.findSuggestedbyList = function(channel_id) {
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            PropsAdminFactory.suggestedbylist({channel_id: channel_id},function(props) {
                $scope.props = props;
            });
        };

        $scope.getProps = function(channel_id) {
            if ( !channel_id ){
                channel_id = $stateParams.channel_id;
            }

            if ( channel_id ){
                PropsAdminFactory.list({channel_id: channel_id},function(props) {
                    $scope.props = props;
                });
            } else {
                $scope.props = [];
            }
        };

        $scope.initCreateAnswers = function(){
            $scope.prop.answers = [];
            $scope.prop.answers[0] = { correct: false };
            $scope.prop.answers[1] = { correct: false };
            $scope.prop.answers[2] = { correct: false };
            $scope.prop.answers[3] = { correct: false };
        };

        $scope.initCreateProductAnswers = function(){
            $scope.prop.answers = [];
        };

        $scope.validateCorrectAnswer = function(answers){
            var valid = false;
            answers.forEach(function(value, key){
                if ( value.correct === true ){
                    valid = true;
                }
            });

            return valid;
        };

        $scope.getNewProduct = function(){
            var product = {};
            product.name = "";

            return product;
        };

        $scope.addProduct = function(){
            var product = $scope.getNewProduct();
            $scope.prop.answers.push(product);
        };

        $scope.removeProduct = function(key){
            $scope.prop.answers.splice(key, 1);
        };

        $scope.create = function() {
            $scope.prop.timestamps = { created: new Date().getTime() };
            $scope.prop.type = $scope.propType;
            //IF type product add a random title
            if ( $scope.propType == 'product'  ){
                var randexp = new RandExp('([A-Za-z0-9]){16}', 'i');
                $scope.prop.name = 'product_'+randexp.gen();
                $scope.prop.type = 'product';
                $scope.prop.timestamps = { created: new Date().getTime(), published: new Date().getTime() };
            }

            if ( $scope.propType == 'image' ){
                $scope.prop.type = 'image';
                $scope.prop.weekof = {
                    from: moment($("#weekof_from_date_input").val()).valueOf(),
                    to: moment($("#weekof_to_date_input").val()).valueOf()
                };
            }
            if ( $scope.prop.type == 'quiz' && !$scope.validateCorrectAnswer($scope.prop.answers) ){
                $alert({
                    title: 'Error!',
                    content: 'Please select a correct answer for question: "' + $scope.prop.name + '"',
                    placement: 'top',
                    type: 'danger',
                    duration: '8',
                    show: true,
                    container: '#alerts-container',
                    animation: 'am-fade-and-slide-top'
                });
            } else {

                $scope.prop.answers.forEach(function(answer, key){
                    var randexp = new RandExp('([A-Za-z0-9]){16}', 'i');
                    answer.id = randexp.gen();
                });

                var isDisabled = $("#create").hasClass('disabled');
                if ( !isDisabled ){
                    PropsAdminFactory.create({channel_id: $stateParams.channel_id }, $scope.prop,function(response) {
                        $location.path('admin/channel/' + $stateParams.channel_id + '/prop/' + response._id + '/edit/single');
                    });
                }

            }

        };

        $scope.update = function() {
            var prop = $scope.prop;
            prop.status_action = $scope.status_action;
            $scope.prop.weekof.from = moment($("#weekof_from_date_input").val()).valueOf();
            $scope.prop.weekof.to = moment($("#weekof_to_date_input").val()).valueOf();
            prop.answers.forEach(function(answer, key){
                if ( _.isUndefined(answer.id) ){
                    var randexp = new RandExp('([A-Za-z0-9]){16}', 'i');
                    answer.id = randexp.gen();
                }
            });

            var isDisabled = $("#update").hasClass('disabled');
            if ( !isDisabled ){
                /* This { channelId: $stateParams.channelId } is going to MAP node router PUT channels/:channelId */
                PropsAdminFactory.save({id: $stateParams.prop_id }, prop, function(response) {
                    $scope.findProp();
                });
            }

        };

        $scope.remove = function(channel_id, prop_id) {
            PropsAdminFactory.deactivate({ id: prop_id },function(prop) {
                $location.path('admin/channel/' + channel_id + "/prop/manage/single/opinion");
            });
        };

        $scope.propStatus = function(prop){
            //created > [suggested] > published > unpublished > closed > graded
            if ( $scope.isSet(prop.timestamps.graded)) return 'graded';
            if ( $scope.isSet(prop.timestamps.closed)) return 'closed';
            if ( $scope.isSet(prop.timestamps.distributed)) return 'distributed';
            if ( $scope.isSet(prop.timestamps.unpublished)) return 'unpublished';
            if ( $scope.isSet(prop.timestamps.published)) return 'published';
            if ( $scope.isSet(prop.timestamps.suggested) && !$scope.isSet(prop.timestamps.created)) return 'suggested';
            if ( $scope.isSet(prop.timestamps.created)) return 'created';
        };

        $scope.isSet = function(date){
            var ts = new Date(date).getTime();
            return ts !== 0;
        };

        $scope.publish = function (event){
            $scope.status_action = 'publish';
            $scope.update();
        };

        $scope.unpublish = function (event){
            $scope.status_action = 'unpublish';
            $scope.update();
        };

        $scope.close = function (event){
            $scope.status_action = 'close';
            $scope.update();
        };

        $scope.grade = function (event){
            $scope.status_action = 'grade';
            $scope.update();
        };

        $scope.createPropImageCallback = function(file){
            $scope.prop.image = file.cdnUrl;
        };

        $scope.editPropImageCallback = function(file){
            $scope.prop.image = file.cdnUrl;
        };

        $scope.propAnswerImageCallback = function(file, answer_id){
            $scope.prop.answers[answer_id].image = file.cdnUrl;
        };

        $scope.findChoiceName = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };


        /* Preview Channel */
        // Show when some event occurs (use $promise property to ensure the template has been loaded)
        $scope.showEditModalPreview = function() {
            var modal = $modal({scope: $scope, contentTemplate: Template.getTemplate('views/admin/channel/prop/preview.html'), show: false, title: "Preview Prop"});
            modal.$promise.then(modal.show);
        };

        $scope.showCreateModalPreview = function() {
            $scope.channel = CurrentChannel.getCurrentChannel();
            var modal = $modal({scope: $scope, contentTemplate: Template.getTemplate('views/admin/channel/prop/preview.html'), show: false, title: "Preview Prop"});
            modal.$promise.then(modal.show);
        };

        /*********** RESULTS CHARTS ***********/
        $scope.pieChartLegend = [];
        $scope.pieChartConfig = Charts.getPiechartConfiguration();
        $scope.barChartConfigGender = Charts.getBarchartConfigurationGender();
        $scope.barChartConfigAge = Charts.getBarchartConfigurationAge();
        $scope.barChartConfigRelationshipStatus = Charts.getBarchartConfigurationRelationshipStatus();
        $scope.lineChartConfig = Charts.getLinechartConfiguration();

        /* CHARTS configurations */
        $scope.resizePieChart = function(){
            $(window).resize(function(){
                var w = $(".pie-chart-pos").filter(":visible").width();
                var h = $(".pie-chart-pos").filter(":visible").height();

                $scope.pieChartConfig.size.width = w;
                $scope.pieChartConfig.size.height = h;
                $scope.$apply();
            });
        };

        $scope.answers = [
            {name: null, correct:false},
            {name: null, correct:false},
            {name: null, correct:false},
            {name: null, correct:false}];

        //for now we need to harcode to 4 answers
        $scope.filterGenderList = { 'male': 'Male', 'female': 'Female', 'other': 'Other'};
        $scope.results;
        
        $scope.filterByDemographic = function(){
            var votes = $scope.prop.votes;
            var gender = this.gender;
            var age_from = this.age_from;
            var age_to = this.age_to;
            var results = [];

            if($scope.prop.type == 'product'){
                votes = $scope.channel.voted_products;

                votes.forEach(function(vote, voteKey){
                    if ( gender ){
                        if ( vote.user && !_.isEmpty(vote.user.facebook_data) && _.isEqual(vote.user.facebook_data.gender, gender) ){
                            results.push(vote);
                        }
                    }

                    if ( !_.isUndefined(age_from) && !_.isUndefined(age_to) ){
                        if ( ( vote.user && vote.user.facebook_data.age >= age_from) && ( vote.user && vote.user.facebook_data.age <= age_to ) ){
                            results.push(vote);
                        }
                    } else {
                        if ( !_.isUndefined(age_from) ){
                            if ( vote.user && vote.user.facebook_data.age >= age_from ){
                                results.push(vote);
                            }
                        } else {
                            if ( !_.isUndefined(age_to) ){
                                if ( vote.user && vote.user.facebook_data.age <= age_from ){
                                    results.push(vote);
                                }
                            }
                        }
                    }
                });
            }
            else {

                votes.forEach(function(vote, voteKey){
                    if ( gender ){
                        if ( vote.user && !_.isEmpty(vote.user.facebook_data) && _.isEqual(vote.user.facebook_data.gender, gender) ){
                            results.push(vote);
                        }
                    }

                    if ( !_.isUndefined(age_from) && !_.isUndefined(age_to) ){
                        if ( ( vote.user && vote.user.facebook_data.age >= age_from) && ( vote.user && vote.user.facebook_data.age <= age_to ) ){
                            results.push(vote);
                        }
                    } else {
                        if ( !_.isUndefined(age_from) ){
                            if ( vote.user && vote.user.facebook_data.age >= age_from ){
                                results.push(vote);
                            }
                        } else {
                            if ( !_.isUndefined(age_to) ){
                                if ( vote.user && vote.user.facebook_data.age <= age_from ){
                                    results.push(vote);
                                }
                            }
                        }
                    }
                });
            }

            if (_.isEmpty(gender) && _.isEmpty(age_from) && _.isEmpty(age_to)){
                results = votes;
            }
            
            console.log("filterByDemographic: results=>", results);

            return results;
        };
        
        $scope.filterByDemographic_bk20160513 = function(){
            var votes = $scope.prop.votes;
            var gender = this.gender;
            var age_from = this.age_from;
            var age_to = this.age_to;
            var results = [];

            votes.forEach(function(vote, voteKey){
                if ( gender ){
                    if ( vote.user && !_.isEmpty(vote.user.facebook_data) && _.isEqual(vote.user.facebook_data.gender, gender) ){
                        results.push(vote);
                    }
                }

                if ( !_.isUndefined(age_from) && !_.isUndefined(age_to) ){
                    if ( ( vote.user && vote.user.facebook_data.age >= age_from) && ( vote.user && vote.user.facebook_data.age <= age_to ) ){
                        results.push(vote);
                    }
                } else {
                    if ( !_.isUndefined(age_from) ){
                        if ( vote.user && vote.user.facebook_data.age >= age_from ){
                            results.push(vote);
                        }
                    } else {
                        if ( !_.isUndefined(age_to) ){
                            if ( vote.user && vote.user.facebook_data.age <= age_from ){
                                results.push(vote);
                            }
                        }
                    }
                }
            });

            if (_.isEmpty(gender) && _.isEmpty(age_from) && _.isEmpty(age_to)){
                results = votes;
            }

            return results;
        };

        $scope.filterByGroup = function(){
            var votes = $scope.prop.votes;
            var group_id = this.group;
            var results = [];

            if (!_.isEmpty(group_id)){
                $scope.groups.forEach(function(group, groupKey){
                    if (_.isEqual(group._id,group_id) ){// means that we are in the right group
                        votes.forEach(function(vote, voteKey){
                            if ( $scope.inList(vote.user._id, group.users) ){
                                results.push(vote);
                            }
                        });
                    }
                });
            }

            return results;
        };

        $scope.filterByPanel = function(){
            var votes = $scope.prop.votes;
            var panel_id = this.panel;
            var results = [];

            if (!_.isEmpty(panel_id)){
                $scope.panels.forEach(function(panel, panelKey){
                    if (_.isEqual(panel._id, panel_id) ){// means that we are in the right group
                        votes.forEach(function(vote, voteKey){
                            if ( $scope.inList(vote.user._id, panel.users) ){
                                results.push(vote);
                            }
                        });
                    }
                });
            }

            return results;
        };

        $scope.filterVotes = function(){
            var option = this.option;
            var results = null;

            switch ( option ){
                case "demographic":
                    results = $scope.filterByDemographic();
                    break;
                case "group":
                    results = $scope.filterByGroup();
                    break;
                case "panel":
                    results = $scope.filterByPanel();
                    break;
            }

            $scope.results = results;
            return $scope.results;
        };

        $scope.findPropResults = function(){
            /* Load Chart Configurations */
            PropsAdminFactory.results({id: $stateParams.prop_id},function(prop) {
                $scope.prop = prop;

                $scope.option = "demographic"; //Option by default
                $scope.demographic_disabled = false;
                $scope.group_disabled = true;
                $scope.panel_disabled = true;
               // $scope.propVotes = Single.getPropVotes(prop);
                $scope.propVotes = Single.getPropVotes(prop, $scope.channel);

                var url = Global.hostname;
                url = url + "/prop/"+prop.guid;
                $scope.facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" + url;
                $scope.twitter_url = "https://twitter.com/intent/tweet?text=" + prop.name + "&url=" + url;

                $scope.findUsersDataForPieChart();
                $scope.findUsersDataForBarChartGender();
                $scope.findUsersDataForBarChartAge();
                $scope.findUsersDataForBarChartRelationShipstatus();
                $scope.findUsersDataForLineChart();
                $scope.findGroupOptions();
                $scope.findPanelOptions();

                $scope.resizePieChart();
            });
        };

        $scope.findGroupOptions = function(){
            GroupsAdminFactory.list({type: "dynamic"},function(groups) {
                $scope.groups = groups;
            });
        };

        $scope.findPanelOptions = function(){
            GroupsAdminFactory.list({type: "static"},function(panels) {
                $scope.panels = panels;
            });
        };

        $scope.updateChart = function(){
            $scope.findUsersDataForPieChart();
            $scope.findUsersDataForBarChartGender();
            $scope.findUsersDataForBarChartAge();
            $scope.findUsersDataForBarChartRelationShipstatus();
            $scope.findUsersDataForLineChart();
        };

        $scope.findUsersDataForBarChartGender = function(){
            var votes = $scope.filterVotes();
            var chart = $scope.barChartConfigGender;
            var tempChartData = [];

            votes.forEach(function(vote, voteKey){
                if ( vote.user && vote.user.facebook_data ){
                    var gender = vote.user.facebook_data.gender;
                    if ( tempChartData[gender] ){
                        tempChartData[gender]['count']++;
                    } else {
                        tempChartData[gender] = [];
                        tempChartData[gender]['count'] = 1;
                    }
                }
            });

            var series = [];
            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var pos = 0;

            for (var k in tempChartData) {
                if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                    pos++;
                }

                var serie = {};
                serie.data = [{ y: tempChartData[k].count, color: palette['analogous_1_'+ pos] }];
                serie.type = "column";
                serie.name = k;
                series.push(serie);
                pos++;
            }
            chart.series = series;
        };

        $scope.findUsersDataForBarChartAge = function(){
            var votes = $scope.filterVotes();
            var chart = $scope.barChartConfigAge;
            var tempChartData = [];

            votes.forEach(function(vote, voteKey){
                if ( vote.user && vote.user.facebook_data ){
                    var age = vote.user.facebook_data.age;
                    if ( age < 21 ){
                        if ( tempChartData['a'] ){
                            tempChartData['a']['count']++;
                        } else {
                            tempChartData['a'] = [];
                            tempChartData['a']['count'] = 1;
                            tempChartData['a']['name'] = "-21";
                        }
                    } else {
                        if ( age > 21 && age <= 25 ){
                            if ( tempChartData['b'] ){
                                tempChartData['b1']['count']++;
                            } else {
                                tempChartData['b'] = [];
                                tempChartData['b']['count'] = 1;
                                tempChartData['b']['name'] = "21-25";
                            }
                        } else {
                            if ( age > 26 && age <= 35 ){
                                if ( tempChartData['c'] ){
                                    tempChartData['c']['count']++;
                                } else {
                                    tempChartData['c'] = [];
                                    tempChartData['c']['count'] = 1;
                                    tempChartData['c']['name'] = "26-35";
                                }
                            } else {
                                if ( age > 36 && age <= 49 ){
                                    if ( tempChartData['d'] ){
                                        tempChartData['d']['count']++;
                                    } else {
                                        tempChartData['d'] = [];
                                        tempChartData['d']['count'] = 1;
                                        tempChartData['d']['name'] = "36-49";
                                    }
                                } else {
                                    if ( age >= 50  ){
                                        if ( tempChartData['e'] ){
                                            tempChartData['e']['count']++;
                                        } else {
                                            tempChartData['e'] = [];
                                            tempChartData['e']['count'] = 1;
                                            tempChartData['e']['name'] = "+50";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            });

            var series = [];
            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var pos = 0;
            for (var k in tempChartData) {
                if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                    pos++;
                }

                var serie = {};
                serie.data = [{ y: tempChartData[k].count, color: palette['split_complementary_1_'+pos] }];
                serie.type = "column";
                serie.name = tempChartData[k].name;
                series.push(serie);
                pos++;
            }

            chart.series = series;
        };

        $scope.findUsersDataForBarChartRelationShipstatus = function(){
            var votes = $scope.filterVotes();
            var chart = $scope.barChartConfigRelationshipStatus;
            var tempChartData = [];

            votes.forEach(function(vote, voteKey){
                if ( vote.user && vote.user.facebook_data ){
                    var relationshipstatus = vote.user.facebook_data.relationship_status;
                    if ( tempChartData[relationshipstatus] ){
                        tempChartData[relationshipstatus]['count']++;
                    } else {
                        tempChartData[relationshipstatus] = [];
                        tempChartData[relationshipstatus]['count'] = 1;
                    }
                }

            });

            var series = [];
            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var pos = 0;
            for (var k in tempChartData) {
                if ( Object.keys(tempChartData).length <= 4 && pos == 2){
                    pos++;
                }

                var serie = {};
                serie.data = [{ y: tempChartData[k].count, color: palette['analogous_3_'+pos] }];
                serie.type = "column";
                serie.name = k;
                series.push(serie);
                pos++;
            }

            chart.series = series;
        };

       $scope.findUsersDataForPieChart = function(){
            var votes = $scope.filterVotes();
            var chart = $scope.pieChartConfig;
            var channel = CurrentChannel.getCurrentChannel();
            var tempChartData = [];
            var count_voted_products = 0;

            if($scope.prop.type == 'product'){
                votes = channel.voted_products;

                votes.forEach(function(vote, voteKey){
                    if(vote.prop._id == $scope.prop._id) {
                        console.log("findUsersDataForPieChart: vote: ", vote);
                        var answers = vote.answers;
                        answers.forEach(function(answer){
                            if ( tempChartData[answer] ){
                                tempChartData[answer]['count']++;
                            } else {
                                tempChartData[answer] = [];
                                tempChartData[answer]['count'] = 1;
                            }
                            tempChartData[answer]['name'] = $scope.findChoiceName($scope.prop.answers, answer);

                            count_voted_products++;
                        });
                    }
                    
                });

            }else {
                votes.forEach(function(vote, voteKey){
                    console.log("findUsersDataForPieChart: vote: ", vote)
                    var choice = vote.choice;
                    if ( tempChartData[choice] ){
                        tempChartData[choice]['count']++;
                    } else {
                        tempChartData[choice] = [];
                        tempChartData[choice]['count'] = 1;
                    }
                    tempChartData[choice]['name'] = $scope.findChoiceName($scope.prop.answers, choice);
                });
            }
            
            var dataList = [];
            for (var k in tempChartData) {
                var data = {};
                data.name = tempChartData[k].name;
                data.y = tempChartData[k].count;
                dataList.push(data);
            }

            var total = votes.length;
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var i = 0;

            if($scope.prop.type == 'product'){
                total = count_voted_products;
            }

            dataList.forEach(function(data, dataKey){
                if ( i == 2 ){
                    i++;
                }
                data.y = (data.y * 100) / total;
                data.color = palette['complementary_2_' + i];
                i++;
            });

            $timeout(function() {
                chart.series[0].data = dataList;
            }, 1000);
            $scope.pieChartLegend = dataList;
        };

        $scope.findUsersDataForPieChart_bk20160513 = function(){
            var votes = $scope.filterVotes();
            var chart = $scope.pieChartConfig;
            var channel = CurrentChannel.getCurrentChannel();
            var tempChartData = [];

            votes.forEach(function(vote, voteKey){
                var choice = vote.choice;
                if ( tempChartData[choice] ){
                    tempChartData[choice]['count']++;
                } else {
                    tempChartData[choice] = [];
                    tempChartData[choice]['count'] = 1;
                }
                tempChartData[choice]['name'] = $scope.findChoiceName($scope.prop.answers, choice);
            });



            var dataList = [];
            for (var k in tempChartData) {
                var data = {};
                data.name = tempChartData[k].name;
                data.y = tempChartData[k].count;
                dataList.push(data);
            }

            var total = votes.length;
            var initialColor = channel.color && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);
            var i = 0;
            dataList.forEach(function(data, dataKey){
                if ( i == 2 ){
                    i++;
                }
                data.y = (data.y * 100) / total;
                data.color = palette['complementary_2_' + i];
                i++;
            });

            $timeout(function() {
                chart.series[0].data = dataList;
            }, 1000);

            $scope.pieChartLegend = dataList;
        };



        $scope.findUsersDataForLineChart = function(){
            var prop = $scope.prop;
            var votes = $scope.filterVotes();
            var chart = $scope.lineChartConfig;
            var tempChartData = [];

            var opendate = new Date(prop.timestamps.published);
            var closedate = new Date(prop.timestamps.closed);
            if ( closedate.getTime() == 0 ){ //means that the prop is not closed yet, so we do a range of 14 days from the open date
                closedate = opendate.addDays(14);
            }
            var dateArray = $scope.getDates(opendate, closedate);
            for (var i = 0; i < dateArray.length; i ++ ) {
                var date = dateArray[i];
                var month = date.getMonth() + 1;
                var day = date.getDate() + 1;
                if ( day < 10 ){
                    day = "0"+day;
                }
                if ( tempChartData[month+"/"+day] ){
                    tempChartData[month+"/"+day]++;
                } else {
                    tempChartData[month+"/"+day] = 0;
                }
            }

            votes.forEach(function(vote, voteKey){
                var voteday = new Date(vote.created);
                if ( voteday >= opendate && voteday <= closedate){
                    var month = voteday.getMonth() + 1;
                    var day = voteday.getDate() + 1;
                    if ( day < 10 ){
                        day = "0"+day;
                    }

                    if ( tempChartData[month+"/"+day] ){
                        tempChartData[month+"/"+day]++;
                    } else {
                        tempChartData[month+"/"+day] = 1;
                    }
                }

            });

            var dataList = [];
            for (var k in tempChartData) {
                var data = {};
                dataList.push(tempChartData[k]);
            }

            chart.series[0].data = array_values(tempChartData);
            chart.xAxis.categories = array_keys(tempChartData);
        };


        $scope.resizePieChart = function(){
            $(window).resize(function(){
                var w = $(".pie-chart-pos").filter(":visible").width();
                var h = $(".pie-chart-pos").filter(":visible").height();

                $scope.pieChartConfig.size.width = w;
                $scope.pieChartConfig.size.height = h;
                $scope.$apply();
            });
        };

        $scope.setDemographicToAll = function(option){
            $scope.gender = "";
            $scope.age_from = "";
            $scope.age_to = "";

            switch ( option ){
                case "demographic":
                    $scope.demographic_disabled = false;
                    $scope.group_disabled = true;
                    $scope.panel_disabled = true;
                    break;
                case "group":
                    $scope.demographic_disabled = true;
                    $scope.group_disabled = false;
                    $scope.panel_disabled = true;
                    break;
                case "panel":
                    $scope.demographic_disabled = true;
                    $scope.group_disabled = true;
                    $scope.panel_disabled = false;
                    break;
            }
        };

        Date.prototype.addDays = function(days) {
            var dat = new Date(this.valueOf())
            dat.setDate(dat.getDate() + days);
            return dat;
        };

        $scope.getDates = function(startDate, stopDate) {
            var dateArray = new Array();
            var currentDate = startDate;
            while (currentDate <= stopDate) {
                dateArray.push(currentDate)
                currentDate = currentDate.addDays(1);
            }
            return dateArray;
        };

        $scope.inList = function(id, userslist){
            var key = '';

            for (key in userslist) {
                if (_.isEqual(userslist[key]._id, id)) {
                    return true;
                }
            }

            return false;
        };

        $scope.gotoCreateSingleQuestionPoll = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/create/single/opinion");
        };

        $scope.gotoCreateImageQuestionPoll = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/create/single/image");
        };

        $scope.gotoCreateSingleQuestionQuiz = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/create/single/quiz");
        };

        $scope.gotoCreateMultiQuestionPoll = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/create/multiple/opinion");
        };

        $scope.gotoCreateMultiQuestionQuiz = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/create/multiple/quiz");
        };

        $scope.gotoManageSinglePolls = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/manage/single/opinion");
        };

        $scope.gotoSuggestedByPolls = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/suggestedby/list");
        };

        $scope.gotoMultiplePolls = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/manage/multiple/opinion");
        };

        $scope.gotoSingleQuizzes = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/manage/single/quiz");
        };

        $scope.gotoMultipleQuizzes = function(){
            $location.path("/admin/channel/"+$stateParams.channel_id+"/prop/manage/multiple/quiz");
        };


        $scope.toggleEmbedded = function(){
            $scope.embedded = !$scope.embedded;
        };

        $scope.initCreateSingle = function(){
            if ( $localStorage.submittedPhotosList ){
                //Timeout is used to WAIT till the DOM is finished loading..
                $timeout(function(){
                    var submitedPhotosList = $localStorage.submittedPhotosList;
                    for (var key in submitedPhotosList ) {
                        var photo = submitedPhotosList[key];
                        if ( photo.user && photo.user.email ){
                            $scope.prop.answers[key].name = photo.user.email;
                        }
                        $scope.prop.answers[key].meta = { instagram: { value: "" } };
                        $scope.prop.answers[key].meta.instagram.value = "@" + photo.instagram;
                        $scope.prop.answers[key].image = photo.image;
                    }
                    $localStorage.submittedPhotosList = null;
                });

            }
        }
}]);
