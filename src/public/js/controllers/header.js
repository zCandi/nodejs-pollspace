'use strict';

angular.module('mean.system').controller('HeaderController', ['$scope', '$state','$rootScope','$location', '$window',
    'Global', 'CurrentChannel',
    function ($scope, $state, $rootScope, $location, $window, Global, CurrentChannel) {
        $scope.global = Global;
        $scope.cguid = Global.cguid;
        $scope.currentchannel = CurrentChannel.getCurrentChannel();
        $scope.isCollapsed = false;

        if ( Global.quiz && Global.quiz.title ){
            $rootScope.quiz_title = Global.quiz.title;
        }

        $scope.quiz_title = $rootScope.quiz_title;
        $scope.quiz_score = $rootScope.quiz_score;
        $scope.quiz_type = $rootScope.quiz_type;

        $rootScope.$watch('activeTab', function(newValue, oldValue) {
            $scope.activetab = $rootScope.activeTab;
        });

        $rootScope.$watch('quiz_title', function(newValue, oldValue) {
            $scope.quiz_title = $rootScope.quiz_title;
        });

        $rootScope.$watch('quiz_score', function(newValue, oldValue) {
            $scope.quiz_score = $rootScope.quiz_score;
        });

        $rootScope.$watch('quiz_type', function(newValue, oldValue) {
            $scope.quiz_type = $rootScope.quiz_type;
        });

        $scope.openMenu = function(){
            $("#menu-slide").trigger("open.mm");
        };

        $scope.setLocationList = function(){
            $scope.urlList = [];
            console.log('IN hedaer');
            console.log(Global);
            console.log($scope.currentchannel);
            if ( Global.cguid ){
                $scope.urlList['pollList'] = '/#!/poll/list';
                $scope.urlList['quizList'] = '/#!/quiz/list';
                $scope.urlList['propSuggest'] = '/#!/prop/suggest';
                $scope.urlList['channelLeaderboard'] = '/#!/leaders';
            }

            $rootScope.urlList = $scope.urlList;
        };

        $scope.goToQuizList = function(){
            //$state.transitionTo('user_channel_quiz_list_subdomain');
            $window.history.back();
        };

        $scope.goToChannelProfile = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $state.transitionTo('user_channel_profile_subdomain');
            } else {
                $state.transitionTo('user_channel_profile', { cguid: $scope.currentchannel.guid });
            }
        };
}]);