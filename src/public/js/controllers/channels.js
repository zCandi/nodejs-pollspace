'use strict';

angular.module('mean.channels').controller('ChannelsController', ['$templateCache','$timeout', '$resource', '$scope', '$state',
    '$rootScope', '$stateParams', '$location', '$window','$sce','Global', 'ChannelFactory', 'PropsFactory','CurrentChannel','Charts',
    function ($templateCache, $timeout, $resource, $scope, $state, $rootScope, $stateParams, $location,
              $window, $sce, Global, ChannelFactory, PropsFactory, CurrentChannel, Charts) {
        $scope.global = Global;
        $scope.channel = CurrentChannel.getCurrentChannel();
        $scope.contact_submit_title = 'Send Email';
        $scope.currentchannel = CurrentChannel.getCurrentChannel();
        $scope.uploadPhotoText = "UPLOAD PHOTO";
        $scope.selectedProducts = [];

        $scope.contactPurposes = {
            //'I have a question about the "Your Thoughts" App.': 'I have a question about the "Your Thoughts" App.',
            //'I have a question for Steve.': 'I have a question for Steve.',
            'I want to reset my password.': "I want to reset my password.",
            'Other.': "Other."
        };

        $scope.contactPurposestwf = {
            'I want to reset my password.': "I want to reset my password.",
            'Other.': "Other."
        };

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        };

        $scope.setCguid = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $stateParams.cguid = Global.cguid;
            }
        };

        $scope.findChannelByGuid = function( guid ){
            if ( CurrentChannel.getCurrentChannel() ){
                $scope.channel = CurrentChannel.getCurrentChannel();
            } else {
                if ( !guid ){
                    guid = $stateParams.cguid;
                }

                if ( !_.isEmpty(guid) ){
                    ChannelFactory.getByGuid({ guid: guid },function(channel) {
                        $scope.channel = channel;
                    });
                }
            }

        };

        $scope.find = function() {
            ChannelFactory.list({},function(channels){
                $scope.channels = channels;
            });
        };

        $scope.applySubscribed = function(){
            $scope.channels.forEach(function(channel){
                var isSuscribed = $scope.isSubscribed(channel._id);
                channel.subscribed = isSuscribed;
            });
        }

        $scope.isSubscribed = function( channelId ) {
            return !!~$scope.global.user.subscriptions.indexOf(channelId);
        };

        $scope.subscribe = function(channelId){
            ChannelFactory.subscribe({id: channelId}, function(result) {
                alert(result.message);
            });
        };

        $scope.getLeaderboardPoints = function(){
            ChannelFactory.leaderboard({ guid: $scope.channel.guid },function(leaderboard) {
                $scope.leaderboardList = leaderboard.leaderboardlist || [];
                $scope.leaderboard = leaderboard.leaderboard || {};
            });
        };

        $scope.sendContactInfo = function(){
            var data = {
                email: this.email,
                subject: this.subject,
                message: this.message
            };

            var isDisabled = $("#sendemail").hasClass('disabled');
            if ( !isDisabled ){
                var channel = CurrentChannel.getCurrentChannel();
                $scope.contact_submit_title = 'Sending email...';
                ChannelFactory.contact({ guid: channel.guid }, data, function(result) {
                    $scope.infomessage = result.message;
                    $location.hash("message");
                    $scope.contact_submit_title = 'Send Email';
                    $scope.message = '';
                });
            }
            
        };

        /**
         * Used in Terms and Cond / Privacy to replace with channel name
         */
        $scope.getName = function(){
            if ( Global.cguid && CurrentChannel.getCurrentChannel()){
                return CurrentChannel.getCurrentChannel().name;
            } else {
                return "GetFanProps.com";
            }

        };

        $scope.getPalette = function(){
            var channel = CurrentChannel.getCurrentChannel();
            var initialColor = channel.color.color2 && channel.color.color2 ? channel.color.color2.replace("#", "") : "57A8D4";
            var palette = Charts.getMatchingColorList(initialColor);

            var palettelist = [];
            for (var key in palette) {
                palettelist.push({
                    'name': key,
                    'color': palette[key]
                });
            }

            $scope.palette = palettelist;
        };

        $scope.uploadedPhotoCallback = function(file){
            $scope.photo.image = file.cdnUrl;
            if ( $scope.photo.image == '/img/fp/ogx587x587.png' ){
                $scope.photo.image_error = true;
            } else {
                $scope.photo.image_error = false;
            }
        };

        $scope.getUploadedPhoto = function(){
            ChannelFactory.getUploadedPhoto({guid: $scope.channel.guid }, function(photo) {
                $scope.photo = photo;
                $scope.user =  $scope.global.user;
                    
                $scope.$root.ogTitle = 'Just uploaded a photo on '+ Global.hostname.replace(/.*?:\/\//g, '');

                var url = Global.hostname + '/photo/' + $scope.photo.guid + '/';

                $scope.facebook_url = "https://www.facebook.com/sharer/sharer.php?u=" + url;
                $scope.twitter_url = "https://twitter.com/intent/tweet?text=" + urlencode($scope.$root.ogTitle) + "&url=" + url;
            });
        };

        $scope.getUserSelectedProducts = function(){
            var channel =  $scope.channel;
            var user =  $scope.global.user;
            $scope.votedProducts = [];
            $scope.tags = [];// Global.tag;
            $scope.tagsImage = [];
            // console.log('tags', Global.tag);
            ChannelFactory.lastProp( { guid: $scope.channel.guid, type: 'product' }, function(prop) {
                if ( prop ){
                    prop.answers.forEach(function(answer, key){
                        answer.tags.forEach(function(tag, tagKey){
                            $scope.tagsImage[tag] = answer.image;
                        });
                        $scope.tags = _.union($scope.tags, answer.tags);
                    });
                }
            });

            channel.voted_products.forEach(function(product){
                if ( user && product && product.user && product.user._id.toString() == user._id.toString() ){
                    product.answers.forEach(function(answer_id){
                        var answer = $scope.answerInPropAnswers(answer_id, product.prop);
                        if ( answer ){
                            $scope.votedProducts.push(answer);
                        }
                    });
                }
            });
        };

        $scope.answerInPropAnswers = function(answer_id, prop){
            var _in = false;
            prop.answers.forEach(function(answer){
                if ( answer && answer.id.toString() == answer_id.toString() ){
                    _in = answer;
                }
            })

            return _in;
        };

        $scope.uploadPhoto = function(){
            if ( $scope.photo.image == '/img/fp/ogx587x587.png' ){
                $scope.photo.image_error = true;
            } else {                
                $scope.photo.image_error = false;
                // $scope.photo.tags = Global.tag;
                //console.log('scope photo', $scope.photo);
                ChannelFactory.uploadPhoto({guid: $scope.channel.guid }, $scope.photo, function(response) {  
                    $state.transitionTo('user_upload_photo_thanks');
                    ChannelFactory.getByGuid({ guid: $scope.channel.guid  },function(channel) {
                        $scope.channel = channel;
                        CurrentChannel.setCurrentChannel(channel);
                    });
                });
            }
        };

        $scope.getFinalFour = function(){
            ChannelFactory.lastProp( { guid: $scope.channel.guid, type: 'product' }, function(prop) {
                if ( prop ){
                    var loggedTotal;
                    Global.user.channel_logged_total.forEach(function(logged, key){
                        if (logged.channel == Global.channel.guid) {
                            loggedTotal = logged.logged;
                        }
                    });

                    if (prop.publishing == 1 || loggedTotal % prop.publishing == 1) {
                        $state.transitionTo('user_product_vote');
                    } else if (prop.publishing != 0) {
                        ChannelFactory.lastProp( { guid: $scope.channel.guid, type: 'image' }, function(prop) {
                            if ( prop && prop._id){
                                $state.transitionTo('user_view_single', { type: prop.type, pguid: prop.guid });
                            }
                        });
                    }
                } else {
                    ChannelFactory.lastProp( { guid: $scope.channel.guid, type: 'image' }, function(prop) {
                        if ( prop && prop._id){
                            $state.transitionTo('user_view_single', { type: prop.type, pguid: prop.guid });
                        }
                    });
                }
            });
        };

        $scope.getLastProduct = function(){
            ChannelFactory.lastProp( { guid: $scope.channel.guid, type: 'product' }, function(prop) {
                if ( prop ){
                    $scope.prop = prop;
                }
            });
        };

        $scope.selectProduct = function(id){
            if ( $scope.selectedProducts[id] ){
                delete $scope.selectedProducts[id];
            } else {
                $scope.selectedProducts[id] = id;
            }
        };

        $scope.voteProduct = function(){
            if (Object.keys($scope.selectedProducts).length >= 1){
                var answers = $scope.getSelectedProducts();
                var user =  $scope.global.user;
                Global.tag = [];

                // update product
                $scope.prop.answers.forEach(function(answer, answerkey){
                    if ($scope.selectedProducts[answer.id]) {
                        answer.counts.vote +=1;
                        answer.counts.view +=1;

                        if (!answer.votes[user._id]) {
                            var userVoted = {};
                            userVoted.user = user._id;
                            userVoted.created = new Date().getTime();

                            answer.votes.push(userVoted);
                        }

                        Global.tag = _.union(Global.tag, answer.tags);
                    }
                });

                $scope.prop.counts.vote +=1;
                $scope.prop.counts.view +=1;

                PropsFactory.voteProduct( { cguid: $scope.channel.guid }, { prop: $scope.prop, answers: answers } ,function(prop) {
                    if ( prop ){
                        //$window.location = "/";
                       // $state.transitionTo('user_final_four');
                       $state.transitionTo('user_upload_photo');
                    }
                    //alert("end voteProduct!!!!!!!!!!!!!!!!!!!!!");
                });
            } else {
                console.log("Please select at least one product to vote. Thanks!");
                

                alert("Please select at least one product to vote. Thanks!");
            }
        };

        $scope.getSelectedProducts = function(){
            var answers = [];
            for (var key in $scope.selectedProducts) {
                answers.push(key);
            }
            return answers;
        }
    }]);
