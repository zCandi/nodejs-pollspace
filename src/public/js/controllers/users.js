'use strict';

angular.module('mean.users').controller('UsersController', ['$scope', '$window','$resource','$stateParams',
    '$location', '$localStorage','Global', 'UsersFactory','UserFPAdminFactory', 'CurrentChannel','ChannelFactory',
    function ($scope, $window, $resource, $stateParams, $location, $localStorage, Global, UsersFactory, UserFPAdminFactory,
        CurrentChannel, ChannelFactory) {
        $scope.global = Global;
        $scope.access_token = Global.access_token;
        $scope.user = Global.user;
        $scope.toggle_signup = false;
        $scope.toggle_tweeter = false;
        $scope.currentchannel = CurrentChannel.getCurrentChannel();

        /* Users Demographic */
        $scope.filterGenderList = { 'male': 'Male', 'female': 'Female', 'other': 'Other'};
        $scope.filterRelationshipStatusList = { 'single': 'Single', 'married': 'Married', 'other': "Other" };

        $scope.update = function() {
            var user = $scope.user;
            var firstname = user.firstname;
            var lastname = user.lastname;
            var username = user.username;

            UsersFactory.get({ id: user._id },function(user) {
                user.firstname = firstname;
                user.lastname = lastname;
                user.username = username;
                user.timestamps.updated = new Date().getTime();

                user.$save({ id: user._id }, function(response){
                    $location.path('my/account');
                });
            });
        };

        $scope.findUser = function(){
            UsersFactory.get({ id: $scope.user._id },function(user) {
                $scope.user =  user;
            });
        };

        $scope.myRankings = function(){
            UsersFactory.ranking({ id: $scope.user._id },function(rankings) {
                $scope.rankings = rankings;
            });
        };

        $scope.findMyChannels = function(){
            UsersFactory.channellist({ id: $scope.user._id },function(channels) {
                $scope.channels = channels;
            });
        };

        $scope.findSuggestedChannels = function(){
            UsersFactory.suggestedchannellist({ id: $scope.user._id },function(channels) {
                $scope.suggestedchannels = channels;
            });
        };

        $scope.signup = function(){
            var data = {
                firstname:  this.firstname,
                lastname:   this.lastname,
                email:      this.email,
                company:   this.company
            };

            var isDisabled = $("#create").hasClass('disabled');
            if ( !isDisabled ){
                UsersFactory.signup(data, function(response) {
                    if ( response.errors ){
                        $scope.errors = response.errors;
                    } else {
                        $scope.success = 'Thank you for signing up with us. We will contact you soon!';
                    }
                });
            }
        };

        $scope.getSignups = function(){
            UsersFactory.getsignup(function(signups) {
                $scope.signups = signups;
            });

        };

        $scope.create = function() {
            var data = {
                firstname:  this.firstname,
                lastname:   this.lastname,
                email:      this.email,
                password:   this.password,
                is_admin:   this.is_admin,
                username:   this.username,
                'facebook_data.location':   this.city+", "+ this.state,
                'facebook_data.zipcode':    this.zipcode
            };

            var isDisabled = $("#create").hasClass('disabled');
            if ( !isDisabled ){
                UsersFactory.save(data, function(response) {
                     if ( response.errors ){
                        $scope.errors = response.errors;
                     } else {
                        if ( response.user ){
                            $scope.errors = { email: { message: "User already exists. Please login."} };
                            $location.hash('errors');
                        } else {
                             window.user = response;
                             Global.user = response;
                             Global.authenticated = true;
                             $location.path('/');
                        }
                     }
                 });
            }
        };

        $scope.forgotpassword = function(){
            var data = {
                email:      this.email
            };

            var isDisabled = $("#submit").hasClass('disabled');
                if ( !isDisabled ){
                UsersFactory.forgotpassword(data,function(result) {
                    $scope.infomessage = result.message;
                    $location.hash("message");
                });
            }
        };

        $scope.resetpassword = function(){
            var data = {
                password:           this.password
            };

            var isDisabled = $("#submit").hasClass('disabled');
            if ( !isDisabled ){
                UsersFactory.resetpassword(data,function(result) {
                    $scope.infomessage = result.message;
                    $location.hash("message");
                });
            }
        };

        $scope.findUsersByDemographic = function(){
            var data = {
                gender:             this.gender,
                age_from:           this.age_from,
                age_to:             this.age_to,
                relationship_status:      this.relationship_status
            };
            UserFPAdminFactory.searchDemographic(data,function(users) {
                $scope.users = users;
            });
        };

        /* This has to be done because we need to remove currentChannel from $localstorage */
        $scope.logout = function(){
            if ( _.isEmpty(Global.cguid) ){
                CurrentChannel.removeCurrentChannel();
            }

            $localStorage.displayProductVote = false;
            $location.path('/login');
        };

        $scope.setCityState = function(){
            $.ajax({
                url: "http://zip.elevenbasetwo.com/v2/US/"+$scope.zipcode,
                cache: false,
                dataType: "json",
                type: "GET",
                success: function(result, success) {
                    $scope.city = result.city;
                    $scope.state = result.state;
                    $scope.$apply();
                },
                error: function(result, success) {
                    $scope.city = "";
                    $scope.state = "";
                    $scope.$apply();
                }
            });
        };

        $scope.back = function(){            
            
            if ($scope.toggle_signup == true) { 
              $scope.toggle_signup = !$scope.toggle_signup; 
              $scope.toggle_tweeter = !$scope.toggle_tweeter;
            } else {
              $scope.toggle_tweeter = !$scope.toggle_tweeter;                
            }
        };

        $scope.removeAllSpaces = function(str){
            return str.replace(/ /g, '');
        }
}])
.filter('range', function() {
    return function(input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i=min; i<max; i++)
            input.push(i);
        return input;
    };
});