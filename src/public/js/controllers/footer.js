'use strict';

angular.module('mean.system').controller('FooterController', ['$scope', '$state','$rootScope','Global', 'CurrentChannel',
    function ($scope, $state, $rootScope, Global, CurrentChannel) {
        $scope.global = Global;
        $scope.cguid = Global.cguid;
        $scope.subdomain = Global.subdomain;
        $scope.currentchannel = CurrentChannel.getCurrentChannel();

        $scope.setLocationList = function(){
            $scope.urlList = [];
            console.log('IN footer');
            console.log(Global);
            if ( Global.cguid ){
                $scope.urlList['pollList'] = '/#!/poll/list';
                $scope.urlList['quizList'] = '/#!/quiz/list';
                $scope.urlList['propSuggest'] = '/#!/prop/suggest';
                $scope.urlList['channelLeaderboard'] = '/#!/leaders';

                $scope.urlList['profile'] = '/#!/profile';
                $scope.urlList['contact'] = '/#!/contact';
                $scope.urlList['privacy'] = '/#!/privacy';
                $scope.urlList['terms'] = '/#!/terms';
            }

            $rootScope.urlList = $scope.urlList;
        };

        $scope.goToPropList = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $state.transitionTo('user_channel_prop_list_subdomain');
            } else {
                $state.transitionTo('user_channel_prop_list', { cguid: $scope.currentchannel.guid });
            }
        };

        $scope.goToQuizList = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $state.transitionTo('user_channel_quiz_list_subdomain');
            } else {
                $state.transitionTo('user_channel_quiz_list', { cguid: $scope.currentchannel.guid });
            }
        };

        $scope.goToPropSuggest = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $state.transitionTo('user_suggest_prop_subdomain');
            } else {
                $state.transitionTo('user_suggest_prop', { cguid: $scope.currentchannel.guid });
            }
        };

        $scope.goToChannelLeaderboard = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $state.transitionTo('user_channel_leaderboard_subdomain');
            } else {
                $state.transitionTo('user_channel_leaderboard', { cguid: $scope.currentchannel.guid });
            }
        };

        $scope.goToChannelProfile = function(){
            if ( !_.isEmpty(Global.cguid) ){
                $state.transitionTo('user_channel_profile_subdomain');
            } else {
                $state.transitionTo('user_channel_profile', { cguid: $scope.currentchannel.guid });
            }
        };
}]);