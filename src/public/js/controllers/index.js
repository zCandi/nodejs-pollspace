'use strict';

angular.module('mean.system').controller('IndexController', ['$scope', 'Global', 'flash', '$location', '$window','$modal', 'CurrentChannel', 'ChannelFactory',
    function ($scope, Global, flash, $location, $window, $modal, CurrentChannel, ChannelFactory) {
        $scope.global = Global;
        $scope.channel = CurrentChannel.getCurrentChannel();

        $scope.redirect = function(){
            if ( $scope.global.user == null){
                $location.path('/login');
            }

            if ( $scope.global.user != null && $scope.global.user.is_admin ){
                if ( _.isEmpty(Global.cguid) ){
                    //$location.path('/admin/channel/list');
                    $window.location = "/admin/my/channels";
                } else {
                    ///admin/channel/:channel_id/prop/manage/single/:type
                    ChannelFactory.getByGuid({ guid: Global.cguid },function(channel) {
                        //$location.path('/admin/channel/'+ channel._id +'/prop/manage/single/opinion');
                        $location.path('/admin/channel/'+ channel._id +'/prop/manage/single/' + (channel.type == 'single' ? 'image' : 'opinion'));
                    });
                }
            }

            if ( $scope.global.user != null && !$scope.global.user.is_admin ){
                if ( _.isEmpty(Global.cguid) ){
                    //$location.path('/my/channels');
                    $window.location = "/my/channels";
                } else {
                    ChannelFactory.getByGuid({ guid: Global.cguid },function(channel) {
                        if ( channel.default_page ){
                            $location.path(channel.default_page);
                        } else {
                            $location.path('/poll/list');
                        }
                    });
                }
            }
        }
}]);