'use strict';

angular.module('mean.propset').controller('PropSetController', ['$scope', '$rootScope', '$stateParams', '$timeout',
    '$location', '$state','PropsFactory', 'PropsetFactory', 'Charts', 'Global', 'User','CurrentChannel', 'config',
    function ($scope, $rootScope, $stateParams, $timeout, $location, $state, PropsFactory, PropsetFactory,
              Charts, Global, User, CurrentChannel, config) {
        $scope.answers = [];
        $scope.propmodel = [];
        $scope.viewResultsButton = true;
        $scope.totalQuestionsAnswerd = 0;
        $scope.urlList = $rootScope.urlList;
        $scope.globalResults = [];

        $scope.users = [];
        $scope.finish = false;
        $scope.user = Global.user;

        $scope.collapseList = [];
        $scope.basicList = [];
        $scope.advancedList = [];
        $scope.currentTab = [];
        $scope.facebookGenderStats = [];
        $scope.facebookAgeStats = [];

        $scope.channel = CurrentChannel.getCurrentChannel();
        $scope.type =  $location.$$url.indexOf('poll') > -1 || $location.$$url.indexOf('opinion') > -1 ? 'opinion' : 'quiz';
        $scope.votingprops = [];

        //turn on the animation
        $scope.myAnimation = null;

        //IF you dont do this, it will set to [] again after vote.
        if ( _.isUndefined($scope.charts) ){
            $scope.charts = [];
        }

        if ( _.isUndefined($scope.legends) ){
            $scope.legends = [];
        }

        if ( _.isUndefined($scope.votes) ){
            $scope.votes = [];
        }

        if ( _.isUndefined($scope.barChartGender) ){
            $scope.barChartGender = [];
        }

        if ( _.isUndefined($scope.barChartAge) ){
            $scope.barChartAge = [];
        }

        $scope.vote = function(propguid, answerid, index){
            var propset = $scope.propset;
            var data = {
                choice: answerid
            };

            $(".prop-answer-loadingresults-wrapper-"+propguid).show();
            $(".prop-answer-choice-wrapper-"+propguid).hide();

            PropsFactory.vote({guid: propguid}, data, function(response) {
                var prop = response.prop;

                if ( prop ){
                    $scope.votingprops.push({prop: prop, answer: answerid});

                    if ( prop.type == 'quiz' ){
                        $scope.calculatePercentage();
                    }

                    $scope.showUserVoteProgressbar(prop, answerid);

                    if (_.isUndefined($scope.votes[propset.guid]) ){
                        $scope.votes[propset.guid] = 0;
                    }
                    $scope.votes[propset.guid] = $scope.votes[propset.guid] + 1;

                    var dataList = User.findUsersDataForPieChart(prop, 'multiple');
                    $scope.charts[prop.guid].series[0].data = dataList;
                    $scope.legends[prop.guid] = dataList;

                    $scope.findUsersDataForBarChartGender(prop);
                    $scope.findUsersDataForBarChartAge(prop);

                    if ( $scope.votes[propset.guid] == Object.keys(propset.props).length ){
                        $timeout(function() {
                            $scope.finish = true;
                        }, 500);
                    }
                }

            });
        };


        $scope.findUsersDataForBarChartGender = function(prop){
            var votes = prop.votes;
            var tempChartData = User.getGenderChartData(prop);

            $scope.facebookGenderStats[prop.guid] = [];
            for (var k in tempChartData) {
                var obj = {};
                obj.id = prop.guid + "_" + k;
                obj.total = Math.round((tempChartData[k].count * 100) / Object.keys(prop.votes).length) + "%";
                obj.name = k;
                $scope.facebookGenderStats[prop.guid].push(obj);

            }
        };

        $scope.findUsersDataForBarChartAge = function(prop){
            var tempChartData = User.getAgeChartData(prop);

            $scope.facebookAgeStats[prop.guid] = [];
            for (var k in tempChartData) {
                var obj = {};
                obj.id = prop.guid + "_" + k;
                obj.total = Math.round((tempChartData[k].count * 100) / Object.keys(prop.votes).length) + "%";
                obj.name = tempChartData[k].name;
                $scope.facebookAgeStats[prop.guid].push(obj);
            }
        };

        $scope.findChoiceName = function(choices, choice){
            var result = null;
            choices.forEach(function(_choice, key){
                if (angular.equals(_choice.id, choice)){
                    result =  _choice.name;
                }
            });
            return result;
        };

        $scope.showUserVoteProgressbar = function(prop, answerid){
            $(".prop-answer-loadingresults-wrapper-"+prop.guid).hide();
            $(".prop-answer-choice-wrapper-"+prop.guid).hide();
            $(".prop-answer-chart-wrapper-"+prop.guid).show();
            $(".prop-collapse-wrapper-"+prop.guid).show();

            if ( prop.type == 'quiz'){
                $(".prop-correctanswer-wrapper-"+prop.guid).html(prop.correct_description);
            } else {
                $(".prop-correctanswer-wrapper-"+prop.guid).html("");
            }

            var initialColor = prop.channel.color && prop.channel.color.color2 ? prop.channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);

            prop.answers.forEach(function(answer, key){
                if ( !_.isEmpty( answer.name ) ){

                    $(".progress-"+answer.id).show();
                    $(".answer-wrapper-"+answer.id).show();

                    var totalanswer = $scope.getTotalPropAnswer(prop, answer.id);
                    var totalvotes = prop.votes.length;
                    var percent = Math.round((totalanswer / totalvotes) * 100);
                    $(".progress-bar-"+answer.id).css("width", percent + "%");
                    $(".progress-bar-"+answer.id).css("background-color", palette['complementary_3_'+key]);

                    $(".answer-name-wrapper-"+answer.id).html("<span>"+answer.name+"</span>");
                    $(".answer-percentage-wrapper-"+answer.id).html(percent + "%");

                    if ( prop.type == 'quiz' && answer.id == answerid ){
                        if ( answer.correct ){
                            $(".ok-"+answer.id).show();
                            $(".collapse-ok-icon-"+prop.guid).show();
                        } else {
                            $(".remove-"+answer.id).show();
                            $(".collapse-remove-icon-"+prop.guid).show();
                        }
                    }

                    if ( prop.type == 'opinion' && answer.id == answerid ){
                        $(".ok-"+answer.id).show();
                    }

                    if ( prop.type == 'quiz' && answer.correct ){
                        $(".ok-"+answer.id).show();
                        if (answer.id != answerid){
                            $(".ok-"+answer.id).addClass('icon-ok-transparent');
                        }
                    }
                }
            });
        };


        $scope.goToThanks = function(guid){
            var guid = guid;
            if (_.isEmpty(guid) ){
                guid = $stateParams.qguid;
            }
            $rootScope.quiz_title = null;
            if ( $scope.propset.type == 'opinion' ){
                $state.transitionTo('user_view_poll_thanks', {'qguid': guid});
            } else {
                $state.transitionTo('user_view_quiz_thanks', {'qguid': guid});
            }

        }

        $scope.userVote = function(prop){
            var flag = false;
            var user = Global.user;
            if ( user ){
                prop.votes.forEach(function(vote, key){
                    var user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                    if ( user && vote.user && _.isEqual(user_id.toString(), user._id.toString()) ){
                        flag = true;
                    }
                });
            }

            return flag;
        };

        $scope.getTotalPropAnswer = function(prop, answerid){
            var total = 0;
            prop.votes.forEach(function(vote, key){
                if ( vote.choice == answerid ){
                    total = total + 1;
                }
            });

            return total;
        };


        $scope.calculatePercentage = function(){
            var user = Global.user;
            var percent = 0;
            var totalCorrect = 0;
            var totalIncorrect = 0;
            $scope.votingprops.forEach(function(data, key){
                var prop = data.prop;
                var answerid = data.answer;
                var correct = $scope.getCorrectPropAnswer(prop);
                if ( correct.toString() == answerid.toString()){
                    totalCorrect = totalCorrect + 1;
                } else {
                    totalIncorrect = totalIncorrect + 1;
                }
            });

            percent = Math.round((totalCorrect / $scope.votingprops.length) * 100);

            $rootScope.quiz_score = percent + "%";
        };

        $scope.findPropsets = function(){
            $scope.propset = null; //We need to remove the prop since this method is called from view and thank u page

            var cguid = CurrentChannel.getCurrentChannel().guid;

            PropsetFactory.list({cguid: cguid, type: $scope.type }, function(propsets) {
                var bannerPrefix = 'img/prop_status';
                $scope.propsets = propsets;
                $scope.propsets.forEach(function(propset, propsetkey){
                    propset.vote = $scope.userVotePropset(propset);

                    var vote = $scope.userVotePropsetStatus(propset);
                    if ( vote == 'no-vote' ){
                        propset.bannerinfo = { banner: '', mobile_banner: '' };
                    } else {
                        if ( vote == 'incomplete' ){
                            propset.bannerinfo = {
                                banner: bannerPrefix + "/incomplete.png",
                                mobile_banner: bannerPrefix + "/incomplete-mobile.png"
                            };
                        } else {
                            if ( propset.type == 'opinion' ){
                                propset.bannerinfo = {
                                    banner: bannerPrefix + "/poll-voted.png",
                                    mobile_banner: bannerPrefix + "/poll-voted-mobile.png"
                                };
                            } else {
                                propset.bannerinfo = {
                                    banner: bannerPrefix + "/quiz-complete.png",
                                    mobile_banner: bannerPrefix + "/quiz-complete-mobile.png"
                                };
                            }

                        }
                    }
                });
            });
        };

        $scope.isSet = function(date){
            var ts = new Date(date).getTime();
            return ts !== 0;
        };

        $scope.propStatus = function(prop){
            //created > published > closed > graded
            if ( $scope.isSet(prop.timestamps.graded)) return 'graded';
            if ( $scope.isSet(prop.timestamps.closed)) return 'closed';
            if ( $scope.isSet(prop.timestamps.published)) return 'published';
            if ( $scope.isSet(prop.timestamps.created)) return 'created';
        };

        $scope.getBanner = function(prop){
            var bannerPrefix = 'img/prop_status';
            var propStatus = $scope.propStatus(prop);

            if ( prop.suggested_by ) {
                if(_.isEqual(prop.suggested_by.toString(), $scope.global.user._id.toString())) {
                    return  { banner: bannerPrefix +  "/prop-selected.png", mobile_banner: bannerPrefix +  "/prop-selected.png" };
                }
            }

            if ( propStatus ){
                if (_.isEqual(propStatus.toString(), 'published') || _.isEqual(propStatus.toString(), 'created') ){
                    if ( $scope.userVote(prop) ){
                        if ( prop.type == 'opinion' ){
                            return { banner: bannerPrefix + "/poll-voted.png", mobile_banner: bannerPrefix + "/poll-voted-mobile.png"};
                        } else {
                            return { banner: bannerPrefix + "/quiz-complete.png", mobile_banner: bannerPrefix + "/quiz-complete-mobile.png"};
                        }
                    } else {
                        return { banner: false, mobile_banner: false};
                    }
                }


                if (_.isEqual(propStatus.toString(), 'closed') ){
                    if ( $scope.userVote(prop) ){
                        return { banner: bannerPrefix + "/voted-full-closed-voted.png", mobile_banner: bannerPrefix + "/voted-mobile-closed-voted.png"};
                    } else {
                        return { banner: bannerPrefix + "/voted-full-closed-novote.png", mobile_banner: bannerPrefix + "/voted-mobile-no-vote.png" };
                    }
                }

                if (_.isEqual(propStatus.toString(), 'graded') ){
                    if ( $scope.userVote(prop) ){
                        if ( $scope.userWon(prop) ){
                            return { banner: bannerPrefix + "/voted-full-won.png", mobile_banner: bannerPrefix + "/voted-mobile-won.png"};
                        } else if ( $scope.userLost(prop) ){
                            return { banner: bannerPrefix + "/voted-full-lost.png", mobile_banner: bannerPrefix + "/voted-mobile-lost.png"};
                        }
                    } else {
                        return { banner: false, mobile_banner: false};
                    }
                }
            } else {
                return { banner: false, mobile_banner: false};
            }
        };

        $scope.findProps = function() {
            var cguid = $stateParams.cguid;
            if (_.isEmpty(cguid) ){
                cguid = Global.cguid;
            }

            PropsFactory.list({cguid: cguid, type: $stateParams.type },function(props) {
                $scope.props = props;
                $scope.props.forEach(function(prop){
                    var bannerinfo = $scope.getBanner(prop);

                    prop.banner = bannerinfo && bannerinfo.banner ? bannerinfo.banner : "";
                    prop.mobile_banner = bannerinfo && bannerinfo.mobile_banner ? bannerinfo.mobile_banner : "";
                    prop.vote = $scope.userVote(prop);
                });
            });
        };

        $scope.findOtherPropsets = function(propset){
            var bannerPrefix = 'img/prop_status';
            var temp = [];
            var cguid = $stateParams.cguid;
            if (_.isEmpty(cguid) ){
                cguid = Global.cguid;
            }
            PropsetFactory.list({cguid: cguid, type: $scope.type }, function(propsets) {
                $scope.propsets = propsets;
                $scope.propsets.forEach(function(_propset, key){
                    if ($scope.propset._id != _propset._id) {
                        temp.push(_propset);
                    }
                });
                $scope.propsets = temp;
                $scope.propsets.forEach(function(propset, propsetkey){
                    propset.vote = $scope.userVotePropset(propset);

                    var vote = $scope.userVotePropsetStatus(propset);
                    if ( vote == 'no-vote' ){
                        propset.bannerinfo = { banner: '', mobile_banner: '' };
                    } else {
                        if ( vote == 'incomplete' ){
                            propset.bannerinfo = {
                                banner: bannerPrefix + "/incomplete.png",
                                mobile_banner: bannerPrefix + "/incomplete-mobile.png"
                            };
                        } else {
                            if ( propset.type == 'opinion' ){
                                propset.bannerinfo = {
                                    banner: bannerPrefix + "/poll-voted.png",
                                    mobile_banner: bannerPrefix + "/poll-voted-mobile.png"
                                };
                            } else {
                                propset.bannerinfo = {
                                    banner: bannerPrefix + "/quiz-complete.png",
                                    mobile_banner: bannerPrefix + "/quiz-complete-mobile.png"
                                };
                            }
                        }
                    }
                });
            });
        };

        $scope.collapse = function(propguid){
            $scope.collapseList[propguid] = true;
        };

        $scope.expand = function(propguid){
            $scope.collapseList[propguid] = false;
        };

        $scope.showBasic = function(propguid){
            $scope.basicList[propguid] = true;
            $scope.advancedList[propguid] = false;

            $scope.currentTab[propguid] = 'basic';

            $timeout(function() {
                var height = $(".basic-"+propguid).height();
                $(".tab-content-"+propguid).addClass('transition');
                $(".tab-content-"+propguid).css('height', height+'px');
            }, 100);
        };

        $scope.showAdvance = function(propguid){
            $scope.basicList[propguid] = false;
            $scope.advancedList[propguid] = true;

            $scope.currentTab[propguid] = 'advanced';

            $timeout(function() {
                var height = $(".advanced-"+propguid).height();
                $(".tab-content-"+propguid).addClass('transition');
                $(".tab-content-"+propguid).css('height', height+'px');
            }, 100);
        };

        $scope.setQuizTitle = function(){
            if ( !_.isEmpty(Global.quiz) ){
                $scope.propset = Global.quiz;
            }

            $rootScope.quiz_title = $scope.propset.title;
            $rootScope.quiz_score = "0%";
            $rootScope.quiz_type = $scope.propset.props && $scope.propset.props[0] ? $scope.propset.props[0].type : null;
        };

        $scope.getViewData = function(){
            var guid = $stateParams.qguid;

            PropsetFactory.get({
                id: guid
            }, function(propset) {
                $scope.propset = propset;
                $scope.total_props = Object.keys(propset.props).length;

                CurrentChannel.setCurrentPropset(propset);

                $scope.setQuizTitle();
                $scope.setCorrectTab(propset);
                $scope.preDefineChart(propset);

                //This timeout is to execute the function AFTER the DOM has finished rendering
                $timeout(function(){
                    if ( $scope.userVotePropset( propset ) ){
                        if ( config.flag_thankyoupageredirect === true ){
                            $scope.goToThanks(guid);
                        }
                    } else {
                        $scope.presetAnswers(propset);
                    }
                });

            });
        };

        $scope.userVotePropset = function(propset){
            var vote = false;
            var totalvotes = [];

            propset.props.forEach(function(prop, key){
                if ($scope.userVote(prop)) {
                    totalvotes.push(true);
                }
            });

            if ( totalvotes.length == propset.props.length ){
                vote = true;
            }

            return vote;
        };

        $scope.userVotePropsetStatus = function(propset){
            var vote = 'no-vote';
            var totalvotes = [];
            propset.props.forEach(function(prop, key){
                if ($scope.userVote(prop)) {
                    totalvotes.push(true);
                }
            });

            if (_.isEmpty(totalvotes) ){
                vote = 'no-vote';
            } else {
                if ( totalvotes.length == propset.props.length ){
                    vote = 'complete';
                } else {
                    if ( totalvotes.length >= 1 && totalvotes.length < propset.props.length ){
                        vote = 'incomplete';
                    }
                }
            }

            return vote;
        };

        $scope.presetAnswers = function(propset){
            var user = Global.user;
            propset.props.forEach(function(prop, key){
                var userchoice = $scope.getUserChoice(user, prop);
                if ( !_.isEmpty(userchoice) ){
                    $(".prop-answer-loadingresults-wrapper-"+prop.guid).show();
                    $(".prop-answer-choice-wrapper-"+prop.guid).hide();

                    $scope.showUserVoteProgressbar(prop, userchoice);
                }
            });
        };

        $scope.getThanksData = function(){
            var guid = $stateParams.qguid;
            var user = Global.user;
            var url = Global.hostname;

            PropsetFactory.get({
                id: guid
            }, function(propset) {
                $scope.quiz_type = propset.props && propset.props[0] ? propset.props[0].type : null;

                $scope.propset = propset;
                $scope.total_props = Object.keys(propset.props).length;
                $scope.showBasicTab = false;
                $scope.showAdvancedTab = false;

                CurrentChannel.setCurrentPropset(propset);

                $scope.score = $scope.getUserScore(user);
                $scope.getPropsetUsers();
                $scope.buildGlobalResults();
                $scope.setCorrectTab(propset);
                $scope.preDefineChart(propset);
                //Timeout is used to WAIT till the DOM is finished loading..
                $timeout(function(){
                    $scope.presetAnswers(propset);
                });

                var quiz_url = url + "/q/"+propset.guid;
                $scope.facebook_quiz_url = "https://www.facebook.com/sharer/sharer.php?u=" + quiz_url;
                $scope.twitter_quiz_url = "https://twitter.com/intent/tweet?text=" + urlencode(propset.title) + "&url=" + quiz_url;
            });
        };

        $scope.setCorrectTab = function(propset){
            //Sets the Correct Tab
            if ( !_.isEmpty(propset) ){
                if ( propset.type == 'opinion' ){
                    $rootScope.activeTab = 'our_props';
                } else if (propset.type == 'quiz'){
                    $rootScope.activeTab = 'our_quizzes';
                }
            }
        };

        $scope.preDefineChart = function(propset){
            propset.props.forEach(function(prop, key){
                if ( _.isUndefined($scope.charts[prop.guid])){

                    $scope.charts[prop.guid] = Charts.getPiechartConfiguration();
                    $scope.charts[prop.guid].size.width = 450;
                    $scope.charts[prop.guid].size.height = 350;

                    $scope.legends[prop.guid] = [];

                    $scope.findUsersDataForBarChartGender(prop);
                    $scope.findUsersDataForBarChartAge(prop);
                }

                var userchoice = $scope.getUserChoice(Global.user, prop);
                if ( !_.isEmpty(userchoice) ){
                    if (_.isUndefined($scope.votes[propset.guid]) ){
                        $scope.votes[propset.guid] = 0;
                    }
                    $scope.votes[propset.guid] = $scope.votes[propset.guid] + 1;
                }

                PropsFactory.get({
                    guid: prop.guid
                }, function(_prop) {
                    var dataList = User.findUsersDataForPieChart(prop, 'multiple');
                    $scope.charts[prop.guid].series[0].data = dataList;
                    $scope.legends[prop.guid] = dataList;

                    $scope.findUsersDataForBarChartGender(_prop);
                    $scope.findUsersDataForBarChartAge(_prop);
                });
            });
        };

        $scope.buildGlobalResults = function(){
            //INIT
            $scope.globalResults['0_39'] = {};
            $scope.globalResults['0_39'].count = 0

            $scope.globalResults['40_59'] = {};
            $scope.globalResults['40_59'].count = 0;

            $scope.globalResults['60_79'] = {};
            $scope.globalResults['60_79'].count = 0;

            $scope.globalResults['80_100'] = {};
            $scope.globalResults['80_100'].count = 0;

            $scope.totalUsers = $scope.users.length;
            $scope.users.forEach(function(user, key){
                var score = $scope.getUserScore(user);

                if ( score > 0 && score <= 39 ){
                    $scope.globalResults['0_39'].count = $scope.globalResults['0_39'].count + 1;
                } else if ( score > 39 && score <= 59 ){
                    $scope.globalResults['40_59'].count = $scope.globalResults['40_59'].count + 1;
                } else if ( score > 60 && score <= 79) {
                    $scope.globalResults['60_79'].count = $scope.globalResults['60_79'].count + 1;
                } else if ( score > 80 && score <= 100 ){
                    $scope.globalResults['80_100'].count = $scope.globalResults['80_100'].count + 1;
                }
            });

            $scope.globalResults['0_39'].text = $scope.globalResults['0_39'].count + " / " + $scope.totalUsers;
            $scope.globalResults['40_59'].text = $scope.globalResults['40_59'].count + " / " + $scope.totalUsers;
            $scope.globalResults['60_79'].text = $scope.globalResults['60_79'].count + " / " + $scope.totalUsers;
            $scope.globalResults['80_100'].text = $scope.globalResults['80_100'].count + " / " + $scope.totalUsers;

            var initialColor = $scope.propset.channel.color && $scope.propset.channel.color.color2 ? $scope.propset.channel.color.color2.replace("#", "") : "57A8D4"
            var palette = Charts.getMatchingColorList(initialColor);

            var percent = Math.round(($scope.globalResults['0_39'].count / $scope.totalUsers) * 100);
            $scope.globalResults['0_39'].percent = percent;
            $(".progress-bar-0_39").css("width", percent + "%");
            $(".progress-bar-0_39").css("background-color", palette['complementary_3_0']);

            var percent = Math.round(($scope.globalResults['40_59'].count / $scope.totalUsers) * 100);
            $scope.globalResults['40_59'].percent = percent;
            $(".progress-bar-40_59").css("width", percent + "%");
            $(".progress-bar-40_59").css("background-color", palette['complementary_3_1']);

            var percent = Math.round(($scope.globalResults['60_79'].count / $scope.totalUsers) * 100);
            $scope.globalResults['60_79'].percent = percent;
            $(".progress-bar-60_79").css("width", percent + "%");
            $(".progress-bar-60_79").css("background-color", palette['complementary_3_2']);

            var percent = Math.round(($scope.globalResults['80_100'].count / $scope.totalUsers) * 100);
            $scope.globalResults['80_100'].percent = percent;
            $(".progress-bar-80_100").css("width", percent + "%");
            $(".progress-bar-80_100").css("background-color", palette['complementary_3_3']);
        };

        $scope.getPropsetUsers = function(){
            $scope.propset.props.forEach(function(prop, key){
                prop.votes.forEach(function(vote, key2){
                    $scope.users.push(vote.user);
                });
            });
        };

        $scope.getUserScore = function(user){
            var totalCorrect = 0;
            $scope.propset.props.forEach(function(prop, key){
                var correct = $scope.getCorrectPropAnswer(prop);
                var userChoice = $scope.getUserChoice(user, prop);
                if ( correct && userChoice && _.isEqual(correct.toString(), userChoice.toString()) ){
                    totalCorrect = totalCorrect + 1;
                }
            });

            var score = Math.round((totalCorrect / Object.keys($scope.propset.props).length) * 100);

            return score;
        };

        $scope.getUserChoice = function(user, prop){
            var choice = null;
            if ( user ){
                prop.votes.forEach(function(vote, key){
                    var vote_user_id = vote.user && !_.isUndefined(vote.user._id) ? vote.user._id : vote.user;
                    var user_id = !_.isUndefined(user._id) ? user._id : user;
                    if ( user && vote.user && _.isEqual(vote_user_id.toString(), user_id.toString()) ){
                        choice = vote.choice;
                    }
                });
            };

            return choice;
        };

        $scope.getCorrectPropAnswer = function(prop){
            var correct = null;
            prop.answers.forEach(function(answer, key){
                if (answer.correct) {
                    correct = answer.id;
                }
            });

            return correct;
        };
    }]);
