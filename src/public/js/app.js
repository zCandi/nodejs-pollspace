'use strict';

angular.module('mean', ['ngCookies',
                        'ngResource',
                        'ngRoute',
                        'ngStorage',
                        'ui.router',
                        'mean.system',
                        'mean.articles',
                        'ngSanitize',
                        'mgcrea.ngStrap',
                        'mean.channels',
                        'mean.props',
                        'mean.propset',
                        'mean.users',
                        'mean.charts',
                        'mean.analytics',
                        'fileSystem',
                        'angularSpinner',
                        'ngAnimate',
                        'ui.sortable',
                        'mgcrea.ngStrap']);

angular.module('mean.system', ['colorpicker.module']);
angular.module('mean.users', ['ngAnimate', 'bootstrap-tagsinput', 'mgcrea.ngStrap.modal']);
angular.module('mean.articles', []);
angular.module('mean.channels', ['colorpicker.module', 'bootstrap-tagsinput', 'mgcrea.ngStrap.modal', 'wysiwyg.module']);
angular.module('mean.props', ['bootstrap-tagsinput', 'mgcrea.ngStrap.modal', 'highcharts-ng']);
angular.module('mean.charts', ['highcharts-ng']);
angular.module('mean.analytics', []);
angular.module('mean.propset', ['ngAnimate', 'ui.sortable']);