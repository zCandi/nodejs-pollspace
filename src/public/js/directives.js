'use strict';

angular.module('mean.system').directive('menuSlide', ['$window',
    function ($window) {
        return {
            restrict: 'AE',
            template: '',
            link: function(scope, element, attrs) {
                $(element).mmenu({
                    position: "right",
                    //zposition: "front",
                    slidingSubmenus: false,
                    searchfield : true
                });
            }
        };
    }
]);


angular.module('mean.system').directive('slideToggle', function() {
    return {
        restrict: 'A',
        scope:{
            isOpen: "=slideToggle" // 'data-slide-toggle' in our html
        },
        link: function($scope, element, attr) {
            var propguid = attr['propguid'];
            $scope.$watch('isOpen', function(newIsOpenVal, oldIsOpenVal){
                if(newIsOpenVal !== oldIsOpenVal){
                    element.stop().slideToggle();

                    var elements = eval(attr['elementtoggle']);
                    elements.forEach(function(val, k){
                        $(val).toggle();
                    });
                }
            });

        }
    };
});

angular.module('mean.system').directive('icheck', function($timeout, $parse) {
    return {
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                });

                $(element).on('ifChecked', function(event){
                    $scope.vote($attrs.propguid, $attrs.answerid);
                });

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-grey',
                    hoverClass: 'checked',
                    focusClass: 'checked'

                }).on('ifChanged', function(event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        $scope.$apply(function() {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        return $scope.$apply(function() {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
});

angular.module('mean.system').directive('relinkEvent', function($rootScope, $stateParams, CurrentChannel) {
    return {
        transclude: 'element',
        restrict: 'A',
        link: function(scope, element, attr, ctrl, transclude) {
            var previousContent = null;
            var triggerRelink = function() {
                scope.currentchannel = CurrentChannel.getCurrentChannel();
                if (previousContent) {
                    previousContent.remove();
                    previousContent = null;
                }

                transclude(function (clone) {
                    element.parent().append(clone);
                    previousContent = clone;
                });

            };

            triggerRelink();
            $rootScope.$on(attr.relinkEvent, triggerRelink);
        }
    };
});


angular.module('mean.system').directive("filepicker", function(){
    return {
        scope: {
            callback: '&',
            'pickerclass': '@'
        },
        transclude: true,
        restrict: "A",
        template: "<a href='javascript:;' class='{{pickerclass}}' ng-click='pickFiles()' ng-transclude></a>",
        link: function(scope, element, attrs) {
            scope.pickFiles = function () {
                var picker_options = {
                    container: 'modal',
                    mimetypes: attrs.mimetypes ? eval(attrs.mimetypes) : ['*'],
                    services: attrs.services ? eval(attrs.services) : ['COMPUTER'],
                    multiple: attrs.multiple ? eval(attrs.multiple) : false
                };

                var path = attrs.path ? attrs.path : '/uploads/',
                    container = attrs.container ? attrs.container : 'documents.e-freightliner.com';

                var store_options = {
                    location: 'S3',
                    path: path,
                    container: container
                };


                filepicker.setKey('AjRXd0efzTbeEAJxgKx66z');
                filepicker.pickAndStore(picker_options, store_options, function (fpfiles) {
                    scope.$apply(function(){
                        scope.callback({file:fpfiles});
                    });
                });
            };
        }
    };
});


angular.module('mean.system').directive('ngUpdateHidden',function() {
    return function(scope, el, attr) {
        var model = attr['ngModel'];
        scope.$watch(model, function(nv) {
            el.val(nv);
        });
    };
});


angular.module('mean.system').directive('ngFormValidate',function() {
    return function(scope, el, attr) {
        //Not working, perhaps we need to do it this way: http://scotch.io/tutorials/javascript/angularjs-form-validation
        $(el).validator();
    };
});

angular.module('mean.system').directive('ngMetaTag',['$rootScope', '$location', function($rootScope, $location) {
    return function($scope, el, attr) {
        var model = attr['ngModel'];
        var url = $location.absUrl();

        $rootScope.$watch(model, function(nv) {
            $(el).attr("content", nv);
        });
    };
}]);


angular.module('mean.system').directive("ngUploadcare", function(){
    return {
        scope: {
            callback: '&'
        },
        transclude: true,
        restrict: "A",
        link: function($scope, element, attrs) {
            $scope.widget = uploadcare.Widget(element);

            if ( attrs['value'] ){
                var file = uploadcare.fileFrom('url', attrs['value']);
                $scope.widget.value(file);
            }

            $scope.widget.onUploadComplete(function(info) {
                var model = attrs['model'];
                $scope.$apply(function(){
                    $scope.callback({file:info});
                });
            });
        }
    };
});


angular.module('mean.system').directive('autoComplete', function($timeout) {
    return function(scope, element, attrs) {
        $( element ).autocomplete({
            source: scope[attrs.uiItems],
            select: function() {
                $timeout(function() {
                    $( element ).trigger('input');
                }, 0);
            }
        });
    };
});

angular.module('mean.system').directive('datePicker', ['$window',
    function ($window) {
        return {
            restrict: 'AE',
            template: '',
            link: function(scope, element, attrs, ctrl) {
                var linkedElementId = attrs.linkedId;
                var restrictionDate = attrs.restrictionDate;

                $(element).datetimepicker({
                    format: 'MM/DD/YYYY'
                });
                if ( linkedElementId ){
                    $(element).on("dp.change", function (e) {
                        var d = moment(e.date).utc().format('MM/DD/YYYY');
                        $(element).find('input').val(d);

                        if ( restrictionDate == 'minDate' ){
                            $('#'+linkedElementId).data("DateTimePicker").minDate(e.date);
                        } else if (restrictionDate == 'maxDate'){
                            $('#'+linkedElementId).data("DateTimePicker").maxDate(e.date);
                        }
                    });
                }
            }
        };
    }
]);


/**
 * There are a lot of great answers here, but I would like to offer my perspective on the differences between
 * '@', '=', and '&' binding that proved useful for me.

 All three bindings are ways of passing data from your parent scope to your directive's
 isolated scope through the element's attributes:

 @ binding is for passing strings. These strings support {{}} expressions for interpolated values.
 For example: . The interpolated expression is evaluated against directive's parent scope.

 = binding is for two-way model binding. The model in parent scope is linked to the model in the directive's
 isolated scope. Changes to one model affects the other, and vice versa.

 & binding is for passing a method into your directive's scope so that it can be called within your directive.
 The method is pre-bound to the directive's parent scope, and supports arguments.
 For example if the method is hello(name) in parent scope, then in order to execute the method from inside
 your directive, you must call $scope.hello({name:'world'})

 I find that it's easier to remember these differences by referring to the scope bindings by a shorter description:
 @ Attribute string binding
 = Two-way model binding
 & Callback method binding
 The symbols also make it clearer as to what the scope variable represents inside of your directive's implementation:

 . @ string
 . = model
 . & method

 In order of usefulness (for me anyways):
 1. =
 2. @
 3. &
 */

angular.module('mean.system').directive("swipe", function(){
    return {
        scope: {
            callback: '&'
        },
        restrict: "A",
        link: function($scope, element, attrs) {
            $(element).swipe( {
                //Generic swipe handler for all directions
                swipe:function(event, direction, distance, duration, fingerCount, fingerData) {

                }
            });
        }
    };
});

angular.module('mean.system').directive('nanoScroller', ['$window',
    function ($window) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $(element).nanoScroller();
            }
        };
    }
]);

angular.module('mean.system').directive('loginPageLogo', ['$window', 
    function ($window){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.setImagePosition = function() {
                    var imgHeight = $(element).children('img').height();
                    var boxHeight = $(element).height();
                    
                    $(element).children('img').css({'top': - (imgHeight - boxHeight)/2 + 'px'});
                    // return { 'top': - (imgHeight - boxHeight)/2 + 'px'};                    
                }

                angular.element($window).on('resize', function(){
                    scope.setImagePosition();
                    scope.$apply();
                });
            }
        };
    }
]);